import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { ConfigService } from 'nestjs-config';
import { GPSService } from '../../entities/gps/gps.service';
import { ECUService } from '../../entities/ecu/ecu.service';
import { CreateGPSDTO } from 'src/entities/gps/gps.dto';
const Kafka = require('node-rdkafka');
const moment = require('moment');

@Injectable()
export class SocketGateway implements OnModuleInit, OnModuleDestroy {

    private groupId;
    private consumerConfig = null;
    private producerConfig = null;
    private consumer;
    private producer;
    private key = null;

    constructor(
        private readonly config: ConfigService,
        private readonly gpsService: GPSService,
        private readonly ecuService: ECUService
    ) {
        this.groupId = 'us';//Math.random().toString(36).substring(7);
        this.consumerConfig = {
            'client.id': config.get(config._mode() + '.clientId'),
            // 'debug': 'broker,consumer,msg',
            'metadata.broker.list': config.get(config._mode() + '.kafkaBroker'),
            'group.id': this.groupId,
            'enable.auto.commit': true,
            // 'rebalance_cb': true,
            // 'socket.keepalive.enable': true,
            'security.protocol': 'SASL_PLAINTEXT',
            'sasl.mechanisms': 'PLAIN',
            'sasl.username': config.get(config._mode() + '.kafkaUser'),
            'sasl.password': config.get(config._mode() + '.kafkaPassword')
        };
        this.producerConfig = {
            'client.id': config.get(config._mode() + '.clientId'),
            // 'debug': 'all',
            'metadata.broker.list': config.get(config._mode() + '.kafkaBroker'),
            // 'group.id': this.groupId,
            'enable.auto.commit': true,
            'security.protocol': 'SASL_PLAINTEXT',
            'sasl.mechanisms': 'PLAIN',
            'sasl.username': config.get(config._mode() + '.kafkaUser'),
            'sasl.password': config.get(config._mode() + '.kafkaPassword'),
            'dr_cb': true  //delivery report callback
        };
        console.log(this.consumerConfig);
    }

    authToken(tokenKey: any) {
        return new Promise((resolve, reject) => {
            const token = tokenKey.toString();
            jwt.verify(token, this.config.get('global.jwtKey'), (err, decoded) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(decoded);
                }
            });
        });
    }

    setKey(key) {
        this.key = key;
    }

    onModuleInit() {
        console.log('consumer setup');
        this.consumerGpsSetup();
        // this.emit('gps', 'apa');
    }

    onModuleDestroy() {
        if (this.consumer) {
            this.consumer.disconnect();
        }
    }

    consumerGpsSetup() {
        const that = this;
        this.consumer = new Kafka.KafkaConsumer(this.consumerConfig);
        this.consumer.on('event.log', function (log) {
            console.log('on log', log);
        });
        this.consumer.on('event.error', function (error) {
            console.log('on error', error);
        });
        this.consumer.on('ready', function (arg) {
            console.log('on ready', arg);
            that.consumer.subscribe([
                that.config.get('global.topicGPS'),
                that.config.get('global.topicECU'),
                that.config.get('global.topicDeviceStatus')
            ]);
            that.consumer.consume();
        });
        this.consumer.on('data', function (data) {
            if (data.topic === that.config.get('global.topicGPS')) {
                that.authToken(data.key).then(info => {
                    that.acceptGPS(data, info);
                }).catch(err => {
                    that.acceptGPS(data, {});
                    console.error('[invalid token]', err);
                })
            }
            if (data.topic === that.config.get('global.topicECU')) {
                that.authToken(data.key).then(info => {
                    that.acceptECU(data, info);
                }).catch(err => {
                    that.acceptECU(data, {});
                    console.error('[invalid token]', err);
                })
            }
            if (data.topic === that.config.get('global.topicDeviceStatus')) {
                that.authToken(data.key).then(info => {
                    that.acceptDeviceStatus(data, info);
                }).catch(err => {
                    that.acceptDeviceStatus(data, {});
                    console.error('[invalid token]', err);
                })
            }
        });
        this.consumer.on('disconnected', function (arg) {
            console.log('on disconnect', arg);
        });
        this.consumer.connect();
    }

    emit(topic, message) {
        const that = this;
        this.producer = new Kafka.Producer(this.producerConfig);
        this.producer.on('event.log', function (log) {
            console.log('on log', log);
        });
        this.producer.on('event.error', function (err) {
            console.error('on error', err);
        });
        this.producer.on('delivery-report', function (err, report) {
            console.log('on delivery', JSON.stringify(report));
            that.producer.disconnect();
        });
        this.producer.on('ready', function (arg) {
            console.log('on ready', JSON.stringify(arg));
            const partition = -1;
            const value = Buffer.from(message);
            that.producer.produce(topic, partition, value, this.key, Date.now(), "", {});
        });
        this.producer.on('disconnected', function (arg) {
            console.log('on disconnect', JSON.stringify(arg));
        });
        this.producer.connect();
    }

    acceptGPS(data, info) {
        const gps = JSON.parse(data.value.toString());
        try {
            this.gpsService.create(gps);
        } catch (error) {
            console.error(error);
        }
    }

    acceptECU(data, info) {
        console.log('on data ECU', data.value.toString()/* , info */);
        const ecu = JSON.parse(data.value.toString());
        try {
            this.ecuService.create(ecu);
        } catch (error) {
            console.error(error);
        }
    }

    acceptDeviceStatus(data, info) {
        console.log('on device status', data.value.toString()/* , info */);
        const ecu = JSON.parse(data.value.toString());
        try {
            // save device status
            // this.ecuService.create(ecu);
        } catch (error) {
            console.error(error);
        }
    }

}
