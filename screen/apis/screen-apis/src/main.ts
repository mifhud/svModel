import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as express from 'express';
import { join } from 'path';
import { AppModule } from './app.module';
import { ErrorFilter } from './handlers/filters/error.filter';
import bodyParser = require('body-parser');

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.useGlobalFilters(new ErrorFilter());
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, }));
    app.enableCors();

    const port = app.get('ConfigService').get('global.port');
    const prefix = app.get('ConfigService').get('global.apiPrefix');
    const version = app.get('ConfigService').get('global.apiVersion');
    const schema = app.get('ConfigService').get('global.schema');
    const swagger = app.get('ConfigService').get('global.swagger');
    const swaggerBase = app.get('ConfigService').get('global.swaggerBase');

    app.setGlobalPrefix(prefix);
    app.use(prefix, express.static(join(__dirname, '..', 'upload')));

    if (swagger) {
        const options = new DocumentBuilder()
            .setTitle('FMS Unit Service')
            .setDescription('Realtime FMS unit service')
            .setVersion(version)
            .setSchemes(schema)
            .setBasePath(prefix)
            .addBearerAuth()
            .build();
        const document = SwaggerModule.createDocument(app, options);
        SwaggerModule.setup(swaggerBase, app, document);
    }

    await app.listen(port);
}
bootstrap();
