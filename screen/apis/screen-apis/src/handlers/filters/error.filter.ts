import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from '@nestjs/common';

@Catch()
export class ErrorFilter implements ExceptionFilter {
  async catch(
    exception: SyntaxError | Error | HttpException,
    host: ArgumentsHost
  ) {
    let http = host.switchToHttp();
    let response = http.getResponse();
    let request = http.getRequest();
    let status = (exception instanceof HttpException) ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

    switch (status) {
      case HttpStatus.INTERNAL_SERVER_ERROR:
        response = await response.status(HttpStatus.OK).send({ response: 'error', message: exception.message });
        break;
      case HttpStatus.UNAUTHORIZED:
        // exception.message['message'] = 'UNAUTHORIZED';
        // delete exception.message['error'];
        // response = await response.status(HttpStatus.OK).send(exception.message);
        response = await response.status(HttpStatus.OK).send({ response: 'error', message: 'UNAUTHORIZED' });
        break;
    }

    return response;
  }
}