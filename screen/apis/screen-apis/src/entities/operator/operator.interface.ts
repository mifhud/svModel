import { Document } from 'mongoose';

export interface Operator extends Document {
    readonly _id: String;
    readonly nik: String;
    readonly name: String;
}