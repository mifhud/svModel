import { Document , Schema} from 'mongoose';
import { detailECUDTO } from './ecu.dto';

export interface ECU extends Document {
    date: Date,
    readonly unit_id: String;
    readonly shift: String;
    readonly plan_id: String;
    readonly ecu: detailECUDTO;
}