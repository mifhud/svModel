import { Controller, Get, Res, HttpStatus, Post, Body, Put, Query, NotFoundException, Delete, Param } from '@nestjs/common';
import { CreateLocationDTO } from './location.dto';
import { LocationService } from './location.service';
import { ApiUseTags, ApiImplicitParam, ApiImplicitBody } from '@nestjs/swagger';

@Controller('locations')
@ApiUseTags('locations')
export class LocationController {
    constructor(private locationService: LocationService) { }

    @Get()
    async getAllLocation(@Res() res) {
        const locations = await this.locationService.findAll();
        return res.status(HttpStatus.OK).json(locations);
    }

    @Get(':locationID')
    @ApiImplicitParam({ name: 'locationID' })
    async getLocation(@Res() res, @Param('locationID') locationID) {
        const location = await this.locationService.find(locationID);
        if (!location) throw new NotFoundException('Location does not exist!');
        return res.status(HttpStatus.OK).json(location);
    }

    @Post()
    async addLocation(@Res() res, @Body() createLocationDTO: CreateLocationDTO) {
        const location = await this.locationService.create(createLocationDTO);
        return res.status(HttpStatus.OK).json({
            message: "Location has been created successfully",
            location
        })
    }

    @Put()
    @ApiImplicitParam({ name: 'locationID' })
    async updateLocation(@Res() res, @Param('locationID') locationID, @Body() createLocationDTO: CreateLocationDTO) {
        const location = await this.locationService.update(locationID, createLocationDTO);
        if (!location) throw new NotFoundException('Location does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Location has been successfully updated',
            location
        })
    }

    @Delete()
    @ApiImplicitParam({ name: 'locationID' })
    async deleteLocation(@Res() res, @Param('locationID') locationID) {
        const location = await this.locationService.delete(locationID);
        if (!location) throw new NotFoundException('Location does not exist');
        return res.status(HttpStatus.OK).json({
            message: 'Location has been deleted',
            location
        })
    }    

}