import { ApiModelProperty } from '@nestjs/swagger';

export class CreateLocationDTO {
    @ApiModelProperty()
    readonly _id: String;

    @ApiModelProperty()
    readonly area: String;

    @ApiModelProperty()    
    readonly fleet: String;
}