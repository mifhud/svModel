import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Location } from './location.interface';
import { CreateLocationDTO } from './location.dto';

@Injectable()
export class LocationService {
    constructor(@InjectModel('Location') private readonly locationModel: Model<Location>) { }

    async findAll(): Promise<Location[]> {
        return await this.locationModel.find().exec();
    }

    async find(locationID): Promise<CreateLocationDTO> {
        const location = await this.locationModel.findById(locationID).exec();
        return location;
    }

    async create(createLocationDTO: CreateLocationDTO): Promise<Location> {
        const created = new this.locationModel(createLocationDTO);
        return await created.save();
    }

    async createIfNotExists(createLocationDTO: CreateLocationDTO): Promise<Location> {
        const find = await this.locationModel.findById(createLocationDTO._id);
        if (find == null) {
            const created = new this.locationModel(createLocationDTO);
            return await created.save();
        } else {
            return Promise.resolve(find);
        }
    }

    async update(locationID, createLocationDTO: CreateLocationDTO): Promise<Location> {
        const updated = await this.locationModel.findOneAndUpdate(locationID, createLocationDTO, { new: true });
        return updated;
    }

    async delete(locationID): Promise<Location> {
        const deleted = await this.locationModel.findOneAndRemove(locationID);
        return deleted;
    }

}
