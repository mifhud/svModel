import { CreateGPSDTO } from "./gps.dto";

export class GPSOutput {
  output: CreateGPSDTO[];
  message: string;
  response: string;
}

export class GPSOneOutput {
    output: CreateGPSDTO;
    message: string;
    response: string;
}