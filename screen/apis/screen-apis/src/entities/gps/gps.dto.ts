import { ApiModelProperty } from '@nestjs/swagger';

export class CreateGPSDTO {

    @ApiModelProperty()
    public date: Number;

    @ApiModelProperty()
    public actual_id: String;

    @ApiModelProperty()
    public actual_support_id: String;

    @ApiModelProperty()
    public operation_loader_id: String;

    @ApiModelProperty()
    public unit_id: String;

    @ApiModelProperty()
    public unit_type: String;

    @ApiModelProperty()
    public user_id: String;

    @ApiModelProperty()
    public latitude: Number;

    @ApiModelProperty()
    public longitude: Number;

    @ApiModelProperty()
    public altitude: Number;

    @ApiModelProperty()
    public user_login: Boolean;

    @ApiModelProperty()
    public user_status: String;

    @ApiModelProperty()
    public user_tum: String;

    @ApiModelProperty()
    public engine: String;

    @ApiModelProperty()
    public status: String;

    @ApiModelProperty()
    public ecu: Object;
}