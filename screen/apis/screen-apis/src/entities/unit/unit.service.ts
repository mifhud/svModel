import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Unit } from './unit.interface';
import { CreateUnitDTO } from './unit.dto';

@Injectable()
export class UnitService {
    constructor(@InjectModel('Unit') private readonly UnitModel: Model<Unit>) { }

    async findAll(): Promise<Unit[]> {
        return await this.UnitModel.find().exec();
    }

    async find(UnitID): Promise<CreateUnitDTO> {
        const Unit = await this.UnitModel.findById(UnitID).exec();
        return Unit;
    }

    async create(createUnitDTO: CreateUnitDTO): Promise<Unit> {
        const created = new this.UnitModel(createUnitDTO);
        return await created.save();
    }

    async update(UnitID, createUnitDTO: CreateUnitDTO): Promise<Unit> {
        const updated = await this.UnitModel.findOneAndUpdate(UnitID, createUnitDTO, { new: true });
        return updated;
    }

    async delete(UnitID): Promise<Unit> {
        const deleted = await this.UnitModel.findOneAndRemove(UnitID);
        return deleted;
    }

    async getOperationDate(unit: string) {
        console.log('before');
        const oprList = await this.UnitModel.aggregate({
            _id: '$_id',
            date: {
                '$first': '$date'
            }
        });
        console.log(oprList);
        return oprList;
    }

}
