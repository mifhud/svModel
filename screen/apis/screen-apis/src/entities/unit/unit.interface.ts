import { Document } from 'mongoose';

export interface Unit extends Document {
    readonly name: String;
    readonly age: Number;
}