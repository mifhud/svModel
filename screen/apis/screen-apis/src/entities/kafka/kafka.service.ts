import { Injectable } from '@nestjs/common';
import { ConfigService } from 'nestjs-config';
import * as async from 'async';
const tcp = require('tcp-port-used');

@Injectable()
export class KafkaService {

    constructor(private readonly config: ConfigService) { }

    check(cbb) {
        let host = '';
        let port = [];
        if (this.config._mode() === 'development') {
            host = this.config.get('development.kafkaHost');
            port = this.config.get('development.kafkaPort');
        } else {
            host = this.config.get('production.kafkaHost');
            port = this.config.get('production.kafkaPort');
        }
        let status = {
            response: 'error',
            message: 'not active'
        }
        async.each(port, function (pp, cb) {
            tcp.waitForStatus(pp, host, true, 1, 1000).then(function () {
                status = {
                    response: 'success',
                    message: 'active'
                };
                cb();
            }, function (err) {
                cb();
            });
        }, function (err) {
            cbb(status);
        });
    }

}
