import { ApiModelProperty } from '@nestjs/swagger';

export class CreateCatDTO {
    @ApiModelProperty()
    readonly name: String;

    @ApiModelProperty()
    readonly age: Number;
}