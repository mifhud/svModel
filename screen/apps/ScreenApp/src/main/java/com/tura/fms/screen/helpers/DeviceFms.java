package com.tura.fms.screen.helpers;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class DeviceFms {

    private String deviceID;
    private int gpsPrefetch;
    private int ecuPrefetch;
    private DeviceEventFms dataDistance;
    private DeviceEventFms dataEngineTemperature;
    private DeviceEventFms dataFuelRate;
    private DeviceEventFms dataHourMeter;
    private DeviceEventFms dataPayload;
    private DeviceEventFms dataVehicleSpeed;
    private DeviceEventFms dataGPS;
    private DeviceEventFms dataECU;
    private Timer tDistance;
    private Timer tEngineTemperature;
    private Timer tFuelRate;
    private Timer tHourMeter;
    private Timer tPayload;
    private Timer tVehicleSpeed;
    private Timer tGPS;
    private Timer tECU;
    private int nDistance = 60;
    private int nEngineTemperature = 60;
    private int nFuelRate = 60;
    private int nHourMeter = 60;
    private int nPayload = 60;
    private int nVehicleSpeed = 60;
    private int nGPS;
    private int nECU;
    private int cDistance = 0;
    private int cEngineTemperature = 0;
    private int cFuelRate = 0;
    private int cHourMeter = 0;
    private int cPayload = 0;
    private int cVehicleSpeed = 0;
    private int cGPS;
    private int cECU;
    private double valDistance = 0.0D;
    private double valEngineTemp = 0.0D;
    private double valFuelRate = 0.0D;
    private double valHourMeter = 0.0D;
    private double valPayload = 0.0D;
    private double valVehicleSpeed = 0.0D;
    private double valLat = 0.0D;
    private double valLon = 0.0D;
    private double valAlt = 0.0D;
    private Timer timerR;

    public DeviceFms(String unitID) {
        this.deviceID = unitID;
        this.tDistance = new Timer();
        this.tDistance.schedule(new TimerTask() {
            public void run() {
                DeviceFms.this.cDistance = DeviceFms.this.cDistance + 1;
                if (DeviceFms.this.cDistance >= DeviceFms.this.nDistance) {
                    DeviceFms.this.cDistance = 0;
                    if (DeviceFms.this.dataDistance != null) {
                        DeviceFms.this.dataDistance.onData(DeviceFms.this.getDistance());
                    }
                }

            }
        }, 0L, 1000L);
        this.tEngineTemperature = new Timer();
        this.tEngineTemperature.schedule(new TimerTask() {
            public void run() {
                DeviceFms.this.cEngineTemperature = DeviceFms.this.cEngineTemperature + 1;
                if (DeviceFms.this.cEngineTemperature >= DeviceFms.this.nEngineTemperature) {
                    DeviceFms.this.cEngineTemperature = 0;
                    if (DeviceFms.this.dataEngineTemperature != null) {
                        DeviceFms.this.dataEngineTemperature.onData(DeviceFms.this.getEngineTemperature());
                    }
                }

            }
        }, 0L, 1000L);
        this.tFuelRate = new Timer();
        this.tFuelRate.schedule(new TimerTask() {
            public void run() {
                DeviceFms.this.cFuelRate = DeviceFms.this.cFuelRate + 1;
                if (DeviceFms.this.cFuelRate >= DeviceFms.this.nFuelRate) {
                    DeviceFms.this.cFuelRate = 0;
                    if (DeviceFms.this.dataFuelRate != null) {
                        DeviceFms.this.dataFuelRate.onData(DeviceFms.this.getFuelRate());
                    }
                }

            }
        }, 0L, 1000L);
        this.tHourMeter = new Timer();
        this.tHourMeter.schedule(new TimerTask() {
            public void run() {
                DeviceFms.this.cHourMeter = DeviceFms.this.cHourMeter + 1;
                if (DeviceFms.this.cHourMeter >= DeviceFms.this.nHourMeter) {
                    DeviceFms.this.cHourMeter = 0;
                    if (DeviceFms.this.dataHourMeter != null) {
                        DeviceFms.this.dataHourMeter.onData(DeviceFms.this.getHourMeter());
                    }
                }

            }
        }, 0L, 1000L);
        this.tPayload = new Timer();
        this.tPayload.schedule(new TimerTask() {
            public void run() {
                DeviceFms.this.cPayload = DeviceFms.this.cPayload + 1;
                if (DeviceFms.this.cPayload >= DeviceFms.this.nPayload) {
                    DeviceFms.this.cPayload = 0;
                    if (DeviceFms.this.dataPayload != null) {
                        DeviceFms.this.dataPayload.onData(DeviceFms.this.getPayload());
                    }
                }

            }
        }, 0L, 1000L);
        this.tVehicleSpeed = new Timer();
        this.tVehicleSpeed.schedule(new TimerTask() {
            public void run() {
                DeviceFms.this.cVehicleSpeed = DeviceFms.this.cVehicleSpeed + 1;
                if (DeviceFms.this.cVehicleSpeed >= DeviceFms.this.nVehicleSpeed) {
                    DeviceFms.this.cVehicleSpeed = 0;
                    if (DeviceFms.this.dataVehicleSpeed != null) {
                        DeviceFms.this.dataVehicleSpeed.onData(DeviceFms.this.getVehicleSpeed());
                    }
                }

            }
        }, 0L, 1000L);
        this.tGPS = new Timer();
        this.tGPS.schedule(new TimerTask() {
            public void run() {
                DeviceFms.this.cGPS = DeviceFms.this.cGPS + 1;
                if (DeviceFms.this.cGPS >= DeviceFms.this.nGPS) {
                    DeviceFms.this.cGPS = 0;
                    if (DeviceFms.this.dataGPS != null) {
                        DeviceFms.this.dataGPS.onData(DeviceFms.this.getGPS());
                    }
                }

            }
        }, 0L, 1000L);
        this.tECU = new Timer();
        this.tECU.schedule(new TimerTask() {
            public void run() {
                DeviceFms.this.cECU = DeviceFms.this.cECU + 1;
                if (DeviceFms.this.cECU >= DeviceFms.this.nECU) {
                    DeviceFms.this.cECU = 0;
                    if (DeviceFms.this.dataECU != null) {
                        DeviceFms.this.dataECU.onData(DeviceFms.this.getDataAll());
                    }
                }

            }
        }, 0L, 1000L);
        this.timerR = new Timer();
        this.timerR.schedule(new TimerTask() {
            Double defaultlat=-3.7658167D;
            Double defaultlong=103.7504072;
            Double increase_lat= 1.0;
            int increase_long= 1;

            public void run() {
                DeviceFms.this.valDistance = DeviceFms.this.getRandom(90.0D, 10.0D);
                DeviceFms.this.valEngineTemp = DeviceFms.this.getRandom(50.0D, 53.0D);
                DeviceFms.this.valFuelRate = DeviceFms.this.getRandom(10.0D, 12.0D);
                DeviceFms.this.valHourMeter = DeviceFms.this.getRandom(30.0D, 35.0D);
                DeviceFms.this.valPayload = DeviceFms.this.getRandom(15.0D, 17.0D);
                DeviceFms.this.valVehicleSpeed = DeviceFms.this.getRandom(50.0D, 70.0D);
                DeviceFms.this.valLat = defaultlat;
                DeviceFms.this.valLon = defaultlong+increase_long;
                increase_lat++;
                increase_long++;
            }
        }, 0L, 2000L);
    }

    public void addDistanceListener(DeviceEventFms distance) {
        this.dataDistance = distance;
    }

    public void addEngineTemperatueListener(DeviceEventFms temperature) {
        this.dataEngineTemperature = temperature;
    }

    public void addFuelRateListener(DeviceEventFms fuelRate) {
        this.dataFuelRate = fuelRate;
    }

    public void addHourMeterListener(DeviceEventFms hourMeter) {
        this.dataHourMeter = hourMeter;
    }

    public void addPayloadListener(DeviceEventFms payload) {
        this.dataPayload = payload;
    }

    public void addVehicleSpeedListener(DeviceEventFms vehicleSpeed) {
        this.dataVehicleSpeed = vehicleSpeed;
    }

    public void addGPSListener(DeviceEventFms gps) {
        this.dataGPS = gps;
    }

    public void addECUListener(DeviceEventFms ecu) {
        this.dataECU = ecu;
    }

    public void setIntervalDistance(int interval) {
        if (interval < 1) {
            interval = 1;
        }

        this.nDistance = interval;
    }

    public void setIntervalEngineTemperature(int interval) {
        if (interval < 1) {
            interval = 1;
        }

        this.nEngineTemperature = interval;
    }

    public void setIntervalFuelRate(int interval) {
        if (interval < 1) {
            interval = 1;
        }

        this.nFuelRate = interval;
    }

    public void setIntervalHourMeter(int interval) {
        if (interval < 1) {
            interval = 1;
        }

        this.nHourMeter = interval;
    }

    public void setIntervalPayload(int interval) {
        if (interval < 1) {
            interval = 1;
        }

        this.nPayload = interval;
    }

    public void setIntervalVehicleSpeed(int interval) {
        if (interval < 1) {
            interval = 1;
        }

        this.nVehicleSpeed = interval;
    }

    public void setIntervalGPS(int interval) {
        if (interval < 1) {
            interval = 1;
        }

        this.nGPS = interval;
    }

    public void setIntervalECU(int interval) {
        if (interval < 1) {
            interval = 1;
        }

        this.nECU = interval;
    }

    public void setUnitID(String id) {
        this.deviceID = id;
    }

    public String getUnitID() {
        return this.deviceID;
    }

    public int getECUPrefetchRate() {
        return this.ecuPrefetch;
    }

    public int getGPSPrefetchRate() {
        return this.gpsPrefetch;
    }

    public String getTime() {
        return "time";
    }

    public JSONObject getFuelRate() {
        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Date date = new Date();
        String strDate = formatter.format(date);
        JSONObject json = new JSONObject();

        try {
            JSONObject subJson = new JSONObject();
            subJson.put("value", this.valFuelRate);
            subJson.put("unit", "liter/hour");
            json.put("date", strDate);
            json.put("fuel_rate", subJson);
        } catch (JSONException var6) {
        }

        return json;
    }

    public JSONObject getHourMeter() {
        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Date date = new Date();
        String strDate = formatter.format(date);
        JSONObject json = new JSONObject();

        try {
            JSONObject subJson = new JSONObject();
            subJson.put("value", this.valHourMeter);
            subJson.put("unit", "hour");
            json.put("date", strDate);
            json.put("hour_meter", subJson);
        } catch (JSONException var6) {
        }

        return json;
    }

    public JSONObject getDistance() {
        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Date date = new Date();
        String strDate = formatter.format(date);
        JSONObject json = new JSONObject();

        try {
            JSONObject subJson = new JSONObject();
            subJson.put("value", this.valDistance);
            subJson.put("unit", "km");
            json.put("date", strDate);
            json.put("distance", subJson);
        } catch (JSONException var6) {
        }

        return json;
    }

    public JSONObject getEngineTemperature() {
        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Date date = new Date();
        String strDate = formatter.format(date);
        JSONObject json = new JSONObject();

        try {
            JSONObject subJson = new JSONObject();
            subJson.put("value", this.valEngineTemp);
            subJson.put("unit", "ton");
            json.put("date", strDate);
            json.put("temperature", subJson);
        } catch (JSONException var6) {
        }

        return json;
    }

    public JSONObject getPayload() {
        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Date date = new Date();
        String strDate = formatter.format(date);
        JSONObject json = new JSONObject();

        try {
            JSONObject subJson = new JSONObject();
            subJson.put("value", this.valPayload);
            subJson.put("unit", "C");
            json.put("date", strDate);
            json.put("payload", subJson);
        } catch (JSONException var6) {
        }

        return json;
    }

    public JSONObject getVehicleSpeed() {
        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Date date = new Date();
        String strDate = formatter.format(date);
        JSONObject json = new JSONObject();

        try {
            JSONObject subJson = new JSONObject();
            subJson.put("value", this.valVehicleSpeed);
            subJson.put("unit", "km/hour");
            json.put("date", strDate);
            json.put("speed", subJson);
        } catch (JSONException var6) {
        }

        return json;
    }

    public JSONObject getGPS() {
        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Date date = new Date();
        String strDate = formatter.format(date);
        JSONObject json = new JSONObject();

        try {
            JSONObject subJson = new JSONObject();
            subJson.put("lon", this.valLon);
            subJson.put("lat", this.valLat);
            subJson.put("z", this.valAlt);

           json.put("date", strDate);
           /* json.put("unit_id", "1");
            json.put("shift", "2");
            json.put("plan_id", "3");
            json.put("type", "3");
            json.put("loader_id", "3");
            json.put("loader_operator", "3");
            json.put("location", "3");
            json.put("destination", "4");
            json.put("operation", "3");
            json.put("material", "4");
            json.put("latitude", this.valLat);
            json.put("longitude", this.valLon);*/




             json.put("gps", subJson);
        } catch (JSONException var6) {
        }

        return json;
    }

    public JSONObject getDataAll() {
        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Date date = new Date();
        String strDate = formatter.format(date);
        JSONObject json = new JSONObject();

        try {
            json.put("date", strDate);
            JSONObject subJson = new JSONObject();
            subJson.put("value", this.valHourMeter);
            subJson.put("unit", "hour");
            json.put("hour_meter", subJson);
            subJson = new JSONObject();
            subJson.put("value", this.valFuelRate);
            subJson.put("unit", "liter/hour");
            json.put("fuel_rate", subJson);
            subJson = new JSONObject();
            subJson.put("value", this.valDistance);
            subJson.put("unit", "km");
            json.put("distance", subJson);
            subJson = new JSONObject();
            subJson.put("value", this.valEngineTemp);
            subJson.put("unit", "C");
            json.put("temperature", subJson);
            subJson = new JSONObject();
            subJson.put("value", this.valPayload);
            subJson.put("unit", "ton");
            json.put("payload", subJson);
            subJson = new JSONObject();
            subJson.put("value", this.valVehicleSpeed);
            subJson.put("unit", "km/hour");
            json.put("speed", subJson);
            subJson = new JSONObject();
            subJson.put("lon", this.valLon);
            subJson.put("lat", this.valLat);
            subJson.put("z", this.valAlt);
            json.put("gps", subJson);
        } catch (JSONException var6) {
        }

        return json;
    }

    public JSONObject getDataRaw() {
        JSONObject json = new JSONObject();
        return json;
    }

    public boolean setSoundVolume(Integer volume) {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.equals("linux")) {
            try {
                Process var3 = Runtime.getRuntime().exec("amixer set 'Master' " + volume + "%");
            } catch (IOException var6) {
                var6.printStackTrace();
                return false;
            }
        } else {
            File f = new File("bin/SetVol.exe");
            if (!f.exists()) {
                return false;
            }

            try {
                Process var4 = Runtime.getRuntime().exec("cmd /c bin\\SetVol.exe " + volume);
            } catch (IOException var5) {
                var5.printStackTrace();
                return false;
            }
        }

        return true;
    }

    public boolean increaseSoundVolume(Integer volume) {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.equals("linux")) {
            try {
                Process var3 = Runtime.getRuntime().exec("amixer set 'Master' " + volume + "%+");
            } catch (IOException var6) {
                var6.printStackTrace();
                return false;
            }
        } else {
            File f = new File("bin/SetVol.exe");
            if (!f.exists()) {
                return false;
            }

            try {
                Process var4 = Runtime.getRuntime().exec("cmd /c bin\\SetVol.exe +" + volume);
            } catch (IOException var5) {
                var5.printStackTrace();
                return false;
            }
        }

        return true;
    }

    public boolean decreaseSoundVolume(Integer volume) {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.equals("linux")) {
            try {
                Process var3 = Runtime.getRuntime().exec("amixer set 'Master' " + volume + "%-");
            } catch (IOException var6) {
                var6.printStackTrace();
                return false;
            }
        } else {
            File f = new File("bin/SetVol.exe");
            if (!f.exists()) {
                return false;
            }

            try {
                Process var4 = Runtime.getRuntime().exec("cmd /c bin\\SetVol.exe -" + volume);
            } catch (IOException var5) {
                var5.printStackTrace();
                return false;
            }
        }

        return true;
    }

    private double getRandom(double min, double max) {
        Double rand = Math.random() * (max - min + 1.0D) + min;
        rand = Double.parseDouble(String.format("%.2f", rand));
        return rand;
    }
}
