package com.tura.fms.screen.models.actual_model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.tura.fms.screen.models.equipment.EquipmentActual;
import com.tura.fms.screen.models.location.LocationActual;
import com.tura.fms.screen.models.material.Material;
import com.tura.fms.screen.models.user_model.UserActual;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "id_plan", "__id_supervisor__", "__has_id_supervisor__", "__id_group_leader__",
        "__has_id_group_leader__", "__id_checker__", "__has_id_checker__", "__id_loader_operator__",
        "__has_id_loader_operator__", "__id_loader__", "__has_id_loader__", "__id_hauler_operator__",
        "__has_id_hauler_operator__", "__id_hauler__", "__has_id_hauler__", "__id_location__", "__has_id_location__",
        "__id_destination__", "__has_id_destination__", "__cd_operation__", "__has_cd_operation__", "__cd_shift__",
        "__has_cd_shift__", "__cd_material__", "__has_cd_material__", "enum_availability", "is_deleted", "date_started",
        "hm_start", "hm_stop", "is_started", "date_ended", "date", "is_weight", "json" })
public class ActualAPI {

    @JsonProperty("id")
    private String id;
    @JsonProperty("id_plan")
    private String idPlan;
    @JsonProperty("__id_supervisor__")
    private UserActual idSupervisor;
    @JsonProperty("__has_id_supervisor__")
    private Boolean hasIdSupervisor;
    @JsonProperty("__id_group_leader__")
    private UserActual idGroupLeader;
    @JsonProperty("__has_id_group_leader__")
    private Boolean hasIdGroupLeader;
    @JsonProperty("__id_checker__")
    private Object idChecker;
    @JsonProperty("__has_id_checker__")
    private Boolean hasIdChecker;
    @JsonProperty("__id_loader_operator__")
    private String idLoaderOperator;
    @JsonProperty("__has_id_loader_operator__")
    private Boolean hasIdLoaderOperator;
    @JsonProperty("__id_loader__")
    private EquipmentActual idLoader;
    @JsonProperty("__has_id_loader__")
    private Boolean hasIdLoader;
    @JsonProperty("__id_hauler_operator__")
    private UserActual idHaulerOperator;
    @JsonProperty("__has_id_hauler_operator__")
    private Boolean hasIdHaulerOperator;
    @JsonProperty("__id_hauler__")
    private EquipmentActual idHauler;
    @JsonProperty("__has_id_hauler__")
    private Boolean hasIdHauler;
    @JsonProperty("__id_location__")
    private LocationActual idLocation;
    @JsonProperty("__has_id_location__")
    private Boolean hasIdLocation;
    @JsonProperty("__id_destination__")
    private String idDestination;
    @JsonProperty("__has_id_destination__")
    private Boolean hasIdDestination;
    @JsonProperty("__cd_operation__")
    private String cdOperation;
    @JsonProperty("__has_cd_operation__")
    private Boolean hasCdOperation;
    @JsonProperty("__cd_shift__")
    private String cdShift;
    @JsonProperty("__has_cd_shift__")
    private Boolean hasCdShift;
    @JsonProperty("__cd_material__")
    private Material cdMaterial;
    @JsonProperty("__has_cd_material__")
    private Boolean hasCdMaterial;
    @JsonProperty("enum_availability")
    private Integer enumAvailability;
    @JsonProperty("is_deleted")
    private Integer isDeleted;
    @JsonProperty("date_started")
    private Integer dateStarted;
    @JsonProperty("hm_start")
    private Integer hmStart;
    @JsonProperty("hm_stop")
    private Integer hmStop;
    @JsonProperty("is_started")
    private Object isStarted;
    @JsonProperty("date_ended")
    private Integer dateEnded;
    @JsonProperty("date")
    private String date;
    @JsonProperty("is_weight")
    private Integer isWeight;
    @JsonProperty("json")
    private Object json;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("id_plan")
    public String getIdPlan() {
        return idPlan;
    }

    @JsonProperty("id_plan")
    public void setIdPlan(String idPlan) {
        this.idPlan = idPlan;
    }

    @JsonProperty("__id_supervisor__")
    public UserActual getIdSupervisor() {
        return idSupervisor;
    }

    @JsonProperty("__id_supervisor__")
    public void setIdSupervisor(UserActual idSupervisor) {
        this.idSupervisor = idSupervisor;
    }

    @JsonProperty("__has_id_supervisor__")
    public Boolean getHasIdSupervisor() {
        return hasIdSupervisor;
    }

    @JsonProperty("__has_id_supervisor__")
    public void setHasIdSupervisor(Boolean hasIdSupervisor) {
        this.hasIdSupervisor = hasIdSupervisor;
    }

    @JsonProperty("__id_group_leader__")
    public UserActual getIdGroupLeader() {
        return idGroupLeader;
    }

    @JsonProperty("__id_group_leader__")
    public void setIdGroupLeader(UserActual idGroupLeader) {
        this.idGroupLeader = idGroupLeader;
    }

    @JsonProperty("__has_id_group_leader__")
    public Boolean getHasIdGroupLeader() {
        return hasIdGroupLeader;
    }

    @JsonProperty("__has_id_group_leader__")
    public void setHasIdGroupLeader(Boolean hasIdGroupLeader) {
        this.hasIdGroupLeader = hasIdGroupLeader;
    }

    @JsonProperty("__id_checker__")
    public Object getIdChecker() {
        return idChecker;
    }

    @JsonProperty("__id_checker__")
    public void setIdChecker(Object idChecker) {
        this.idChecker = idChecker;
    }

    @JsonProperty("__has_id_checker__")
    public Boolean getHasIdChecker() {
        return hasIdChecker;
    }

    @JsonProperty("__has_id_checker__")
    public void setHasIdChecker(Boolean hasIdChecker) {
        this.hasIdChecker = hasIdChecker;
    }

    @JsonProperty("__id_loader_operator__")
    public String getIdLoaderOperator() {
        return idLoaderOperator;
    }

    @JsonProperty("__id_loader_operator__")
    public void setIdLoaderOperator(String idLoaderOperator) {
        this.idLoaderOperator = idLoaderOperator;
    }

    @JsonProperty("__has_id_loader_operator__")
    public Boolean getHasIdLoaderOperator() {
        return hasIdLoaderOperator;
    }

    @JsonProperty("__has_id_loader_operator__")
    public void setHasIdLoaderOperator(Boolean hasIdLoaderOperator) {
        this.hasIdLoaderOperator = hasIdLoaderOperator;
    }

    @JsonProperty("__id_loader__")
    public EquipmentActual getIdLoader() {
        return idLoader;
    }

    @JsonProperty("__id_loader__")
    public void setIdLoader(EquipmentActual idLoader) {
        this.idLoader = idLoader;
    }

    @JsonProperty("__has_id_loader__")
    public Boolean getHasIdLoader() {
        return hasIdLoader;
    }

    @JsonProperty("__has_id_loader__")
    public void setHasIdLoader(Boolean hasIdLoader) {
        this.hasIdLoader = hasIdLoader;
    }

    @JsonProperty("__id_hauler_operator__")
    public UserActual getIdHaulerOperator() {
        return idHaulerOperator;
    }

    @JsonProperty("__id_hauler_operator__")
    public void setIdHaulerOperator(UserActual idHaulerOperator) {
        this.idHaulerOperator = idHaulerOperator;
    }

    @JsonProperty("__has_id_hauler_operator__")
    public Boolean getHasIdHaulerOperator() {
        return hasIdHaulerOperator;
    }

    @JsonProperty("__has_id_hauler_operator__")
    public void setHasIdHaulerOperator(Boolean hasIdHaulerOperator) {
        this.hasIdHaulerOperator = hasIdHaulerOperator;
    }

    @JsonProperty("__id_hauler__")
    public EquipmentActual getIdHauler() {
        return idHauler;
    }

    @JsonProperty("__id_hauler__")
    public void setIdHauler(EquipmentActual idHauler) {
        this.idHauler = idHauler;
    }

    @JsonProperty("__has_id_hauler__")
    public Boolean getHasIdHauler() {
        return hasIdHauler;
    }

    @JsonProperty("__has_id_hauler__")
    public void setHasIdHauler(Boolean hasIdHauler) {
        this.hasIdHauler = hasIdHauler;
    }

    @JsonProperty("__id_location__")
    public LocationActual getIdLocation() {
        return idLocation;
    }

    @JsonProperty("__id_location__")
    public void setIdLocation(LocationActual idLocation) {
        this.idLocation = idLocation;
    }

    @JsonProperty("__has_id_location__")
    public Boolean getHasIdLocation() {
        return hasIdLocation;
    }

    @JsonProperty("__has_id_location__")
    public void setHasIdLocation(Boolean hasIdLocation) {
        this.hasIdLocation = hasIdLocation;
    }

    @JsonProperty("__id_destination__")
    public String getIdDestination() {
        return idDestination;
    }

    @JsonProperty("__id_destination__")
    public void setIdDestination(String idDestination) {
        this.idDestination = idDestination;
    }

    @JsonProperty("__has_id_destination__")
    public Boolean getHasIdDestination() {
        return hasIdDestination;
    }

    @JsonProperty("__has_id_destination__")
    public void setHasIdDestination(Boolean hasIdDestination) {
        this.hasIdDestination = hasIdDestination;
    }

    @JsonProperty("__cd_operation__")
    public String getCdOperation() {
        return cdOperation;
    }

    @JsonProperty("__cd_operation__")
    public void setCdOperation(String cdOperation) {
        this.cdOperation = cdOperation;
    }

    @JsonProperty("__has_cd_operation__")
    public Boolean getHasCdOperation() {
        return hasCdOperation;
    }

    @JsonProperty("__has_cd_operation__")
    public void setHasCdOperation(Boolean hasCdOperation) {
        this.hasCdOperation = hasCdOperation;
    }

    @JsonProperty("__cd_shift__")
    public String getCdShift() {
        return cdShift;
    }

    @JsonProperty("__cd_shift__")
    public void setCdShift(String cdShift) {
        this.cdShift = cdShift;
    }

    @JsonProperty("__has_cd_shift__")
    public Boolean getHasCdShift() {
        return hasCdShift;
    }

    @JsonProperty("__has_cd_shift__")
    public void setHasCdShift(Boolean hasCdShift) {
        this.hasCdShift = hasCdShift;
    }

    @JsonProperty("__cd_material__")
    public Material getCdMaterial() {
        return cdMaterial;
    }

    @JsonProperty("__cd_material__")
    public void setCdMaterial(Material cdMaterial) {
        this.cdMaterial = cdMaterial;
    }

    @JsonProperty("__has_cd_material__")
    public Boolean getHasCdMaterial() {
        return hasCdMaterial;
    }

    @JsonProperty("__has_cd_material__")
    public void setHasCdMaterial(Boolean hasCdMaterial) {
        this.hasCdMaterial = hasCdMaterial;
    }

    @JsonProperty("enum_availability")
    public Integer getEnumAvailability() {
        return enumAvailability;
    }

    @JsonProperty("enum_availability")
    public void setEnumAvailability(Integer enumAvailability) {
        this.enumAvailability = enumAvailability;
    }

    @JsonProperty("is_deleted")
    public Integer getIsDeleted() {
        return isDeleted;
    }

    @JsonProperty("is_deleted")
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    @JsonProperty("date_started")
    public Integer getDateStarted() {
        return dateStarted;
    }

    @JsonProperty("date_started")
    public void setDateStarted(Integer dateStarted) {
        this.dateStarted = dateStarted;
    }

    @JsonProperty("hm_start")
    public Integer getHmStart() {
        return hmStart;
    }

    @JsonProperty("hm_start")
    public void setHmStart(Integer hmStart) {
        this.hmStart = hmStart;
    }

    @JsonProperty("hm_stop")
    public Integer getHmStop() {
        return hmStop;
    }

    @JsonProperty("hm_stop")
    public void setHmStop(Integer hmStop) {
        this.hmStop = hmStop;
    }

    @JsonProperty("is_started")
    public Object getIsStarted() {
        return isStarted;
    }

    @JsonProperty("is_started")
    public void setIsStarted(Object isStarted) {
        this.isStarted = isStarted;
    }

    @JsonProperty("date_ended")
    public Integer getDateEnded() {
        return dateEnded;
    }

    @JsonProperty("date_ended")
    public void setDateEnded(Integer dateEnded) {
        this.dateEnded = dateEnded;
    }

    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("is_weight")
    public Integer getIsWeight() {
        return isWeight;
    }

    @JsonProperty("is_weight")
    public void setIsWeight(Integer isWeight) {
        this.isWeight = isWeight;
    }

    @JsonProperty("json")
    public Object getJson() {
        return json;
    }

    @JsonProperty("json")
    public void setJson(Object json) {
        this.json = json;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}