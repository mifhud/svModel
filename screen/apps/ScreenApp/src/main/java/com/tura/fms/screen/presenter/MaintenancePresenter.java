package com.tura.fms.screen.presenter;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXSpinner;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.controllers.MechanicDashboard;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.maintenance_model.task_model.ResponseTaskOutput;
import com.tura.fms.screen.models.maintenance_model.task_model.Taskmodel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.tura.fms.screen.models.ResponseModel;

import java.util.List;
import java.util.Map;

import static javafx.geometry.Pos.TOP_RIGHT;
import static javafx.scene.paint.Color.BLACK;
import static javafx.scene.shape.StrokeType.OUTSIDE;
import static org.javalite.app_config.AppConfig.p;

public class MaintenancePresenter {
    private NavigationUI navigationUI = new NavigationUI();

    public static final NetworkConnection networkConnection = new NetworkConnection();
    private static List<Taskmodel> taskmodelList = null;

    private static final Logger log = LoggerFactory.getLogger(MaintenancePresenter.class);

    public MaintenancePresenter() {

    }

    public void getDataRestApiAsync(Map<String, String> token, int page, int limit, GridPane gridata,
            JFXSpinner loading, StackPane stackPane, Runnable cbReload) {

        // if (networkConnection.check_connection_bersih()) {

        Call<ResponseTaskOutput> call = APIClient.getInstance().getMaintenance(token, page, limit,
                MechanicDashboard.activeButton, MainApp.equipmentLocal.getId());

        call.enqueue(new Callback<ResponseTaskOutput>() {
            @Override
            public void onResponse(Call<ResponseTaskOutput> call, Response<ResponseTaskOutput> response) {

                ResponseTaskOutput maintenance = response.body();

                if (response.isSuccessful()) {

                    System.out.println("Current Page " + maintenance.getCurrentPage());
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            loading.setVisible(false);
                            tampilandata(maintenance, gridata, stackPane, cbReload);
                        }
                    });

                } else {

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            loading.setVisible(false);

                        }
                    });

                }

            }

            // jika server error
            @Override
            public void onFailure(Call<ResponseTaskOutput> call, Throwable throwable) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        loading.setVisible(false);

                    }
                });

            }
        });

        /*
         * }else{ Platform.runLater(new Runnable() {
         *
         * @Override public void run() { loading.setVisible(false);
         *
         *
         * } });;
         *
         * }
         */
    }

    public void tampilandata(ResponseTaskOutput responseTaskOutput, GridPane gridata, StackPane stackPane,
            Runnable cbReload) {

        int Col = 0;
        int Row = 0;

        try {

            taskmodelList = responseTaskOutput.getOutput();
            String tipe_maintenance = "";

            Boolean isEmpty = (taskmodelList == null) || (taskmodelList != null && taskmodelList.size() == 0);
            if (isEmpty) {
                VBox vbox1 = new VBox();
                vbox1.setPadding(new Insets(20));
                vbox1.getStyleClass().add("bg-dark-mekanik");
                vbox1.setPrefHeight(125.0);
                vbox1.setPrefWidth(710);
                vbox1.setSpacing(10.0);
                Label label = new Label("Tidak ada data");
                label.setStyle("-fx-font-size:20px");
                vbox1.getChildren().add(label);
                gridata.add(vbox1, 0, 0);
            }

            if (taskmodelList != null) {
                // System.out.println(taskmodelList.size());

                MechanicDashboard.acceptedData = taskmodelList.size();

                for (int i = 0; i < taskmodelList.size(); i++) {

                    Taskmodel item = taskmodelList.get(i);

                    VBox vbox1 = new VBox();
                    vbox1.setPadding(new Insets(10, 10, 10, 10));
                    vbox1.getStyleClass().add("bg-dark-mekanik");
                    vbox1.setPrefHeight(125.0);
                    vbox1.setPrefWidth(710);
                    vbox1.setSpacing(10.0);

                    // System.out.println(taskmodelList.get(i).getEquipment().getName());

                    HBox HBox1 = new HBox();
                    HBox1.setPadding(new Insets(0));
                    HBox1.setPrefHeight(72.0);
                    HBox1.setPrefWidth(610);
                    HBox1.setMargin(vbox1, new Insets(50));

                    Label label1 = new Label(taskmodelList.get(i).getEquipment().getName());
                    label1.setStyle("-fx-font-size:20px");
                    label1.setPrefHeight(15);
                    label1.setPrefWidth(190);

                    if (taskmodelList.get(i).getType().equals(ConstantValues.MECHANIC_MENU_TASK)) {
                        tipe_maintenance = "Request";
                    } else if (taskmodelList.get(i).getType().equals(ConstantValues.MECHANIC_MENU_SCHEDULE)) {
                        tipe_maintenance = "Schedule";
                    } else if (taskmodelList.get(i).getType().equals(ConstantValues.MECHANIC_MENU_BACKLOG)) {
                        tipe_maintenance = "Backlog";
                    }

                    Label label2 = new Label(tipe_maintenance);
                    // label2.setPrefHeight(15);
                    label2.setStyle("-fx-font-size:20px");
                    label2.setPrefWidth(216);

                    String tgl = "";
                    if (taskmodelList.get(i).getCreated_at() != null) {
                        tgl = Utilities.timeStampToDate(taskmodelList.get(i).getCreated_at(), "tanggal_started");
                    }

                    Label label3 = new Label(tgl);
                    label3.setStyle("-fx-font-size:20px");
                    label3.setPrefHeight(15);
                    label3.setPrefWidth(300);

                    HBox1.getChildren().add(label1);
                    HBox1.getChildren().add(label2);
                    HBox1.getChildren().add(label3);

                    HBox HBox2 = new HBox();
                    HBox2.setPadding(new Insets(0));
                    // HBox2.setPrefHeight(72.0);
                    HBox2.setPrefWidth(610);
                    HBox2.setSpacing(20.0);

                    vbox1.getChildren().add(HBox1);

                    vbox1.getChildren().add(HBox2);

                    String buttonStartLabel = "Start";

                    if (taskmodelList.get(i).getType().equals(ConstantValues.MECHANIC_MENU_TASK)) {
                        Text text1 = new Text("PROBLEM : " + taskmodelList.get(i).getProblem().getName());
                        text1.setStyle("-fx-font-size:20px");
                        text1.setStrokeType(OUTSIDE);
                        text1.setStrokeWidth(0.0);
                        text1.setFill(Color.WHITE);
                        HBox2.getChildren().add(text1);

                        Object obj = taskmodelList.get(i).getMaintenance_request_id();
                        if (obj != null) {
                            Map<String, Object> mRequest = Synchronizer.mapper.convertValue(obj, Map.class);
                            if (mRequest.get("operator_id") != null) {
                                Map<String, Object> operator = Synchronizer.mapper
                                        .convertValue(mRequest.get("operator_id"), Map.class);
                                String name = operator.get("name").toString();
                                String nik = operator.get("staff_id").toString();
                                Text textOperator = new Text("OPERATOR : " + name + " (" + nik + ")");
                                textOperator.setStyle("-fx-font-size:20px");
                                textOperator.setStrokeType(OUTSIDE);
                                textOperator.setStrokeWidth(0.0);
                                textOperator.setFill(Color.WHITE);

                                // HBox2.getChildren().add(textOperator);
                                vbox1.getChildren().add(textOperator);
                            }
                        }

                        Object objL = taskmodelList.get(i).getLocation_id();
                        if (objL != null) {
                            Map<String, Object> loc = Synchronizer.mapper.convertValue(objL, Map.class);
                            String nameL = loc.get("name").toString();
                            Text textLocation = new Text("LOCATION : " + nameL);
                            textLocation.setStyle("-fx-font-size:20px");
                            textLocation.setStrokeType(OUTSIDE);
                            textLocation.setStrokeWidth(0.0);
                            textLocation.setFill(Color.WHITE);

                            // HBox2.getChildren().add(textLocation);
                            vbox1.getChildren().add(textLocation);

                        }

                        HBox HBox3 = new HBox();
                        HBox3.setPadding(new Insets(0));
                        HBox3.setPrefHeight(72.0);
                        // HBox3.setPrefWidth(610);
                        HBox3.setAlignment(Pos.CENTER_RIGHT);
                        HBox3.setSpacing(15.0);

                        Object fore = taskmodelList.get(i).getForeman_id();
                        if (fore != null) {
                            Map<String, Object> ufor = Synchronizer.mapper.convertValue(fore, Map.class);
                            String nameF = ufor.get("name").toString();
                            String nikF = ufor.get("staff_id").toString();
                            Text textFore = new Text("FOREMAN : " + nameF + " (" + nikF + ")");
                            textFore.setStyle("-fx-font-size:20px");
                            textFore.setStrokeType(OUTSIDE);
                            textFore.setStrokeWidth(0.0);
                            textFore.setFill(Color.WHITE);

                            // HBox3.getChildren().add(textFore);
                            vbox1.getChildren().add(textFore);
                        }

                        String stat = taskmodelList.get(i).getStatus();
                        Label textS = new Label("STATUS : " + stat.toUpperCase());

                        if (stat.equals(ConstantValues.MAINTENANCE_STATUS_NOT)) {
                            textS.setStyle(
                                    "-fx-font-size:20px;-fx-background-color: " + ConstantValues.COLOR_DOWN + ";");
                        }
                        if (stat.equals(ConstantValues.MAINTENANCE_STATUS_PROCESS)) {
                            textS.setStyle("-fx-font-size:20px;-fx-background-color: "
                                    + ConstantValues.COLOR_MAINTENANCE + ";");
                            buttonStartLabel = "Open";
                        }
                        if (stat.equals(ConstantValues.MAINTENANCE_STATUS_DONE)) {
                            textS.setStyle(
                                    "-fx-font-size:20px;-fx-background-color: " + ConstantValues.COLOR_RFU + ";");
                            buttonStartLabel = "Open";
                        }

                        // HBox3.getChildren().add(textS);
                        vbox1.getChildren().add(textS);

                        JFXButton button1 = new JFXButton("Move to Backlog");
                        button1.setButtonType(JFXButton.ButtonType.RAISED);
                        button1.getStyleClass().add("button-primary-sm");
                        button1.setPrefHeight(27);
                        button1.setPrefWidth(200);
                        button1.setRipplerFill(BLACK);
                        button1.setId(String.valueOf(i));
                        // aksi
                        final String idm = taskmodelList.get(i).getId();
                        button1.setOnAction((ActionEvent) -> {

                            button1.setDisable(true);
                            // pesanAlert("Apakah yakin akan di proses ?", stackPane);
                            Utilities.confirm(stackPane, "Yakin akan dipindah ke Backlog ?", new Runnable() {

                                @Override
                                public void run() {
                                    Call<ResponseModel> call = APIClient.getInstance()
                                            .getMoveToBacklog(MainApp.requestHeader, idm);
                                    call.enqueue(new Callback<ResponseModel>() {
                                        @Override
                                        public void onResponse(Call<ResponseModel> call,
                                                Response<ResponseModel> response) {
                                            button1.setDisable(false);
                                            if (response.isSuccessful()) {
                                                if (cbReload != null) {
                                                    cbReload.run();
                                                }
                                                Platform.runLater(new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        Utilities.showMessageDialog("Success", "",
                                                                "Data sudah dipindah ke backlog");
                                                    }
                                                });
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<ResponseModel> call, Throwable t) {
                                            button1.setDisable(false);
                                            t.printStackTrace();
                                        }
                                    });
                                }
                            }, null);

                        });
                        HBox3.setMargin(button1, new Insets(0, 10, 0, 0));

                        JFXButton button2 = new JFXButton(buttonStartLabel);
                        button2.setButtonType(JFXButton.ButtonType.RAISED);
                        button2.getStyleClass().add("button-raised");
                        button2.setPrefHeight(27);
                        button2.setPrefWidth(101);
                        button2.setRipplerFill(BLACK);
                        button2.setId(String.valueOf(i));
                        // aksi
                        button2.setOnAction((ActionEvent event) -> {

                            pesanAlert("Apakah yakin akan di proses ?", stackPane, item, event);

                        });

                        HBox3.setMargin(button2, new Insets(0, 10, 0, 0));

                        if (taskmodelList.get(i).getStatus().equals(ConstantValues.MAINTENANCE_STATUS_NOT)) {
                            HBox3.getChildren().add(button1);
                        }
                        HBox3.getChildren().add(button2);
                        vbox1.getChildren().add(HBox3);

                    } else if (taskmodelList.get(i).getType().equals(ConstantValues.MECHANIC_MENU_SCHEDULE)) {

                        Object obj = taskmodelList.get(i).getMaintenance_group_id();
                        if (obj != null) {
                            Map<String, Object> mgroup = Synchronizer.mapper.convertValue(obj, Map.class);
                            String name = mgroup.get("name").toString();
                            Text text1 = new Text("MAINTENANCE : " + name);
                            text1.setStyle("-fx-font-size:20px");
                            text1.setStrokeType(OUTSIDE);
                            text1.setStrokeWidth(0.0);
                            text1.setFill(Color.WHITE);
                            HBox2.getChildren().add(text1);
                        }

                        String tglSch = Utilities.timeStampToDate(taskmodelList.get(i).getScheduled_at(), "tanggal");
                        Text textSch = new Text("SCHEDULE : " + tglSch);
                        textSch.setStyle("-fx-font-size:20px");
                        textSch.setStrokeType(OUTSIDE);
                        textSch.setStrokeWidth(0.0);
                        textSch.setFill(Color.WHITE);
                        HBox2.getChildren().add(textSch);

                        HBox HBox3 = new HBox();
                        HBox3.setPadding(new Insets(15, 10, 10, 10));
                        HBox3.setPrefHeight(72.0);
                        HBox3.setPrefWidth(610);
                        HBox3.setAlignment(TOP_RIGHT);
                        HBox3.setSpacing(15.0);

                        String stat = taskmodelList.get(i).getStatus();
                        Label textS = new Label("STATUS : " + stat.toUpperCase());
                        if (stat.equals(ConstantValues.MAINTENANCE_STATUS_NOT)) {
                            textS.setStyle(
                                    "-fx-font-size:20px;-fx-background-color: " + ConstantValues.COLOR_DOWN + ";");
                        }
                        if (stat.equals(ConstantValues.MAINTENANCE_STATUS_PROCESS)) {
                            textS.setStyle("-fx-font-size:20px;-fx-background-color: "
                                    + ConstantValues.COLOR_MAINTENANCE + ";");
                            buttonStartLabel = "Open";
                        }
                        if (stat.equals(ConstantValues.MAINTENANCE_STATUS_DONE)) {
                            textS.setStyle(
                                    "-fx-font-size:20px;-fx-background-color: " + ConstantValues.COLOR_RFU + ";");
                            buttonStartLabel = "Open";
                        }

                        // HBox3.getChildren().add(textS);
                        vbox1.getChildren().add(textS);

                        JFXButton button1 = new JFXButton(buttonStartLabel);
                        button1.setButtonType(JFXButton.ButtonType.RAISED);
                        button1.getStyleClass().add("button-raised");
                        button1.setPrefHeight(27);
                        button1.setPrefWidth(101);
                        button1.setRipplerFill(BLACK);
                        button1.setId(String.valueOf(i));

                        // aksi
                        button1.setOnAction((ActionEvent event) -> {

                            pesanAlert("Apakah yakin akan di proses ?", stackPane, item, event);

                        });

                        HBox3.setMargin(button1, new Insets(0, 10, 0, 0));
                        HBox3.getChildren().add(button1);

                        vbox1.getChildren().add(HBox3);

                    } else if (taskmodelList.get(i).getType().equals(ConstantValues.MECHANIC_MENU_BACKLOG)) {
                        Text text1 = new Text("PROBLEM : " + taskmodelList.get(i).getProblem().getName());
                        text1.setStyle("-fx-font-size:20px");
                        text1.setStrokeType(OUTSIDE);
                        text1.setStrokeWidth(0.0);
                        text1.setFill(Color.WHITE);
                        HBox2.getChildren().add(text1);

                        Object obj = taskmodelList.get(i).getMaintenance_request_id();
                        if (obj != null) {
                            Map<String, Object> mRequest = Synchronizer.mapper.convertValue(obj, Map.class);
                            if (mRequest.get("operator_id") != null) {
                                Map<String, Object> operator = Synchronizer.mapper
                                        .convertValue(mRequest.get("operator_id"), Map.class);
                                String name = operator.get("name").toString();
                                String nik = operator.get("staff_id").toString();
                                Text textOperator = new Text("OPERATOR : " + name + " (" + nik + ")");
                                textOperator.setStyle("-fx-font-size:20px");
                                textOperator.setStrokeType(OUTSIDE);
                                textOperator.setStrokeWidth(0.0);
                                textOperator.setFill(Color.WHITE);

                                // HBox2.getChildren().add(textOperator);
                                vbox1.getChildren().add(textOperator);
                            }
                        }

                        Object objL = taskmodelList.get(i).getLocation_id();
                        if (objL != null) {
                            Map<String, Object> loc = Synchronizer.mapper.convertValue(objL, Map.class);
                            String nameL = loc.get("name").toString();
                            Text textLocation = new Text("LOCATION : " + nameL);
                            textLocation.setStyle("-fx-font-size:20px");
                            textLocation.setStrokeType(OUTSIDE);
                            textLocation.setStrokeWidth(0.0);
                            textLocation.setFill(Color.WHITE);

                            // HBox2.getChildren().add(textLocation);
                            vbox1.getChildren().add(textLocation);
                        }

                        HBox HBox3 = new HBox();
                        HBox3.setPadding(new Insets(15, 10, 10, 10));
                        HBox3.setPrefHeight(72.0);
                        HBox3.setPrefWidth(610);
                        HBox3.setAlignment(TOP_RIGHT);
                        HBox3.setSpacing(15.0);

                        Object fore = taskmodelList.get(i).getForeman_id();
                        if (fore != null) {
                            Map<String, Object> ufor = Synchronizer.mapper.convertValue(fore, Map.class);
                            String nameF = ufor.get("name").toString();
                            String nikF = ufor.get("staff_id").toString();
                            Text textFore = new Text("FOREMAN : " + nameF + " (" + nikF + ")");
                            textFore.setStyle("-fx-font-size:20px");
                            textFore.setStrokeType(OUTSIDE);
                            textFore.setStrokeWidth(0.0);
                            textFore.setFill(Color.WHITE);
                            // HBox3.getChildren().add(textFore);
                            vbox1.getChildren().add(textFore);
                        }

                        String stat = taskmodelList.get(i).getStatus();
                        Label textS = new Label("STATUS : " + stat.toUpperCase());
                        if (stat.equals(ConstantValues.MAINTENANCE_STATUS_NOT)) {
                            textS.setStyle(
                                    "-fx-font-size:20px;-fx-background-color: " + ConstantValues.COLOR_DOWN + ";");
                        }
                        if (stat.equals(ConstantValues.MAINTENANCE_STATUS_PROCESS)) {
                            textS.setStyle("-fx-font-size:20px;-fx-background-color: "
                                    + ConstantValues.COLOR_MAINTENANCE + ";");
                            buttonStartLabel = "Open";
                        }
                        if (stat.equals(ConstantValues.MAINTENANCE_STATUS_DONE)) {
                            textS.setStyle(
                                    "-fx-font-size:20px;-fx-background-color: " + ConstantValues.COLOR_RFU + ";");
                            buttonStartLabel = "Open";
                        }
                        // HBox3.getChildren().add(textS);
                        vbox1.getChildren().add(textS);

                        JFXButton button1 = new JFXButton(buttonStartLabel);
                        button1.setButtonType(JFXButton.ButtonType.RAISED);
                        button1.getStyleClass().add("button-raised");
                        button1.setPrefHeight(27);
                        button1.setPrefWidth(101);
                        button1.setRipplerFill(BLACK);
                        button1.setId(String.valueOf(i));

                        // aksi
                        button1.setOnAction((ActionEvent event) -> {

                            pesanAlert("Apakah yakin akan di proses ?", stackPane, item, event);

                        });

                        HBox3.setMargin(button1, new Insets(0, 10, 0, 0));
                        HBox3.getChildren().add(button1);

                        vbox1.getChildren().add(HBox3);
                    }

                    // Next Row
                    Row = (MechanicDashboard.limit * (MechanicDashboard.page - 1)) + i;

                    gridata.add(vbox1, Col, Row);

                    // Reset Column
                    Col = 0;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void pesanAlert(String pesan, StackPane stackPane, Taskmodel item, ActionEvent gEvent) {
        JFXDialogLayout content = new JFXDialogLayout();
        Text txt = new Text("KONFIRMASI");
        txt.setStyle("-fx-font-size:20px");
        content.setHeading(txt);
        Text txtContent = new Text(pesan);
        txtContent.setStyle("-fx-font-size:20px");
        content.setBody(txtContent);
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);

        JFXButton confirm = new JFXButton("TIDAK");
        confirm.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        confirm.setPrefHeight(30.0);
        confirm.setPrefWidth(100.0);
        confirm.getStyleClass().add("button-primary");

        JFXButton running = new JFXButton("YA");
        running.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        running.setPrefHeight(30.0);
        running.setPrefWidth(100.0);
        running.getStyleClass().add("button-primary");

        running.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                // navigationUI.back_dashboard(sessionFMS.getSessionFms("sesi_level"),event);

                MainApp.currentMaintenanceId = item.getId();

                if (item.getStatus().equals(ConstantValues.MAINTENANCE_STATUS_NOT)) {

                    Utilities.startMaintenance(MainApp.currentMaintenanceId, new CustomRunnableX<Taskmodel>() {

                        @Override
                        public void run() {
                            super.run();

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {

                                    MechanicDashboard.page = 1;

                                    try {
                                        navigationUI.windowsTab(p("app.linkMechanicMaintenance"),
                                                p("app.titleMechanicMaintenance"), event);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            dialog.close();
                        }
                    });

                } else {

                    try {
                        navigationUI.windowsTab(p("app.linkMechanicMaintenance"), p("app.titleMechanicMaintenance"),
                                event);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        confirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });

        content.setActions(confirm, running);

        if (item.getStatus().equals(ConstantValues.MAINTENANCE_STATUS_NOT)) {

            dialog.show();

        } else {

            MainApp.currentMaintenanceId = item.getId();

            MechanicDashboard.page = 1;

            try {
                navigationUI.windowsTab(p("app.linkMechanicMaintenance"), p("app.titleMechanicMaintenance"), gEvent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}
