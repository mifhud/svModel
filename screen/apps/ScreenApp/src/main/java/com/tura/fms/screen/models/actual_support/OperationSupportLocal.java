package com.tura.fms.screen.models.actual_support;

import com.tura.fms.screen.models.actual_model.CdMaterial;

public class OperationSupportLocal {

    private String id;
    private Integer date;
    private Double hm_start;
    private Double hm_stop;
    private Integer created_at;
    private Integer is_deleted;
    private EquipmentSupportLocal equipment_id;
    private LocationSupportLocal location_id;
    private LocationSupportLocal destination_id;
    private CdActivitySupportLocal cd_activity;
    private CdActivitySupportLocal cd_shift;
    private CdMaterial cd_material;
    private UserSupportLocal operator_id;
    private UserSupportLocal supervisor_id;
    private UserSupportLocal group_leader_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Double getHm_start() {
        return hm_start;
    }

    public void setHm_start(Double hmStart) {
        this.hm_start = hmStart;
    }

    public Double getHm_stop() {
        return hm_stop;
    }

    public void setHm_stop(Double hmStop) {
        this.hm_stop = hmStop;
    }

    public Integer getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Integer createdAt) {
        this.created_at = createdAt;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Integer isDeleted) {
        this.is_deleted = isDeleted;
    }

    public EquipmentSupportLocal getEquipment_id() {
        return equipment_id;
    }

    public void setEquipment_id(EquipmentSupportLocal equipmentId) {
        this.equipment_id = equipmentId;
    }

    public LocationSupportLocal getLocation_id() {
        return location_id;
    }

    public void setLocation_id(LocationSupportLocal location_id) {
        this.location_id = location_id;
    }

    public LocationSupportLocal getDestination_id() {
        return destination_id;
    }

    public void setDestination_id(LocationSupportLocal destination_id) {
        this.destination_id = destination_id;
    }

    public CdActivitySupportLocal getCd_activity() {
        return cd_activity;
    }

    public void setCd_activity(CdActivitySupportLocal cd_activity) {
        this.cd_activity = cd_activity;
    }

    public CdActivitySupportLocal getCd_shift() {
        return cd_shift;
    }

    public void setCd_shift(CdActivitySupportLocal cd_shift) {
        this.cd_shift = cd_shift;
    }

    public CdMaterial getCd_material() {
        return cd_material;
    }

    public void setCd_material(CdMaterial cd_material) {
        this.cd_material = cd_material;
    }

    public UserSupportLocal getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(UserSupportLocal operator_id) {
        this.operator_id = operator_id;
    }

    public UserSupportLocal getSupervisor_id() {
        return supervisor_id;
    }

    public void setSupervisor_id(UserSupportLocal supervisor_id) {
        this.supervisor_id = supervisor_id;
    }

    public UserSupportLocal getGroup_leader_id() {
        return group_leader_id;
    }

    public void setGroup_leader_id(UserSupportLocal group_leader_id) {
        this.group_leader_id = group_leader_id;
    }

}