package com.tura.fms.screen.presenter;

import static com.tura.fms.screen.helpers.Utilities.timeStampToDate;

import java.io.IOException;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Actual_DB;
import com.tura.fms.screen.database.User_Info_DB;
import com.tura.fms.screen.helpers.GPS;
import com.tura.fms.screen.helpers.SessionFMS;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.helpers.ui.AlertMaker;
import com.tura.fms.screen.models.actual_model.ActualData;
import com.tura.fms.screen.models.user_model.CdDepartement;
import com.tura.fms.screen.models.user_model.CdJsonDepartement;
import com.tura.fms.screen.models.user_model.JsonCdRole;
import com.tura.fms.screen.models.user_model.UserAPILocal;
import com.tura.fms.screen.models.user_model.UserCdRole;
import com.tura.fms.screen.models.user_model.UserData;
import com.tura.fms.screen.models.user_model.UserDetails;
import com.tura.fms.screen.models.user_model.UserOrg;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit2.Call;
import retrofit2.Response;

public class UserPresenter {

    String token = "";
    UserAPILocal userDetails = null;
    UserOrg userOrg;
    UserCdRole userCdOrg;
    CdDepartement cdDepartement;
    JsonCdRole jsonCdRole;
    CdJsonDepartement cdJsonDepartement;

    UserData userData;
    String respon, message;
    User_Info_DB user_info_db;
    ActualData actualData;
    Actual_DB actual_db;

    private static final Logger log = LoggerFactory.getLogger(UserPresenter.class);

    public static final NetworkConnection networkConnection = new NetworkConnection();

    public UserData requestLogin(String username, String password, String sesi_level, Boolean setSession) {
        UserData userData = new UserData();
        user_info_db = new User_Info_DB();

        userDetails = user_info_db.userLoginLocal(username, Utilities.getMd5(password));
        int ceklogin = user_info_db.getExistingLogin(username, Utilities.getMd5(password));

        try {

            if (NetworkConnection.getServiceStatus()) {

                GPS gps = Utilities.getCurrentGPS();

                Call<UserData> call = APIClient.getInstance().ProsesLogin(username, Utilities.getMd5(password),
                        username + "/" + MainApp.config("config.unit_id"), ConstantValues.AUTH_STATE_LOGIN,
                        MainApp.equipmentLocal.getId(), MainApp.config("config.unit_type"), gps.getLat(), gps.getLon());

                Response<UserData> response = call.execute();

                if (response.isSuccessful()) {
                    respon = response.body().getRespon();
                    message = response.body().getMessage();

                    if (response.body().getRespon().equalsIgnoreCase("success")) {
                        UserDetails userDet = response.body().getKeluaran();

                        userDetails = new UserAPILocal();
                        userDetails.setId(userDet.getId());
                        if (userDet.getId_crew() != null) {
                            userDetails.setIdCrew(userDet.getId_crew());
                        } else {
                            userDetails.setIdCrew("");
                        }
                        if (userDet.getOrg() != null) {
                            userDetails.setIdOrg(userDet.getOrg().getId());
                        } else {
                            userDetails.setIdOrg("");
                        }
                        if (userDet.getCd_role() != null) {
                            userDetails.setCdRole(userDet.getCd_role().getCd());
                        } else {
                            userDetails.setCdRole("");
                        }
                        if (userDet.getCd_department() != null) {
                            userDetails.setCdDepartment(userDet.getCd_department().getCd());
                        } else {
                            userDetails.setCdDepartment("");
                        }
                        userDetails.setName(userDet.getName());
                        userDetails.setPwd(userDet.getPwd());
                        userDetails.setStaffId(userDet.getStaff_id());
                        userDetails.setFingerprintId(userDet.getFingerprint_id());
                        userDetails.setSocketId(userDet.getSocket_id());
                        userDetails.setFilePicture(userDet.getFile_picture());
                        userDetails.setIpAddress(userDet.getIp_address());
                        userDetails.setToken(userDet.getToken());
                        userDetails.setEnumAvailability(userDet.getEnum_availability());
                        userDetails.setIsAuthenticated(userDet.getIs_authenticated());
                        userDetails.setJson(userDet.getJson());

                        // MainApp.loginCallback(true, userDetails);

                        if (setSession) {
                            setSessionUser(userDetails);
                        }

                    } else {
                        message = "Username dan Password Salah";
                        respon = "error";
                    }
                }

            } else {

                if (ceklogin > 0) {
                    // ngambil di lokal jika koneksi mati
                    message = "Koneksi bermasalah data masuk ke mode offline";
                    respon = "offline";

                    // MainApp.loginCallback(true, userDetails);

                    if (setSession) {
                        setSessionUser(userDetails);
                    }
                } else {
                    message = "Username dan Password Error";
                    respon = "error";
                }

            }

        } catch (IOException e) {

            e.printStackTrace();
            // ngambil di lokal jika reques gagal

            if (ceklogin > 0) {

                message = "Terjadi masalah " + e.getMessage() + " saat reques data ke mode offline Server Sedang Down";
                respon = "offline";

                // MainApp.loginCallback(true, userDetails);

                if (setSession) {
                    setSessionUser(userDetails);
                }
            } else {
                message = "Username dan Password Error";
                respon = "error";
            }

        }

        userData.setResult(userDetails);
        userData.setRespon(respon);
        userData.setMessage(message);

        return userData;
    }

    public int saveActual() {
        // String tanggal = Utilities.getCurrentDate(SIMPLE_DATE_FORMAT);
        String tanggal = Utilities.getCurrentDateOnly();
        // System.out.println("==========================");
        // System.out.println(tanggal);

        // String tanggal = "2019-01-01";
        actualData = new ActualData();
        actual_db = new Actual_DB();

        int sukses;

        Call<ActualData> call = APIClient.getInstance().getActual(MainApp.requestHeader, tanggal,
                MainApp.config("config.unit_id").trim(), MainApp.config("config.unit_type").trim(), "");

        try {
            // untuk koneksi online
            if (NetworkConnection.getServiceStatus()) {

                ActualData response = call.execute().body();
                MainApp.actualInfo = response.getOutput();

                // Gson gson = new Gson();

                // String json = gson.toJson(MainApp.actualInfo);

                // System.out.println("Print ID " + gson.toJson(response));

                if (MainApp.actualInfo != null) {
                    System.out.println("actual id");

                    SessionFMS sessionFMS = new SessionFMS();
                    sessionFMS.setSessionFms("sesi_from_location", MainApp.actualInfo.getId_location().getName());
                    sessionFMS.setSessionFms("sesi_destination_location",
                            MainApp.actualInfo.getId_destination().getName());
                    sessionFMS.setSessionFms("sesi_exavator", MainApp.actualInfo.getId_loader().getName());
                    sessionFMS.setSessionFms("sesi_id_plan", MainApp.actualInfo.getId_plan());
                    sessionFMS.setSessionFms("sesi_shift", MainApp.actualInfo.getCd_shift().getName());
                    sessionFMS.setSessionFms("sesi_tanggal_operation",
                            timeStampToDate(MainApp.actualInfo.getDate(), "tanggal"));

                    // save data ke local
                    actual_db.insertData(MainApp.actualInfo);

                    sukses = 1;
                    // jika dta gagal atau server down ngambil di lokal

                    if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)) {
                        Utilities.getHaulerListAsync(null, null);
                    }

                } else {
                    sukses = 0;

                    // aktual tidak ada, belum diset
                    // tampilkan pesan. dan logout

                    AlertMaker.showSimpleAlert("Actual Not Found !", response.getMessage());
                }

                // untuk koneksi offline
            } else {
                sukses = 0;

                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> alert svc down");

                AlertMaker.showSimpleAlert("Service Down !", "Tidak dapat mengakses FMS");
            }

        } catch (Exception e) {
            sukses = 0;

            System.out.println("Print ID " + e.getMessage());

            AlertMaker.showSimpleAlert("Service Down !", "Tidak dapat mengakses FMS");
        }

        return sukses;

    }

    /*
     * ====================== untuk menyimpan di database local public void
     * saveLocalStorage_(UserDetails userDetails, UserOrg userOrg, UserCdRole
     * userCdOrg, CdDepartement cdDepartement, JsonCdRole jsonCdRole,
     * CdJsonDepartement cdJsonDepartement, String pass_param ) {
     *
     * UserLocal userLocal = new UserLocal(); userLocal.setId(userDetails.getId());
     * //userLocal.setId_crew(userDetails.getId_crew());
     * userLocal.setHas_id_crew(userDetails.getHas_id_crew()); //user org
     * userLocal.setId_org(userOrg.getId());
     * userLocal.setName_org(userOrg.getName());
     * userLocal.setAbbr_org(userOrg.getAbbr());
     * userLocal.setHas_id_org(userDetails.getHas_id_org()); //cd org
     * userLocal.setCd_role(userCdOrg.getCd());
     * userLocal.setCd_self_role(userCdOrg.getCd_self());
     * userLocal.setName_role(userCdOrg.getName());
     *
     * //json role belum userLocal.setCd_role_json(jsonCdRole.getCd());
     * userLocal.setSeq_role_json(jsonCdRole.getSeq());
     * userLocal.setName_role_json(jsonCdRole.getSeq());
     * userLocal.setSlug_role_json(jsonCdRole.getSlug());
     * userLocal.setTag_1_role_json(jsonCdRole.getTag_1());
     * userLocal.setTag_2_role_json(jsonCdRole.getTag_2());
     * userLocal.setTag_3_role_json(jsonCdRole.getTag_3());
     * userLocal.setCd_self_role_json(jsonCdRole.getCd_self());
     * userLocal.setIs_fixed_role_json(jsonCdRole.getIs_fixed());
     * userLocal.setIs_deleted_role_json(jsonCdRole.getIs_deleted());
     * userLocal.setHas_cd_role(userDetails.getHas_cd_role());
     *
     * //cd departement userLocal.setCd_departement(cdDepartement.getCd());
     * userLocal.setCd_self_departement(cdDepartement.getCd_self());
     * userLocal.setCd_name_departement(cdDepartement.getName()); //json departement
     * blum userLocal.setCd_departement_json(cdJsonDepartement.getCd());
     * userLocal.setSeq_departement_json(cdJsonDepartement.getSeq());
     * userLocal.setName_departement_json(cdJsonDepartement.getSeq());
     * userLocal.setSlug_departement_json(cdJsonDepartement.getSlug());
     * userLocal.setTag_1_departement_json(cdJsonDepartement.getTag_1());
     * userLocal.setTag_2_departement_json(cdJsonDepartement.getTag_2());
     * userLocal.setTag_3_departement_json(cdJsonDepartement.getTag_3());
     * userLocal.setCd_self_departement_json(cdJsonDepartement.getCd_self());
     * userLocal.setIs_fixed_departement_json(cdJsonDepartement.getIs_fixed());
     * userLocal.setIs_deleted_departement_json(cdJsonDepartement.getIs_deleted());
     *
     * userLocal.setHas_cd_department(userDetails.getHas_cd_department());
     * userLocal.setName(userDetails.getName());
     * userLocal.setEmail(userDetails.getEmail());
     * userLocal.setPwd(Utilities.getMd5(pass_param));
     * userLocal.setStaff_id(userDetails.getStaff_id());
     * userLocal.setFingerprint_id(userDetails.getFingerprint_id());
     * userLocal.setSocket_id(userDetails.getSocket_id());
     * userLocal.setFile_picture(userDetails.getFile_picture());
     * userLocal.setIp_address(userDetails.getIp_address());
     * userLocal.setToken(userDetails.getToken());
     * userLocal.setEnum_availability(userDetails.getEnum_availability());
     * userLocal.setIs_authenticated(userDetails.getIs_authenticated());
     * userLocal.setJson(userDetails.getJson()); //
     * System.out.println(user_info_db.getExisting(userDetails.getId())); if
     * (user_info_db.getExisting(userDetails.getId()) > 0) { // User already exists
     * //user_info_db.updateUserData(userLocal); } else { // Insert Details of New
     * User user_info_db.insertIgnore(userLocal); } }
     */

    /*
     * ====================== untuk set session
     */
    public void setSessionUser(UserAPILocal userDetails
    // UserOrg userOrg,
    // UserCdRole userCdOrg,
    // CdDepartement cdDepartement,
    // JsonCdRole jsonCdRole,
    // CdJsonDepartement cdJsonDepartement,
    // String sesi_level
    ) {
        System.out.println("----- set session ------");
        try {
            SessionFMS sessionFMS = new SessionFMS();
            // sessionFMS.logoutFms();
            sessionFMS.proseLogin("root", "secret");
            sessionFMS.setSessionFms("sesi_level", MainApp.config("config.unit_type").trim());
            sessionFMS.setSessionFms("unit_id", MainApp.config("config.unit_id").trim());

            sessionFMS.setSessionFms("id", userDetails.getId());
            sessionFMS.setSessionFms("sesi_name_operator", userDetails.getName());
            sessionFMS.setSessionFms("token", userDetails.getToken());
            /**
             * sessionFMS.setSessionFms("id_crew", userDetails.getId_crew());
             *
             * try { if (userDetails.getHas_id_crew() != null) {
             * sessionFMS.setSessionFms("has_id_crew",
             * userDetails.getHas_id_crew().toString()); } } catch (Exception e) {
             *
             * } //user org sessionFMS.setSessionFms("id_org", userOrg.getId());
             * sessionFMS.setSessionFms("name_org", userOrg.getName());
             * sessionFMS.setSessionFms("abbr_org", userOrg.getAbbr());
             *
             * sessionFMS.setSessionFms("has_id_org",
             * userDetails.getHas_id_org().toString());
             *
             * //cd org sessionFMS.setSessionFms("cd_role", userCdOrg.getCd());
             * sessionFMS.setSessionFms("cd_self_role", userCdOrg.getCd_self());
             * sessionFMS.setSessionFms("name_role", userCdOrg.getName());
             *
             * //json role belum sessionFMS.setSessionFms("cd_role_json",
             * jsonCdRole.getCd()); sessionFMS.setSessionFms("seq_role_json",
             * jsonCdRole.getSeq()); sessionFMS.setSessionFms("name_role_json",
             * jsonCdRole.getSeq()); sessionFMS.setSessionFms("slug_role_json",
             * jsonCdRole.getSlug()); sessionFMS.setSessionFms("tag_1_role_json",
             * jsonCdRole.getTag_1()); sessionFMS.setSessionFms("tag_2_role_json",
             * jsonCdRole.getTag_2()); sessionFMS.setSessionFms("tag_3_role_json",
             * jsonCdRole.getTag_3()); sessionFMS.setSessionFms("cd_self_role_json",
             * jsonCdRole.getCd_self()); sessionFMS.setSessionFms("is_fixed_role_json",
             * String.valueOf(jsonCdRole.getIs_fixed()));
             * sessionFMS.setSessionFms("is_deleted_role_json",
             * String.valueOf(jsonCdRole.getIs_deleted()));
             *
             * sessionFMS.setSessionFms("has_cd_role",
             * String.valueOf(userDetails.getHas_cd_role()));
             *
             * //cd departement sessionFMS.setSessionFms("cd_departement",
             * cdDepartement.getCd()); sessionFMS.setSessionFms("cd_self_departement",
             * cdDepartement.getCd_self()); sessionFMS.setSessionFms("cd_name_departement",
             * cdDepartement.getName()); //json departement blum
             * sessionFMS.setSessionFms("cd_departement_json", cdJsonDepartement.getCd());
             * sessionFMS.setSessionFms("seq_departement_json", cdJsonDepartement.getSeq());
             * sessionFMS.setSessionFms("name_departement_json",
             * cdJsonDepartement.getSeq());
             * sessionFMS.setSessionFms("slug_departement_json",
             * cdJsonDepartement.getSlug());
             * sessionFMS.setSessionFms("tag_1_departement_json",
             * cdJsonDepartement.getTag_1());
             * sessionFMS.setSessionFms("tag_2_departement_json",
             * cdJsonDepartement.getTag_2());
             * sessionFMS.setSessionFms("tag_3_departement_json",
             * cdJsonDepartement.getTag_3());
             * sessionFMS.setSessionFms("cd_self_departement_json",
             * cdJsonDepartement.getCd_self());
             * sessionFMS.setSessionFms("is_fixed_departement_json",
             * String.valueOf(cdJsonDepartement.getIs_fixed()));
             * sessionFMS.setSessionFms("is_deleted_departement_json",
             * String.valueOf(cdJsonDepartement.getIs_deleted()));
             *
             * sessionFMS.setSessionFms("has_cd_department",
             * userDetails.getHas_cd_department().toString());
             * sessionFMS.setSessionFms("name", userDetails.getName());
             * sessionFMS.setSessionFms("email", userDetails.getEmail());
             * sessionFMS.setSessionFms("pwd", userDetails.getPwd());
             * sessionFMS.setSessionFms("staff_id", userDetails.getStaff_id());
             * sessionFMS.setSessionFms("fingerprint_id", userDetails.getFingerprint_id());
             * sessionFMS.setSessionFms("socket_id", userDetails.getSocket_id());
             * sessionFMS.setSessionFms("file_picture", userDetails.getFile_picture());
             * sessionFMS.setSessionFms("ip_address", userDetails.getIp_address());
             * sessionFMS.setSessionFms("token", userDetails.getToken());
             * sessionFMS.setSessionFms("enum_availability",
             * String.valueOf(userDetails.getEnum_availability()));
             * sessionFMS.setSessionFms("is_authenticated",
             * String.valueOf(userDetails.getIs_authenticated()));
             * sessionFMS.setSessionFms("json", userDetails.getJson());
             */
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
