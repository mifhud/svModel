package com.tura.fms.screen.models.bank_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BankModel {

    @SerializedName("output")
    @Expose
    private List<BankDetail> output = null;

    @SerializedName("totalItems")
    @Expose
    private int totalItems;

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public List<BankDetail> getOutput() {
        return output;
    }

    public void setOutput(List<BankDetail> output) {
        this.output = output;
    }
}
