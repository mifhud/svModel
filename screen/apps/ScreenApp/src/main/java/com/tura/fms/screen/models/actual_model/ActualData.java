package com.tura.fms.screen.models.actual_model;

import com.google.gson.annotations.SerializedName;

public class ActualData {

    @SerializedName("output")
    private ActualDetail output;

    @SerializedName("response")
    private String response;

    @SerializedName("message")
    private String message;

    public ActualDetail getOutput() {
        return output;
    }

    public void setOutput(ActualDetail output) {
        this.output = output;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String res) {
        this.response = res;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String mes) {
        this.message = mes;
    }

}
