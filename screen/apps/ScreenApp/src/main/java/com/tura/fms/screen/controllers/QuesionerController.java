package com.tura.fms.screen.controllers;

import com.jfoenix.controls.JFXButton;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.SessionFMS;
import com.tura.fms.screen.models.activity_model.ActivityData;
import com.tura.fms.screen.models.quesioner_model.QuesionerDetail;
import com.tura.fms.screen.presenter.ActivityPresenter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;
import com.tura.fms.screen.network.NetworkConnection;

import static org.javalite.app_config.AppConfig.p;

public class QuesionerController implements Initializable {

    @FXML
    private Button menu;
    private NavigationUI navigationUI = new NavigationUI();

    // SessionFMS sessionFMS;

    ActivityData activityData;
    ActivityPresenter activityPresenter = new ActivityPresenter();
    // String token;

    @FXML
    private GridPane gridata;

    @FXML
    private Label mode_connect;

    @FXML
    private Text TitleBar;

    private ObservableList<QuesionerDetail> recordsData = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        TitleBar.setText(p("app.titleQuesioner"));

        // sessionFMS = new SessionFMS();
        // token = sessionFMS.getSessionFms("token");
        // set indikator connection
        mode_connect.setText(NetworkConnection.getStringStatus());

        // sessionFMS.touchFms();

        // System.out.println(sessionFMS.getSessionFms("sesi_level"));

        isiModel();
        tampilanData();

    }

    @FXML
    private void toDashboard(ActionEvent event) throws Exception {

        // ((Node) (event.getSource())).getScene().getWindow().hide();

        // if (sessionFMS.getSessionFms("sesi_level").equals("Hauler")) {

        // navigationUI.windowsNew(p("app.linkDashboard"), p("app.titleDashboard"));

        // } else if (sessionFMS.getSessionFms("sesi_level").equals("Support")) {

        // navigationUI.windowsNew(p("app.linkDashboardSupport"),
        // p("app.titleDashboardSupport"));

        // } else {

        // navigationUI.windowsNew(p("app.linkDashboardLoader"),
        // p("app.titleDashboardLoader"));

        // }

        System.out.println("---------- questionainer controller to dashboard no action");

    }

    private void tampilanData() {

        int cols = 0, colCnt = 0, rowCnt = 0;
        for (int i = 0; i < recordsData.size(); i++) {

            ImageView gambar = new ImageView(new Image("/images/icondash.png"));
            gambar.setPickOnBounds(true);
            gambar.setPreserveRatio(true);
            gambar.setFitHeight(81.0);
            gambar.setFitWidth(129.0);

            VBox vbox = new VBox();
            JFXButton rowbuttonYa = new JFXButton("Ya");
            rowbuttonYa.setButtonType(JFXButton.ButtonType.RAISED);
            rowbuttonYa.getStyleClass().add("button-primary");
            rowbuttonYa.setPrefHeight(6.0);
            rowbuttonYa.setPrefWidth(87.0);

            JFXButton rowbuttonTidak = new JFXButton("Tidak");
            rowbuttonTidak.setButtonType(JFXButton.ButtonType.RAISED);
            rowbuttonTidak.getStyleClass().add("button-raised");
            rowbuttonTidak.setPrefHeight(6.0);
            rowbuttonTidak.setPrefWidth(87.0);

            Label lblpertanyaan = new Label(recordsData.get(i).getPertanyaan());
            lblpertanyaan.setPrefHeight(42.0);
            lblpertanyaan.setPrefWidth(198.0);

            Text lblquesioner = new Text(recordsData.get(i).getQuesioner());
            lblquesioner.setStrokeType(StrokeType.OUTSIDE);
            lblquesioner.setStrokeWidth(0.0);
            lblquesioner.setWrappingWidth(609.3468818664551);
            lblquesioner.setFont(Font.font("Verdana", 20));

            HBox hBox = new HBox();
            hBox.setMargin(rowbuttonTidak, new Insets(0, 0, 0, 10));
            hBox.setPadding(new Insets(0, 10, 0, 10));

            hBox.getChildren().add(rowbuttonYa);
            hBox.getChildren().add(rowbuttonTidak);

            vbox.setMargin(lblquesioner, new Insets(0, 0, 10, 10));
            vbox.setMargin(lblpertanyaan, new Insets(10, 10, 10, 10));

            vbox.getChildren().add(lblpertanyaan);
            vbox.getChildren().add(lblquesioner);
            vbox.getChildren().add(hBox);

            gridata.add(gambar, 1, rowCnt);
            gridata.add(vbox, colCnt, rowCnt);

            colCnt++;
            if (colCnt > cols) {
                rowCnt++;
                colCnt = 0;
            }

        }

    }

    // isi data model
    public void isiModel() {

        recordsData.add(new QuesionerDetail("Pertanyaan 1", "Berapa jam anda tidur kemarin ?"));
        recordsData.add(new QuesionerDetail("Pertanyaan 2", "Berapa jam anda tidur kemarin ?"));
        recordsData.add(new QuesionerDetail("Pertanyaan 3", "Berapa jam anda tidur kemarin ?"));

    }

}
