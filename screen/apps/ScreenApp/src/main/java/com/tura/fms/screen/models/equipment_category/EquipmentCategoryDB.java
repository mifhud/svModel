package com.tura.fms.screen.models.equipment_category;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.database.SQLiteJDBC;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EquipmentCategoryDB extends Operation {

    public static final String TABLE_NAME = "EquipmentCategory_Record";

    private static final String id = "id";
    private static final String cd_manufactur = "cd_manufactur";
    private static final String cd_product = "cd_product";
    private static final String cd_type = "cd_type";
    private static final String cd_brand = "cd_brand";
    private static final String cd_model = "cd_model";
    private static final String cd_class = "cd_class";
    private static final String bucket_capacity = "bucket_capacity";
    private static final String fuel_ratio = "fuel_ratio";
    private static final String kapasitas_tangki = "kapasitas_tangki";
    private static final String productivity = "productivity";
    private static final String pa = "pa";

    private static final String sync_status = "sync_status";

    public EquipmentCategoryDB() {
        super(EquipmentCategoryDB.TABLE_NAME, ConstantValues.DATA_EQUIPMENT_CATEGORY);
    }

    public void createTableEquipmentCategory() {
        try {
            conn().setAutoCommit(true);
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table equipment category exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table equipment category created");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + cd_manufactur
                + " TEXT NULL," + cd_product + " TEXT NULL," + cd_type + " TEXT NULL," + cd_brand + " TEXT NULL,"
                + cd_model + " TEXT NULL," + cd_class + " TEXT NULL," + bucket_capacity + " NUMBER NULL," + fuel_ratio
                + " NUMBER NULL," + kapasitas_tangki + " NUMBER NULL," + productivity + " NUMBER NULL," + pa
                + " NUMBER NULL," + sync_status + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        EquipmentCategory eq = Synchronizer.mapper.convertValue(eqa, EquipmentCategory.class);
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + cd_manufactur + "," + cd_product + ","
                    + cd_type + "," + cd_brand + "," + cd_model + "," + cd_class + "," + bucket_capacity + ","
                    + fuel_ratio + "," + kapasitas_tangki + "," + productivity + "," + pa + "," + sync_status + ")"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,1) ON CONFLICT(id) DO UPDATE SET " + id + "=?," + cd_manufactur
                    + "=?," + cd_product + "=?," + cd_type + "=?," + cd_brand + "=?," + cd_model + "=?," + cd_class
                    + "=?," + bucket_capacity + "=?," + fuel_ratio + "=?," + kapasitas_tangki + "=?," + productivity
                    + "=?," + pa + "=?;";

            st = conn().prepareStatement(sql);
            st.setString(1, eq.getId());
            st.setString(13, eq.getId());
            st.setDouble(9, eq.getFuelRatio());
            st.setDouble(21, eq.getFuelRatio());
            st.setDouble(10, eq.getKapasitasTangki());
            st.setDouble(22, eq.getKapasitasTangki());
            st.setDouble(11, eq.getProductivity());
            st.setDouble(23, eq.getProductivity());
            st.setDouble(12, eq.getPA());
            st.setDouble(24, eq.getPA());
            if (eq.getCdManufactur() != null) {
                st.setString(2, eq.getCdManufactur().getCd());
                st.setString(14, eq.getCdManufactur().getCd());
            } else {
                st.setString(2, "");
                st.setString(14, "");
            }
            if (eq.getCdProduct() != null) {
                st.setString(3, eq.getCdProduct().getCd());
                st.setString(15, eq.getCdProduct().getCd());
            } else {
                st.setString(3, "");
                st.setString(15, "");
            }
            if (eq.getCdType() != null) {
                st.setString(4, eq.getCdType().getCd());
                st.setString(16, eq.getCdType().getCd());
            } else {
                st.setString(4, "");
                st.setString(16, "");
            }
            if (eq.getCdBrand() != null) {
                st.setString(5, eq.getCdBrand().getCd());
                st.setString(17, eq.getCdBrand().getCd());
            } else {
                st.setString(5, "");
                st.setString(17, "");
            }
            if (eq.getCdModel() != null) {
                st.setString(6, eq.getCdModel().getCd());
                st.setString(18, eq.getCdModel().getCd());
            } else {
                st.setString(6, "");
                st.setString(18, "");
            }
            if (eq.getCdClass() != null) {
                st.setString(7, eq.getCdClass().getCd());
                st.setString(19, eq.getCdClass().getCd());
            } else {
                st.setString(7, "");
                st.setString(19, "");
            }
            if (eq.getBucketCapacity() != null) {
                st.setDouble(8, eq.getBucketCapacity());
                st.setDouble(20, eq.getBucketCapacity());
            } else {
                st.setDouble(8, 0);
                st.setDouble(20, 0);
            }
            rs = st.execute();
            // System.out.println("----------------");
            // System.out.println(rs);
            st.close();

            return eq;

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_NAME + " where id=" + id;
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getEquipmentCategory(MainApp.requestHeader, page, size);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Object getFromServerById(String id) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getEquipmentCategoryById(MainApp.requestHeader, id);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        Call<Object> call = APIClient.getInstance().getEquipmentCategoryCount(MainApp.requestHeader);
        try {
            Response<Object> response = call.execute();
            Map<String, Number> mapped = APIClient.mapper.convertValue(response.body(), Map.class);
            if (mapped.get("output") != null) {
                return mapped.get("output").intValue();
            } else {
                return 0;
            }
        } catch (IOException ex) {
            // ex.printStackTrace();
            System.out.println("[EquipmentCategory getRemoteCount] " + ex.getMessage());
        }
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                EquipmentCategoryLocal eq = new EquipmentCategoryLocal();
                eq.setId(cursor.getString("id"));
                eq.setCdManufactur(cursor.getString("cd_manufactur"));
                eq.setCdProduct(cursor.getString("cd_product"));
                eq.setCdType(cursor.getString("cd_type"));
                eq.setCdBrand(cursor.getString("cd_brand"));
                eq.setCdModel(cursor.getString("cd_model"));
                eq.setCdClass(cursor.getString("cd_class"));
                eq.setBucketCapacity(cursor.getDouble("bucket_capacity"));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        // if (!NetworkConnection.getServiceStatus()) {
        // return;
        // }
        // System.out.println("--- post synchronization ---");
        // Call<ResponseModel> call =
        // APIClient.getInstance().postEquipment(MainApp.requestHeader, eq);
        // try {
        // Response<ResponseModel> response = call.execute();
        // System.out.println(response);
        // System.out.println(response.body().getResponse());
        // System.out.println(response.body().getMessage());
        // if (response.body().getResponse().equals("success")) {
        // System.out.println("post success");
        // /**
        // * change flag (sync_status to 1)
        // */
        // for (Object obj : eq) {
        // EquipmentLocal oqe = (EquipmentLocal) obj;
        // setSynchronized(oqe.getId());
        // }
        // }
        // } catch (IOException ex) {
        // ex.printStackTrace();
        // }
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where id='" + id + "'";
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getLocalOneById(String fieldId, String searchId) {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + fieldId + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, searchId);
            ResultSet cursor = preparedStatement.executeQuery();
            if (cursor.next()) {
                EquipmentCategoryLocal eq = new EquipmentCategoryLocal();
                eq.setId(cursor.getString("id"));
                eq.setCdManufactur(cursor.getString("cd_manufactur"));
                eq.setCdProduct(cursor.getString("cd_product"));
                eq.setCdType(cursor.getString("cd_type"));
                eq.setCdBrand(cursor.getString("cd_brand"));
                eq.setCdModel(cursor.getString("cd_model"));
                eq.setCdClass(cursor.getString("cd_class"));
                eq.setBucketCapacity(cursor.getDouble("bucket_capacity"));
                cursor.close();
                return (Object) eq;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
