package com.tura.fms.screen.models.safety_status;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "date", "actual_id", "operation_support_id", "operation_loader_id", "cd_safety", "latitude",
        "longitude", "location_id", "operator_id", "equipment_id" })
public class SafetyStatusLocal {

    @JsonProperty("id")
    private String id;
    @JsonProperty("date")
    private Number date;
    @JsonProperty("actual_id")
    private String id_actual;
    @JsonProperty("operation_support_id")
    private String id_operation_support;
    @JsonProperty("operation_loader_id")
    private String id_operation_loader;
    @JsonProperty("cd_safety")
    private String cd_safety;
    @JsonProperty("latitude")
    private Number latitude;
    @JsonProperty("longitude")
    private Number longitude;
    @JsonProperty("location_id")
    private String id_location;
    @JsonProperty("operator_id")
    private String id_operator;
    @JsonProperty("equipment_id")
    private String id_equipment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Number getDate() {
        return date;
    }

    public void setDate(Number date) {
        this.date = date;
    }

    public String getIdActual() {
        return id_actual;
    }

    public void setIdActual(String id_actual) {
        this.id_actual = id_actual;
    }

    public String getIdOperationSupport() {
        return id_operation_support;
    }

    public void setIdOperationSupport(String id_operation_support) {
        this.id_operation_support = id_operation_support;
    }

    public String getIdOperationLoader() {
        return id_operation_loader;
    }

    public void setIdOperationLoader(String id_operation_loader) {
        this.id_operation_loader = id_operation_loader;
    }

    public String getCdSafety() {
        return cd_safety;
    }

    public void setCdSafety(String cd_safety) {
        this.cd_safety = cd_safety;
    }

    public Number getLatitude() {
        return latitude;
    }

    public void setLatitude(Number latitude) {
        this.latitude = latitude;
    }

    public Number getLongitude() {
        return longitude;
    }

    public void setLongitude(Number longitude) {
        this.longitude = longitude;
    }

    public String getIdLocation() {
        return id_location;
    }

    public void setIdLocation(String id_location) {
        this.id_location = id_location;
    }

    public String getIdOperator() {
        return id_operator;
    }

    public void setIdOperator(String id_operator) {
        this.id_operator = id_operator;
    }

    public String getIdEquipment() {
        return id_equipment;
    }

    public void setIdEquipment(String id_equipment) {
        this.id_equipment = id_equipment;
    }

}
