package com.tura.fms.screen.models.maintenance_model.task_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.constant.Constant;
import com.tura.fms.screen.models.user_model.UserActual;

public class MaintenanceDescription {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("date")
    @Expose
    private Long date;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("hm")
    @Expose
    private Double hm;

    @SerializedName("wo_number")
    @Expose
    private String wo_number;

    @SerializedName("maintenance_id")
    @Expose
    private Object maintenance_id;

    @SerializedName("cd_shift")
    @Expose
    private Constant cd_shift;

    @SerializedName("created_by")
    @Expose
    private UserActual created_by;

    @SerializedName("supervisor_id")
    @Expose
    private UserActual supervisor_id;

    @SerializedName("foreman_id")
    @Expose
    private UserActual foreman_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long dt) {
        this.date = dt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String desc) {
        this.description = desc;
    }

    public Double getHm() {
        return hm;
    }

    public void setHm(Double hm) {
        this.hm = hm;
    }

    public String getWo_number() {
        return wo_number;
    }

    public void setWo_number(String wo_number) {
        this.wo_number = wo_number;
    }

    public Object getMaintenanceId() {
        return maintenance_id;
    }

    public void setMaintenanceId(Object mainte) {
        this.maintenance_id = mainte;
    }

    public Constant getCdShift() {
        return cd_shift;
    }

    public void setCdShift(Constant shift) {
        this.cd_shift = shift;
    }

    public UserActual getCreatedBy() {
        return created_by;
    }

    public void setCreatedBy(UserActual created) {
        this.created_by = created;
    }

    public UserActual getSupervisorId() {
        return supervisor_id;
    }

    public void setSupervisorId(UserActual sup) {
        this.supervisor_id = sup;
    }

    public UserActual getForemanId() {
        return foreman_id;
    }

    public void setForemanId(UserActual fore) {
        this.foreman_id = fore;
    }

    public String getDateLabel() {
        return Utilities.timeStampToDate(this.date, "tanggal");
    }

    public String getShiftLabel() {
        return this.cd_shift.getName();
    }

    public String getSupervisorLabel() {
        return this.supervisor_id.getName();
    }

    public String getForemanLabel() {
        return this.foreman_id.getName();
    }

    public String getWoLabel() {
        return this.wo_number;
    }

    public String getHmLabel() {
        return this.hm.toString();
    }

    public String getDescriptionLabel() {
        return this.description;
    }

}