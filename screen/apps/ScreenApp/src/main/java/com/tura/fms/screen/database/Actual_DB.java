package com.tura.fms.screen.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.DefaultResponse;
import com.tura.fms.screen.models.PagingResponse;
import com.tura.fms.screen.models.actual_model.ActualAPI;
import com.tura.fms.screen.models.actual_model.ActualDetail;
import com.tura.fms.screen.models.actual_model.CdMaterial;
import com.tura.fms.screen.models.actual_model.CdOperation;
import com.tura.fms.screen.models.actual_model.Destination;
import com.tura.fms.screen.models.actual_model.GroupLeader;
import com.tura.fms.screen.models.actual_model.Hauler;
import com.tura.fms.screen.models.actual_model.Loader;
import com.tura.fms.screen.models.actual_model.Location;
import com.tura.fms.screen.models.actual_model.Supervisor;
import com.tura.fms.screen.models.constant.Constant;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

public class Actual_DB extends Operation {

    // Table Name
    private static final String TABLE_INFO = "Actual_Record";

    // Table Columns
    private static final String id = "id";
    private static final String name = "name";
    private static final String date = "date";
    private static final String id_plan = "id_plan";
    private static final String id_supervisor = "id_supervisor";
    private static final String id_group_leader = "id_group_leader";
    private static final String id_checker = "id_checker";
    private static final String id_loader_operator = "id_loader_operator";
    private static final String id_loader = "id_loader";
    private static final String id_hauler_operator = "id_hauler_operator";
    private static final String id_hauler = "id_hauler";
    private static final String id_location = "id_location";
    private static final String id_destination = "id_destination";
    private static final String cd_operation = "cd_operation";
    private static final String cd_material = "cd_material";
    private static final String cd_shift = "cd_shift";
    private static final String sync_status = "sync_status";

    public static String createTable() {

        return "CREATE TABLE " + TABLE_INFO + "(" + id + " TEXT PRIMARY KEY," + name + " TEXT," + id_plan + " TEXT,"
                + id_supervisor + " TEXT," + id_group_leader + " TEXT," + id_checker + " TEXT," + id_loader_operator
                + " TEXT," + id_loader + " TEXT," + id_hauler_operator + " TEXT," + id_hauler + " TEXT," + id_location
                + " TEXT," + id_destination + " TEXT," + cd_operation + " TEXT," + cd_shift + " TEXT," + sync_status
                + " NUMBER NULL," + date + " TEXT," + cd_material + " TEXT" + ")";
    }

    public Actual_DB() {
        super(Actual_DB.TABLE_INFO, ConstantValues.DATA_ACTUAL);
    }

    public void createTableActual() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_INFO, null);
            Statement stmt = null;

            if (rs.next()) {
                System.out.println("Table exists Actual");
                // stmt.close();
                // conn().close();
            } else {
                stmt = conn().createStatement();
                stmt.executeUpdate(createTable());
                stmt.close();
                // conn().close();
                System.out.println("Table created Actual");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getExisting(String cd_param) {
        try {
            String sql = "SELECT count(*) as total FROM " + TABLE_INFO + " where " + id + "=?";

            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, cd_param);

            ResultSet cursor = preparedStatement.executeQuery();

            int jumlah;

            if (cursor.next()) {
                jumlah = cursor.getInt("total");
            } else {
                jumlah = 0;
            }
            cursor.close();
            // conn().close();
            return jumlah;
        } catch (Exception e) {
            return 0;
        }
    }

    public ActualDetail getData() {
        try {
            ResultSet cursor = null;

            String column = id + "," + id_plan + "," + id_supervisor + "," + id_group_leader + "," + id_checker + ","
                    + id_loader_operator + "," + id_loader + "," + id_hauler_operator + "," + id_hauler + ","
                    + id_location + "," + id_destination + "," + cd_operation + "," + cd_shift + "," + date + ","
                    + cd_material;

            String sql = "SELECT " + column + " FROM " + TABLE_INFO + " ORDER BY " + id + " desc LIMIT 1";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);

            cursor = preparedStatement.executeQuery();

            ActualDetail actualDetail = new ActualDetail();

            while (cursor.next()) {

                actualDetail.setId(cursor.getString(1));
                actualDetail.setId_plan(cursor.getString(2));

                Supervisor supervisor = new Supervisor();
                supervisor.setId(cursor.getString(3));
                actualDetail.setId_supervisor(supervisor);
                GroupLeader groupLeader = new GroupLeader();
                groupLeader.setId(cursor.getString(4));
                actualDetail.setId_group_leader(groupLeader);

                actualDetail.setId_checker(cursor.getString(5));
                actualDetail.setId_loader_operator(cursor.getString(6));

                Loader loader = new Loader();
                loader.setId(cursor.getString(7));
                actualDetail.setId_loader(loader);
                actualDetail.setId_hauler_operator(cursor.getString(8));

                Hauler hauler = new Hauler();
                hauler.setId(cursor.getString(9));
                actualDetail.setId_hauler(hauler);

                Location location = new Location();
                location.setId(cursor.getString(10));
                actualDetail.setId_location(location);

                Location destination = new Location();
                destination.setId(cursor.getString(11));
                actualDetail.setId_destination(destination);
                CdOperation operation = new CdOperation();
                operation.setCd(cursor.getString(12));

                actualDetail.setCd_operation(operation);

                Constant shift = new Constant();
                shift.setCd(cursor.getString(13));

                actualDetail.setCd_shift(shift);
                actualDetail.setDate(cursor.getLong(14));

                CdMaterial mat = new CdMaterial();
                mat.setCd(cursor.getString(15));
                actualDetail.setCd_material(mat);
            }

            // conn().close();
            cursor.close();
            return actualDetail;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void insertData(ActualDetail actualDetail) {
        PreparedStatement st = null;
        Boolean rs = null;
        String proses;
        try {
            String sql;

            if (getExisting(actualDetail.getId()) == 0) {

                sql = "INSERT INTO " + TABLE_INFO + " " + "(" + id + "," + name + "," + id_plan + "," + id_supervisor
                        + "," + id_group_leader + "," + id_checker + "," + id_loader_operator + "," + id_loader + ","
                        + id_hauler_operator + "," + id_hauler + "," + id_location + "," + id_destination + ","
                        + cd_operation + "," + cd_shift + "," + sync_status + "," + date + "," + cd_material + ")"
                        + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,0, ?, ?);";

                proses = "proses";

            } else {

                sql = "UPDATE  " + TABLE_INFO + " SET " + "" + id + "=?," + name + "=?," + id_plan + "=?,"
                        + id_supervisor + "=?," + id_group_leader + "=?," + id_checker + "=?," + id_loader_operator
                        + "=?," + id_loader + "=?," + id_hauler_operator + "=?," + id_hauler + "=?," + id_location
                        + "=?," + id_destination + "=?," + cd_operation + "=?," + cd_shift + "=?" + date + "=?"
                        + cd_material + "=?" + " WHERE " + id + "=?;";

                proses = "update";

            }

            st = conn().prepareStatement(sql);

            st.setString(1, actualDetail.getId());
            st.setString(2, actualDetail.getName());
            st.setString(3, actualDetail.getId_plan());
            st.setString(4, actualDetail.getId_supervisor().getId());
            st.setString(5, actualDetail.getId_group_leader().getId());
            st.setString(6, actualDetail.getId_checker());
            st.setString(7, actualDetail.getId_loader_operator());
            st.setString(8, actualDetail.getId_loader().getId());
            st.setString(9, actualDetail.getId_hauler_operator());
            st.setString(10, actualDetail.getId_hauler().getId());
            st.setString(11, actualDetail.getId_location().getId());
            st.setString(12, actualDetail.getId_destination().getId());
            st.setString(13, actualDetail.getCd_operation().getCd());
            st.setString(14, actualDetail.getCd_shift().getCd());
            st.setLong(15, actualDetail.getDate());
            st.setString(16, actualDetail.getCd_material().getCd());

            if (proses.equals("update")) {
                st.setString(17, actualDetail.getId());
            }
            rs = st.execute();
            st.close();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            if (rs) {
                try {
                    st.close();
                    // conn().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int localCount() {
        try {
            Statement s = conn().createStatement();
            ResultSet r = s.executeQuery("SELECT COUNT(*) AS rowcount FROM " + TABLE_INFO + " WHERE date>="
                    + Utilities.getCurrentTimestamp().getTime());
            r.next();
            int count = r.getInt("rowcount");
            r.close();
            s.close();
            return count;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    @Override
    public Object insertIgnore(Object eqa) {
        ActualAPI eq = Synchronizer.mapper.convertValue(eqa, ActualAPI.class);
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_INFO + " " + "(" + id + "," + name + "," + id_plan + "," + id_supervisor
                    + "," + id_group_leader + "," + id_checker + "," + id_loader_operator + "," + id_loader + ","
                    + id_hauler_operator + "," + id_hauler + "," + id_location + "," + id_destination + ","
                    + cd_operation + "," + cd_shift + "," + sync_status + "," + date + "," + cd_material + ")"
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,1,?,?) ON CONFLICT(id) DO UPDATE SET sync_status=sync_status;";
            st = conn().prepareStatement(sql);
            st.setString(1, eq.getId());
            st.setString(2, "");
            st.setString(3, eq.getIdPlan());
            st.setString(4, eq.getIdSupervisor().getId());
            st.setString(5, eq.getIdGroupLeader().getId());
            st.setString(6, "");
            st.setString(7, eq.getIdLoaderOperator());
            st.setString(8, eq.getIdLoader().getId());
            st.setString(9, eq.getIdHaulerOperator().getId());
            st.setString(10, eq.getIdHauler().getId());
            st.setString(11, eq.getIdLocation().getId());
            st.setString(12, eq.getIdDestination());
            st.setString(13, eq.getCdOperation());
            st.setString(14, eq.getCdShift());
            st.setString(15, eq.getDate());
            st.setString(16, eq.getCdMaterial().getCd());
            rs = st.execute();
            // System.out.println("---------------- insert ignore ");
            // System.out.println(rs);
            st.close();

            return eqa;

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_INFO + " where id=" + id;
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        String id_loader = null;
        String id_hauler = null;
        if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)) {
            id_loader = MainApp.equipmentLocal.getId();
        }
        if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER)) {
            id_hauler = MainApp.equipmentLocal.getId();
        }
        Call<PagingResponse> call = APIClient.getInstance().getActual(MainApp.requestHeader, page, size, id_loader,
                id_hauler, "startDay");
        try {
            Response<PagingResponse> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        Call<DefaultResponse> call = APIClient.getInstance().getCountActualToday(MainApp.requestHeader,
                MainApp.equipmentLocal.getId(), MainApp.config("config.unit_type"));
        try {
            Response<DefaultResponse> response = call.execute();
            if (response.body() != null) {
                if (response.body().getOutput() != null) {
                    return Double.valueOf(response.body().getOutput().toString()).intValue();
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String column = id + "," + id_plan + "," + id_supervisor + "," + id_group_leader + "," + id_checker + ","
                    + id_loader_operator + "," + id_loader + "," + id_hauler_operator + "," + id_hauler + ","
                    + id_location + "," + id_destination + "," + cd_operation + "," + cd_shift + "," + date + ","
                    + cd_material;
            String sql = "SELECT " + column + " FROM " + TABLE_INFO + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                ActualDetail actualDetail = new ActualDetail();

                actualDetail.setId(cursor.getString(1));
                actualDetail.setId_plan(cursor.getString(2));

                Supervisor supervisor = new Supervisor();
                supervisor.setId(cursor.getString(3));
                actualDetail.setId_supervisor(supervisor);
                GroupLeader groupLeader = new GroupLeader();
                groupLeader.setId(cursor.getString(4));
                actualDetail.setId_group_leader(groupLeader);

                actualDetail.setId_checker(cursor.getString(5));
                actualDetail.setId_loader_operator(cursor.getString(6));

                Loader loader = new Loader();
                loader.setId(cursor.getString(7));
                actualDetail.setId_loader(loader);
                actualDetail.setId_hauler_operator(cursor.getString(8));

                Hauler hauler = new Hauler();
                hauler.setId(cursor.getString(9));
                actualDetail.setId_hauler(hauler);

                Location location = new Location();
                location.setId(cursor.getString(10));
                actualDetail.setId_location(location);

                Location destination = new Location();
                destination.setId(cursor.getString(11));
                actualDetail.setId_destination(destination);
                CdOperation operation = new CdOperation();
                operation.setCd(cursor.getString(12));

                actualDetail.setCd_operation(operation);

                Constant shift = new Constant();
                shift.setCd(cursor.getString(13));

                actualDetail.setCd_shift(shift);
                actualDetail.setDate(cursor.getLong(14));

                CdMaterial mat = new CdMaterial();
                mat.setCd(cursor.getString(15));
                actualDetail.setCd_material(mat);

                result.add(actualDetail);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getFlaggedDataOne() {
        try {
            String column = id + "," + id_plan + "," + id_supervisor + "," + id_group_leader + "," + id_checker + ","
                    + id_loader_operator + "," + id_loader + "," + id_hauler_operator + "," + id_hauler + ","
                    + id_location + "," + id_destination + "," + cd_operation + "," + cd_shift + "," + date + ","
                    + cd_material;
            String sql = "SELECT " + column + " FROM " + TABLE_INFO + " WHERE sync_status=? LIMIT 1";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                ActualDetail actualDetail = new ActualDetail();

                actualDetail.setId(cursor.getString(1));
                actualDetail.setId_plan(cursor.getString(2));

                Supervisor supervisor = new Supervisor();
                supervisor.setId(cursor.getString(3));
                actualDetail.setId_supervisor(supervisor);
                GroupLeader groupLeader = new GroupLeader();
                groupLeader.setId(cursor.getString(4));
                actualDetail.setId_group_leader(groupLeader);

                actualDetail.setId_checker(cursor.getString(5));
                actualDetail.setId_loader_operator(cursor.getString(6));

                Loader loader = new Loader();
                loader.setId(cursor.getString(7));
                actualDetail.setId_loader(loader);
                actualDetail.setId_hauler_operator(cursor.getString(8));

                Hauler hauler = new Hauler();
                hauler.setId(cursor.getString(9));
                actualDetail.setId_hauler(hauler);

                Location location = new Location();
                location.setId(cursor.getString(10));
                actualDetail.setId_location(location);

                Location destination = new Location();
                destination.setId(cursor.getString(11));
                actualDetail.setId_destination(destination);
                CdOperation operation = new CdOperation();
                operation.setCd(cursor.getString(12));

                actualDetail.setCd_operation(operation);

                Constant shift = new Constant();
                shift.setCd(cursor.getString(13));

                actualDetail.setCd_shift(shift);
                actualDetail.setDate(cursor.getLong(14));

                CdMaterial mat = new CdMaterial();
                mat.setCd(cursor.getString(15));
                actualDetail.setCd_material(mat);

                cursor.close();

                return actualDetail;

            } else {

                cursor.close();
                return null;
            }

        } catch (Exception e) {

            // e.printStackTrace();
            System.out.println(e.getMessage());
            return null;

        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        Call<DefaultResponse> call = APIClient.getInstance().postOperationActual(MainApp.requestHeader, eq);
        try {
            Response<DefaultResponse> response = call.execute();
            System.out.println(response);
            System.out.println(response.body().getRespon());
            System.out.println(response.body().getMessage());
            if (response.body().getRespon().equals("success")) {
                System.out.println("post success");
                /**
                 * change flag (sync_status to 1)
                 */
                for (Object obj : eq) {
                    ActualDetail oqe = (ActualDetail) obj;
                    setSynchronized(oqe.getId());
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_INFO + " set sync_status=1 where id='" + id + "'";
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getLocalOneById(String fieldId, String searchId) {
        try {
            String column = id + "," + id_plan + "," + id_supervisor + "," + id_group_leader + "," + id_checker + ","
                    + id_loader_operator + "," + id_loader + "," + id_hauler_operator + "," + id_hauler + ","
                    + id_location + "," + id_destination + "," + cd_operation + "," + cd_shift + "," + date + ","
                    + cd_material;
            String sql = "SELECT " + column + " FROM " + TABLE_INFO + " WHERE " + fieldId + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, searchId);
            ResultSet cursor = preparedStatement.executeQuery();
            if (cursor.next()) {
                ActualDetail actualDetail = new ActualDetail();

                actualDetail.setId(cursor.getString(1));
                actualDetail.setId_plan(cursor.getString(2));

                Supervisor supervisor = new Supervisor();
                supervisor.setId(cursor.getString(3));
                actualDetail.setId_supervisor(supervisor);
                GroupLeader groupLeader = new GroupLeader();
                groupLeader.setId(cursor.getString(4));
                actualDetail.setId_group_leader(groupLeader);

                actualDetail.setId_checker(cursor.getString(5));
                actualDetail.setId_loader_operator(cursor.getString(6));

                Loader loader = new Loader();
                loader.setId(cursor.getString(7));
                actualDetail.setId_loader(loader);
                actualDetail.setId_hauler_operator(cursor.getString(8));

                Hauler hauler = new Hauler();
                hauler.setId(cursor.getString(9));
                actualDetail.setId_hauler(hauler);

                Location location = new Location();
                location.setId(cursor.getString(10));
                actualDetail.setId_location(location);

                Location destination = new Location();
                destination.setId(cursor.getString(11));
                actualDetail.setId_destination(destination);
                CdOperation operation = new CdOperation();
                operation.setCd(cursor.getString(12));

                actualDetail.setCd_operation(operation);

                Constant shift = new Constant();
                shift.setCd(cursor.getString(13));

                actualDetail.setCd_shift(shift);
                actualDetail.setDate(cursor.getLong(14));

                CdMaterial mat = new CdMaterial();
                mat.setCd(cursor.getString(15));
                actualDetail.setCd_material(mat);

                cursor.close();
                return (Object) actualDetail;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
