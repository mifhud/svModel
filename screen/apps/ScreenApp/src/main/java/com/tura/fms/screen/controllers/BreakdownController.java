package com.tura.fms.screen.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXSnackbar;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Activity_DB;
import com.tura.fms.screen.database.Actual_DB;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.SessionFMS;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.activity_model.ActivityData;
import com.tura.fms.screen.models.activity_model.ActivityDetail;
import com.tura.fms.screen.models.actual_model.ActualDetail;
import com.tura.fms.screen.models.maintenance_model.MainConstModel;
import com.tura.fms.screen.models.maintenance_model.MaintenancePost;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;
import com.tura.fms.screen.presenter.ActivityPresenter;
import com.tura.fms.screen.presenter.BreakdownPresenter;
import com.tura.fms.screen.presenter.CustomRunnable;
import com.tura.fms.screen.helpers.Utilities;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static com.tura.fms.screen.constanst.ConstantValues.EQUIPMENT_HAULER;
import static org.javalite.app_config.AppConfig.p;

public class BreakdownController implements Initializable {

    private NavigationUI navigationUI = new NavigationUI();
    @FXML
    private Button TitleBar;

    @FXML
    private GridPane gridata;

    @FXML
    private GridPane gridschedule;

    @FXML
    private GridPane gridunschedule;

    // @FXML
    // private JFXButton loginmekanik;

    @FXML
    private StackPane stackPane;

    private ObservableList<MainConstModel> recordsData = FXCollections.observableArrayList();

    private ObservableList<MainConstModel> recordsDataUnschedule = FXCollections.observableArrayList();

    BreakdownPresenter breakdownPresenter = new BreakdownPresenter();

    ActivityData activityData;

    ActivityPresenter activityPresenter = new ActivityPresenter();

    @FXML
    VBox rootpane;

    JFXSnackbar snackbar;

    Activity_DB activity_db;

    Actual_DB actual_db;

    ActualDetail actualDetail;

    ResponseModel responseModel;

    JSONObject json_gps;

    public static String breakdownInfo = "";

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MainApp.clockDashboard.stop();

        TitleBar.setText(p("app.textLoading"));

        // sessionFMS = new SessionFMS();
        // sessionFMS.touchFms();
        // token = sessionFMS.getSessionFms("token");

        activity_db = new Activity_DB();
        actual_db = new Actual_DB();

        actualDetail = MainApp.actualInfo;
        snackbar = new JFXSnackbar(rootpane);
        snackbar.setPrefWidth(300);
        snackbar.getStyleClass().add("-fx-background-color: #ccc;");

        drawerAction();

        // tampilkanDataBreakdown();

        json_gps = MainApp.device.getGPS();

        // tampilkanData();
        // tampilkanDataUnschedule();

        NavigationUI.publicStackPane = stackPane;

        getData();
    }

    private void getData() {
        List<Object> localData = MainApp.constantDB.getLocalById("cd_self", "M");

        if (localData != null) {

            TitleBar.setText(p("app.titleBreakdown"));
            ActivityData act = new ActivityData();
            act.setOutputObject(localData);
            tampilkanDataBrekdown(act);

        } else {

            if (NetworkConnection.getServiceStatus()) {

                Call<ActivityData> call = APIClient.getInstance().GetActivity(MainApp.requestHeader, "M");
                call.enqueue(new Callback<ActivityData>() {

                    @Override
                    public void onResponse(Call<ActivityData> call, Response<ActivityData> response) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                TitleBar.setText(p("app.titleBreakdown"));
                                tampilkanDataBrekdown(response.body());
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<ActivityData> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

            } else {
                System.out.println("Service not available and local data is empty");
            }

        }
    }

    private void tampilkanDataBrekdown(ActivityData activityData) {

        int Col = 0;
        int Row = 0;

        String status;

        try {

            if (activityData.getOutput() != null) {

                for (int i = 0; i < activityData.getOutput().size(); i++) {

                    // System.out.println(activityData.getOutput().get(i).getCd());
                    ActivityDetail activityDetail = new ActivityDetail();
                    activityDetail.setCd(activityData.getOutput().get(i).getCd());
                    activityDetail.setName(activityData.getOutput().get(i).getName());
                    activityDetail.setCd_self(activityData.getOutput().get(i).getCd_self());

                    // activity_db.insertData(activityDetail);

                    JFXButton rowbutton = new JFXButton(activityData.getOutput().get(i).getName());
                    VBox vboxForButtons = new VBox();

                    rowbutton.setButtonType(JFXButton.ButtonType.RAISED);
                    rowbutton.getStyleClass().add("button-activity");
                    rowbutton.setFont(Font.font("Arial", FontWeight.BOLD, 90));

                    rowbutton.setPrefHeight(80);
                    rowbutton.setPrefWidth(250);
                    rowbutton.setId(String.valueOf(i));

                    // rowbutton.(new Insets(0,0,0,0));
                    vboxForButtons.setPadding(new Insets(10, 5, 10, 5));

                    vboxForButtons.getChildren().add(rowbutton);

                    gridata.add(vboxForButtons, Col, Row);
                    gridata.setHalignment(rowbutton, HPos.CENTER);
                    gridata.setValignment(rowbutton, VPos.CENTER);

                    rowbutton.setOnAction((actionEvent) -> {

                        rowbutton.setDisable(true);

                        int index = Integer.valueOf(rowbutton.getId());

                        String selectedProblem = activityData.getOutput().get(index).getCd();

                        if (selectedProblem.equals(ConstantValues.PROBLEM_OTHER)) {

                            Utilities.showCustomDialogProblem(actionEvent, new Runnable() {

                                @Override
                                public void run() {
                                    System.out.println(BreakdownController.breakdownInfo);

                                    pesanConfirmation(selectedProblem, BreakdownController.breakdownInfo,
                                            new CustomRunnable() {

                                                @Override
                                                public void run() {
                                                    rowbutton.setDisable(false);
                                                    // if (this.getData())
                                                    String cmd = (String) this.getData();
                                                    if (cmd != null) {
                                                        if (cmd.equals("logout")) {
                                                            navigationUI.back_dashboard(actionEvent, new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    // Utilities.doLogOut(actionEvent);
                                                                }
                                                            });
                                                        }
                                                    }
                                                }
                                            });
                                }
                            });

                        } else {

                            pesanConfirmation(activityData.getOutput().get(index).getCd(), rowbutton.getText(),
                                    new CustomRunnable() {

                                        @Override
                                        public void run() {
                                            rowbutton.setDisable(false);
                                            // if (this.getData())
                                            String cmd = (String) this.getData();
                                            if (cmd != null) {
                                                if (cmd.equals("logout")) {
                                                    navigationUI.back_dashboard(actionEvent, new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            // Utilities.doLogOut(actionEvent);
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    });

                        }

                    });

                    Col++;

                    if (Col > 3) {
                        // Reset Column
                        Col = 0;
                        // Next Row
                        Row++;
                    }

                }

                status = "success";
            } else {
                status = "success";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void drawerAction() {

        // loginmekanik.setOnAction((ActionEvent evt) -> {
        // try {
        // navigationUI.windowsTab(p("app.linkLoginMekanik"),
        // p("app.titleLoginMekanik"), evt);
        // } catch (Exception e) {
        // e.printStackTrace();
        // }
        // });

        TitleBar.setOnAction((ActionEvent evt) -> {
            TitleBar.setText(p("app.textLoading"));

            TitleBar.setDisable(true);

            navigationUI.back_dashboard(evt, new Runnable() {

                @Override
                public void run() {
                    TitleBar.setDisable(false);
                }
            });

            MainApp.clockDashboard.play();
        });
    }

    public void pesanConfirmation(String cd, String label, CustomRunnable callback) {
        JFXDialogLayout content = new JFXDialogLayout();
        Text txt = new Text("KONFIRMASI - " + label);
        txt.setStyle("-fx-font-size:20px;");
        content.setHeading(txt);
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        JFXButton confirm = new JFXButton("Yes");
        confirm.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        confirm.setPrefHeight(30.0);
        confirm.setPrefWidth(100.0);
        confirm.getStyleClass().add("button-primary");
        confirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if (MainApp.equipmentLocal == null) {

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            Utilities.showMessageDialog("Equipment not registered", "",
                                    MainApp.config("config.unit_id") + " tidak belum terdaftar dalam database");
                        }
                    });

                } else {

                    MaintenancePost maintenancePost = new MaintenancePost();
                    // if (sessionFMS.getSessionFms("sesi_level").equals(EQUIPMENT_HAULER)) {
                    // maintenancePost.setEquipment_id(actualDetail.getId_hauler().getId());
                    // } else {
                    // maintenancePost.setEquipment_id(actualDetail.getId_loader().getId());
                    // }
                    maintenancePost.setEquipment_id(MainApp.equipmentLocal.getId());
                    maintenancePost.setIs_deleted(0);

                    // if (Utilities.isLoaderHauler()) {
                    // if (MainApp.isOperator) {
                    // maintenancePost.setLocation_id(MainApp.actualInfo.getId_location().getId());
                    // maintenancePost.setOperation_actual_id(MainApp.actualInfo.getId());
                    // }
                    // } else {
                    // if (MainApp.isOperator) {
                    // maintenancePost.setLocation_id(MainApp.actualSupport.getLocationId().getId());
                    // maintenancePost.setActual_support_id(MainApp.actualSupport.getId());
                    // }
                    // }

                    maintenancePost.setLocation_id(
                            MainApp.actualInfo == null ? null : MainApp.actualInfo.getId_location().getId());
                    maintenancePost
                            .setOperation_actual_id(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                    maintenancePost
                            .setActual_support_id(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());

                    maintenancePost.setProblem_id(cd);

                    if (cd.equals(ConstantValues.PROBLEM_OTHER)) {
                        maintenancePost.setInfo(label);
                    }

                    try {
                        maintenancePost.setLatitude(json_gps.getJSONObject("gps").getDouble("lon"));
                        maintenancePost.setLatitude(json_gps.getJSONObject("gps").getDouble("lat"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    maintenancePost.setOperator_id(MainApp.user.getId());

                    breakdownPresenter.postRestApi(maintenancePost, stackPane, label, cd, callback);
                }

                dialog.close();
            }
        });

        JFXButton keluar = new JFXButton("No");
        keluar.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        keluar.setPrefHeight(30.0);
        keluar.setPrefWidth(100.0);
        keluar.getStyleClass().add("button-primary");

        keluar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (callback != null) {
                    callback.run();
                }
                dialog.close();
            }
        });

        content.setActions(confirm, keluar);

        dialog.show();
    }

}
