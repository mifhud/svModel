package com.tura.fms.screen.presenter;

public class CustomRunnable implements Runnable {
    private Object data;

    public void setData(Object data) {
        this.data = data;
    }

    public Object getData() {
        return this.data;
    }

    @Override
    public void run() {
    }
}