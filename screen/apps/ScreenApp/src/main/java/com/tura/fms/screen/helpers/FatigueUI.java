package com.tura.fms.screen.helpers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.models.fatigue_model.FatigueModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FatigueUI {

    private JFXButton[] confirm = new JFXButton[4];

    private int pertanyaan;

    private JFXDialog dialog;

    public static List<JFXDialog> fatigueDialog = new ArrayList<JFXDialog>();

    public int getPertanyaan() {
        return pertanyaan;
    }

    private ObservableList<FatigueModel> recordsData = FXCollections.observableArrayList();

    public void setPertanyaan(int pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public void showFatigePopup(StackPane stackPane, ObservableList<FatigueModel> fatigueData, Runnable callback,
            Runnable cbTrue) {
        recordsData = fatigueData;
        Date date = new Date();

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        String current_time = formatter.format(date);

        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        String current_date = formatter1.format(date);
        pertanyaan = Utilities.createRandomNumberBetween(0, 9);
        int total = pertanyaan + 4;

        Boolean isFatigueTime = false;

        for (int i = 0; i < recordsData.size(); i++) {

            if (recordsData.get(i).getTime().equals(current_time)) {

                System.out.println("current time " + current_time + " time fatigue : " + recordsData.get(i).getTime());

                isFatigueTime = true;
                if (recordsData.get(i).getStatus().equals("belum")) {
                    if (recordsData.get(i).getLevel() < ConstantValues.SCREEN_CONFIG_FATIGUE_MAX_LEVEL) {
                        inisialisasiFatigue(pertanyaan, total, stackPane, i, callback, cbTrue);
                        if (callback != null) {
                            callback.run();
                        }
                    }

                }
                recordsData.set(i, new FatigueModel(recordsData.get(i).getTime(), current_date,
                        recordsData.get(i).getLevel(), "running", recordsData.get(i).getCount(), MainApp.user.getId()));

            } else {

                // System.out.println(
                // "NOT SHOW, current time " + current_time + " time fatigue : " +
                // recordsData.get(i).getTime());

            }
        }

        if (!isFatigueTime) {
            // set titme fatigue
            Utilities.setupFatigueTime();
        }

    }

    public void showMessage(String msg, StackPane stackPane) {
        pesanAlert(msg, stackPane);
    }

    public void closeAlertDialog() {
        if (dialog != null) {
            dialog.close();
        }
    }

    public static void closeFatgiueDialog() {
        // if (fatigueDialog != null) {
        // fatigueDialog.close();
        // }
        for (JFXDialog dlg : FatigueUI.fatigueDialog) {
            dlg.close();
        }
        FatigueUI.fatigueDialog.clear();
    }

    public void inisialisasiFatigue(int pertanyaan, int option, StackPane stackPane, int index, Runnable callback,
            Runnable cbTrue) {
        closeFatgiueDialog();

        System.out.print(">>>>>>>>>>>>>>>>>>>>>>>>>> level");
        System.out.println(MainApp.fatigueLevel);

        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text(
                "FATIGUE QUESTION " + recordsData.get(index).getTime() + " Level " + MainApp.fatigueLevel.toString()));
        Text body = new Text(String.valueOf(pertanyaan));
        body.setFont(Font.font("Verdana", 30));
        content.setBody(body);

        JFXDialog fatigueDialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        FatigueUI.fatigueDialog.add(fatigueDialog);

        int posisi = Utilities.createRandomNumberBetween(0, confirm.length - 1);
        confirm[posisi] = new JFXButton(String.valueOf(pertanyaan));

        for (int i = 0; i < confirm.length; i++) {
            if (i != posisi) {
                int pertanyaan3 = Utilities.createRandomNumberBetween(pertanyaan + 1, option);
                confirm[i] = new JFXButton(String.valueOf(pertanyaan3));
            }
        }

        HBox hBox = new HBox(5);
        for (JFXButton b : confirm) {
            b.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
            b.setPrefHeight(30.0);
            b.setPrefWidth(100.0);
            b.getStyleClass().add("button-primary");

            b.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    try {
                        int level = recordsData.get(index).getLevel() + 1;
                        int count = recordsData.get(index).getCount() + 1;
                        String status = recordsData.get(index).getStatus();

                        if (pertanyaan == Integer.valueOf(b.getText().toString())) {
                            level = recordsData.get(index).getLevel();
                            status = "sudah";
                            if (cbTrue != null) {
                                cbTrue.run();
                            }
                        } else {
                            MainApp.fatigueLevel++;
                            level += 1;
                            if (level < ConstantValues.SCREEN_CONFIG_FATIGUE_MAX_LEVEL) {
                                status = "lanjut";
                                inisialisasiFatigue(pertanyaan, option, stackPane, index, callback, cbTrue);
                            } else {
                                status = "sudah";
                            }
                        }

                        fatigueDialog.close();

                    } catch (Exception e) {
                        e.printStackTrace();
                        fatigueDialog.close();
                    }
                }
            });

            hBox.getChildren().add(b);
        }

        content.setActions(hBox);
        fatigueDialog.setOverlayClose(false);

        if (MainApp.fatigueLevel >= ConstantValues.SCREEN_CONFIG_FATIGUE_MAX_LEVEL) {
            fatigueDialog.close();
            String pesan = "Level 3 Anda Harus Waspada";
            pesanAlert(pesan, stackPane);
        } else {
            fatigueDialog.show();
        }
        if (callback != null) {
            callback.run();
        }
    }

    public void pesanAlert(String pesan, StackPane stackPane) {

        JFXDialogLayout content = new JFXDialogLayout();

        content.setHeading(new Text("HASIL JAWABAN"));
        Text body = new Text(pesan);
        body.setFont(Font.font("Verdana", 30));
        content.setBody(body);
        dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER, false);

        JFXButton confirm = new JFXButton("Keluar");
        confirm.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        confirm.setPrefHeight(30.0);
        confirm.setPrefWidth(100.0);
        confirm.getStyleClass().add("button-primary");

        confirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });

        content.setActions(confirm);
        dialog.show();

    }

}
