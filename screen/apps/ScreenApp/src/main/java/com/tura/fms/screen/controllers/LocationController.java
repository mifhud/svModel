package com.tura.fms.screen.controllers;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.*;
import com.lynden.gmapsfx.service.directions.*;
import com.lynden.gmapsfx.util.MarkerImageFactory;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.SessionFMS;
import de.westnordost.osmapi.OsmConnection;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static org.javalite.app_config.AppConfig.p;

public class LocationController implements Initializable, MapComponentInitializedListener, DirectionsServiceCallback {

    @FXML
    private Button menu;
    private NavigationUI navigationUI = new NavigationUI();

    @FXML
    private Text TitleBar;

    // final MapView map = new MapView();
    protected DirectionsService directionsService;
    protected DirectionsPane directionsPane;

    protected StringProperty from = new SimpleStringProperty();
    protected StringProperty to = new SimpleStringProperty();
    protected DirectionsRenderer directionsRenderer = null;

    @FXML
    protected GoogleMapView mapView;

    GoogleMap map;
    MarkerOptions markerOptions;
    Marker marker;

    @FXML
    protected TextField fromTextField;

    @FXML
    protected TextField toTextField;
    // private Map map;

    // SessionFMS sessionFMS;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        TitleBar.setText(p("app.titleLocation"));

        // sessionFMS = new SessionFMS();
        // sessionFMS.touchFms();
        // System.out.println("ID :"+sessionFMS.getSessionFms("id"));

        drawerAction();

        mapView.addMapInializedListener(this);
        mapView.setKey("AIzaSyDMbDRsw5E8XlhDbc1j7Uziitc27Jz29Qk");

        to.bindBidirectional(toTextField.textProperty());
        from.bindBidirectional(fromTextField.textProperty());

        OsmConnection osm = new OsmConnection("http://159.65.0.218:8080", "my user agent", null);

    }

    private void drawerAction() {

        menu.setOnAction((ActionEvent evt) -> {
            try {

                navigationUI.windowsTab(p("app.linkDashboard"), p("app.titleDashboard"), evt);

            } catch (Exception e) {

            }
        });

    }

    @FXML
    private void toTextFieldAction(ActionEvent event) {
        // DirectionsRequest request = new DirectionsRequest(from.get(), to.get(),
        // TravelModes.DRIVING);
        // directionsRenderer = new DirectionsRenderer(true, mapView.getMap(),
        // directionsPane);
        // directionsService.getRoute(request, this, directionsRenderer);
    }

    @FXML
    private void clearDirections(ActionEvent event) {
        directionsRenderer.clearDirections();
    }

    @Override
    public void directionsReceived(DirectionsResult results, DirectionStatus status) {
    }

    @Override
    public void mapInitialized() {
        MapOptions options = new MapOptions();

        /*
         * options.center(new LatLong(-2.9547949, 104.6929232)) .zoomControl(true)
         * .zoom(12) .overviewMapControl(false) .mapType(MapTypeIdEnum.ROADMAP);
         *
         * map = mapView.createMap(options);
         *
         * // directionsService = new DirectionsService(); /// directionsPane =
         * mapView.getDirec();
         *
         * markerOptions = new MarkerOptions(); // String iconstr =
         * MarkerImageFactory.createMarkerImage(baseDir()+"images/truck.png", "png"); //
         * System.out.println("ieu icon : "+iconstr); markerOptions.position( new
         * LatLong(-2.9549663, 104.6929235) ) .visible(Boolean.TRUE) // .icon(iconstr)
         * .title("Lokasi");
         *
         * marker = new Marker( markerOptions );
         *
         * map.addMarker(marker);
         *
         * InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
         * infoWindowOptions.content("<h2>Truck 01</h2>" + "Driver : Ahlan Wasahlan<br>"
         * + "Operator : Dahlan" );
         *
         * InfoWindow fredWilkeInfoWindow = new InfoWindow(infoWindowOptions);
         * fredWilkeInfoWindow.open(map, marker);
         *
         * LatLong ok = new LatLong(47.6597, -122.3357);
         *
         *
         *
         *
         * /* var bandung = new google.maps.LatLng(-6.917464,107.619123);        var
         * lingkaran = new google.maps.Circle({        center: bandung,        radius:
         * 50000,        strokeColor: "#0000F7",       strokeOpacity: 0.9,       
         * strokeWeight: 1,        fillColor: "#0000F7",        fillOpacity: 0.2     
         * }); lingkaran.setMap(map);
         */

        LatLong joeSmithLocation = new LatLong(47.6197, -122.3231);
        LatLong joshAndersonLocation = new LatLong(47.6297, -122.3431);
        LatLong bobUnderwoodLocation = new LatLong(47.6397, -122.3031);
        LatLong tomChoiceLocation = new LatLong(47.6497, -122.3325);
        LatLong fredWilkieLocation = new LatLong(47.6597, -122.3357);

        // Set the initial properties of the map.
        MapOptions mapOptions = new MapOptions();

        mapOptions.center(new LatLong(47.6097, -122.3331))
                // .mapType(MapType.ROADMAP)
                .overviewMapControl(false).panControl(false).rotateControl(false).scaleControl(false)
                .streetViewControl(false).zoomControl(false).zoom(12);

        map = mapView.createMap(mapOptions);
        // map.addMapShape();

        // Add markers to the map
        MarkerOptions markerOptions1 = new MarkerOptions();
        markerOptions1.position(joeSmithLocation);

        MarkerOptions markerOptions2 = new MarkerOptions();
        markerOptions2.position(joshAndersonLocation);

        MarkerOptions markerOptions3 = new MarkerOptions();
        markerOptions3.position(bobUnderwoodLocation);

        MarkerOptions markerOptions4 = new MarkerOptions();
        markerOptions4.position(tomChoiceLocation);

        MarkerOptions markerOptions5 = new MarkerOptions();
        markerOptions5.position(fredWilkieLocation);

        Marker joeSmithMarker = new Marker(markerOptions1);
        Marker joshAndersonMarker = new Marker(markerOptions2);
        Marker bobUnderwoodMarker = new Marker(markerOptions3);
        Marker tomChoiceMarker = new Marker(markerOptions4);
        Marker fredWilkieMarker = new Marker(markerOptions5);

        map.addMarker(joeSmithMarker);
        map.addMarker(joshAndersonMarker);
        map.addMarker(bobUnderwoodMarker);
        map.addMarker(tomChoiceMarker);
        map.addMarker(fredWilkieMarker);

        InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
        infoWindowOptions.content("<h2>Operator</h2>" + "Current Location: Sumetera Pelembang<br>" + "ETA: 45 minutes");

        InfoWindow fredWilkeInfoWindow = new InfoWindow(infoWindowOptions);
        fredWilkeInfoWindow.open(map, fredWilkieMarker);

    }

    private String baseDir() {
        String baseDir = "";
        try {
            baseDir = new File(".").getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        baseDir = baseDir.replace('\\', '/');
        baseDir = "file:///" + baseDir + "/src/";
        return baseDir;
    }

    private String getIcon(String relativeFileLocation) {
        String path = System.getProperty("user.dir") + relativeFileLocation;
        path = path.replace("\\", "/");
        path = "file:///" + path.replace(" ", "%20");
        String imgpath = MarkerImageFactory.createMarkerImage(path, "png");
        imgpath = imgpath.replace("(", "");
        imgpath = imgpath.replace(")", "");

        return imgpath;
    }

}
