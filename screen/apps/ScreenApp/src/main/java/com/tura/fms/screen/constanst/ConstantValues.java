package com.tura.fms.screen.constanst;

public class ConstantValues {

    // public static final String BASE_URL = "http://159.65.0.218:4001/";
    public static final String BASE_URL = "https://minefleet.tura.tech/";

    public static boolean IS_USER_LOGGED_IN;
    public static int SESSIONTIMEOUT = 1000000;
    public static int LASTSESSION = 0;
    public static String broker_server = "tcp://iot.eclipse.org:1883";
    public static String socket_io_server = "ws://192.168.100.67:8080/";
    public static int qos_client = 2;
    public static String sample_token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJlbWFpbCI6ImFkbWluQG1haWwuY29tIiwibmFtZSI6IkFkbWluIiwiY2Rfcm9sZSI6eyJjZCI6IjAwMzAwMSIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJBZG1pbiIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJjZF9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sInN0YWZmX2lkIjoiMTAwMDAwMDAiLCJmaW5nZXJwcmludF9pZCI6bnVsbCwic29ja2V0X2lkIjpudWxsLCJmaWxlX3BpY3R1cmUiOiJhc3NldHMvaW1hZ2VzL2F2YXRhcnMvdXNlci5qcGciLCJlbnVtX2F2YWlsYWJpbGl0eSI6NCwiaXNfYXV0aGVudGljYXRlZCI6MSwiX29yZyI6eyJpZCI6IjEiLCJuYW1lIjoiUFQgU2F0cmlhIEJhaGFuYSBTYXJhbmEiLCJhYmJyIjoiU0JTIiwianNvbiI6bnVsbH0sIl9yb2xlIjp7ImNkIjoiMDAzMDAxIiwiY2Rfc2VsZiI6IjAwMyIsIm5hbWUiOiJBZG1pbmlzdHJhdG9yIiwianNvbiI6eyJjZCI6IjAwMzAwMSIsInNlcSI6bnVsbCwibmFtZSI6IkFkbWluIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwMyIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sIl9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sImlhdCI6MTU0ODE1MjM4MSwiZXhwIjoxODYzNTEyMzgxfQ.A890UWWgAvQBQUk_JE3waKC7bu9IkUfN3eF7zhBaNqY";

    public static final String PLAN_GROUP_LEADER = "planGroupLeader";
    public static final String PLAN_GROUP_LEADER_NAME = "planGroupLeaderName";
    public static final String PLAN_SUPERVISOR = "planSupervisor";
    public static final String PLAN_SUPERVISOR_NAME = "planSupervisorName";
    public static final String PLAN_MATERIAL = "planMaterial";
    public static final String PLAN_MATERIAL_NAME = "planMaterialName";
    public static final String PLAN_OPERATION = "planOperation";
    public static final String PLAN_OPERATION_NAME = "planOperationName";
    public static final String PLAN_LOADER = "planLoader";
    public static final String PLAN_LOADER_NAME = "planLoaderName";
    public static final String PLAN_HAULER = "planHauler";
    public static final String PLAN_HAULER_NAME = "planHaulerName";
    public static final String PLAN_LOADER_OPERATOR = "planLoaderOperator";
    public static final String PLAN_LOADER_OPERATOR_NAME = "planLoaderOperatorName";
    public static final String PLAN_HAULER_OPERATOR = "planHaulerOperator";
    public static final String PLAN_HAULER_OPERATOR_NAME = "planHaulerOperatorName";
    public static final String PLAN_LOCATION = "planLocation";
    public static final String PLAN_LOCATION_NAME = "planLocationName";
    public static final String PLAN_DESTINATION = "planDestination";
    public static final String PLAN_DESTINATION_NAME = "planDestinationName";

    public static final String EQUIPMENT_HAULER = "015001";
    public static final String EQUIPMENT_LOADER = "015002";
    public static final String EQUIPMENT_GRADDER = "015003";
    public static final String EQUIPMENT_DOZZER = "015004";
    public static final String EQUIPMENT_FUEL_TANK = "015005";
    public static final String EQUIPMENT_FUEL_TRUCK = "015006";

    public static final String SHIFT_DAY = "007001";
    public static final String SHIFT_NIGHT = "007002";

    public static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd";

    public static final String DATA_USER = "user";
    public static final String DATA_ACTUAL = "actual";
    public static final String DATA_ACTUAL_SUPPORT = "actualSupport";
    public static final String DATA_EQUIPMENT = "equipment";
    public static final String DATA_EQUIPMENT_CATEGORY = "equipmentCategory";
    public static final String DATA_CONSTANT = "constant";
    public static final String DATA_LOCATION = "location";
    public static final String DATA_OPERATION = "operation";
    public static final String DATA_OPERATION_LOG_DETAIL = "operationLogDetail";
    public static final String DATA_MATERIAL = "material";
    public static final String DATA_FATIGUE_LOG = "fatigueLog";
    public static final String DATA_GPS = "gps";
    public static final String DATA_HM = "hm";
    public static final String DATA_LOG_HISTORY = "logHistory";
    public static final String DATA_SCREEN_CONFIG = "screenConfig";
    public static final String DATA_DEVICE = "device";
    public static final String DATA_OPERATION_STATUS = "operationStatus";
    public static final String DATA_SAFETY_STATUS = "safetyStatus";
    public static final String DATA_PRESTART = "prestart";
    public static final String DATA_MAINTENANCE_TYPE = "maintenanceType";
    public static final String DATA_MAINTENANCE_STATUS = "maintenanceStatus";
    public static final String DATA_MAINTENANCE_OPERATION = "maintenanceOperation";
    public static final String DATA_MAINTENANCE_SCHEDULE = "maintenanceSchedule";
    public static final String DATA_MAINTENANCE_UNSCHEDULE = "maintenanceUnschedule";
    public static final String DATA_DASHBOARD = "dashboard";
    public static final String DATA_BANK = "bank";
    public static final String DATA_EQUIPMENT_LOG_START = "equipmentLogStart";

    public static final String SYNC_STATE_DOWNLOAD = "download";
    public static final String SYNC_STATE_UPLOAD = "upload";

    public static final String STATUS_STATE_START = "017001";
    public static final String STATUS_STATE_STOP = "017002";

    public static final String NOTIF_TYPE_SOUND = "sound";
    public static final String NOTIF_TYPE_STRING = "string";

    public static final String CYCLE_TYPE_LOADER = "loader";
    public static final String CYCLE_TYPE_HAULER = "hauler";

    public static final String CYCLE_ARRIVE_LOADING = "arriveLoading";
    public static final String CYCLE_FIRST_BUCKET = "firstBucket";
    public static final String CYCLE_FULL_BUCKET = "fullBucket";
    public static final String CYCLE_ARRIVE_DUMPING = "arriveDumping";
    public static final String CYCLE_START_DUMP = "startDump";
    public static final String CYCLE_FINISH_DUMP = "finishDump";

    public static final String TIME_CYCLE_ARRIVE_LOADING = "021001";
    public static final String TIME_CYCLE_FIRST_BUCKET = "021002";
    public static final String TIME_CYCLE_FULL_BUCKET = "021003";
    public static final String TIME_CYCLE_ARRIVE_DUMPING = "021004";
    public static final String TIME_CYCLE_START_DUMP = "021005";
    public static final String TIME_CYCLE_FINISH_DUMP = "021006";

    public static final String ACTIVITY_CHANGE_SHIFT = "SB08";
    public static final String ACTIVITY_PRESTART = "SB01";
    public static final String ACTIVITY_HAULING = "A107";
    public static final String ACTIVITY_LOADING = "A109";
    public static final String ACTIVITY_GENERAL = "G105";

    public static final String TRAVEL_EMPTY = "empty";
    public static final String TRAVEL_FULL = "full";

    public static final String KAFKA_DESTINATION_ALL = "all";

    public static final String TUM_WORKING = "TUM1";
    public static final String TUM_DELAY = "TUM2";
    public static final String TUM_IDLE = "TUM3";
    public static final String TUM_DOWN = "TUM4";
    public static final String TUM_RFU = "TUM5";
    public static final String TUM_STANDBY = "TUM6";
    public static final String TUM_ASSIGNED = "TUM7";
    public static final String TUM_MAINTENANCE = "TUM8";

    public static final String SCREEN_CONFIG_FATIGUE_TIME = "fatigue-time";
    public static final String SCREEN_CONFIG_FATIGUE_TIMEOUT = "fatigue-timeout";
    public static final String SCREEN_CONFIG_GEOFENCING_RADIUS = "geofencing-radius-meter";
    public static final Integer SCREEN_CONFIG_FATIGUE_MAX_LEVEL = 3;
    public static final Integer SCREEN_CONFIG_FATIGUE_DEFAULT_TIMEOUT = 30;

    public static final String KAFKA_CMD_SYNC = "sync";
    public static final String KAFKA_CMD_LOGOUT = "logout";
    public static final String KAFKA_CMD_DATA_CHANGE = "data-change";

    public static final String KAFKA_CMD_DATA_CHANGE_HAULER_LOADER = "hauler-change-loader";
    public static final String KAFKA_CMD_DATA_CHANGE_HAULER_LOCATION = "hauler-change-location";
    public static final String KAFKA_CMD_DATA_CHANGE_HAULER_DESTINATION = "hauler-change-destination";
    public static final String KAFKA_CMD_DATA_CHANGE_HAULER_GROUPLEADER = "hauler-change-groupleader";
    public static final String KAFKA_CMD_DATA_CHANGE_HAULER_SUPERVISOR = "hauler-change-supervisor";
    public static final String KAFKA_CMD_DATA_CHANGE_HAULER_MATERIAL = "hauler-change-material";
    public static final String KAFKA_CMD_DATA_CHANGE_HAULER_ACTIVITY = "hauler-change-activity";
    public static final String KAFKA_CMD_DATA_CHANGE_HAULER_OPERATOR = "hauler-change-operator";

    public static final String KAFKA_CMD_DATA_CHANGE_LOADER_LOCATION = "loader-change-location";
    public static final String KAFKA_CMD_DATA_CHANGE_LOADER_DESTINATION = "loader-change-destination";
    public static final String KAFKA_CMD_DATA_CHANGE_LOADER_GROUPLEADER = "loader-change-groupleader";
    public static final String KAFKA_CMD_DATA_CHANGE_LOADER_SUPERVISOR = "loader-change-supervisor";
    public static final String KAFKA_CMD_DATA_CHANGE_LOADER_MATERIAL = "loader-change-material";
    public static final String KAFKA_CMD_DATA_CHANGE_LOADER_ACTIVITY = "loader-change-activity";
    public static final String KAFKA_CMD_DATA_CHANGE_LOADER_OPERATOR = "loader-change-operator";

    public static final String KAFKA_CMD_DATA_CHANGE_SUPPORT_LOCATION = "support-change-location";
    public static final String KAFKA_CMD_DATA_CHANGE_SUPPORT_DESTINATION = "support-change-destination";
    public static final String KAFKA_CMD_DATA_CHANGE_SUPPORT_GROUPLEADER = "support-change-groupleader";
    public static final String KAFKA_CMD_DATA_CHANGE_SUPPORT_SUPERVISOR = "support-change-supervisor";
    public static final String KAFKA_CMD_DATA_CHANGE_SUPPORT_MATERIAL = "support-change-material";
    public static final String KAFKA_CMD_DATA_CHANGE_SUPPORT_ACTIVITY = "support-change-activity";
    public static final String KAFKA_CMD_DATA_CHANGE_SUPPORT_OPERATOR = "support-change-operator";

    public static final String KAFKA_TYPE_OPERATION_ACTUAL = "operation-actual";
    public static final String KAFKA_TYPE_SCREEN_CONFIG = "screen-config";

    public static final String BREAKDOWN_WAITING_MECHANIC = "B3";

    public static final String ROLE_ADMINISTRATOR = "003001";
    public static final String ROLE_DIRECTOR = "003002";
    public static final String ROLE_MANAGER = "003003";
    public static final String ROLE_SUPERVISOR = "003004";
    public static final String ROLE_CCR = "003005";
    public static final String ROLE_CHECKER = "003006";
    public static final String ROLE_OPERATOR = "003007";
    public static final String ROLE_MECHANIC = "003008";
    public static final String ROLE_GROUP_LEADER = "003009";

    public static final String MAINTENANCE_SCHEDULE = "schedule";
    public static final String MAINTENANCE_UNSCHEDULE = "unschedule";
    public static final String MAINTENANCE_BACKLOG = "backlog";

    public static final String MAINTENANCE_STATUS_NOT = "not";
    public static final String MAINTENANCE_STATUS_PROCESS = "process";
    public static final String MAINTENANCE_STATUS_DONE = "done";

    public static final String MECHANIC_MENU_TASK = "unschedule";
    public static final String MECHANIC_MENU_SCHEDULE = "schedule";
    public static final String MECHANIC_MENU_BACKLOG = "backlog";

    public static final String COLOR_DOWN = "#d32f2f";
    public static final String COLOR_MAINTENANCE = "#fbc02d";
    public static final String COLOR_RFU = "#a5d6a7";
    public static final String COLOR_STANDBY = "#388e3c";
    public static final String COLOR_ASSIGNED = "#90caf9";
    public static final String COLOR_WORKING = "#1976d2";
    public static final String COLOR_DELAY = "#fc6f03";
    public static final String COLOR_IDLE = "#a37343";

    public static final String KAFKA_DATA_STATE_REQUEST = "request";
    public static final String KAFKA_DATA_STATE_RESPONSE = "response";

    public static final String KAFKA_DATA_TYPE_PAYLOAD = "payload";
    public static final String KAFKA_DATA_TYPE_HM = "hourMeter";
    public static final String KAFKA_DATA_TYPE_TIMBANGAN = "timbangan";
    public static final String KAFKA_DATA_TYPE_SYNC = "sinkronisasi";

    public static final String MATERIAL_OB = "Overburden";
    public static final String MATERIAL_COAL = "Coal";
    public static final String MATERIAL_MUD = "Mud";

    public static final String TABLE_SCREEN_CONFIG = "screenConfig";
    public static final String TABLE_USER = "user";
    public static final String TABLE_EQUIPMENT_CATEGORY = "equipmentCategory";
    public static final String TABLE_EQUIPMENT = "equipment";
    public static final String TABLE_LOCATION = "location";
    public static final String TABLE_CONSTANT = "constant";
    public static final String TABLE_MATERIAL = "material";
    public static final String TABLE_PRESTART = "prestart";
    public static final String TABLE_GPS = "gps";
    public static final String TABLE_HM = "ecu";
    public static final String TABLE_DEVICE_STATUS = "deviceStatus";
    public static final String TABLE_OPERATION_STATUS = "operationStatus";
    public static final String TABLE_SAFETY_STATUS = "safetyStatus";
    public static final String TABLE_OPERATION = "operation";
    public static final String TABLE_PRESTART_LOG = "prestartLog";
    public static final String TABLE_FATIGUE_LOG = "fatigueLog";
    public static final String TABLE_MAINTENANCE_SCHEDULE = "maintenanceSchedule";
    public static final String TABLE_MAINTENANCE_UNSCHEDULE = "maintenanceUnschedule";
    public static final String TABLE_OPERATION_LOG_DETAIL = "operationLogDetail";

    public static final String AUTH_STATE_LOGIN = "login";
    public static final String AUTH_STATE_LOGOUT = "logout";

    public static final String PROBLEM_OTHER = "M36";

    public static final String HAULER_STATE_QUEUE_AT_LOADING = "queueAtLoading";
    public static final String HAULER_STATE_SPOTING_AT_LOADING = "spotingAtLoading";
    public static final String HAULER_STATE_LOADING = "loading";
    public static final String HAULER_STATE_FULL_TRAVEL = "fullTravel";
    public static final String HAULER_STATE_SPOTING_AT_DUMP = "spotingAtDump";
    public static final String HAULER_STATE_START_DUMP = "startDump";
    public static final String HAULER_STATE_FINISH_DUMP = "finishDump";
    public static final String HAULER_STATE_EMPTY_TRAVEL = "emptyTravel";

    public static final String PROCESSING = "processing";
    public static final String DONE = "done";
}