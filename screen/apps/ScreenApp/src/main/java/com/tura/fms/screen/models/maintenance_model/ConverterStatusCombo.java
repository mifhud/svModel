package com.tura.fms.screen.models.maintenance_model;

import javafx.util.StringConverter;

public class ConverterStatusCombo extends StringConverter<MaintenanceStatus> {

    // Method to convert a Person-Object to a String
    @Override
    public String toString(MaintenanceStatus person)
    {
        return person == null? null : person.getName();
    }


    // Method to convert a String to a Person-Object
    @Override
    public MaintenanceStatus fromString(String string)
    {
        MaintenanceStatus person = null;

        if (string == null)
        {
            return person;
        }

        int commaIndex = string.indexOf(",");

        if (commaIndex == -1)
        {
            person = new MaintenanceStatus(string, null);
        }
        else
        {
            String id = string.substring(commaIndex + 2);
            String name = string.substring(0, commaIndex);
            person = new MaintenanceStatus(id, name);
        }

        return person;
    }
}
