package com.tura.fms.screen.models.log_history;

public class LogHistory {

    private String id;
    private Long local_date;
    private String actual_id;
    private String actual_support_id;
    private String equipment_id;
    private String user_id;
    private String message;
    private Integer is_deleted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getLocalDate() {
        return local_date;
    }

    public void setLocalDate(Long localdate) {
        this.local_date = localdate;
    }

    public String getActualId() {
        return actual_id;
    }

    public void setActualId(String actualId) {
        this.actual_id = actualId;
    }

    public String getActualSupportId() {
        return actual_support_id;
    }

    public void setActualSupportId(String actualSupportId) {
        this.actual_support_id = actualSupportId;
    }

    public String getEquipmentId() {
        return equipment_id;
    }

    public void setEquipmentId(String equipmentId) {
        this.equipment_id = equipmentId;
    }

    public String getUserId() {
        return user_id;
    }

    public void setUserId(String userid) {
        this.user_id = userid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getIsDeleted() {
        return is_deleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.is_deleted = isDeleted;
    }

}
