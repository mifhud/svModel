package com.tura.fms.screen.models.fatigue_model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.database.SQLiteJDBC;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

public class FatigueLogDB extends Operation {

    public static final String TABLE_NAME = "FatigueLog_Record";

    private static final String id = "id";
    private static final String id_equipment = "id_equipment";
    private static final String id_user = "id_user";
    private static final String level = "level";
    private static final String id_location = "id_location";
    private static final String date = "date_submitted";
    private static final String id_actual = "id_actual";
    private static final String id_actual_support = "id_actual_support";
    private static final String id_actual_loader = "id_actual_loader";
    private static final String sync_status = "sync_status";

    public FatigueLogDB() {
        super(FatigueLogDB.TABLE_NAME, ConstantValues.DATA_FATIGUE_LOG);
    }

    public void createTableFatigueLog() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table fatigue exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table fatigue created");
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + id_equipment
                + " TEXT NULL," + id_user + " TEXT NULL," + level + " NUMBER NULL," + id_location + " TEXT NULL," + date
                + " NUMBER NULL," + id_actual + " TEXT NULL," + id_actual_support + " TEXT NULL," + id_actual_loader
                + " TEXT NULL," + sync_status + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        FatigueLogLocal eq = (FatigueLogLocal) eqa;
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + id_equipment + "," + id_user + "," + level
                    + "," + id_location + "," + date + "," + id_actual + "," + id_actual_support + ","
                    + id_actual_loader + "," + sync_status + ")" + " VALUES(?,?,?,?,?,?,?,?,?,0) ON CONFLICT(" + id
                    + ") DO UPDATE SET " + id + "=?," + id_equipment + "=?," + id_user + "=?," + level + "=?,"
                    + id_location + "=?," + date + "=?," + id_actual + "=?," + id_actual_support + "=?,"
                    + id_actual_loader + "=?;";

            st = conn().prepareStatement(sql);
            final String uuid = UUID.randomUUID().toString().replace("-", "");
            // st.setString(1, eq.getId());
            st.setString(1, uuid);
            st.setString(10, uuid);
            st.setString(2, eq.getIdEquipment());
            st.setString(11, eq.getIdEquipment());
            st.setString(3, eq.getIdUser());
            st.setString(12, eq.getIdUser());
            st.setInt(4, eq.getLevel());
            st.setInt(13, eq.getLevel());
            st.setString(5, eq.getIdLocation());
            st.setString(14, eq.getIdLocation());
            st.setString(6, eq.getDate());
            st.setString(15, eq.getDate());
            st.setString(7, eq.getIdActual());
            st.setString(16, eq.getIdActual());
            st.setString(8, eq.getIdActualSupport());
            st.setString(17, eq.getIdActualSupport());
            st.setString(9, eq.getIdActualLoader());
            st.setString(18, eq.getIdActualLoader());

            rs = st.execute();
            st.close();

            return this.getLocalOneById(id, uuid);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

    @Override
    public Object getLocalOneById(String fieldId, String searchId) {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + fieldId + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, searchId);
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                FatigueLogLocal eq = new FatigueLogLocal();
                eq.setId(cursor.getString(id));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdUser(cursor.getString(id_user));
                eq.setDate(cursor.getString(date));
                eq.setLevel(cursor.getInt(level));
                eq.setIdLocation(cursor.getString(id_location));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdActualSupport(cursor.getString(id_actual_support));
                eq.setIdActualLoader(cursor.getString(id_actual_loader));

                cursor.close();

                return eq;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_NAME + " where id=?";
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        // no action
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        // no action
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                FatigueLogLocal eq = new FatigueLogLocal();
                eq.setId(cursor.getString(id));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdUser(cursor.getString(id_user));
                eq.setDate(cursor.getString(date));
                eq.setLevel(cursor.getInt(level));
                eq.setIdLocation(cursor.getString(id_location));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdActualSupport(cursor.getString(id_actual_support));
                eq.setIdActualLoader(cursor.getString(id_actual_loader));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getFlaggedDataOne() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=? AND " + id_equipment + "=? LIMIT 1";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            String eqId = MainApp.equipmentLocal != null ? MainApp.equipmentLocal.getId() : "";
            preparedStatement.setString(2, eqId);
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                FatigueLogLocal eq = new FatigueLogLocal();
                eq.setId(cursor.getString(id));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdUser(cursor.getString(id_user));
                eq.setDate(cursor.getString(date));
                eq.setLevel(cursor.getInt(level));
                eq.setIdLocation(cursor.getString(id_location));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdActualSupport(cursor.getString(id_actual_support));
                eq.setIdActualLoader(cursor.getString(id_actual_loader));

                cursor.close();
                return eq;

            } else {

                cursor.close();
                return null;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        Call<ResponseModel> call = APIClient.getInstance().postFatigueLog(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            System.out.println(response);
            System.out.println(response.body().getResponse());
            System.out.println(response.body().getMessage());
            if (response.body().getResponse().equals("success")) {
                System.out.println("post success");
                /**
                 * change flag (sync_status to 1)
                 */
                for (Object obj : eq) {
                    FatigueLogLocal oqe = (FatigueLogLocal) obj;
                    setSynchronized(oqe.getId());
                }
            }
        } catch (IOException ex) {

            ex.printStackTrace();
        }
    }

    @Override
    public void postData(Object eq, Runnable cb) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }

        Call<ResponseModel> call = APIClient.getInstance().postOfflineFatigueLog(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            if (response.body() != null) {
                if (response.body().getResponse() != null) {
                    if (response.body().getResponse().equals("success")) {
                        System.out.println("post success");

                        if (cb != null) {
                            cb.run();
                        }
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where id=?";
        // deleteData(id);
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
