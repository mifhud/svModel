package com.tura.fms.screen.models.maintenance_model.task_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenanceDescriptionPost {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("hm")
    @Expose
    private Double hm;

    @SerializedName("wo_number")
    @Expose
    private String wo_number;

    @SerializedName("maintenance_id")
    @Expose
    private String maintenance_id;

    @SerializedName("cd_shift")
    @Expose
    private String cd_shift;

    @SerializedName("created_by")
    @Expose
    private String created_by;

    @SerializedName("supervisor_id")
    @Expose
    private String supervisor_id;

    @SerializedName("foreman_id")
    @Expose
    private String foreman_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String dt) {
        this.date = dt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String desc) {
        this.description = desc;
    }

    public Double getHm() {
        return hm;
    }

    public void setHm(Double hm) {
        this.hm = hm;
    }

    public String getWo_number() {
        return wo_number;
    }

    public void setWo_number(String wo_number) {
        this.wo_number = wo_number;
    }

    public String getMaintenanceId() {
        return maintenance_id;
    }

    public void setMaintenanceId(String mainte) {
        this.maintenance_id = mainte;
    }

    public String getCdShift() {
        return cd_shift;
    }

    public void setCdShift(String shift) {
        this.cd_shift = shift;
    }

    public String getCreatedBy() {
        return created_by;
    }

    public void setCreatedBy(String created) {
        this.created_by = created;
    }

    public String getSupervisorId() {
        return supervisor_id;
    }

    public void setSupervisorId(String sup) {
        this.supervisor_id = sup;
    }

    public String getForemanId() {
        return foreman_id;
    }

    public void setForemanId(String fore) {
        this.foreman_id = fore;
    }

}