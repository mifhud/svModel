package com.tura.fms.screen.models.maintenance_model.task_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Equipment{

	@SerializedName("name")
	@Expose
	private String name;

	@SerializedName("id")
	@Expose
	private String id;

	@SerializedName("__id_org__")
	@Expose
	private String id_org;


	@SerializedName("__has_id_org__")
	@Expose
	private Boolean has_id_org;

	public String getId_org() {
		return id_org;
	}

	public void setId_org(String id_org) {
		this.id_org = id_org;
	}

	public Boolean getHas_id_org() {
		return has_id_org;
	}

	public void setHas_id_org(Boolean has_id_org) {
		this.has_id_org = has_id_org;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}
}
