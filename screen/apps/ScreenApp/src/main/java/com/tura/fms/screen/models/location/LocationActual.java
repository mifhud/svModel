package com.tura.fms.screen.models.location;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "cd_location", "name", "longitude", "latitude", "altitude", "date_submitted",
        "date_modified", "json" })
public class LocationActual {

    @JsonProperty("id")
    private String id;
    @JsonProperty("cd_location")
    private String cdLocation;
    @JsonProperty("name")
    private String name;
    @JsonProperty("longitude")
    private Integer longitude;
    @JsonProperty("latitude")
    private Integer latitude;
    @JsonProperty("altitude")
    private Integer altitude;
    @JsonProperty("date_submitted")
    private Integer dateSubmitted;
    @JsonProperty("date_modified")
    private Integer dateModified;
    @JsonProperty("json")
    private Object json;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("cd_location")
    public String getCdLocation() {
        return cdLocation;
    }

    @JsonProperty("cd_location")
    public void setCdLocation(String cdLocation) {
        this.cdLocation = cdLocation;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("longitude")
    public Integer getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(Integer longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("latitude")
    public Integer getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(Integer latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("altitude")
    public Integer getAltitude() {
        return altitude;
    }

    @JsonProperty("altitude")
    public void setAltitude(Integer altitude) {
        this.altitude = altitude;
    }

    @JsonProperty("date_submitted")
    public Integer getDateSubmitted() {
        return dateSubmitted;
    }

    @JsonProperty("date_submitted")
    public void setDateSubmitted(Integer dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    @JsonProperty("date_modified")
    public Integer getDateModified() {
        return dateModified;
    }

    @JsonProperty("date_modified")
    public void setDateModified(Integer dateModified) {
        this.dateModified = dateModified;
    }

    @JsonProperty("json")
    public Object getJson() {
        return json;
    }

    @JsonProperty("json")
    public void setJson(Object json) {
        this.json = json;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}