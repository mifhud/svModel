package com.tura.fms.screen.database;

import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.network.NetworkConnection;

public class Synchronizer {

    public static Timer timer;

    public static TimerTask synTask;

    public static Synchronizer syn = null;

    public static String syncData = null;

    public static String syncState = null;

    public static Operation syncObject = null;

    public static Boolean syncStatus = false;

    public static Boolean syncRunStatus = false;

    public static ObjectMapper mapper = null;

    public static Long delay = Long.parseLong(MainApp.config("config.synchronizer_delay"));

    public static Long interval;

    public static Synchronizer createSynchronizer() {
        if (Synchronizer.syn == null) {
            Synchronizer.syn = new Synchronizer();
            Synchronizer.mapper = new ObjectMapper();
        }
        return Synchronizer.syn;
    }

    public Synchronizer() {
        Synchronizer.synTask = new TimerTask() {
            @Override
            public void run() {
                System.out.println(" --- synchronizer --- ");
                System.out.println(Synchronizer.syncStatus + " " + MainApp.loggedIn);

                if (NetworkConnection.getServiceStatus()) {
                    if (Synchronizer.syncStatus == false && MainApp.requestHeader != null) {
                        System.out.println(" --- synchronizer check status --- ");
                        // cek status
                        check();

                    } else if (Synchronizer.syncStatus && MainApp.requestHeader != null) {
                        System.out.println(" --- synchronizer is running --- ");
                        System.out.println(" --- " + Synchronizer.syncStatus.toString() + " --- ");

                        // run jika ada yang harus di sinkronisasi
                        runSync(false);

                    } else {
                        System.out.println(" --- synchronizer pass --- ");
                    }
                } else {
                    System.out.println(" --- service down --- ");
                }
            }
        };
        Synchronizer.timer = new Timer();
        interval = Long.parseLong(MainApp.config("config.synchronizer_interval"));
        Synchronizer.timer.schedule(Synchronizer.synTask, 0, interval);
    }

    public static void doSync() {
        System.out.println(" --- synchronizer --- ");
        System.out.println(Synchronizer.syncStatus + " " + MainApp.loggedIn);

        if (NetworkConnection.getServiceStatus()) {
            if (Synchronizer.syncStatus == false && MainApp.requestHeader != null) {
                System.out.println(" --- synchronizer check status --- ");
                // cek status
                check();

                if (Synchronizer.syncStatus) {
                    doSync();
                }

            } else if (Synchronizer.syncStatus && MainApp.requestHeader != null) {
                System.out.println(" --- synchronizer is running --- ");
                System.out.println(" --- " + Synchronizer.syncStatus.toString() + " --- ");
                // run jika ada yang harus di sinkronisasi
                runSync(true);

            } else {
                System.out.println(" --- synchronizer pass --- ");

            }

        } else {
            System.out.println(" --- service down --- ");

        }
    }

    public static Boolean checkState(Operation obj) {
        System.out.println("===== check state " + obj.getClass().getName());

        if (Synchronizer.syncStatus == false) {
            Integer remoteCount = obj.getRemoteCount();
            Integer localCount = obj.localCount();

            /**
             * beberapa data terdapat data dengan id=0 sengaja tidak disend ke client
             * seperti data location
             */
            if (obj.getId().equals(ConstantValues.DATA_LOCATION) || obj.getId().equals(ConstantValues.DATA_CONSTANT)) {
                remoteCount--;
            }

            System.out.println("\t count remote:local -> " + remoteCount + " - " + localCount);
            if (remoteCount > localCount) {
                Synchronizer.syncStatus = true;
                Synchronizer.syncData = obj.getId();
                Synchronizer.syncState = ConstantValues.SYNC_STATE_DOWNLOAD;
                Synchronizer.syncObject = obj;
            }

            /**
             * synchronize upload dipisah, untuk menghindari kompleksitas logic
             */
            // if (Synchronizer.syncStatus == false) {
            // List<Object> flagged = (List<Object>) obj.getFlaggedData();
            // System.out.println("\t flagged " + (flagged == null ? "null" :
            // flagged.size()));
            // if (flagged != null && flagged.size() > 0) {
            // Synchronizer.syncStatus = true;
            // Synchronizer.syncData = obj.getId();
            // Synchronizer.syncState = ConstantValues.SYNC_STATE_UPLOAD;
            // Synchronizer.syncObject = obj;
            // }
            // }
        }

        try {
            Thread.sleep(delay);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return Synchronizer.syncStatus;
    }

    public static void check() {
        System.out.println(" --- synchronizer check --- ");

        /**
         * data-data master, yang berasal dari server, tergantung ke server
         */

        if (checkState(MainApp.userDB)) {
        } else if (checkState(MainApp.equipmentCategorytDB)) {
        } else if (checkState(MainApp.equipmentDB)) {
        } else if (checkState(MainApp.locationDB)) {
        } else if (checkState(MainApp.constantDB)) {
        } else if (checkState(MainApp.materialDB)) {
        } else if (checkState(MainApp.prestartDB)) {
            // } else if (checkState(MainApp.activityDB)) {
            // } else if (checkState(MainApp.actualDB)) {
            // } else if (checkState(MainApp.actualSupportDB)) {
            // } else if (checkState(MainApp.gpsDB)) {
            // } else if (checkState(MainApp.hmDB)) {
            // } else if (checkState(MainApp.deviceStatusDB)) {
            // } else if (checkState(MainApp.operationStatusDB)) {
            // } else if (checkState(MainApp.safetyStatusDB)) {
            // } else if (checkState(MainApp.operationDB)) {
            // } else if (checkState(MainApp.prestartLogDB)) {
            // } else if (checkState(MainApp.fatigueLogDB)) {
            // } else if (checkState(MainApp.maintenanceScheduleDB)) {
            // } else if (checkState(MainApp.maintenanceUnscheduleDB)) {
            // } else if (checkState(MainApp.operationLogDetailDB)) {
        } else {
            // if (Utilities.isLoaderHauler()) {
            // checkState(MainApp.actualDB);
            // } else {
            // checkState(MainApp.actualSupportDB);
            // }
        }
    }

    public static void resetLocalTableForSync(String tbl) {
        if (tbl.equals(ConstantValues.TABLE_USER)) {
            MainApp.userDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_EQUIPMENT_CATEGORY)) {
            MainApp.equipmentCategorytDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_EQUIPMENT)) {
            MainApp.equipmentDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_LOCATION)) {
            MainApp.locationDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_CONSTANT)) {
            MainApp.constantDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_MATERIAL)) {
            MainApp.materialDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_PRESTART)) {
            MainApp.prestartDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_GPS)) {
            MainApp.gpsDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_HM)) {
            MainApp.hmDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_DEVICE_STATUS)) {
            MainApp.deviceStatusDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_OPERATION_STATUS)) {
            MainApp.operationStatusDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_SAFETY_STATUS)) {
            MainApp.safetyStatusDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_OPERATION)) {
            MainApp.operationDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_PRESTART_LOG)) {
            MainApp.prestartLogDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_FATIGUE_LOG)) {
            MainApp.fatigueLogDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_MAINTENANCE_SCHEDULE)) {
            MainApp.maintenanceScheduleDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_MAINTENANCE_UNSCHEDULE)) {
            MainApp.maintenanceUnscheduleDB.triggerSync();
        } else if (tbl.equals(ConstantValues.TABLE_OPERATION_LOG_DETAIL)) {
            MainApp.operationLogDetailDB.triggerSync();
        }
    }

    public static void runSync(Boolean callDoSync) {
        if (Synchronizer.syncRunStatus) {
            return;
        }

        System.out.println(" --- RUN synchronizer --- ");

        if (Synchronizer.syncStatus && Synchronizer.syncData != null && Synchronizer.syncObject != null) {
            Synchronizer.syncRunStatus = true;
            /**
             * synchronize data from server
             */
            if (Synchronizer.syncState == ConstantValues.SYNC_STATE_DOWNLOAD) {

                // set label status di dashboard
                Utilities.setNotificationText("Sinkronisasi data " + Synchronizer.syncData);

                System.out.println(" --- synchronizer DOWNLOAD --- ");
                Integer page = 1;
                Integer size = Integer.parseInt(MainApp.config("config.sync_page_size"));
                Map<Object, Object> fs = null;
                Double currentPage;
                Double totalItems;
                List<Object> output;
                Integer remoteCount;
                Integer localCount;

                // Synchronizer.timer.cancel();

                do {
                    System.out.println(" --- " + Synchronizer.syncData + " --- ");
                    Object obj = Synchronizer.syncObject.getFromServer(page, size);
                    // System.out.println(obj);
                    fs = Synchronizer.mapper.convertValue(obj, Map.class);
                    currentPage = Double.parseDouble(fs.get("currentPage").toString());
                    totalItems = Double.parseDouble(fs.get("totalItems").toString());
                    output = (List<Object>) fs.get("output");
                    System.out.println(" --- " + Synchronizer.syncData + " --- ");
                    // System.out.println(" --- response-page:" + currentPage.toString() + ",
                    // total-items:"
                    // + totalItems.toString() + " ___ " + page.toString() + "|" + output.size());
                    // System.out.println(" --- response-size:" + output.size() + " --- ");
                    for (Object q : output) {
                        Synchronizer.syncObject.insertIgnore(q);
                    }
                    remoteCount = Synchronizer.syncObject.getRemoteCount();

                    // remote count dikurangi
                    // data location ga usah dikurangi
                    // Synchronizer.syncData.equals(ConstantValues.DATA_LOCATION) ||
                    if (Synchronizer.syncData.equals(ConstantValues.DATA_CONSTANT)) {
                        remoteCount--;
                    }

                    localCount = Synchronizer.syncObject.localCount();
                    System.out.println(" --- RUNNING done for --- " + page.toString());
                    System.out.println(
                            "\t count remote:local -> " + remoteCount.toString() + ":" + localCount.toString());

                    page++;
                    try {
                        Thread.sleep(delay);
                    } catch (Exception ex) {
                        // Synchronizer.timer.schedule(Synchronizer.synTask, 0, interval);
                        ex.printStackTrace();
                    }

                    System.out.println("\t conditions -> " + output.size() + "," + size.toString() + " ---- "
                            + localCount.toString() + ", " + remoteCount.toString());
                } while (output.size() >= size && localCount < remoteCount);

                if (localCount >= remoteCount) {
                    Synchronizer.syncStatus = false;
                    Synchronizer.syncData = null;
                    Synchronizer.syncState = null;
                    Synchronizer.syncObject = null;
                }

            } else if (Synchronizer.syncState == ConstantValues.SYNC_STATE_UPLOAD) {
                System.out.println(" --- synchronizer UPLOAD --- ");
                /**
                 * synchronize to server
                 */
                List<Object> offlineData = Synchronizer.syncObject.getFlaggedData();

                Synchronizer.syncObject.postBulkData(offlineData);

                Synchronizer.syncStatus = false;
                Synchronizer.syncData = null;
                Synchronizer.syncState = null;
                Synchronizer.syncObject = null;
            }

            Synchronizer.syncRunStatus = false;
        }

        if (callDoSync) {
            doSync();
        }

    }

}
