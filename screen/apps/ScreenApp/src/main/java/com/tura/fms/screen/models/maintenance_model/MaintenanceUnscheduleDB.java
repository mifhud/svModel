package com.tura.fms.screen.models.maintenance_model;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.database.SQLiteJDBC;

public class MaintenanceUnscheduleDB extends Operation {

    public static final String TABLE_NAME = "MaintenanceUnschedule_Record";

    private static final String id = "id";
    private static final String sync_status = "sync_status";

    public MaintenanceUnscheduleDB() {
        super(MaintenanceUnscheduleDB.TABLE_NAME, ConstantValues.DATA_MAINTENANCE_UNSCHEDULE);
    }

    public void createTableMaintenanceUnschedule() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table maintenance unschedule exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table maintenance unschedule created");
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + sync_status
                + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        return null;
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_NAME + " where id=?";
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        // no action
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        // no action
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        return null;
    }

    @Override
    public void postBulkData(List<Object> eq) {

    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where id=?";
        // deleteData(id);
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
