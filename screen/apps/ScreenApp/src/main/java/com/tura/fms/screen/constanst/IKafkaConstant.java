package com.tura.fms.screen.constanst;

import com.tura.fms.screen.MainApp;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public interface IKafkaConstant {

    public static String KAFKA_BROKERS = MainApp.config("config.kafka_address");

    public static Integer MESSAGE_COUNT = 1000;

    public static String CLIENT_ID = "screen-app-client";

    public static List<String> TOPIC_SUBSCRIBE_HAULER = Collections
            .unmodifiableList(Arrays.asList("cycle", "data", "notification", "command"));

    public static List<String> TOPIC_SUBSCRIBE_LOADER = Collections
            .unmodifiableList(Arrays.asList("cycle", "data", "notification", "command", "distance"));

    public static List<String> TOPIC_SUBSCRIBE_SUPPORT = Collections
            .unmodifiableList(Arrays.asList("data", "notification", "command"));

    public static String TOPIC_SUBSCRIBE_CYCLE = "cycle";

    public static String TOPIC_SUBSCRIBE_DATA = "data";

    public static String TOPIC_SUBSCRIBE_NOTIFICATION = "notification";

    public static String TOPIC_SUBSCRIBE_COMMAND = "command";

    public static String TOPIC_SUBSCRIBE_DISTANCE = "distance";

    public static String TOPIC_PRODUCE_GPS = "gps";

    public static String TOPIC_PRODUCE_DEVICE = "devices";

    public static String TOPIC_PRODUCE_ECU = "ecu";

    public static String TOPIC_PRODUCE_STATUS = "status";

    public static String TOPIC_PRODUCE_SAFETY = "safety-status";

    public static String TOPIC_PRODUCE_DATA = "data";

    public static String TOPIC_PRODUCE_DISTANCE = "distance";

    public static String TOPIC_PRODUCE_CYCLE = "cycle";

    public static String JWT_TOKEN_KEY = "turatech";

    /** group id disamakan dengan nama unit */
    public static String GROUP_ID_CONFIG = MainApp.config("config.unit_id");

    public static Integer MAX_NO_MESSAGE_FOUND_COUNT = 100;

    public static Integer AUTO_COMMIT_INTERVAL = 1000;

    public static Integer FETCH_TIMEOUT = 1000;

    public static Integer SESSION_TIMEOUT = 30000;

    public static Integer MAX_POLL_INTERVAL_MS = 60000;

    public static Integer SESSION_TIMEOUT_MS = 40000;

    public static Integer MAX_POLL_RECORDS = 6;

    public static String OFFSET_RESET_LATEST = "latest";

    public static String OFFSET_RESET_EARLIER = "earliest";

    public static String SECURITY_PROTOCOL = "SASL_PLAINTEXT";

    public static String username_kafka = "coba";

    public static String password_kafka = "cobacoba";

    public static String mechanism_kafka = "PLAIN";
}
