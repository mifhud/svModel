package com.tura.fms.screen.presenter;

import com.google.gson.Gson;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.models.prestart_model.Prestart_DB;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.prestart_model.PrestartDataDetail;
import com.tura.fms.screen.models.prestart_model.PrestartDataRest;
import com.tura.fms.screen.models.prestart_model.submit.HourMeterSubmit;
import com.tura.fms.screen.models.prestart_model.submit.PrestartResponse;
import com.tura.fms.screen.models.prestart_model.submit.PrestartSubmit;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;

public class PrestartPresenter {

    private static List<PrestartDataDetail> prestartDataDetails = null;
    public static final NetworkConnection networkConnection = new NetworkConnection();
    Prestart_DB prestart_db;
    private static final Logger log = LoggerFactory.getLogger(PrestartPresenter.class);

    // untuk submit hour
    public void submit_hour(HourMeterSubmit hourMeterSubmit) {

        Call<ResponseModel> call = APIClient.getInstance().PrestartSubmitHour(MainApp.requestHeader, hourMeterSubmit);

        call.enqueue(new Callback<ResponseModel>() {
            @Override

            public void onResponse(Call<ResponseModel> call, retrofit2.Response<ResponseModel> response) {

                Gson gson = new Gson();
                String json = gson.toJson(response.body());

                System.out.println(json);

            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                System.out.print(t.getMessage());

            }

        });

    }

    // untuk submit checkbox form
    public void submit_check_form(PrestartSubmit prestartSubmit) {

        Call<PrestartResponse> call = APIClient.getInstance().PrestartSubmit(MainApp.requestHeader, prestartSubmit);

        call.enqueue(new Callback<PrestartResponse>() {
            @Override

            public void onResponse(Call<PrestartResponse> call, retrofit2.Response<PrestartResponse> response) {

                Gson gson = new Gson();
                String json = gson.toJson(response.body());

                System.out.println(json);

            }

            @Override
            public void onFailure(Call<PrestartResponse> call, Throwable t) {
                System.out.print(t.getMessage());

            }

        });

    }

    // untuk get prestart
    public void getDataRestApi(String cd, String type, CustomRunnable cb) {
        if (NetworkConnection.getServiceStatus()) {

            Call<PrestartDataRest> call = APIClient.getInstance().GetPrestart(MainApp.requestHeader, cd, 1, 100, type);

            call.enqueue(new Callback<PrestartDataRest>() {
                @Override
                public void onResponse(Call<PrestartDataRest> call, Response<PrestartDataRest> response) {
                    if (response.isSuccessful()) {
                        prestartDataDetails = response.body().getOutput();
                    } else {
                        prestartDataDetails = MainApp.prestartDB.getData(cd, type);
                    }
                    cb.setData(prestartDataDetails);
                    cb.run();
                }

                @Override
                public void onFailure(Call<PrestartDataRest> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            prestartDataDetails = MainApp.prestartDB.getData(cd, type);
        }
    }

}
