package com.tura.fms.screen.models.prestart_model;

import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.prestart_model.submit.PrestartResponseBulk;
import com.tura.fms.screen.models.prestart_model.submit.PrestartSubmit;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

public class PrestartLogDB extends Operation {
    public static final String TABLE_NAME = "PrestartLog_Record";

    private static final String id = "id";
    private static final String id_actual = "id_actual";
    private static final String id_operation_support = "id_operation_support";
    private static final String id_operation_loader = "id_operation_loader";
    private static final String time = "time";
    private static final String id_equipment = "id_equipment";
    private static final String id_operator = "id_operator";
    private static final String id_prestart = "id_prestart";
    private static final String value = "value";
    private static final String qty = "qty";
    private static final String sync_status = "sync_status";

    public PrestartLogDB() {
        super(PrestartLogDB.TABLE_NAME, ConstantValues.DATA_OPERATION);
    }

    public void createTablePrestartLog() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table prestart log exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table prestart log created");
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + id_actual + " TEXT NULL,"
                + id_operation_support + " TEXT NULL," + id_operation_loader + " TEXT NULL," + time + " NUMBER NULL,"
                + id_equipment + " TEXT NULL," + id_operator + " TEXT NULL," + id_prestart + " TEXT NULL," + value
                + " TEXT NULL," + qty + " NUMBER NULL," + sync_status + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        PrestartSubmit eq = (PrestartSubmit) eqa;
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + id_actual + "," + id_operation_support
                    + "," + id_operation_loader + "," + time + "," + id_equipment + "," + id_operator + ","
                    + id_prestart + "," + value + "," + qty + "," + sync_status + ")"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,0) ON CONFLICT(" + id + ") DO UPDATE SET " + id + "=?," + id_actual
                    + "=?," + id_operation_support + "=?," + id_operation_loader + "=?," + time + "=?," + id_equipment
                    + "=?," + id_operator + "=?," + id_prestart + "=?," + value + "=?," + qty + "=?";

            st = conn().prepareStatement(sql);

            final String uuid = UUID.randomUUID().toString().replace("-", "");

            st.setString(1, uuid);
            st.setString(11, uuid);
            st.setString(2, eq.getIdActual() == null ? null : eq.getIdActual().toString());
            st.setString(12, eq.getIdActual() == null ? null : eq.getIdActual().toString());
            st.setString(3, eq.getIdOperationSupport() == null ? null : eq.getIdOperationSupport().toString());
            st.setString(13, eq.getIdOperationSupport() == null ? null : eq.getIdOperationSupport().toString());
            st.setString(4, eq.getIdOperationLoader() == null ? null : eq.getIdOperationLoader().toString());
            st.setString(14, eq.getIdOperationLoader() == null ? null : eq.getIdOperationLoader().toString());
            st.setLong(5, eq.getTime());
            st.setLong(15, eq.getTime());
            st.setString(6, eq.getIdEquipment() == null ? null : eq.getIdEquipment().toString());
            st.setString(16, eq.getIdEquipment() == null ? null : eq.getIdEquipment().toString());
            st.setString(7, eq.getIdOperator() == null ? null : eq.getIdOperator().toString());
            st.setString(17, eq.getIdOperator() == null ? null : eq.getIdOperator().toString());
            st.setString(8, eq.getIdPrestart() == null ? null : eq.getIdPrestart().toString());
            st.setString(18, eq.getIdPrestart() == null ? null : eq.getIdPrestart().toString());
            st.setString(9, eq.getValue() == null ? null : eq.getValue().toString());
            st.setString(19, eq.getValue() == null ? null : eq.getValue().toString());
            st.setInt(10, eq.getQty() == null ? 0 : eq.getQty());
            st.setInt(20, eq.getQty() == null ? 0 : eq.getQty());

            rs = st.execute();
            st.close();

            eq.setId(uuid);

            return eq;

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    @Override
    public void deleteData(String ids) {
        String sql = "delete from " + TABLE_NAME + " where " + id + "=?";
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, ids);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        // no action
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        // no action
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                PrestartSubmit eq = new PrestartSubmit();
                eq.setId(cursor.getString(id));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdOperationSupport(cursor.getString(id_operation_support));
                eq.setIdOperationLoader(cursor.getString(id_operation_loader));
                eq.setTime(cursor.getLong(time));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setIdPrestart(cursor.getString(id_prestart));
                eq.setValue(cursor.getString(value));
                eq.setQty(cursor.getInt(qty));

                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getFlaggedDataOne() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=? AND " + id_equipment + "=? LIMIT 1";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            String eqId = MainApp.equipmentLocal != null ? MainApp.equipmentLocal.getId() : "";
            preparedStatement.setString(2, eqId);
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                PrestartSubmit eq = new PrestartSubmit();
                eq.setId(cursor.getString(id));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdOperationSupport(cursor.getString(id_operation_support));
                eq.setIdOperationLoader(cursor.getString(id_operation_loader));
                eq.setTime(cursor.getLong(time));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setIdPrestart(cursor.getString(id_prestart));
                eq.setValue(cursor.getString(value));
                eq.setQty(cursor.getInt(qty));

                cursor.close();
                return eq;

            } else {

                cursor.close();
                return null;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        Call<PrestartResponseBulk> call = APIClient.getInstance().prestartSubmitBulk(MainApp.requestHeader, eq);
        try {
            Response<PrestartResponseBulk> response = call.execute();
            System.out.println(response);
            if (response.body().getRespon().equals("success")) {
                System.out.println("post success");
                /**
                 * change flag (sync_status to 1)
                 */
                for (Object obj : eq) {
                    PrestartSubmit oqe = (PrestartSubmit) obj;
                    setSynchronized(oqe.getId());
                }
            }
        } catch (IOException ex) {

            ex.printStackTrace();
        }
    }

    @Override
    public void postData(Object eq, Runnable cb) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }

        Call<ResponseModel> call = APIClient.getInstance().postOfflinePrestart(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            if (response.body() != null) {
                if (response.body().getResponse() != null) {
                    if (response.body().getResponse().equals("success")) {
                        System.out.println("post success");

                        if (cb != null) {
                            cb.run();
                        }
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void setSynchronized(String ids) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where " + id + "=?";
        // deleteData(id);
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, ids);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
