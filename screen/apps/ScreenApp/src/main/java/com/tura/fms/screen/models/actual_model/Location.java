package com.tura.fms.screen.models.actual_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Location {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("cd_location")
    @Expose
    private String cd_location;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("longitude")
    @Expose
    private Double longitude;

    @SerializedName("latitude")
    @Expose
    private Double latitude;

    @SerializedName("date_submitted")
    @Expose
    private long date_submitted;

    @SerializedName("date_modified")
    @Expose
    private long date_modified;

    @SerializedName("_location")
    @Expose
    private Location _location;

    public Location() {

    }

    public Location get_location() {
        return _location;
    }

    public void set_location(Location _location) {
        this._location = _location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCd_location() {
        return cd_location;
    }

    public void setCd_location(String cd_location) {
        this.cd_location = cd_location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public long getDate_submitted() {
        return date_submitted;
    }

    public void setDate_submitted(long date_submitted) {
        this.date_submitted = date_submitted;
    }

    public long getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(long date_modified) {
        this.date_modified = date_modified;
    }

}
