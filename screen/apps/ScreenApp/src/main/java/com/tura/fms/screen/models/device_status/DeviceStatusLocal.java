package com.tura.fms.screen.models.device_status;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "date", "cpu_usage", "ram_usage", "disk_usage", "db_size" })
public class DeviceStatusLocal {

    @JsonProperty("date")
    private String date;
    @JsonProperty("cpu_usage")
    private String cpu_usage;
    @JsonProperty("ram_usage")
    private String ram_usage;
    @JsonProperty("disk_usage")
    private String disk_usage;
    @JsonProperty("db_size")
    private String db_size;
    @JsonProperty("offline")
    private boolean offline;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCpuUsage() {
        return cpu_usage;
    }

    public void setCpuUsage(String cpu_usage) {
        this.cpu_usage = cpu_usage;
    }

    public String getRamUsage() {
        return ram_usage;
    }

    public void setRamUsage(String ram_usage) {
        this.ram_usage = ram_usage;
    }

    public String getDiskUsage() {
        return disk_usage;
    }

    public void setDiskUsage(String disk_usage) {
        this.disk_usage = disk_usage;
    }

    public String getDbSize() {
        return db_size;
    }

    public void setDbSize(String db_size) {
        this.db_size = db_size;
    }

    public boolean getOffline() {
        return offline;
    }

    public void setOffline(boolean offline) {
        this.offline = offline;
    }

}
