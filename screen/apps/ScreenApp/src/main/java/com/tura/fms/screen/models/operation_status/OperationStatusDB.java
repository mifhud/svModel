package com.tura.fms.screen.models.operation_status;

import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

/**
 * untuk menampung activity
 */
public class OperationStatusDB extends Operation {
    public static final String TABLE_NAME = "OperationStatus_Record";

    private static final String id = "id";
    private static final String id_actual = "id_actual";
    private static final String id_operation_support = "id_operation_support";
    private static final String id_operation_loader = "id_operation_loader";
    private static final String time = "time";
    private static final String id_equipment = "id_equipment";
    private static final String id_operator = "id_operator";
    private static final String cd_activity = "cd_activity";
    private static final String cd_tum = "cd_tum";
    private static final String cd_type = "cd_type";
    private static final String latitude = "latitude";
    private static final String longitude = "longitude";
    private static final String sync_status = "sync_status";

    public OperationStatusDB() {
        super(OperationStatusDB.TABLE_NAME, ConstantValues.DATA_OPERATION_STATUS);
    }

    public void createTableOperationStatus() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table operation status exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table operation status created");
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + id_actual + " TEXT NULL,"
                + id_operation_support + " TEXT NULL," + id_operation_loader + " TEXT NULL," + time + " NUMBER NULL,"
                + id_equipment + " TEXT NULL," + id_operator + " TEXT NULL," + cd_activity + " TEXT NULL," + cd_tum
                + " TEXT NULL," + cd_type + " TEXT NULL," + latitude + " TEXT NULL," + longitude + " TEXT NULL,"
                + sync_status + " NUMBER NULL" + ")";
    }

    public List<OperationStatusLocal> getLatestActivity(String idEquipment) {
        Long startShift = Utilities.getStartOfShift();
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + id_equipment + "=? AND " + time + ">=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, idEquipment);
            preparedStatement.setLong(2, startShift);
            ResultSet cursor = preparedStatement.executeQuery();
            List<OperationStatusLocal> result = new ArrayList<OperationStatusLocal>();
            while (cursor.next()) {
                OperationStatusLocal eq = new OperationStatusLocal();
                eq.setId(cursor.getString(id));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdOperationSupport(cursor.getString(id_operation_support));
                eq.setIdOperationLoader(cursor.getString(id_operation_loader));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setCdActivity(cursor.getString(cd_activity));
                eq.setCdTum(cursor.getString(cd_tum));
                eq.setCdType(cursor.getString(cd_type));
                eq.setTime(cursor.getLong(time));
                eq.setLatitude(cursor.getDouble(latitude));
                eq.setLongitude(cursor.getDouble(longitude));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object insertIgnore(Object eqa) {
        OperationStatusLocal eq = (OperationStatusLocal) eqa;
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + id_actual + "," + id_operation_support
                    + "," + id_operation_loader + "," + time + "," + id_equipment + "," + id_operator + ","
                    + cd_activity + "," + cd_tum + "," + cd_type + "," + latitude + "," + longitude + "," + sync_status
                    + ")" + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,0) ON CONFLICT(" + id + ") DO UPDATE SET " + id + "=?,"
                    + id_actual + "=?," + id_operation_support + "=?," + id_operation_loader + "=?," + time + "=?,"
                    + id_equipment + "=?," + id_operator + "=?," + cd_activity + "=?," + cd_tum + "=?," + cd_type
                    + "=?," + latitude + "=?," + longitude + "=?;";

            st = conn().prepareStatement(sql);

            final String uuid = UUID.randomUUID().toString().replace("-", "");

            st.setString(1, uuid);
            st.setString(13, uuid);
            st.setString(2, eq.getIdActual() != null ? eq.getIdActual().toString() : null);
            st.setString(14, eq.getIdActual() != null ? eq.getIdActual().toString() : null);
            st.setString(3, eq.getIdOperationSupport() != null ? eq.getIdOperationSupport().toString() : null);
            st.setString(15, eq.getIdOperationSupport() != null ? eq.getIdOperationSupport().toString() : null);
            st.setString(4, eq.getIdOperationLoader() != null ? eq.getIdOperationLoader().toString() : null);
            st.setString(16, eq.getIdOperationLoader() != null ? eq.getIdOperationLoader().toString() : null);
            st.setLong(5, eq.getTime());
            st.setLong(17, eq.getTime());
            st.setString(6, eq.getIdEquipment() != null ? eq.getIdEquipment().toString() : null);
            st.setString(18, eq.getIdEquipment() != null ? eq.getIdEquipment().toString() : null);
            st.setString(7, eq.getIdOperator() != null ? eq.getIdOperator().toString() : null);
            st.setString(19, eq.getIdOperator() != null ? eq.getIdOperator().toString() : null);
            st.setString(8, eq.getCdActivity() != null ? eq.getCdActivity().toString() : null);
            st.setString(20, eq.getCdActivity() != null ? eq.getCdActivity().toString() : null);
            st.setString(9, eq.getCdTum() != null ? eq.getCdTum().toString() : null);
            st.setString(21, eq.getCdTum() != null ? eq.getCdTum().toString() : null);
            st.setString(10, eq.getCdType() != null ? eq.getCdType().toString() : null);
            st.setString(22, eq.getCdType() != null ? eq.getCdType().toString() : null);
            st.setDouble(11, eq.getLatitude());
            st.setDouble(23, eq.getLatitude());
            st.setDouble(12, eq.getLongitude());
            st.setDouble(24, eq.getLongitude());

            rs = st.execute();
            st.close();

            return this.getLocalOneById(id, uuid);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        return null;

    }

    @Override
    public void deleteData(String ids) {
        String sql = "delete from " + TABLE_NAME + " where " + id + "=?";
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, ids);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        // no action
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        // no action
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                OperationStatusLocal eq = new OperationStatusLocal();
                eq.setId(cursor.getString(id));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdOperationSupport(cursor.getString(id_operation_support));
                eq.setIdOperationLoader(cursor.getString(id_operation_loader));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setCdActivity(cursor.getString(cd_activity));
                eq.setCdTum(cursor.getString(cd_tum));
                eq.setCdType(cursor.getString(cd_type));
                eq.setTime(cursor.getLong(time));
                eq.setLatitude(cursor.getDouble(latitude));
                eq.setLongitude(cursor.getDouble(longitude));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getFlaggedDataOne() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=? AND " + id_equipment + "=? LIMIT 1";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            String eqId = MainApp.equipmentLocal != null ? MainApp.equipmentLocal.getId() : "";
            preparedStatement.setString(2, eqId);
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                OperationStatusLocal eq = new OperationStatusLocal();
                eq.setId(cursor.getString(id));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdOperationSupport(cursor.getString(id_operation_support));
                eq.setIdOperationLoader(cursor.getString(id_operation_loader));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setCdActivity(cursor.getString(cd_activity));
                eq.setCdTum(cursor.getString(cd_tum));
                eq.setCdType(cursor.getString(cd_type));
                eq.setTime(cursor.getLong(time));
                eq.setLatitude(cursor.getDouble(latitude));
                eq.setLongitude(cursor.getDouble(longitude));

                cursor.close();
                return eq;
            } else {

                cursor.close();
                return null;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public Object getLocalOneById(String fieldId, String searchId) {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + fieldId + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, searchId);
            ResultSet cursor = preparedStatement.executeQuery();
            if (cursor.next()) {
                OperationStatusLocal eq = new OperationStatusLocal();
                eq.setId(cursor.getString(id));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdOperationSupport(cursor.getString(id_operation_support));
                eq.setIdOperationLoader(cursor.getString(id_operation_loader));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setCdActivity(cursor.getString(cd_activity));
                eq.setCdTum(cursor.getString(cd_tum));
                eq.setCdType(cursor.getString(cd_type));
                eq.setTime(cursor.getLong(time));
                eq.setLatitude(cursor.getDouble(latitude));
                eq.setLongitude(cursor.getDouble(longitude));

                cursor.close();

                return eq;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        Call<ResponseModel> call = APIClient.getInstance().postOperationStatus(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            System.out.println(response);
            System.out.println(response.body().getResponse());
            System.out.println(response.body().getMessage());
            if (response.body().getResponse().equals("success")) {
                System.out.println("post success");
                /**
                 * change flag (sync_status to 1)
                 */
                for (Object obj : eq) {
                    OperationStatusLocal oqe = (OperationStatusLocal) obj;
                    setSynchronized(oqe.getId());
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void postData(Object eq, Runnable cb) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }

        Call<ResponseModel> call = APIClient.getInstance().postOfflineOperationStatus(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            if (response.body() != null) {
                if (response.body().getResponse() != null) {
                    if (response.body().getResponse().equals("success")) {
                        System.out.println("post success");

                        if (cb != null) {
                            cb.run();
                        }
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void setSynchronized(String ids) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where " + id + "=?";
        // deleteData(id);
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, ids);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
