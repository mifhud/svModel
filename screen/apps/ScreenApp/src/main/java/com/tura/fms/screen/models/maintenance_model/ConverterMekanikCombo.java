package com.tura.fms.screen.models.maintenance_model;

import javafx.util.StringConverter;

public class ConverterMekanikCombo extends StringConverter<MaintenanceUserMakanik> {

    // Method to convert a Person-Object to a String
    @Override
    public String toString(MaintenanceUserMakanik person)
    {
        return person == null? null : person.getName();
    }



    // Method to convert a String to a Person-Object
    @Override
    public MaintenanceUserMakanik fromString(String string)
    {
        MaintenanceUserMakanik person = null;

        if (string == null)
        {
            return person;
        }

        int commaIndex = string.indexOf(",");

        if (commaIndex == -1)
        {
            person = new MaintenanceUserMakanik(string, null);
        }
        else
        {
            String id = string.substring(commaIndex + 2);
            String name = string.substring(0, commaIndex);
            person = new MaintenanceUserMakanik(id, name);
        }

        return person;
    }

}
