package com.tura.fms.screen.models.plan;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.tura.fms.screen.models.equipment.Equipment;
import com.tura.fms.screen.models.location.Location;
import com.tura.fms.screen.models.material.Material;
import com.tura.fms.screen.models.operation.Operation;
import com.tura.fms.screen.models.shift.Shift;
import com.tura.fms.screen.models.user_model.UserDetails;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "__id_supervisor__",
        "__has_id_supervisor__",
        "__id_group_leader__",
        "__has_id_group_leader__",
        "__id_checker__",
        "__has_id_checker__",
        "__id_loader_operator__",
        "__has_id_loader_operator__",
        "__id_loader__",
        "__has_id_loader__",
        "__id_hauler_operator__",
        "__has_id_hauler_operator__",
        "__id_hauler__",
        "__has_id_hauler__",
        "__id_location__",
        "__has_id_location__",
        "__id_destination__",
        "__has_id_destination__",
        "__cd_operation__",
        "__has_cd_operation__",
        "__cd_shift__",
        "__has_cd_shift__",
        "__cd_material__",
        "__has_cd_material__",
        "is_deleted",
        "date",
        "date_submitted",
        "date_modified",
        "json"
})
public class Plan {

    @JsonProperty("id")
    private String id;
    @JsonProperty("__id_supervisor__")
    private UserDetails idSupervisor;
    @JsonProperty("__has_id_supervisor__")
    private Boolean hasIdSupervisor;
    @JsonProperty("__id_group_leader__")
    private UserDetails idGroupLeader;
    @JsonProperty("__has_id_group_leader__")
    private Boolean hasIdGroupLeader;
    @JsonProperty("__id_checker__")
    private String idChecker;
    @JsonProperty("__has_id_checker__")
    private Boolean hasIdChecker;
    @JsonProperty("__id_loader_operator__")
    private String idLoaderOperator;
    @JsonProperty("__has_id_loader_operator__")
    private Boolean hasIdLoaderOperator;
    @JsonProperty("__id_loader__")
    private Equipment idLoader;
    @JsonProperty("__has_id_loader__")
    private Boolean hasIdLoader;
    @JsonProperty("__id_hauler_operator__")
    private String idHaulerOperator;
    @JsonProperty("__has_id_hauler_operator__")
    private Boolean hasIdHaulerOperator;
    @JsonProperty("__id_hauler__")
    private Equipment idHauler;
    @JsonProperty("__has_id_hauler__")
    private Boolean hasIdHauler;
    @JsonProperty("__id_location__")
    private Location idLocation;
    @JsonProperty("__has_id_location__")
    private Boolean hasIdLocation;
    @JsonProperty("__id_destination__")
    private Location idDestination;
    @JsonProperty("__has_id_destination__")
    private Boolean hasIdDestination;
    @JsonProperty("__cd_operation__")
    private Operation cdOperation;
    @JsonProperty("__has_cd_operation__")
    private Boolean hasCdOperation;
    @JsonProperty("__cd_shift__")
    private Shift cdShift;
    @JsonProperty("__has_cd_shift__")
    private Boolean hasCdShift;
    @JsonProperty("__cd_material__")
    private Material cdMaterial;
    @JsonProperty("__has_cd_material__")
    private Boolean hasCdMaterial;
    @JsonProperty("is_deleted")
    private Integer isDeleted;
    @JsonProperty("date")
    private Integer date;
    @JsonProperty("date_submitted")
    private Integer dateSubmitted;
    @JsonProperty("date_modified")
    private Integer dateModified;
    @JsonProperty("json")
    private Object json;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("__id_supervisor__")
    public UserDetails getIdSupervisor() {
        return idSupervisor;
    }

    @JsonProperty("__id_supervisor__")
    public void setIdSupervisor(UserDetails idSupervisor) {
        this.idSupervisor = idSupervisor;
    }

    @JsonProperty("__has_id_supervisor__")
    public Boolean getHasIdSupervisor() {
        return hasIdSupervisor;
    }

    @JsonProperty("__has_id_supervisor__")
    public void setHasIdSupervisor(Boolean hasIdSupervisor) {
        this.hasIdSupervisor = hasIdSupervisor;
    }

    @JsonProperty("__id_group_leader__")
    public UserDetails getIdGroupLeader() {
        return idGroupLeader;
    }

    @JsonProperty("__id_group_leader__")
    public void setIdGroupLeader(UserDetails idGroupLeader) {
        this.idGroupLeader = idGroupLeader;
    }

    @JsonProperty("__has_id_group_leader__")
    public Boolean getHasIdGroupLeader() {
        return hasIdGroupLeader;
    }

    @JsonProperty("__has_id_group_leader__")
    public void setHasIdGroupLeader(Boolean hasIdGroupLeader) {
        this.hasIdGroupLeader = hasIdGroupLeader;
    }

    @JsonProperty("__id_checker__")
    public String getIdChecker() {
        return idChecker;
    }

    @JsonProperty("__id_checker__")
    public void setIdChecker(String idChecker) {
        this.idChecker = idChecker;
    }

    @JsonProperty("__has_id_checker__")
    public Boolean getHasIdChecker() {
        return hasIdChecker;
    }

    @JsonProperty("__has_id_checker__")
    public void setHasIdChecker(Boolean hasIdChecker) {
        this.hasIdChecker = hasIdChecker;
    }

    @JsonProperty("__id_loader_operator__")
    public String getIdLoaderOperator() {
        return idLoaderOperator;
    }

    @JsonProperty("__id_loader_operator__")
    public void setIdLoaderOperator(String idLoaderOperator) {
        this.idLoaderOperator = idLoaderOperator;
    }

    @JsonProperty("__has_id_loader_operator__")
    public Boolean getHasIdLoaderOperator() {
        return hasIdLoaderOperator;
    }

    @JsonProperty("__has_id_loader_operator__")
    public void setHasIdLoaderOperator(Boolean hasIdLoaderOperator) {
        this.hasIdLoaderOperator = hasIdLoaderOperator;
    }

    @JsonProperty("__id_loader__")
    public Equipment getIdLoader() {
        return idLoader;
    }

    @JsonProperty("__id_loader__")
    public void setIdLoader(Equipment idLoader) {
        this.idLoader = idLoader;
    }

    @JsonProperty("__has_id_loader__")
    public Boolean getHasIdLoader() {
        return hasIdLoader;
    }

    @JsonProperty("__has_id_loader__")
    public void setHasIdLoader(Boolean hasIdLoader) {
        this.hasIdLoader = hasIdLoader;
    }

    @JsonProperty("__id_hauler_operator__")
    public String getIdHaulerOperator() {
        return idHaulerOperator;
    }

    @JsonProperty("__id_hauler_operator__")
    public void setIdHaulerOperator(String idHaulerOperator) {
        this.idHaulerOperator = idHaulerOperator;
    }

    @JsonProperty("__has_id_hauler_operator__")
    public Boolean getHasIdHaulerOperator() {
        return hasIdHaulerOperator;
    }

    @JsonProperty("__has_id_hauler_operator__")
    public void setHasIdHaulerOperator(Boolean hasIdHaulerOperator) {
        this.hasIdHaulerOperator = hasIdHaulerOperator;
    }

    @JsonProperty("__id_hauler__")
    public Equipment getIdHauler() {
        return idHauler;
    }

    @JsonProperty("__id_hauler__")
    public void setIdHauler(Equipment idHauler) {
        this.idHauler = idHauler;
    }

    @JsonProperty("__has_id_hauler__")
    public Boolean getHasIdHauler() {
        return hasIdHauler;
    }

    @JsonProperty("__has_id_hauler__")
    public void setHasIdHauler(Boolean hasIdHauler) {
        this.hasIdHauler = hasIdHauler;
    }

    @JsonProperty("__id_location__")
    public Location getIdLocation() {
        return idLocation;
    }

    @JsonProperty("__id_location__")
    public void setIdLocation(Location idLocation) {
        this.idLocation = idLocation;
    }

    @JsonProperty("__has_id_location__")
    public Boolean getHasIdLocation() {
        return hasIdLocation;
    }

    @JsonProperty("__has_id_location__")
    public void setHasIdLocation(Boolean hasIdLocation) {
        this.hasIdLocation = hasIdLocation;
    }

    @JsonProperty("__id_destination__")
    public Location getIdDestination() {
        return idDestination;
    }

    @JsonProperty("__id_destination__")
    public void setIdDestination(Location idDestination) {
        this.idDestination = idDestination;
    }

    @JsonProperty("__has_id_destination__")
    public Boolean getHasIdDestination() {
        return hasIdDestination;
    }

    @JsonProperty("__has_id_destination__")
    public void setHasIdDestination(Boolean hasIdDestination) {
        this.hasIdDestination = hasIdDestination;
    }

    @JsonProperty("__cd_operation__")
    public Operation getCdOperation() {
        return cdOperation;
    }

    @JsonProperty("__cd_operation__")
    public void setCdOperation(Operation cdOperation) {
        this.cdOperation = cdOperation;
    }

    @JsonProperty("__has_cd_operation__")
    public Boolean getHasCdOperation() {
        return hasCdOperation;
    }

    @JsonProperty("__has_cd_operation__")
    public void setHasCdOperation(Boolean hasCdOperation) {
        this.hasCdOperation = hasCdOperation;
    }

    @JsonProperty("__cd_shift__")
    public Shift getCdShift() {
        return cdShift;
    }

    @JsonProperty("__cd_shift__")
    public void setCdShift(Shift cdShift) {
        this.cdShift = cdShift;
    }

    @JsonProperty("__has_cd_shift__")
    public Boolean getHasCdShift() {
        return hasCdShift;
    }

    @JsonProperty("__has_cd_shift__")
    public void setHasCdShift(Boolean hasCdShift) {
        this.hasCdShift = hasCdShift;
    }

    @JsonProperty("__cd_material__")
    public Material getCdMaterial() {
        return cdMaterial;
    }

    @JsonProperty("__cd_material__")
    public void setCdMaterial(Material cdMaterial) {
        this.cdMaterial = cdMaterial;
    }

    @JsonProperty("__has_cd_material__")
    public Boolean getHasCdMaterial() {
        return hasCdMaterial;
    }

    @JsonProperty("__has_cd_material__")
    public void setHasCdMaterial(Boolean hasCdMaterial) {
        this.hasCdMaterial = hasCdMaterial;
    }

    @JsonProperty("is_deleted")
    public Integer getIsDeleted() {
        return isDeleted;
    }

    @JsonProperty("is_deleted")
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    @JsonProperty("date")
    public Integer getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(Integer date) {
        this.date = date;
    }

    @JsonProperty("date_submitted")
    public Integer getDateSubmitted() {
        return dateSubmitted;
    }

    @JsonProperty("date_submitted")
    public void setDateSubmitted(Integer dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    @JsonProperty("date_modified")
    public Integer getDateModified() {
        return dateModified;
    }

    @JsonProperty("date_modified")
    public void setDateModified(Integer dateModified) {
        this.dateModified = dateModified;
    }

    @JsonProperty("json")
    public Object getJson() {
        return json;
    }

    @JsonProperty("json")
    public void setJson(Object json) {
        this.json = json;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

