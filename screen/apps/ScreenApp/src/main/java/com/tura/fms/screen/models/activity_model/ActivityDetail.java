package com.tura.fms.screen.models.activity_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ActivityDetail {


    @SerializedName("cd")
    @Expose
    private String cd;

    @SerializedName("cd_self")
    @Expose
    private String cd_self;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("json")
    @Expose
    private ActivityJson json = null;


    public String getCd() {
        return cd;
    }

    public void setCd(String cd) {
        this.cd = cd;
    }

    public String getCd_self() {
        return cd_self;
    }

    public void setCd_self(String cd_self) {
        this.cd_self = cd_self;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ActivityJson getJson() {
        return json;
    }

    public void setJson(ActivityJson json) {
        this.json = json;
    }


}
