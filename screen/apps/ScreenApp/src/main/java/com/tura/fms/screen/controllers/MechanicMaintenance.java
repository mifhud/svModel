package com.tura.fms.screen.controllers;

import static org.javalite.app_config.AppConfig.p;

import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXSpinner;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.constant.Constant;
import com.tura.fms.screen.models.constant.ConstantData;
import com.tura.fms.screen.models.maintenance_model.MainConstModel;
import com.tura.fms.screen.models.maintenance_model.MaintenanceStatus;
import com.tura.fms.screen.models.maintenance_model.task_model.MaintenanceDescription;
import com.tura.fms.screen.models.maintenance_model.task_model.MaintenanceDescriptionPost;
import com.tura.fms.screen.models.maintenance_model.task_model.MaintenanceDetails;
import com.tura.fms.screen.models.maintenance_model.task_model.MaintenanceDetailsPost;
import com.tura.fms.screen.models.maintenance_model.task_model.MaintenanceDetailsStopPost;
import com.tura.fms.screen.models.maintenance_model.task_model.ResponseMaintenanceDescriptionOutput;
import com.tura.fms.screen.models.maintenance_model.task_model.ResponseMaintenanceDetailsOutput;
import com.tura.fms.screen.models.maintenance_model.task_model.Taskmodel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;
import com.tura.fms.screen.presenter.CustomRunnableX;
import com.tura.fms.screen.presenter.MaintenancePresenter;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MechanicMaintenance implements Initializable {

    @FXML
    private Button TitleBar;

    @FXML
    private Text textHour;

    @FXML
    private Label textTanggal;

    @FXML
    private Label textShift;

    @FXML
    private Label textWO;

    @FXML
    private Text unitName;

    @FXML
    private JFXButton back;

    @FXML
    private JFXButton buttonDone;

    @FXML
    private JFXButton btnNewMaintenance;

    @FXML
    private JFXButton btnNewProcess;

    @FXML
    private GridPane gridInfo;

    @FXML
    private VBox vBoxMain;

    @FXML
    private StackPane stackPane;

    @FXML
    private Text usermekanik;

    @FXML
    private TableColumn columnDate;

    @FXML
    private TableColumn columnStart;

    Timeline clock;

    private ObservableList<MainConstModel> recordsData = FXCollections.observableArrayList();
    private ObservableList<MaintenanceStatus> comboStatusData = FXCollections.observableArrayList();

    MaintenancePresenter maintenancePresenter;

    @FXML
    private Label mode_connect;

    @FXML
    private JFXSpinner loading;

    @FXML
    private TableView<MaintenanceDescription> tblMaintenance;

    @FXML
    private TableView<MaintenanceDetails> tblProcess;

    private Boolean formMaintenanceShow = false;

    private Boolean formProcessShow = false;

    private String maintenanceStatus;

    public static Map<String, Object> maintenance;

    public static List<MaintenanceDescription> maintenanceDescription;

    public static List<MaintenanceDetails> maintenanceDetail;

    public static List<Constant> problem;

    public static List<Constant> status;

    public static List<Map<String, Object>> mechanic;

    public static List<Map<String, Object>> supervisor;

    public static List<Map<String, Object>> foreman;

    javafx.util.Callback<ListView<Map<String, Object>>, ListCell<Map<String, Object>>> cellFactoryComboUser = //
            new javafx.util.Callback<ListView<Map<String, Object>>, ListCell<Map<String, Object>>>() {

                @Override
                public ListCell<Map<String, Object>> call(ListView<Map<String, Object>> l) {
                    return new ListCell<Map<String, Object>>() {

                        @Override
                        protected void updateItem(Map<String, Object> item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                            } else {
                                Map<String, Object> user = item.get("user_id") != null
                                        ? Utilities.mapObject(item.get("user_id"))
                                        : null;
                                String nik, name;
                                nik = "";
                                name = "";
                                if (user != null) {
                                    nik = user.get("staff_id").toString();
                                    name = user.get("name").toString();
                                }
                                setText(nik + " - " + name);
                            }
                        }
                    };
                }
            };

    javafx.util.Callback<ListView<Constant>, ListCell<Constant>> cellFactoryComboConstant = //
            new javafx.util.Callback<ListView<Constant>, ListCell<Constant>>() {

                @Override
                public ListCell<Constant> call(ListView<Constant> l) {
                    return new ListCell<Constant>() {

                        @Override
                        protected void updateItem(Constant item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                            } else {
                                String cd, name;
                                cd = item.getCd();
                                name = item.getName();
                                setText(cd + " - " + name);
                            }
                        }
                    };
                }
            };

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        TitleBar.setText(p("app.titleMechanicMaintenance"));

        loading.setVisible(true);

        usermekanik.setText(MainApp.user.getName());

        maintenancePresenter = new MaintenancePresenter();

        unitName.setText(MainApp.config("config.unit_id"));

        String tgl = Utilities.getHariTanggal();

        textTanggal.setPadding(new Insets(10, 5, 5, 5));
        textShift.setPadding(new Insets(10, 5, 5, 5));
        textWO.setPadding(new Insets(10, 5, 5, 5));
        textTanggal.setText(tgl);

        if (Utilities.getCurrentShift().equals(ConstantValues.SHIFT_DAY)) {
            textShift.setText("Shift : DAY");
        } else {
            textShift.setText("Shift : NIGHT");
        }

        buttonDone.setWrapText(true);

        btnNewProcess.setDisable(true);

        clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {

            mode_connect.setText(NetworkConnection.getStringStatus());

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    String tm = Utilities.getCurrentTimeString();
                    textHour.setText(tm);
                }
            });

        }), new KeyFrame(Duration.seconds(MainApp.dashboardInterval)));

        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();

        setUpTable();

        getAllData();

        drawerAction();

        NavigationUI.publicStackPane = stackPane;

        buttonDone.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Utilities.confirm(stackPane, "Yakin maintenance ini sudah selesai ?", new Runnable() {

                    @Override
                    public void run() {
                        Boolean check = checkMaintenanceDetailsStop();
                        if (check) {
                            loading.setVisible(true);
                            Utilities.stopMaintenance(MainApp.currentMaintenanceId, new CustomRunnableX<Taskmodel>() {

                                @Override
                                public void run() {
                                    super.run();
                                    loading.setVisible(false);
                                    getAllData();
                                }
                            });
                        } else {
                            Utilities.showMessageDialog("Failed", "",
                                    "Silakan STOP semua proses yang sedang dilakukan");
                        }
                    }
                }, new Runnable() {

                    @Override
                    public void run() {
                        // no action
                    }
                });
            }

        });
    }

    public void getAllData() {
        loading.setVisible(true);

        getData(new Runnable() {

            @Override
            public void run() {
                showMaster();
            }
        }, new Runnable() {

            @Override
            public void run() {
                showDescription();
            }
        }, new Runnable() {

            @Override
            public void run() {
                loading.setVisible(false);

                showDetail();
            }
        });
    }

    public Boolean checkMaintenanceDetailsStop() {
        Boolean allStopped = true;
        for (MaintenanceDetails md : MechanicMaintenance.maintenanceDetail) {
            Boolean isStopped = md.getEndTime() != null ? md.getEndTime().trim().length() > 1 : false;
            if (!isStopped) {
                allStopped = false;
            }
        }
        return allStopped;
    }

    public void setUpTable() {
        javafx.util.Callback<TableColumn<MaintenanceDescription, String>, TableCell<MaintenanceDescription, String>> cellFactory = //
                new javafx.util.Callback<TableColumn<MaintenanceDescription, String>, TableCell<MaintenanceDescription, String>>() {

                    @Override
                    public TableCell call(final TableColumn<MaintenanceDescription, String> param) {
                        final TableCell<MaintenanceDescription, String> cell = new TableCell<MaintenanceDescription, String>() {

                            final JFXButton btn = new JFXButton("");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);

                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    final MaintenanceDescription data = getTableView().getItems().get(getIndex());

                                    btn.setPrefWidth(110);

                                    Boolean isCurrentShift = Utilities.isSameDateAndShift(data.getDate(),
                                            data.getCdShift().getCd(), Utilities.getCurrentUnix(),
                                            Utilities.getCurrentShift());
                                    if (!isCurrentShift || !enableButton()) {
                                        btn.setText("View");
                                        btn.setStyle("-fx-background-color: #13635e;");
                                        btn.getStyleClass().add("button-other");
                                    } else {
                                        btn.setText("Edit");
                                        btn.setStyle("-fx-background-color: #520a12;");
                                        btn.getStyleClass().add("button-raised");
                                    }

                                    btn.setOnAction(event -> {
                                        formMaintenance(event, data, btn.getText().toLowerCase());
                                    });

                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };

        TableColumn actionCol = new TableColumn("ACTION");
        actionCol.setPrefWidth(110);
        actionCol.setStyle("-fx-text-fill: BLACK;");
        actionCol.setCellFactory(cellFactory);
        tblMaintenance.getColumns().add(actionCol);

        javafx.util.Callback<TableColumn<MaintenanceDetails, String>, TableCell<MaintenanceDetails, String>> cellFactoryD = //
                new javafx.util.Callback<TableColumn<MaintenanceDetails, String>, TableCell<MaintenanceDetails, String>>() {

                    @Override
                    public TableCell call(final TableColumn<MaintenanceDetails, String> param) {
                        final TableCell<MaintenanceDetails, String> cell = new TableCell<MaintenanceDetails, String>() {

                            final JFXButton btn = new JFXButton("");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);

                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    final MaintenanceDetails data = getTableView().getItems().get(getIndex());

                                    btn.setPrefWidth(110);

                                    Boolean isCurrentShift = Utilities.isSameDateAndShift(data.getDate(),
                                            data.getCdShift().getCd(), Utilities.getCurrentUnix(),
                                            Utilities.getCurrentShift());
                                    Boolean isStopped = data.getEndTime() != null
                                            ? data.getEndTime().trim().length() > 1
                                            : false;
                                    if (!isCurrentShift || isStopped) {
                                        btn.setText("View");
                                        btn.setStyle("-fx-background-color: #13635e;");
                                        btn.getStyleClass().add("button-other");
                                    } else {
                                        btn.setText("Edit");
                                        btn.setStyle("-fx-background-color: #520a12;");
                                        btn.getStyleClass().add("button-raised");
                                    }

                                    btn.setOnAction(event -> {
                                        formProcess(event, data, btn.getText().toLowerCase());
                                    });

                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };

        TableColumn actionColD = new TableColumn("ACTION");
        actionColD.setPrefWidth(110);
        actionColD.setStyle("-fx-text-fill: BLACK;");
        actionColD.setCellFactory(cellFactoryD);
        tblProcess.getColumns().add(actionColD);

        tblMaintenance.setSortPolicy(tv -> {
            final ObservableList<MaintenanceDescription> itemsList = tblMaintenance.getItems();
            if (itemsList == null || itemsList.isEmpty()) {
                return true;
            }
            Comparator<MaintenanceDescription> comparator = new Comparator<MaintenanceDescription>() {
                @Override
                public int compare(MaintenanceDescription o1, MaintenanceDescription o2) {
                    final int tagCompare = o1.getDate() > o2.getDate() ? 1 : 0;
                    return tagCompare;
                }
            };
            FXCollections.sort(itemsList, comparator);
            return true;
        });

        tblProcess.setSortPolicy(tv -> {
            final ObservableList<MaintenanceDetails> itemsList = tblProcess.getItems();
            if (itemsList == null || itemsList.isEmpty()) {
                return true;
            }
            Comparator<MaintenanceDetails> comparator = new Comparator<MaintenanceDetails>() {
                @Override
                public int compare(MaintenanceDetails o1, MaintenanceDetails o2) {
                    final int tagCompare = o1.getStartTime().compareTo(o2.getStartTime());
                    return tagCompare;
                }
            };
            FXCollections.sort(itemsList, comparator);
            return true;
        });

        tblMaintenance.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                showProcessList((MaintenanceDescription) newSelection);
            }
        });
    }

    public void sortMaintenance() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                columnDate.setSortType(TableColumn.SortType.ASCENDING);
                tblMaintenance.getSortOrder().clear();
                tblMaintenance.getSortOrder().add(columnDate);
            }
        });
    }

    public void sortProcess() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                columnStart.setSortType(TableColumn.SortType.ASCENDING);
                tblProcess.getSortOrder().clear();
                tblProcess.getSortOrder().add(columnStart);
            }
        });
    }

    public void showProcessList(MaintenanceDescription ma) {
        tblProcess.getItems().clear();
        for (MaintenanceDetails item : MechanicMaintenance.maintenanceDetail) {
            boolean isSame = Utilities.isSameDateAndShift(ma.getDate(), ma.getCdShift().getCd(), item.getDate(),
                    item.getCdShift().getCd());
            if (isSame) {
                tblProcess.getItems().add(item);
            }
        }
        boolean isCurrentShift = Utilities.isSameDateAndShift(ma.getDate(), ma.getCdShift().getCd(),
                Utilities.getCurrentUnix(), Utilities.getCurrentShift());
        if (isCurrentShift && enableButton()) {
            btnNewProcess.setDisable(false);
        } else {
            btnNewProcess.setDisable(true);
        }

        sortProcess();
    }

    public Boolean enableButton() {
        if (maintenanceStatus == null) {
            return false;
        }
        if (!maintenanceStatus.equals(ConstantValues.MAINTENANCE_STATUS_PROCESS)) {
            return false;
        } else {
            return true;
        }
    }

    public static void getReferenceData(Runnable callback) {
        if (NetworkConnection.getServiceStatus()) {

            Call<ConstantData> call = APIClient.getInstance().GetConstant(MainApp.requestHeader, "M");
            call.enqueue(new Callback<ConstantData>() {

                @Override
                public void onResponse(Call<ConstantData> call, Response<ConstantData> response) {
                    if (response.isSuccessful()) {
                        MechanicMaintenance.problem = response.body().getOutput();
                    }
                }

                @Override
                public void onFailure(Call<ConstantData> call, Throwable t) {
                    t.printStackTrace();
                }
            });

            Call<ConstantData> callS = APIClient.getInstance().GetConstant(MainApp.requestHeader, "B");
            callS.enqueue(new Callback<ConstantData>() {

                @Override
                public void onResponse(Call<ConstantData> call, Response<ConstantData> response) {
                    if (response.isSuccessful()) {
                        MechanicMaintenance.status = response.body().getOutput();
                    }
                }

                @Override
                public void onFailure(Call<ConstantData> call, Throwable t) {
                    t.printStackTrace();
                }
            });

            Call<ResponseModel> callM = APIClient.getInstance().getCurrentCrew(MainApp.requestHeader,
                    ConstantValues.ROLE_MECHANIC);
            callM.enqueue(new Callback<ResponseModel>() {

                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                    if (response.isSuccessful()) {
                        try {
                            MechanicMaintenance.mechanic = Utilities.mapListObject(response.body().getOutput());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            MechanicMaintenance.mechanic = new ArrayList<>();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    t.printStackTrace();
                }
            });

            Call<ResponseModel> callSup = APIClient.getInstance().getCurrentCrew(MainApp.requestHeader,
                    ConstantValues.ROLE_SUPERVISOR);
            callSup.enqueue(new Callback<ResponseModel>() {

                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                    if (response.isSuccessful()) {
                        try {
                            MechanicMaintenance.supervisor = Utilities.mapListObject(response.body().getOutput());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            MechanicMaintenance.supervisor = new ArrayList<>();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    t.printStackTrace();
                }
            });

            Call<ResponseModel> callFore = APIClient.getInstance().getCurrentCrew(MainApp.requestHeader,
                    ConstantValues.ROLE_GROUP_LEADER);
            callFore.enqueue(new Callback<ResponseModel>() {

                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                    if (response.isSuccessful()) {
                        try {
                            MechanicMaintenance.foreman = Utilities.mapListObject(response.body().getOutput());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            MechanicMaintenance.foreman = new ArrayList<>();
                        }
                    }

                    if (callback != null) {
                        callback.run();
                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            System.out.println("---- no service");
        }
    }

    public void getData(Runnable callbackMaster, Runnable callbackDesc, Runnable callbackDetail) {
        if (NetworkConnection.getServiceStatus()) {

            Call<ResponseModel> call = APIClient.getInstance().getMaintenanceById(MainApp.requestHeader,
                    MainApp.currentMaintenanceId);
            call.enqueue(new Callback<ResponseModel>() {

                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                    if (response.isSuccessful()) {
                        try {
                            MechanicMaintenance.maintenance = Utilities.mapObject(response.body().getOutput());
                        } catch (Exception ex) {
                            MechanicMaintenance.maintenance = null;
                            ex.printStackTrace();
                        }

                        if (callbackMaster != null) {
                            callbackMaster.run();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    t.printStackTrace();
                }
            });

            Call<ResponseMaintenanceDescriptionOutput> call3 = APIClient.getInstance()
                    .getMaintenanceDescription(MainApp.requestHeader, 1, 100, MainApp.currentMaintenanceId);
            call3.enqueue(new Callback<ResponseMaintenanceDescriptionOutput>() {

                @Override
                public void onResponse(Call<ResponseMaintenanceDescriptionOutput> call,
                        Response<ResponseMaintenanceDescriptionOutput> response) {
                    if (response.isSuccessful()) {
                        try {
                            MechanicMaintenance.maintenanceDescription = response.body().getOutput();
                        } catch (Exception ex) {
                            MechanicMaintenance.maintenanceDescription = new ArrayList<>();
                            ex.printStackTrace();
                        }

                        if (callbackDesc != null) {
                            callbackDesc.run();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseMaintenanceDescriptionOutput> call, Throwable t) {
                    t.printStackTrace();
                }
            });

            Call<ResponseMaintenanceDetailsOutput> call2 = APIClient.getInstance()
                    .getMaintenanceDetails(MainApp.requestHeader, 1, 100, MainApp.currentMaintenanceId);
            call2.enqueue(new Callback<ResponseMaintenanceDetailsOutput>() {

                @Override
                public void onResponse(Call<ResponseMaintenanceDetailsOutput> call,
                        Response<ResponseMaintenanceDetailsOutput> response) {
                    if (response.isSuccessful()) {
                        try {
                            MechanicMaintenance.maintenanceDetail = response.body().getOutput();
                        } catch (Exception ex) {
                            MechanicMaintenance.maintenanceDetail = new ArrayList<>();
                            ex.printStackTrace();
                        }

                        if (callbackDetail != null) {
                            callbackDetail.run();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseMaintenanceDetailsOutput> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            System.out.println("---- no service");
        }
    }

    private void drawerAction() {
        // TitleBar.setOnAction((ActionEvent evt) -> {
        // navigationUI.back_dashboard(sessionFMS.getSessionFms("sesi_level"),evt);
        // });
        back.setOnAction((ActionEvent evt) -> {

            Utilities.confirm(stackPane, "Yakin akan keluar dari proses maintenance ?", new Runnable() {

                @Override
                public void run() {
                    try {
                        NavigationUI.windowsNewScene(p("app.linkMechanicTask"), p("app.titleMechanicTash"), evt);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, null);
        });
    }

    @FXML
    public void logoutAction(ActionEvent event) throws Exception {
        clock.stop();
        Utilities.confirm(stackPane, "Yakin akan keluar ?", new Runnable() {

            @Override
            public void run() {
                Utilities.doLogOut(event, null);
            }
        }, new Runnable() {

            @Override
            public void run() {

            }
        });
    }

    public void showMaster() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                String type = MechanicMaintenance.maintenance.get("type") != null
                        ? MechanicMaintenance.maintenance.get("type").toString()
                        : null;
                String title = type == null ? p("app.titleMechanicMaintenance")
                        : p("app.titleMechanicMaintenance") + " " + type.toUpperCase();
                TitleBar.setText(title);

                String wo = MechanicMaintenance.maintenance.get("wo_number") != null
                        ? MechanicMaintenance.maintenance.get("wo_number").toString()
                        : "-";
                textWO.setText("Work Order : " + wo);

                if (type.trim().equals(ConstantValues.MAINTENANCE_SCHEDULE)) {
                    VBox wrap = new VBox();
                    wrap.setSpacing(10);
                    wrap.setPadding(new Insets(10));
                    Label headLabel = new Label();
                    headLabel.setStyle("--fx-font-size:15px; -fx-font-weight:normal;");
                    headLabel.setText("Status :");
                    Label contentLabel = new Label();
                    contentLabel.setStyle("--fx-font-size:20px;  -fx-font-weight:normal;");
                    contentLabel.setPrefWidth(220.0);
                    contentLabel.setAlignment(Pos.CENTER);
                    contentLabel.setTextFill(Color.YELLOW);
                    String stat = MechanicMaintenance.maintenance.get("status").toString();
                    contentLabel.setText(stat.toUpperCase());
                    maintenanceStatus = stat;
                    wrap.getChildren().addAll(headLabel, contentLabel);
                    if (stat.equals(ConstantValues.MAINTENANCE_STATUS_NOT)) {
                        wrap.setStyle("-fx-background-color: " + ConstantValues.COLOR_DOWN + ";");
                        buttonDone.setVisible(false);
                    }
                    if (stat.equals(ConstantValues.MAINTENANCE_STATUS_PROCESS)) {
                        wrap.setStyle("-fx-background-color: " + ConstantValues.COLOR_MAINTENANCE + ";");
                        buttonDone.setVisible(true);
                    }
                    if (stat.equals(ConstantValues.MAINTENANCE_STATUS_DONE)) {
                        wrap.setStyle("-fx-background-color: " + ConstantValues.COLOR_RFU + ";");
                        buttonDone.setVisible(false);
                    }
                    gridInfo.add(wrap, 0, 0);

                    VBox wrap2 = new VBox();
                    wrap2.setSpacing(10);
                    wrap2.setPadding(new Insets(10));
                    wrap2.getStyleClass().add("bg-dark-sub");
                    Label headLabel2 = new Label();
                    headLabel2.setStyle("--fx-font-size:15px; -fx-font-weight:normal;");
                    headLabel2.setText("Maintenance :");
                    Label contentLabel2 = new Label();
                    contentLabel2.setStyle("--fx-font-size:20px;  -fx-font-weight:normal;");
                    contentLabel2.setPrefWidth(220.0);
                    contentLabel2.setAlignment(Pos.CENTER);
                    contentLabel2.setTextFill(Color.YELLOW);
                    Map<String, Object> data = Utilities
                            .mapObject(MechanicMaintenance.maintenance.get("maintenance_group_id"));
                    String value2 = data.get("name").toString();
                    contentLabel2.setText(value2.toUpperCase());
                    wrap2.getChildren().addAll(headLabel2, contentLabel2);
                    gridInfo.add(wrap2, 1, 0);

                    VBox wrap3 = new VBox();
                    wrap3.setSpacing(10);
                    wrap3.setPadding(new Insets(10));
                    wrap3.getStyleClass().add("bg-dark-sub");
                    Label headLabel3 = new Label();
                    headLabel3.setStyle("--fx-font-size:15px; -fx-font-weight:normal;");
                    headLabel3.setText("Schedule at :");
                    Label contentLabel3 = new Label();
                    contentLabel3.setStyle("--fx-font-size:20px;  -fx-font-weight:normal;");
                    contentLabel3.setPrefWidth(220.0);
                    contentLabel3.setAlignment(Pos.CENTER);
                    contentLabel3.setTextFill(Color.YELLOW);
                    if (MechanicMaintenance.maintenance.get("scheduled_at") != null) {
                        Double tmp = Double.parseDouble(MechanicMaintenance.maintenance.get("scheduled_at").toString());
                        String value3 = Utilities.timeStampToDate(tmp.longValue(), "tanggal");
                        contentLabel3.setText(value3.toUpperCase());
                    } else {
                        contentLabel3.setText("-");
                    }
                    wrap3.getChildren().addAll(headLabel3, contentLabel3);
                    gridInfo.add(wrap3, 2, 0);

                } else if (type.trim().equals(ConstantValues.MAINTENANCE_UNSCHEDULE)) {
                    VBox wrap = new VBox();
                    wrap.setSpacing(10);
                    wrap.setPadding(new Insets(10));
                    Label headLabel = new Label();
                    headLabel.setStyle("--fx-font-size:15px; -fx-font-weight:normal;");
                    headLabel.setText("Status :");
                    Label contentLabel = new Label();
                    contentLabel.setStyle("--fx-font-size:20px;  -fx-font-weight:normal;");
                    contentLabel.setPrefWidth(150.0);
                    contentLabel.setAlignment(Pos.CENTER);
                    contentLabel.setTextFill(Color.YELLOW);
                    String stat = MechanicMaintenance.maintenance.get("status").toString();
                    contentLabel.setText(stat.toUpperCase());
                    maintenanceStatus = stat;
                    wrap.getChildren().addAll(headLabel, contentLabel);
                    if (stat.equals(ConstantValues.MAINTENANCE_STATUS_NOT)) {
                        wrap.setStyle("-fx-background-color: " + ConstantValues.COLOR_DOWN + ";");
                        buttonDone.setVisible(false);
                    }
                    if (stat.equals(ConstantValues.MAINTENANCE_STATUS_PROCESS)) {
                        wrap.setStyle("-fx-background-color: " + ConstantValues.COLOR_MAINTENANCE + ";");
                        buttonDone.setVisible(true);
                    }
                    if (stat.equals(ConstantValues.MAINTENANCE_STATUS_DONE)) {
                        wrap.setStyle("-fx-background-color: " + ConstantValues.COLOR_RFU + ";");
                        buttonDone.setVisible(false);
                    }
                    gridInfo.add(wrap, 0, 0);

                    VBox wrap2 = new VBox();
                    wrap2.setSpacing(10);
                    wrap2.setPadding(new Insets(10));
                    wrap2.getStyleClass().add("bg-dark-sub");
                    Label headLabel2 = new Label();
                    headLabel2.setStyle("--fx-font-size:15px; -fx-font-weight:normal;");
                    headLabel2.setText("Problem :");
                    Label contentLabel2 = new Label();
                    contentLabel2.setStyle("--fx-font-size:20px;  -fx-font-weight:normal;");
                    contentLabel2.setPrefWidth(290.0);
                    contentLabel2.setAlignment(Pos.CENTER);
                    contentLabel2.setTextFill(Color.YELLOW);
                    if (MechanicMaintenance.maintenance.get("problem_id") != null) {
                        Map<String, Object> data = Utilities
                                .mapObject(MechanicMaintenance.maintenance.get("problem_id"));
                        String value2 = data.get("name").toString();
                        contentLabel2.setText(value2.toUpperCase());
                    } else {
                        contentLabel2.setText("-");
                    }
                    wrap2.getChildren().addAll(headLabel2, contentLabel2);
                    gridInfo.add(wrap2, 1, 0);

                    VBox wrap3 = new VBox();
                    wrap3.setSpacing(10);
                    wrap3.setPadding(new Insets(10));
                    wrap3.getStyleClass().add("bg-dark-sub");
                    Label headLabel3 = new Label();
                    headLabel3.setStyle("--fx-font-size:15px; -fx-font-weight:normal;");
                    headLabel3.setText("Request at :");
                    Label contentLabel3 = new Label();
                    contentLabel3.setStyle("--fx-font-size:20px;  -fx-font-weight:normal;");
                    contentLabel3.setPrefWidth(220.0);
                    contentLabel3.setAlignment(Pos.CENTER);
                    contentLabel3.setTextFill(Color.YELLOW);
                    if (MechanicMaintenance.maintenance.get("created_at") != null) {
                        Double tmp = Double.parseDouble(MechanicMaintenance.maintenance.get("created_at").toString());
                        String value3 = Utilities.timeStampToDate(tmp.longValue(), "tanggal");
                        contentLabel3.setText(value3.toUpperCase());
                    } else {
                        contentLabel3.setText("-");
                    }
                    wrap3.getChildren().addAll(headLabel3, contentLabel3);
                    gridInfo.add(wrap3, 2, 0);

                } else if (type.trim().equals(ConstantValues.MAINTENANCE_BACKLOG)) {
                    VBox wrap = new VBox();
                    wrap.setSpacing(10);
                    wrap.setPadding(new Insets(10));
                    Label headLabel = new Label();
                    headLabel.setStyle("--fx-font-size:15px; -fx-font-weight:normal;");
                    headLabel.setText("Status :");
                    Label contentLabel = new Label();
                    contentLabel.setStyle("--fx-font-size:20px;  -fx-font-weight:normal;");
                    contentLabel.setPrefWidth(150.0);
                    contentLabel.setAlignment(Pos.CENTER);
                    contentLabel.setTextFill(Color.YELLOW);
                    String stat = MechanicMaintenance.maintenance.get("status").toString();
                    contentLabel.setText(stat.toUpperCase());
                    maintenanceStatus = stat;
                    wrap.getChildren().addAll(headLabel, contentLabel);
                    if (stat.equals(ConstantValues.MAINTENANCE_STATUS_NOT)) {
                        wrap.setStyle("-fx-background-color: " + ConstantValues.COLOR_DOWN + ";");
                        buttonDone.setVisible(false);
                    }
                    if (stat.equals(ConstantValues.MAINTENANCE_STATUS_PROCESS)) {
                        wrap.setStyle("-fx-background-color: " + ConstantValues.COLOR_MAINTENANCE + ";");
                        buttonDone.setVisible(true);
                    }
                    if (stat.equals(ConstantValues.MAINTENANCE_STATUS_DONE)) {
                        wrap.setStyle("-fx-background-color: " + ConstantValues.COLOR_RFU + ";");
                        buttonDone.setVisible(false);
                    }
                    gridInfo.add(wrap, 0, 0);

                    VBox wrap2 = new VBox();
                    wrap2.setSpacing(10);
                    wrap2.setPadding(new Insets(10));
                    wrap2.getStyleClass().add("bg-dark-sub");
                    Label headLabel2 = new Label();
                    headLabel2.setStyle("--fx-font-size:15px; -fx-font-weight:normal;");
                    headLabel2.setText("Problem :");
                    Label contentLabel2 = new Label();
                    contentLabel2.setStyle("--fx-font-size:20px;  -fx-font-weight:normal;");
                    contentLabel2.setPrefWidth(290.0);
                    contentLabel2.setAlignment(Pos.CENTER);
                    contentLabel2.setTextFill(Color.YELLOW);
                    if (MechanicMaintenance.maintenance.get("problem_id") != null) {
                        Map<String, Object> data = Utilities
                                .mapObject(MechanicMaintenance.maintenance.get("problem_id"));
                        String value2 = data.get("name").toString();
                        contentLabel2.setText(value2.toUpperCase());
                    } else {
                        contentLabel2.setText("-");
                    }
                    wrap2.getChildren().addAll(headLabel2, contentLabel2);
                    gridInfo.add(wrap2, 1, 0);

                    VBox wrap3 = new VBox();
                    wrap3.setSpacing(10);
                    wrap3.setPadding(new Insets(10));
                    wrap3.getStyleClass().add("bg-dark-sub");
                    Label headLabel3 = new Label();
                    headLabel3.setStyle("--fx-font-size:15px; -fx-font-weight:normal;");
                    headLabel3.setText("Request at :");
                    Label contentLabel3 = new Label();
                    contentLabel3.setStyle("--fx-font-size:20px;  -fx-font-weight:normal;");
                    contentLabel3.setPrefWidth(220.0);
                    contentLabel3.setAlignment(Pos.CENTER);
                    contentLabel3.setTextFill(Color.YELLOW);
                    if (MechanicMaintenance.maintenance.get("created_at") != null) {
                        Double tmp = Double.parseDouble(MechanicMaintenance.maintenance.get("created_at").toString());
                        String value3 = Utilities.timeStampToDate(tmp.longValue(), "tanggal");
                        contentLabel3.setText(value3.toUpperCase());
                    } else {
                        contentLabel3.setText("-");
                    }
                    wrap3.getChildren().addAll(headLabel3, contentLabel3);
                    gridInfo.add(wrap3, 2, 0);
                }

                loading.setVisible(false);

                if (!maintenanceStatus.equals(ConstantValues.MAINTENANCE_STATUS_PROCESS)) {
                    btnNewMaintenance.setDisable(true);
                    btnNewProcess.setDisable(true);
                }
            }
        });
    }

    public void showDescription() {
        tblMaintenance.getItems().clear();
        /** cek apakah sudah ada data maintenance description current shift */
        for (MaintenanceDescription ma : MechanicMaintenance.maintenanceDescription) {
            boolean isCurrentShift = Utilities.isSameDateAndShift(ma.getDate(), ma.getCdShift().getCd(),
                    Utilities.getCurrentUnix(), Utilities.getCurrentShift());
            if (isCurrentShift) {
                btnNewMaintenance.setDisable(true);
            }
        }
        tblMaintenance.getItems().addAll(MechanicMaintenance.maintenanceDescription);

        sortMaintenance();
    }

    public void showDetail() {
    }

    @FXML
    public void newMaintenance(ActionEvent event) throws Exception {
        formMaintenance(event, null, "new");
    }

    @FXML
    public void newProcess(ActionEvent event) throws Exception {
        formProcess(event, null, "new");
    }

    public void formMaintenance(Event event, MaintenanceDescription data, String type) {
        if (formMaintenanceShow) {
            return;
        }

        Boolean isEdit = type.equals("edit") ? true : false;
        Boolean isView = type.equals("view") ? true : false;
        Boolean isNew = type.equals("new") ? true : false;

        String currentDate = Utilities.getHariTanggal();

        JFXDialogLayout content = new JFXDialogLayout();
        Text headText = new Text("New Maintenance");
        headText.setStyle("-fx-font-size:23px;");
        content.setHeading(headText);
        content.setPrefSize(700, 500);
        content.getStyleClass().add("bg-dark-light");

        if (isEdit) {
            headText.setText("Edit Maintenance");
        } else if (isView) {
            headText.setText("View Maintenance");
        }

        VBox vbox = new VBox();
        vbox.setSpacing(20.0);

        HBox hbDate = new HBox();
        Label lblDate = new Label("Date");
        lblDate.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
        lblDate.setPrefWidth(220.0);
        Label txtDate = new Label();
        txtDate.setStyle("-fx-font-size:20px;-fx-text-fill:BLACK;");
        hbDate.getChildren().add(lblDate);
        hbDate.getChildren().add(txtDate);
        vbox.getChildren().add(hbDate);

        if (isNew) {
            txtDate.setText(currentDate);
        } else if (data != null) {
            currentDate = Utilities.timeStampToDate(data.getDate(), "hari");
            txtDate.setText(currentDate);
        }

        HBox hbShift = new HBox();
        Label lblShift = new Label("Shift");
        lblShift.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
        lblShift.setPrefWidth(220.0);
        Label txtShift = new Label();
        txtShift.setStyle("-fx-font-size:20px;-fx-text-fill:BLACK;");
        hbShift.getChildren().add(lblShift);
        hbShift.getChildren().add(txtShift);
        vbox.getChildren().add(hbShift);

        if (isNew) {
            if (Utilities.getCurrentShift().equals(ConstantValues.SHIFT_DAY)) {
                txtShift.setText("Day");
            } else if (Utilities.getCurrentShift().equals(ConstantValues.SHIFT_NIGHT)) {
                txtShift.setText("Night");
            }
        } else if (data != null) {
            txtShift.setText(data.getCdShift().getName());
        }

        HBox hbSupervisor = new HBox();
        Label lblSup = new Label("Supervisor");
        lblSup.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
        lblSup.setPrefWidth(220.0);
        Integer selected = null;
        ComboBox comboSup = new ComboBox(FXCollections.observableArrayList(MechanicMaintenance.supervisor));
        for (Map<String, Object> item : MechanicMaintenance.supervisor) {
            Map<String, Object> user = item.get("user_id") != null ? Utilities.mapObject(item.get("user_id")) : null;
            if (data != null) {
                String id = user.get("id").toString();
                if (data.getSupervisorId().getId().equals(id)) {
                    selected = MechanicMaintenance.supervisor.indexOf(item);
                }
            }
        }
        comboSup.setStyle("-fx-font-size:18px;");
        comboSup.setButtonCell(cellFactoryComboUser.call(null));
        comboSup.setCellFactory(cellFactoryComboUser);
        hbSupervisor.getChildren().add(lblSup);
        hbSupervisor.getChildren().add(comboSup);
        vbox.getChildren().add(hbSupervisor);
        if (data != null && selected != null) {
            comboSup.getSelectionModel().select(selected.intValue());
        }
        if (isView) {
            comboSup.setDisable(true);
            comboSup.setOpacity(1.0);
        } else {
            comboSup.setDisable(false);
        }

        HBox hbForeman = new HBox();
        Label lblFore = new Label("Foreman");
        lblFore.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
        lblFore.setPrefWidth(220.0);
        ComboBox comboFore = new ComboBox(FXCollections.observableArrayList(MechanicMaintenance.foreman));
        selected = null;
        for (Map<String, Object> item : MechanicMaintenance.foreman) {
            Map<String, Object> user = item.get("user_id") != null ? Utilities.mapObject(item.get("user_id")) : null;
            if (data != null) {
                String id = user.get("id").toString();
                if (data.getForemanId().getId().equals(id)) {
                    selected = MechanicMaintenance.foreman.indexOf(item);
                }
            }
        }
        comboFore.setStyle("-fx-font-size:18px;");
        comboFore.setButtonCell(cellFactoryComboUser.call(null));
        comboFore.setCellFactory(cellFactoryComboUser);
        hbForeman.getChildren().add(lblFore);
        hbForeman.getChildren().add(comboFore);
        vbox.getChildren().add(hbForeman);
        if (data != null && selected != null) {
            comboFore.getSelectionModel().select(selected.intValue());
        }
        if (isView) {
            comboFore.setDisable(true);
            comboFore.setOpacity(1.0);
        } else {
            comboFore.setDisable(false);
        }

        HBox hbHM = new HBox();
        Label lblHM = new Label("Hour Meter");
        lblHM.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
        lblHM.setPrefWidth(220.0);
        TextField txtHM = new TextField();
        txtHM.setStyle("-fx-font-size:20px;");
        txtHM.getProperties().put("vkType", "numeric");
        hbHM.getChildren().add(lblHM);
        hbHM.getChildren().add(txtHM);
        vbox.getChildren().add(hbHM);
        if (data != null && selected != null) {
            txtHM.setText(data.getHm().toString());
        }
        if (isView) {
            txtHM.setDisable(true);
        } else {
            txtHM.setDisable(false);
        }

        HBox hbDesc = new HBox();
        Label lblDesc = new Label("Description");
        lblDesc.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
        lblDesc.setPrefWidth(220.0);
        TextArea textDesc = new TextArea();
        textDesc.setStyle("-fx-font-size:20px;");
        textDesc.setPrefWidth(400);
        hbDesc.getChildren().add(lblDesc);
        hbDesc.getChildren().add(textDesc);
        vbox.getChildren().add(hbDesc);
        if (data != null && selected != null) {
            textDesc.setText(data.getDescription());
        }
        if (isView) {
            textDesc.setDisable(true);
        } else {
            textDesc.setDisable(false);
        }

        content.setBody(vbox);
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER, false);

        JFXButton simpan = new JFXButton("      Simpan      ");
        JFXButton batal = new JFXButton("      Batal      ");

        simpan.getStyleClass().add("button-raised");
        simpan.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                boolean isCurrentShift = false;
                for (MaintenanceDescription ma : MechanicMaintenance.maintenanceDescription) {
                    boolean check = Utilities.isSameDateAndShift(ma.getDate(), ma.getCdShift().getCd(),
                            Utilities.getCurrentUnix(), Utilities.getCurrentShift());
                    if (check) {
                        isCurrentShift = check;
                    }
                }

                if (isCurrentShift && isNew) {
                    Utilities.showMessageDialog("Failed", "", "Data maintenance pada shift ini sudah ada");
                    return;
                }

                MaintenanceDescriptionPost mdp = new MaintenanceDescriptionPost();
                mdp.setMaintenanceId(MechanicMaintenance.maintenance.get("id").toString());
                if (isNew) {
                    mdp.setDate("current");
                } else {
                    mdp.setDate(data.getDate().toString());
                }
                mdp.setCdShift(Utilities.getCurrentShift());
                mdp.setWo_number("");
                mdp.setDescription(textDesc.getText());
                Map<String, Object> crewSup = (Map<String, Object>) comboSup.getValue();
                if (crewSup == null) {
                    Utilities.showMessageDialog("Supervisor Required", "", "Supervisor harus diisi");
                    return;
                } else {
                    Map<String, Object> sup = crewSup.get("user_id") != null
                            ? Utilities.mapObject(crewSup.get("user_id"))
                            : null;
                    mdp.setSupervisorId(sup.get("id").toString());
                }
                Map<String, Object> crewFore = (Map<String, Object>) comboFore.getValue();
                if (crewFore == null) {
                    Utilities.showMessageDialog("Foreman Required", "", "Foreman harus diisi");
                    return;
                } else {
                    Map<String, Object> fore = crewFore.get("user_id") != null
                            ? Utilities.mapObject(crewFore.get("user_id"))
                            : null;
                    mdp.setForemanId(fore.get("id").toString());
                }
                if (txtHM.getText().length() == 0) {
                    Utilities.showMessageDialog("HM Required", "", "Hour meter harus diisi");
                    return;
                } else {
                    Double hmd = Double.parseDouble(txtHM.getText());
                    if (hmd <= MainApp.latestHourMeter) {
                        Utilities.showMessageDialog("HM Invalid", "",
                                "Hour meter harus lebih besar dari HM sebelumnya (" + MainApp.latestHourMeter.toString()
                                        + ")");
                        return;
                    }
                    mdp.setHm(hmd);
                }

                loading.setVisible(true);
                simpan.setDisable(true);
                batal.setDisable(true);

                if (isNew) {

                    Utilities.postMaintenanceDescription(mdp, new CustomRunnableX<MaintenanceDescription>() {

                        @Override
                        public void run() {
                            super.run();
                            tblMaintenance.getItems().removeAll(MechanicMaintenance.maintenanceDescription);
                            MechanicMaintenance.maintenanceDescription.add(getData());
                            tblMaintenance.getItems().addAll(MechanicMaintenance.maintenanceDescription);
                            tblMaintenance.refresh();

                            showProcessList(getData());

                            btnNewMaintenance.setDisable(true);
                            formMaintenanceShow = false;
                            loading.setVisible(false);
                            simpan.setDisable(false);
                            batal.setDisable(false);
                            dialog.close();
                        }

                    });

                } else if (isEdit) {

                    mdp.setId(data.getId());

                    Utilities.postEditMaintenanceDescription(mdp, new CustomRunnableX<MaintenanceDescription>() {

                        @Override
                        public void run() {
                            super.run();
                            tblMaintenance.getItems().removeAll(MechanicMaintenance.maintenanceDescription);
                            MechanicMaintenance.maintenanceDescription.remove(data);
                            MechanicMaintenance.maintenanceDescription.add(getData());
                            tblMaintenance.getItems().addAll(MechanicMaintenance.maintenanceDescription);
                            tblMaintenance.refresh();

                            showProcessList(data);

                            formMaintenanceShow = false;
                            loading.setVisible(false);
                            simpan.setDisable(false);
                            batal.setDisable(false);
                            dialog.close();
                        }

                    });

                } else {

                    formMaintenanceShow = false;
                    loading.setVisible(false);
                    simpan.setDisable(false);
                    batal.setDisable(false);
                    dialog.close();

                }

            }
        });

        batal.getStyleClass().add("button-cancel");
        batal.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                formMaintenanceShow = false;
                dialog.close();
            }
        });

        if (isView) {
            batal.setText("    Tutup    ");
            content.setActions(batal);
        } else {
            if (isEdit) {
                batal.setText("    Tutup    ");
            }
            content.setActions(simpan, batal);
        }

        dialog.show();

        formMaintenanceShow = true;
    }

    public void formProcess(Event event, MaintenanceDetails data, String type) {
        if (formProcessShow) {
            return;
        }

        MaintenanceDescription parent = tblMaintenance.getSelectionModel().getSelectedItem();

        if (parent == null) {
            Utilities.showMessageDialog("Failed", "", "Data shift belum dipilih");
            return;
        }

        Boolean isEdit = type.equals("edit") ? true : false;
        Boolean isView = type.equals("view") ? true : false;
        Boolean isNew = type.equals("new") ? true : false;

        Boolean isCurrentShift = Utilities.isSameDateAndShift(parent.getDate(), parent.getCdShift().getCd(),
                Utilities.getCurrentUnix(), Utilities.getCurrentShift());
        if (!isCurrentShift) {
            if (isNew) {
                Utilities.showMessageDialog("Failed", "", "Tidak dapat menambah data baru");
                return;
            }
            if (isEdit) {
                Utilities.showMessageDialog("Failed", "", "Tidak dapat mengubah data");
                return;
            }
        }

        String currentDate = Utilities.getHariTanggal();

        JFXDialogLayout content = new JFXDialogLayout();
        Text headText = new Text("New Process");
        headText.setStyle("-fx-font-size:23px;");
        content.setHeading(headText);
        content.setPrefSize(700, 400);
        content.getStyleClass().add("bg-dark-light");

        if (isEdit) {
            headText.setText("Edit Process");
        } else if (isView) {
            headText.setText("View Process");
        }

        VBox vbox = new VBox();
        vbox.setSpacing(20.0);

        HBox hbDate = new HBox();
        Label lblDate = new Label("Date");
        lblDate.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
        lblDate.setPrefWidth(150.0);
        Label txtDate = new Label();
        txtDate.setStyle("-fx-font-size:20px;-fx-text-fill:BLACK;");
        hbDate.getChildren().add(lblDate);
        hbDate.getChildren().add(txtDate);
        vbox.getChildren().add(hbDate);

        if (isNew) {
            txtDate.setText(currentDate);
        } else if (data != null) {
            currentDate = Utilities.timeStampToDate(data.getDate(), "hari");
            txtDate.setText(currentDate);
        }

        HBox hbShift = new HBox();
        Label lblShift = new Label("Shift");
        lblShift.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
        lblShift.setPrefWidth(150.0);
        Label txtShift = new Label();
        txtShift.setStyle("-fx-font-size:20px;-fx-text-fill:BLACK;");
        hbShift.getChildren().add(lblShift);
        hbShift.getChildren().add(txtShift);
        vbox.getChildren().add(hbShift);

        if (isNew) {
            if (Utilities.getCurrentShift().equals(ConstantValues.SHIFT_DAY)) {
                txtShift.setText("Day");
            } else if (Utilities.getCurrentShift().equals(ConstantValues.SHIFT_NIGHT)) {
                txtShift.setText("Night");
            }
        } else if (data != null) {
            txtShift.setText(data.getCdShift().getName());
        }

        HBox hbProblem = new HBox();
        Label lblProblem = new Label("Problem");
        lblProblem.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
        lblProblem.setPrefWidth(150.0);
        Integer selected = null;
        ComboBox comboProblem = new ComboBox(FXCollections.observableArrayList(MechanicMaintenance.problem));
        for (Constant item : MechanicMaintenance.problem) {
            if (data != null) {
                String cdP = data.getProblemId().getCd();
                if (item.getCd().equals(cdP)) {
                    selected = MechanicMaintenance.problem.indexOf(item);
                }
            }
        }
        comboProblem.setStyle("-fx-font-size:18px;");
        comboProblem.setButtonCell(cellFactoryComboConstant.call(null));
        comboProblem.setCellFactory(cellFactoryComboConstant);
        hbProblem.getChildren().add(lblProblem);
        hbProblem.getChildren().add(comboProblem);
        vbox.getChildren().add(hbProblem);
        if (data != null && selected != null) {
            comboProblem.getSelectionModel().select(selected.intValue());
        }
        if (isView) {
            comboProblem.setDisable(true);
            comboProblem.setOpacity(1.0);
        } else {
            comboProblem.setDisable(false);
        }

        HBox hbStatus = new HBox();
        Label lblStatus = new Label("Status");
        lblStatus.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
        lblStatus.setPrefWidth(150.0);
        selected = null;
        ComboBox comboStatus = new ComboBox(FXCollections.observableArrayList(MechanicMaintenance.status));
        for (Constant item : MechanicMaintenance.status) {
            if (data != null) {
                String cdS = data.getStatusId().getCd();
                if (item.getCd().equals(cdS)) {
                    selected = MechanicMaintenance.status.indexOf(item);
                }
            }
        }
        comboStatus.setStyle("-fx-font-size:18px;");
        comboStatus.setButtonCell(cellFactoryComboConstant.call(null));
        comboStatus.setCellFactory(cellFactoryComboConstant);
        hbStatus.getChildren().add(lblStatus);
        hbStatus.getChildren().add(comboStatus);
        vbox.getChildren().add(hbStatus);
        if (data != null && selected != null) {
            comboStatus.getSelectionModel().select(selected.intValue());
        }
        if (isView) {
            comboStatus.setDisable(true);
            comboStatus.setOpacity(1.0);
        } else {
            comboStatus.setDisable(false);
        }

        HBox hbMechanic = new HBox();
        Label lblMechanic = new Label("Mekanik");
        lblMechanic.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
        lblMechanic.setPrefWidth(150.0);
        selected = null;
        ComboBox comboMechanic = new ComboBox(FXCollections.observableArrayList(MechanicMaintenance.mechanic));
        for (Map<String, Object> item : MechanicMaintenance.mechanic) {
            Map<String, Object> user = item.get("user_id") != null ? Utilities.mapObject(item.get("user_id")) : null;
            if (data != null) {
                String id = user.get("id").toString();
                if (data.getMechanicId().getId().equals(id)) {
                    selected = MechanicMaintenance.mechanic.indexOf(item);
                }
            }
        }
        comboMechanic.setStyle("-fx-font-size:18px;");
        comboMechanic.setButtonCell(cellFactoryComboUser.call(null));
        comboMechanic.setCellFactory(cellFactoryComboUser);
        hbMechanic.getChildren().add(lblMechanic);
        hbMechanic.getChildren().add(comboMechanic);
        vbox.getChildren().add(hbMechanic);
        if (data != null && selected != null) {
            comboMechanic.getSelectionModel().select(selected.intValue());
        }
        if (isView) {
            comboMechanic.setDisable(true);
            comboMechanic.setOpacity(1.0);
        } else {
            comboMechanic.setDisable(false);
        }

        if (isView) {

            HBox hbStart = new HBox();
            Label lblStart = new Label("Start");
            lblStart.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
            lblStart.setPrefWidth(150.0);
            TextField txtStartJam = new TextField();
            txtStartJam.setStyle("-fx-font-size:20px;");
            txtStartJam.getProperties().put("vkType", "numeric");
            txtStartJam.setPrefWidth(100.0);
            TextField txtStartMenit = new TextField();
            txtStartMenit.setStyle("-fx-font-size:20px;");
            txtStartMenit.getProperties().put("vkType", "numeric");
            txtStartMenit.setPrefWidth(100.0);
            Label lblSep1 = new Label(":");
            lblSep1.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
            hbStart.getChildren().add(lblStart);
            hbStart.getChildren().add(txtStartJam);
            hbStart.getChildren().add(lblSep1);
            hbStart.getChildren().add(txtStartMenit);
            vbox.getChildren().add(hbStart);
            if (data != null && selected != null) {
                txtStartJam.setText(data.getStartHour());
                txtStartMenit.setText(data.getStartMinute());
            }
            if (isView) {
                txtStartJam.setDisable(true);
                txtStartMenit.setDisable(true);
            } else {
                txtStartJam.setDisable(false);
                txtStartMenit.setDisable(false);
            }

            HBox hbEnd = new HBox();
            Label lblEnd = new Label("Stop");
            lblEnd.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
            lblEnd.setPrefWidth(150.0);
            TextField txtEndJam = new TextField();
            txtEndJam.setStyle("-fx-font-size:20px;");
            txtEndJam.setPrefWidth(100.0);
            txtEndJam.getProperties().put("vkType", "numeric");
            TextField txtEndMenit = new TextField();
            txtEndMenit.setStyle("-fx-font-size:20px;");
            txtEndMenit.getProperties().put("vkType", "numeric");
            txtEndMenit.setPrefWidth(100.0);
            Label lblSep2 = new Label(":");
            lblSep2.setStyle("-fx-text-fill:black;-fx-font-size:20px;");
            hbEnd.getChildren().add(lblEnd);
            hbEnd.getChildren().add(txtEndJam);
            hbEnd.getChildren().add(lblSep2);
            hbEnd.getChildren().add(txtEndMenit);
            vbox.getChildren().add(hbEnd);
            if (data != null && selected != null) {
                txtEndJam.setText(data.getEndHour());
                txtEndMenit.setText(data.getEndMinute());
            }
            if (isView) {
                txtEndJam.setDisable(true);
                txtEndMenit.setDisable(true);
            } else {
                txtEndJam.setDisable(false);
                txtEndMenit.setDisable(false);
            }
        }

        content.setBody(vbox);
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER, false);

        JFXButton simpan = new JFXButton("      Simpan      ");
        JFXButton batal = new JFXButton("      Batal      ");
        JFXButton stop = new JFXButton("      Stop      ");

        stop.getStyleClass().add("button-other");
        simpan.getStyleClass().add("button-raised");
        simpan.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                MaintenanceDetailsPost mdp = new MaintenanceDetailsPost();
                if (isNew) {
                    mdp.setDate("current");
                } else {
                    mdp.setDate(data.getDate().toString());
                }
                mdp.setMaintenanceId(MechanicMaintenance.maintenance.get("id").toString());
                mdp.setCdShift(Utilities.getCurrentShift());
                mdp.setStartTime(Utilities.getCurrentHourMinute());

                Constant pro = (Constant) comboProblem.getValue();
                if (pro == null) {
                    Utilities.showMessageDialog("Problem Required", "", "Problem harus diisi");
                    return;
                } else {
                    mdp.setProblemId(pro.getCd());
                }

                Constant stat = (Constant) comboStatus.getValue();
                if (stat == null) {
                    Utilities.showMessageDialog("Status Required", "", "Status harus diisi");
                    return;
                } else {
                    mdp.setStatusId(stat.getCd());
                }

                Map<String, Object> mecha = (Map<String, Object>) comboMechanic.getValue();
                if (mecha == null) {
                    Utilities.showMessageDialog("Mechanic Required", "", "Mekanik harus diisi");
                    return;
                } else {
                    Map<String, Object> mekanik = mecha.get("user_id") != null
                            ? Utilities.mapObject(mecha.get("user_id"))
                            : null;
                    mdp.setMechanicId(mekanik.get("id").toString());
                }

                loading.setVisible(true);
                simpan.setDisable(true);
                batal.setDisable(true);

                if (isNew) {

                    Utilities.postMaintenanceDetails(mdp, new CustomRunnableX<MaintenanceDetails>() {

                        @Override
                        public void run() {
                            super.run();
                            tblProcess.getItems().removeAll(MechanicMaintenance.maintenanceDetail);
                            MechanicMaintenance.maintenanceDetail.add(getData());
                            // tblProcess.getItems().addAll(MechanicMaintenance.maintenanceDetail);
                            // tblProcess.refresh();
                            showProcessList(parent);

                            formProcessShow = false;
                            loading.setVisible(false);
                            simpan.setDisable(false);
                            batal.setDisable(false);
                            dialog.close();
                        }

                    });

                } else if (isEdit) {

                    mdp.setId(data.getId());

                    Utilities.postEditMaintenanceDetails(mdp, new CustomRunnableX<MaintenanceDetails>() {

                        @Override
                        public void run() {
                            super.run();
                            tblProcess.getItems().removeAll(MechanicMaintenance.maintenanceDetail);
                            MechanicMaintenance.maintenanceDetail.remove(data);
                            MechanicMaintenance.maintenanceDetail.add(getData());
                            // tblProcess.getItems().addAll(MechanicMaintenance.maintenanceDetail);
                            // tblProcess.refresh();
                            showProcessList(parent);

                            formProcessShow = false;
                            loading.setVisible(false);
                            simpan.setDisable(false);
                            batal.setDisable(false);
                            dialog.close();
                        }

                    });

                } else {

                    formProcessShow = false;
                    loading.setVisible(false);
                    simpan.setDisable(false);
                    batal.setDisable(false);
                    dialog.close();

                }

            }
        });

        batal.getStyleClass().add("button-cancel");
        batal.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                formProcessShow = false;
                dialog.close();
            }
        });

        stop.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                MaintenanceDetailsStopPost ms = new MaintenanceDetailsStopPost();
                ms.setId(data.getId());
                ms.setEndTime(Utilities.getCurrentHourMinute());

                Utilities.stopMaintenanceDetails(ms, new CustomRunnableX<MaintenanceDetails>() {

                    @Override
                    public void run() {
                        super.run();
                        tblProcess.getItems().removeAll(MechanicMaintenance.maintenanceDetail);
                        MechanicMaintenance.maintenanceDetail.remove(data);
                        MechanicMaintenance.maintenanceDetail.add(getData());
                        // tblProcess.getItems().addAll(MechanicMaintenance.maintenanceDetail);
                        // tblProcess.refresh();
                        showProcessList(parent);

                        formProcessShow = false;
                        loading.setVisible(false);
                        simpan.setDisable(false);
                        batal.setDisable(false);
                        dialog.close();
                    }

                });
            }

        });

        if (isView) {
            batal.setText("    Tutup    ");
            content.setActions(batal);
        } else {
            if (isEdit) {
                batal.setText("    Tutup    ");
                content.setActions(stop, simpan, batal);
            } else {
                content.setActions(simpan, batal);
            }
        }

        dialog.show();

        formProcessShow = true;
    }
}
