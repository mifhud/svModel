package com.tura.fms.screen.models.actual_support;

import java.util.HashMap;
import java.util.Map;

public class UserSupportLocal {

    private String id;
    private String idOrg;
    private Boolean hasIdOrg;
    private String cdRole;
    private Boolean hasCdRole;
    private String cdDepartment;
    private Boolean hasCdDepartment;
    private String name;
    private String email;
    private String pwd;
    private String staffId;
    private Integer fingerprintId;
    private Object socketId;
    private Object filePicture;
    private Object ipAddress;
    private Object token;
    private Integer enumAvailability;
    private Integer isAuthenticated;
    private Object json;
    private Object skill;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_org() {
        return idOrg;
    }

    public void setId_org(String idOrg) {
        this.idOrg = idOrg;
    }

    public Boolean getHasIdOrg() {
        return hasIdOrg;
    }

    public void setHasIdOrg(Boolean hasIdOrg) {
        this.hasIdOrg = hasIdOrg;
    }

    public String getCd_role() {
        return cdRole;
    }

    public void setCd_role(String cdRole) {
        this.cdRole = cdRole;
    }

    public Boolean getHasCdRole() {
        return hasCdRole;
    }

    public void setHasCdRole(Boolean hasCdRole) {
        this.hasCdRole = hasCdRole;
    }

    public String getCd_department() {
        return cdDepartment;
    }

    public void setCd_department(String cdDepartment) {
        this.cdDepartment = cdDepartment;
    }

    public Boolean getHasCdDepartment() {
        return hasCdDepartment;
    }

    public void setHasCdDepartment(Boolean hasCdDepartment) {
        this.hasCdDepartment = hasCdDepartment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getStaff_id() {
        return staffId;
    }

    public void setStaff_id(String staffId) {
        this.staffId = staffId;
    }

    public Integer getFingerprint_id() {
        return fingerprintId;
    }

    public void setFingerprint_id(Integer fingerprintId) {
        this.fingerprintId = fingerprintId;
    }

    public Object getSocket_id() {
        return socketId;
    }

    public void setSocket_id(Object socketId) {
        this.socketId = socketId;
    }

    public Object getFile_picture() {
        return filePicture;
    }

    public void setFile_picture(Object filePicture) {
        this.filePicture = filePicture;
    }

    public Object getIp_address() {
        return ipAddress;
    }

    public void setIp_address(Object ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Object getToken() {
        return token;
    }

    public void setToken(Object token) {
        this.token = token;
    }

    public Integer getEnum_availability() {
        return enumAvailability;
    }

    public void setEnum_availability(Integer enumAvailability) {
        this.enumAvailability = enumAvailability;
    }

    public Integer getIs_authenticated() {
        return isAuthenticated;
    }

    public void setIs_authenticated(Integer isAuthenticated) {
        this.isAuthenticated = isAuthenticated;
    }

    public Object getJson() {
        return json;
    }

    public void setJson(Object json) {
        this.json = json;
    }

    public Object getSkill() {
        return skill;
    }

    public void setSkill(Object skill) {
        this.skill = skill;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}