package com.tura.fms.screen.models.location;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.tura.fms.screen.models.constant.Constant;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "cd_location", "name", "longitude", "latitude", "date_submitted", "date_modified", "json",
        "_location", "id_area" })
public class Location {

    @JsonProperty("id")
    private String id;
    @JsonProperty("cd_location")
    private String cdLocation;
    @JsonProperty("name")
    private String name;
    @JsonProperty("longitude")
    private Double longitude;
    @JsonProperty("latitude")
    private Double latitude;
    @JsonProperty("date_submitted")
    private Integer dateSubmitted;
    @JsonProperty("date_modified")
    private Integer dateModified;
    @JsonProperty("json")
    private String json;
    @JsonProperty("_location")
    private Constant location;
    @JsonProperty("id_area")
    private Area idArea;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("cd_location")
    public String getCdLocation() {
        return cdLocation;
    }

    @JsonProperty("cd_location")
    public void setCdLocation(String cdLocation) {
        this.cdLocation = cdLocation;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("date_submitted")
    public Integer getDateSubmitted() {
        return dateSubmitted;
    }

    @JsonProperty("date_submitted")
    public void setDateSubmitted(Integer dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    @JsonProperty("date_modified")
    public Integer getDateModified() {
        return dateModified;
    }

    @JsonProperty("date_modified")
    public void setDateModified(Integer dateModified) {
        this.dateModified = dateModified;
    }

    @JsonProperty("json")
    public String getJson() {
        return json;
    }

    @JsonProperty("json")
    public void setJson(String json) {
        this.json = json;
    }

    @JsonProperty("_location")
    public Constant getLocation() {
        return location;
    }

    @JsonProperty("_location")
    public void setLocation(Constant location) {
        this.location = location;
    }

    @JsonProperty("id_area")
    public Area getIdArea() {
        return idArea;
    }

    @JsonProperty("id_area")
    public void setIdArea(Area idArea) {
        this.idArea = idArea;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
