package com.tura.fms.screen.models.actual_support;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.tura.fms.screen.models.actual_model.CdMaterial;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "date", "hm_start", "hm_stop", "created_at", "is_deleted", "equipment_id", "location_id",
        "destination_id", "cd_activity", "cd_shift", "cd_material", "operator_id", "supervisor_id", "group_leader_id",
        "created_by" })

public class OperationSupport {

    @JsonProperty("id")
    private String id;
    @JsonProperty("date")
    private Integer date;
    @JsonProperty("hm_start")
    private Double hmStart;
    @JsonProperty("hm_stop")
    private Double hmStop;
    @JsonProperty("created_at")
    private Integer createdAt;
    @JsonProperty("is_deleted")
    private Integer isDeleted;
    @JsonProperty("equipment_id")
    private EquipmentSupport equipmentId;
    @JsonProperty("location_id")
    private LocationSupport locationId;
    @JsonProperty("destination_id")
    private LocationSupport destinationId;
    @JsonProperty("cd_activity")
    private CdActivitySupport cdActivity;
    @JsonProperty("cd_shift")
    private CdActivitySupport cdShift;
    @JsonProperty("cd_material")
    private CdMaterial cdMaterial;
    @JsonProperty("operator_id")
    private UserSupport operatorId;
    @JsonProperty("supervisor_id")
    private UserSupport supervisorId;
    @JsonProperty("group_leader_id")
    private UserSupport groupLeaderId;
    @JsonProperty("created_by")
    private String createdBy;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("date")
    public Integer getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(Integer date) {
        this.date = date;
    }

    @JsonProperty("hm_start")
    public Double getHmStart() {
        return hmStart;
    }

    @JsonProperty("hm_start")
    public void setHmStart(Double hmStart) {
        this.hmStart = hmStart;
    }

    @JsonProperty("hm_stop")
    public Double getHmStop() {
        return hmStop;
    }

    @JsonProperty("hm_stop")
    public void setHmStop(Double hmStop) {
        this.hmStop = hmStop;
    }

    @JsonProperty("created_at")
    public Integer getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(Integer createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("is_deleted")
    public Integer getIsDeleted() {
        return isDeleted;
    }

    @JsonProperty("is_deleted")
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    @JsonProperty("equipment_id")
    public EquipmentSupport getEquipmentId() {
        return equipmentId;
    }

    @JsonProperty("equipment_id")
    public void setEquipmentId(EquipmentSupport equipmentId) {
        this.equipmentId = equipmentId;
    }

    @JsonProperty("location_id")
    public LocationSupport getLocationId() {
        return locationId;
    }

    @JsonProperty("location_id")
    public void setLocationId(LocationSupport locationId) {
        this.locationId = locationId;
    }

    @JsonProperty("destination_id")
    public LocationSupport getDestinationId() {
        return destinationId;
    }

    @JsonProperty("destination_id")
    public void setDestinationId(LocationSupport destinationId) {
        this.destinationId = destinationId;
    }

    @JsonProperty("cd_activity")
    public CdActivitySupport getCdActivity() {
        return cdActivity;
    }

    @JsonProperty("cd_activity")
    public void setCdActivity(CdActivitySupport cdActivity) {
        this.cdActivity = cdActivity;
    }

    @JsonProperty("cd_shift")
    public CdActivitySupport getCdShift() {
        return cdShift;
    }

    @JsonProperty("cd_shift")
    public void setCdShift(CdActivitySupport cdShift) {
        this.cdShift = cdShift;
    }

    @JsonProperty("cd_material")
    public CdMaterial getCdMaterial() {
        return cdMaterial;
    }

    @JsonProperty("cd_material")
    public void setCdMaterial(CdMaterial cdMaterial) {
        this.cdMaterial = cdMaterial;
    }

    @JsonProperty("operator_id")
    public UserSupport getOperatorId() {
        return operatorId;
    }

    @JsonProperty("operator_id")
    public void setOperatorId(UserSupport operatorId) {
        this.operatorId = operatorId;
    }

    @JsonProperty("supervisor_id")
    public UserSupport getSupervisorId() {
        return supervisorId;
    }

    @JsonProperty("supervisor_id")
    public void setSupervisorId(UserSupport supervisorId) {
        this.supervisorId = supervisorId;
    }

    @JsonProperty("group_leader_id")
    public UserSupport getGroupLeaderId() {
        return groupLeaderId;
    }

    @JsonProperty("group_leader_id")
    public void setGroupLeaderId(UserSupport groupLeaderId) {
        this.groupLeaderId = groupLeaderId;
    }

    @JsonProperty("created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("created_by")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}