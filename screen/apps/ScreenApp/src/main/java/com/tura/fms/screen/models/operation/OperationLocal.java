package com.tura.fms.screen.models.operation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "id_equipment", "id_operator", "id_hauler", "material_type", "shift", "date",
        "id_hauler_operator", "id_material", "id_hauler", "id_loader_operator", "id_material", "id_hauler",
        "id_loader_operator", "id_loader", "id_location", "id_destination", "date_operated", "load_weight",
        "empty_weight", "bucket_count", "tonnage", "tonnage_timbangan", "tonnage_ujipetik", "tonnage_bucket",
        "distance", "is_supervised", "is_volume", "latitude", "longitude" })

public class OperationLocal {

    @JsonProperty("id")
    private String id;
    @JsonProperty("id_equipment")
    private String id_equipment;
    @JsonProperty("id_operator")
    private String id_operator;
    @JsonProperty("id_actual")
    private String id_actual;
    @JsonProperty("material_type")
    private String material_type;
    @JsonProperty("shift")
    private String shift;
    @JsonProperty("date")
    private String date;
    @JsonProperty("id_hauler_operator")
    private String id_hauler_operator;
    @JsonProperty("id_material")
    private String id_material;
    @JsonProperty("id_hauler")
    private String id_hauler;
    @JsonProperty("id_loader_operator")
    private String id_loader_operator;
    @JsonProperty("id_loader")
    private String id_loader;
    @JsonProperty("id_location")
    private String id_location;
    @JsonProperty("id_destination")
    private String id_destination;
    @JsonProperty("date_operated")
    private String date_operated;
    @JsonProperty("load_weight")
    private Double load_weight;
    @JsonProperty("empty_weight")
    private Double empty_weight;
    @JsonProperty("bucket_count")
    private int bucket_count;
    @JsonProperty("tonnage")
    private Double tonnage;
    @JsonProperty("tonnage_timbangan")
    private Double tonnage_timbangan;
    @JsonProperty("tonnage_ujipetik")
    private Double tonnage_ujipetik;
    @JsonProperty("tonnage_bucket")
    private Double tonnage_bucket;
    @JsonProperty("distance")
    private Double distance;
    @JsonProperty("is_supervised")
    private Integer is_supervised;
    @JsonProperty("is_volume")
    private Integer is_volume;
    @JsonProperty("latitude")
    private Double latitude;
    @JsonProperty("longitude")
    private Double longitude;

    public void setLat(Double lat) {
        latitude = lat;
    }

    public Double getLat() {
        return latitude;
    }

    public void setLon(Double lon) {
        longitude = lon;
    }

    public Double getLon() {
        return longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdEquipment() {
        return id_equipment;
    }

    public void setIdEquipment(String id_equipment) {
        this.id_equipment = id_equipment;
    }

    public String getIdOperator() {
        return id_operator;
    }

    public void setIdOperator(String id_operator) {
        this.id_operator = id_operator;
    }

    public String getIdActual() {
        return id_actual;
    }

    public void setIdActual(String id_actual) {
        this.id_actual = id_actual;
    }

    public String getMaterialType() {
        return material_type;
    }

    public void setMaterialType(String material_type) {
        this.material_type = material_type;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIdHaulerOperator() {
        return id_hauler_operator;
    }

    public void setIdHaulerOperator(String id_hauler_operator) {
        this.id_hauler_operator = id_hauler_operator;
    }

    public String getIdMaterial() {
        return id_material;
    }

    public void setIdMaterial(String id_material) {
        this.id_material = id_material;
    }

    public String getIdHauler() {
        return id_hauler;
    }

    public void setIdHauler(String id_hauler) {
        this.id_hauler = id_hauler;
    }

    public String getIdLoaderOperator() {
        return id_loader_operator;
    }

    public void setIdLoaderOperator(String id_loader_operator) {
        this.id_loader_operator = id_loader_operator;
    }

    public String getIdLoader() {
        return id_loader;
    }

    public void setIdLoader(String id_loader) {
        this.id_loader = id_loader;
    }

    public String getIdLocation() {
        return id_location;
    }

    public void setIdLocation(String id_location) {
        this.id_location = id_location;
    }

    public String getIdDestination() {
        return id_destination;
    }

    public void setIdDestination(String id_destination) {
        this.id_destination = id_destination;
    }

    public String getDateOperated() {
        return date_operated;
    }

    public void setDateOperated(String date_operated) {
        this.date_operated = date_operated;
    }

    public Double getLoadWeight() {
        return load_weight;
    }

    public void setLoadWeight(Double load_weight) {
        this.load_weight = load_weight;
    }

    public int getBucketCount() {
        return bucket_count;
    }

    public void setBucketCount(int bucket_count) {
        this.bucket_count = bucket_count;
    }

    public Double getEmptyWeight() {
        return empty_weight;
    }

    public void setEmptyWeight(Double empty_weight) {
        this.empty_weight = empty_weight;
    }

    public Double getTonnage() {
        return tonnage;
    }

    public void setTonnage(Double tonnage) {
        this.tonnage = tonnage;
    }

    public Double getTonnageTimbangan() {
        return tonnage_timbangan;
    }

    public void setTonnageTimbangan(Double tonnage_timbangan) {
        this.tonnage_timbangan = tonnage_timbangan;
    }

    public Double getTonnageUjipetik() {
        return tonnage_ujipetik;
    }

    public void setTonnageUjipetik(Double tonnage_ujipetik) {
        this.tonnage_ujipetik = tonnage_ujipetik;
    }

    public Double getTonnageBucket() {
        return tonnage_bucket;
    }

    public void setTonnageBucket(Double tonnage_bucket) {
        this.tonnage_bucket = tonnage_bucket;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Integer getIsSupervised() {
        return is_supervised;
    }

    public void setIsSupervised(Integer is_supervised) {
        this.is_supervised = is_supervised;
    }

    public Integer getIsVolume() {
        return is_volume;
    }

    public void setIsVolume(Integer is_volume) {
        this.is_volume = is_volume;
    }

}
