package com.tura.fms.screen.models.equipment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tura.fms.screen.models.constant.Constant;
import com.tura.fms.screen.models.user_model.UserAPI;

public class EquipmentLogStart {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("id_actual")
    @Expose
    private String id_actual;
    @SerializedName("id_operation_support")
    @Expose
    private String id_operation_support;
    @SerializedName("id_equipment")
    @Expose
    private Equipment id_equipment;
    @SerializedName("id_operator")
    @Expose
    private UserAPI id_operator;
    @SerializedName("cd_activity")
    @Expose
    private Constant id_activity;
    @SerializedName("cd_tum")
    @Expose
    private Constant cd_tum;
    @SerializedName("time")
    @Expose
    private Integer time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserAPI getIdOperator() {
        return id_operator;
    }

    public void setIdOperator(UserAPI id_operator) {
        this.id_operator = id_operator;
    }

    public Constant getCdActivity() {
        return id_activity;
    }

    public void setCdActivity(Constant id_activity) {
        this.id_activity = id_activity;
    }

    public Constant getCdTum() {
        return cd_tum;
    }

    public void setCdTum(Constant cd_tum) {
        this.cd_tum = cd_tum;
    }

    public Equipment getIdEquipment() {
        return id_equipment;
    }

    public void setIdEquipment(Equipment id_equipment) {
        this.id_equipment = id_equipment;
    }

    public String getIdActual() {
        return id_actual;
    }

    public void setIdActual(String id_actual) {
        this.id_actual = id_actual;
    }

    public String getIdOperationSupport() {
        return id_operation_support;
    }

    public void setIdOperationSupport(String id_operation_support) {
        this.id_operation_support = id_operation_support;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

}