package com.tura.fms.screen.models.prestart_model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "alias", "type", "__cd_type_equipment__", "__has_cd_type_equipment__", "severinity",
        "date_created", "date_modified", "is_deleted" })
public class PrestartAPI {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("alias")
    private String alias;
    @JsonProperty("type")
    private String type;
    @JsonProperty("__cd_type_equipment__")
    private CdTypeEquipment cdTypeEquipment;
    @JsonProperty("__has_cd_type_equipment__")
    private Boolean hasCdTypeEquipment;
    @JsonProperty("severinity")
    private Object severinity;
    @JsonProperty("date_created")
    private Object dateCreated;
    @JsonProperty("date_modified")
    private Object dateModified;
    @JsonProperty("is_deleted")
    private Object isDeleted;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("alias")
    public String getAlias() {
        return alias;
    }

    @JsonProperty("alias")
    public void setAlias(String alias) {
        this.alias = alias;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("__cd_type_equipment__")
    public CdTypeEquipment getCdTypeEquipment() {
        return cdTypeEquipment;
    }

    @JsonProperty("__cd_type_equipment__")
    public void setCdTypeEquipment(CdTypeEquipment cdTypeEquipment) {
        this.cdTypeEquipment = cdTypeEquipment;
    }

    @JsonProperty("__has_cd_type_equipment__")
    public Boolean getHasCdTypeEquipment() {
        return hasCdTypeEquipment;
    }

    @JsonProperty("__has_cd_type_equipment__")
    public void setHasCdTypeEquipment(Boolean hasCdTypeEquipment) {
        this.hasCdTypeEquipment = hasCdTypeEquipment;
    }

    @JsonProperty("severinity")
    public Object getSeverinity() {
        return severinity;
    }

    @JsonProperty("severinity")
    public void setSeverinity(Object severinity) {
        this.severinity = severinity;
    }

    @JsonProperty("date_created")
    public Object getDateCreated() {
        return dateCreated;
    }

    @JsonProperty("date_created")
    public void setDateCreated(Object dateCreated) {
        this.dateCreated = dateCreated;
    }

    @JsonProperty("date_modified")
    public Object getDateModified() {
        return dateModified;
    }

    @JsonProperty("date_modified")
    public void setDateModified(Object dateModified) {
        this.dateModified = dateModified;
    }

    @JsonProperty("is_deleted")
    public Object getIsDeleted() {
        return isDeleted;
    }

    @JsonProperty("is_deleted")
    public void setIsDeleted(Object isDeleted) {
        this.isDeleted = isDeleted;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}