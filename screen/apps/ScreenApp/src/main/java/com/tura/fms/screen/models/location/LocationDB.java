package com.tura.fms.screen.models.location;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.database.SQLiteJDBC;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LocationDB extends Operation {

    public static final String TABLE_NAME = "Location_Record";

    private static final String id = "id";
    private static final String id_self = "id_self";
    private static final String cd_location = "cd_location";
    private static final String id_area = "id_area";
    private static final String name = "name";
    private static final String longitude = "longitude";
    private static final String latitude = "latitude";
    private static final String is_deleted = "is_deleted";
    private static final String date_submitted = "date_submitted";
    private static final String date_modified = "date_modified";
    private static final String json = "json";
    private static final String sync_status = "sync_status";

    public LocationDB() {
        super(LocationDB.TABLE_NAME, ConstantValues.DATA_LOCATION);
    }

    public void createTableLocation() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table location exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table location created");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + id_self + " TEXT NULL,"
                + cd_location + " TEXT NULL," + id_area + " TEXT NULL," + name + " TEXT NULL," + longitude
                + " DOUBLE NULL," + latitude + " DOUBLE NULL," + is_deleted + " TINYINT NULL," + date_modified
                + " INTEGER NULL," + date_submitted + " INTEGER NULL," + json + " TEXT NULL," + sync_status
                + " TINYINT NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        Location eq = Synchronizer.mapper.convertValue(eqa, Location.class);
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + id_self + "," + cd_location + ","
                    + id_area + "," + name + "," + longitude + "," + latitude + "," + is_deleted + "," + date_modified
                    + "," + date_submitted + "," + json + "," + sync_status + ")"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?,1) ON CONFLICT(id) DO UPDATE SET " + id + "=?," + id_self + "=?,"
                    + cd_location + "=?," + id_area + "=?," + name + "=?," + longitude + "=?," + latitude + "=?,"
                    + is_deleted + "=?;";
            st = conn().prepareStatement(sql);
            st.setString(1, eq.getId());
            st.setString(12, eq.getId());
            st.setString(2, null);
            st.setString(13, null);
            if (eq.getCdLocation() != null) {
                st.setString(3, eq.getCdLocation());
                st.setString(14, eq.getCdLocation());
            } else {
                st.setString(3, "");
                st.setString(14, "");
            }
            if (eq.getIdArea() != null) {
                st.setString(4, eq.getIdArea().getId());
                st.setString(15, eq.getIdArea().getId());
            } else {
                st.setString(4, "");
                st.setString(15, "");
            }
            if (eq.getName() != null) {
                st.setString(5, eq.getName());
                st.setString(16, eq.getName());
            } else {
                st.setString(5, "");
                st.setString(16, "");
            }
            if (eq.getLongitude() != null) {
                st.setDouble(6, eq.getLongitude());
                st.setDouble(17, eq.getLongitude());
            } else {
                st.setDouble(6, 0);
                st.setDouble(17, 0);
            }
            if (eq.getLatitude() != null) {
                st.setDouble(7, eq.getLatitude());
                st.setDouble(18, eq.getLatitude());
            } else {
                st.setDouble(7, 0);
                st.setDouble(18, 0);
            }
            st.setInt(8, 0);
            st.setInt(19, 0);
            if (eq.getDateModified() != null) {
                st.setInt(9, eq.getDateModified());
            } else {
                st.setInt(9, 0);
            }
            if (eq.getDateSubmitted() != null) {
                st.setInt(10, eq.getDateSubmitted());
            } else {
                st.setInt(10, 0);
            }
            if (eq.getJson() != null) {
                st.setString(11, eq.getJson().toString());
            } else {
                st.setString(11, null);
            }
            rs = st.execute();
            // System.out.println("----------------");
            // System.out.println(rs);
            st.close();

            return eq;

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_NAME + " where id=" + id;
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getLocation(MainApp.requestHeader, "", page, size);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Object getFromServerById(String id) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getLocationById(MainApp.requestHeader, id);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        Call<Object> call = APIClient.getInstance().getLocationCount(MainApp.requestHeader);
        try {
            Response<Object> response = call.execute();
            Map<String, Number> mapped = APIClient.mapper.convertValue(response.body(), Map.class);
            if (mapped.get("output") != null) {
                return mapped.get("output").intValue();
            } else {
                return 0;
            }
        } catch (IOException ex) {
            // ex.printStackTrace();
            System.out.println("[Location getRemoteCount] " + ex.getMessage());
        }
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                LocationLocal eq = new LocationLocal();
                eq.setId(cursor.getString("id"));
                eq.setId_self(null);
                eq.setCd_location(cursor.getString("cd_location"));
                eq.setId_area(cursor.getString("id_area"));
                eq.setName(cursor.getString("name"));
                eq.setLongitude(cursor.getDouble("longitude"));
                eq.setLatitude(cursor.getDouble("latitude"));
                eq.setIs_deleted(0);
                eq.setDate_submitted(cursor.getInt("date_submitted"));
                eq.setDate_modified(cursor.getInt("date_modified"));
                // eq.setJson(cursor.getString("cd_operation"));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        Call<ResponseModel> call = APIClient.getInstance().postLocation(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            System.out.println(response);
            System.out.println(response.body().getResponse());
            System.out.println(response.body().getMessage());
            if (response.body().getResponse().equals("success")) {
                System.out.println("post success");
                /**
                 * change flag (sync_status to 1)
                 */
                for (Object obj : eq) {
                    LocationLocal oqe = (LocationLocal) obj;
                    setSynchronized(oqe.getId());
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where id='" + id + "'";
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
