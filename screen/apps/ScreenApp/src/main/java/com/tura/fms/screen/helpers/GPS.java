package com.tura.fms.screen.helpers;

public class GPS {

    private Double latitude;

    private Double longitude;

    GPS() {
        latitude = 0.0;
        longitude = 0.0;
    }

    public void setLat(Double lat) {
        latitude = lat;
    }

    public Double getLat() {
        return latitude;
    }

    public void setLon(Double lon) {
        longitude = lon;
    }

    public Double getLon() {
        return longitude;
    }
}