package com.tura.fms.screen.controllers;

import static org.javalite.app_config.AppConfig.p;

import java.net.ConnectException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.TimeoutException;

import com.jfoenix.controls.JFXSnackbar;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.database.Activity_DB;
import com.tura.fms.screen.database.Actual_DB;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.activity_model.ActivityData;
import com.tura.fms.screen.models.activity_model.ActivityModel;
import com.tura.fms.screen.models.actual_model.ActualDetail;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnDutyController implements Initializable {

    private NavigationUI navigationUI = new NavigationUI();

    @FXML
    private Button TitleBar;

    // @FXML
    // private GridPane gridata;

    private ObservableList<ActivityModel> recordsData = FXCollections.observableArrayList();

    private ActivityData activityData;

    private Activity_DB activity_db;

    private Actual_DB actual_db;

    private ActualDetail actualDetail;

    @FXML
    private VBox rootpane;

    @FXML
    private VBox vBoxDuty;

    @FXML
    private Label dutyTitle;

    @FXML
    private Label dutyDate;

    @FXML
    private Label dutyShift;

    @FXML
    private Label dutyNote;

    @FXML
    private GridPane gridData;

    private JFXSnackbar snackbar;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MainApp.clockDashboard.stop();

        activity_db = new Activity_DB();
        actual_db = new Actual_DB();
        snackbar = new JFXSnackbar(rootpane);
        snackbar.setPrefWidth(300);
        snackbar.getStyleClass().add("-fx-background-color: #ccc;");

        drawerAction();

        // TitleBar.setText(p("app.textLoading"));
        TitleBar.setText(p("app.titleOnDuty"));

        dutyDate.setText("Hari/Tanggal : " + Utilities.getHariTanggal());

        String shift = "";

        if (Utilities.isLoaderHauler()) {
            if (MainApp.isOperator) {
                shift = MainApp.actualInfo == null ? "" : MainApp.actualInfo.getCd_shift().getName();
            }

        } else {
            if (MainApp.isOperator) {
                shift = MainApp.actualSupport == null ? "" : MainApp.actualSupport.getCdShift().getName();
            }

        }

        dutyShift.setText("Shift " + shift);

        getData();

        // if (MainApp.isOperator) {

        // getData();

        // } else {

        // Platform.runLater(new Runnable() {

        // @Override
        // public void run() {
        // gridData.setVisible(false);
        // }
        // });

        // }
    }

    private void getData() {

        if (NetworkConnection.getServiceStatus()) {

            TitleBar.setText(p("app.textLoading"));

            String actual = "";
            String acsupport = "";

            if (Utilities.isLoaderHauler()) {
                if (MainApp.isOperator && MainApp.actualInfo != null) {
                    actual = MainApp.actualInfo.getId();
                }
            } else {
                if (MainApp.isOperator && MainApp.actualSupport != null) {
                    acsupport = MainApp.actualSupport.getId();
                }
            }

            Call<ResponseModel> call = APIClient.getInstance().getOnDuty(MainApp.requestHeader, actual, acsupport);
            call.enqueue(new Callback<ResponseModel>() {

                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                    if (response.isSuccessful()) {

                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                TitleBar.setText(p("app.titleOnDuty"));
                                tampilkanData(response.body());
                            }
                        });

                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    // t.printStackTrace();

                    if (t instanceof ConnectException) {
                        Utilities.messageSocketException(t, "Mgmt. OnDuty");
                    } else if (t instanceof TimeoutException) {
                        Utilities.messageTimeoutException(t, "Mgmt. OnDuty");
                    } else {
                        Utilities.messageOtherException(t, "Mgmt. OnDuty");
                    }

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            TitleBar.setText(p("app.titleOnDuty"));
                            gridData.getChildren().clear();
                            dutyTitle.setText("");
                            dutyDate.setText("");
                            dutyShift.setText("");
                        }
                    });
                }
            });

        } else {
            System.out.println("Service not available and local data is empty");

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    gridData.getChildren().clear();
                    dutyTitle.setText("");
                    dutyDate.setText("");
                    dutyShift.setText("");
                }
            });

        }
    }

    private void tampilkanData(ResponseModel res) {

        int Col = 0;
        int Row = 0;

        boolean setInfo = false;

        try {

            if (res.getOutput() != null) {
                if (res.getOutput().toString().length() > 10) {

                    Map<String, Object> data = Utilities.mapObject(res.getOutput());

                    Map<String, Object> area = Utilities.mapObject(data.get("id_area"));
                    if (area.get("name") != null) {
                        dutyTitle.setText("SETTINGAN PENGAWAS PT SBS " + area.get("name"));
                    }
                    if (data.get("date") != null) {
                        Double dt = Double.parseDouble(data.get("date").toString());
                        String tgl = Utilities.timeStampToDate(dt.longValue(), "hari");
                        dutyDate.setText("Hari/Tanggal :  " + tgl);
                    }
                    Map<String, Object> shift = Utilities.mapObject(data.get("cd_shift"));
                    if (shift.get("name") != null) {
                        dutyShift.setText(shift.get("name").toString());
                    }
                    gridData.getChildren().clear();

                    Map<String, Object> supt_production = Utilities.mapObject(data.get("supt_production"));
                    Label supt_production_title = new Label("Supt. Production");
                    Label supt_production_content = new Label(": " + supt_production.get("name"));
                    gridData.add(supt_production_title, 0, 0);
                    gridData.add(supt_production_content, 1, 0);

                    Map<String, Object> supt_pit_service = Utilities.mapObject(data.get("supt_pit_service"));
                    Label supt_pit_service_title = new Label("Supt. Pit Service");
                    Label supt_pit_service_content = new Label(": " + supt_pit_service.get("name"));
                    gridData.add(supt_pit_service_title, 0, 1);
                    gridData.add(supt_pit_service_content, 1, 1);

                    Map<String, Object> supt_production_ob = Utilities.mapObject(data.get("supt_production_ob"));
                    Label supt_production_ob_title = new Label("Supt. Production OB");
                    Label supt_production_ob_content = new Label(": " + supt_production_ob.get("name"));
                    gridData.add(supt_production_ob_title, 0, 2);
                    gridData.add(supt_production_ob_content, 1, 2);

                    Map<String, Object> sov_pit_service = Utilities.mapObject(data.get("sov_pit_service"));
                    Label sov_pit_service_title = new Label("Sov. Pit Service");
                    Label sov_pit_service_content = new Label(": " + sov_pit_service.get("name"));
                    gridData.add(sov_pit_service_title, 0, 3);
                    gridData.add(sov_pit_service_content, 1, 3);

                    Map<String, Object> gl_overburden = Utilities.mapObject(data.get("gl_overburden"));
                    Label gl_overburden_title = new Label("GL. Overburden");
                    Label gl_overburden_content = new Label(": " + gl_overburden.get("name"));
                    gridData.add(gl_overburden_title, 0, 4);
                    gridData.add(gl_overburden_content, 1, 4);

                    Map<String, Object> gl_coal_getting = Utilities.mapObject(data.get("gl_coal_getting"));
                    Label gl_coal_getting_title = new Label("GL. Coal Getting");
                    Label gl_coal_getting_content = new Label(": " + gl_coal_getting.get("name"));
                    gridData.add(gl_coal_getting_title, 0, 5);
                    gridData.add(gl_coal_getting_content, 1, 5);

                    Map<String, Object> gl_pit_service = Utilities.mapObject(data.get("gl_pit_service"));
                    Label gl_pit_service_title = new Label("GL. Pit Service");
                    Label gl_pit_service_content = new Label(": " + gl_pit_service.get("name"));
                    gridData.add(gl_pit_service_title, 0, 6);
                    gridData.add(gl_pit_service_content, 1, 6);

                    Map<String, Object> man_power = Utilities.mapObject(data.get("man_power"));
                    Label man_power_title = new Label("Map Power");
                    Label man_power_content = new Label(": " + man_power.get("name"));
                    gridData.add(man_power_title, 0, 7);
                    gridData.add(man_power_content, 1, 7);

                    Map<String, Object> plan = Utilities.mapObject(data.get("plan"));
                    Label plan_title = new Label("Plan");
                    Label plan_content = new Label(": " + plan.get("name"));
                    gridData.add(plan_title, 0, 8);
                    gridData.add(plan_content, 1, 8);

                    Map<String, Object> she = Utilities.mapObject(data.get("she"));
                    Label she_title = new Label("SHE");
                    Label she_content = new Label(": " + she.get("name"));
                    gridData.add(she_title, 0, 9);
                    gridData.add(she_content, 1, 9);

                    Map<String, Object> hr_ga_gl = Utilities.mapObject(data.get("hr_ga_gl"));
                    Label hr_ga_gl_title = new Label("HR / GA-GL");
                    Label hr_ga_gl_content = new Label(": " + hr_ga_gl.get("name"));
                    gridData.add(hr_ga_gl_title, 0, 10);
                    gridData.add(hr_ga_gl_content, 1, 10);

                    Label emergency_title = new Label("Contact Emergency");
                    Label emergency_content = new Label(": " + data.get("contact_emergency"));
                    gridData.add(emergency_title, 0, 11);
                    gridData.add(emergency_content, 1, 11);

                    Label note_title = new Label("Note");
                    Label note_content = new Label(": " + data.get("note"));
                    gridData.add(note_title, 0, 12);
                    gridData.add(note_content, 1, 12);

                    // int row = 0;

                    // for (Map<String, Object> mm : data) {

                    // if (setInfo == false) {
                    // Map<String, Object> area = Utilities.mapObject(mm.get("id_area"));
                    // if (area.get("name") != null) {
                    // dutyTitle.setText("SETTINGAN PENGAWAS PT SBS " + area.get("name"));
                    // }
                    // if (mm.get("date") != null) {
                    // Double dt = Double.parseDouble(mm.get("date").toString());
                    // String tgl = Utilities.timeStampToDate(dt.longValue(), "hari");
                    // dutyDate.setText("Hari/Tanggal : " + tgl);
                    // }
                    // Map<String, Object> shift = Utilities.mapObject(mm.get("cd_shift"));
                    // if (shift.get("name") != null) {
                    // dutyShift.setText(shift.get("name").toString());
                    // }
                    // gridData.getChildren().clear();
                    // setInfo = true;
                    // }

                    // Double ord = Double.parseDouble(mm.get("order").toString());

                    // Label title = new Label(String.valueOf(ord.intValue()) + ". " +
                    // mm.get("title"));
                    // Label content = new Label(": " + mm.get("content"));

                    // gridData.add(title, 0, row);
                    // gridData.add(content, 1, row);

                    // row++;
                    // }
                }

            } else {

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void drawerAction() {
        TitleBar.setOnAction((ActionEvent evt) -> {
            TitleBar.setText(p("app.textLoading"));

            TitleBar.setDisable(true);

            navigationUI.back_dashboard(evt, new Runnable() {

                @Override
                public void run() {
                    TitleBar.setDisable(false);
                }
            });

            MainApp.clockDashboard.play();
        });
    }

}
