package com.tura.fms.screen.models.constant;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.database.SQLiteJDBC;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ConstantDB extends Operation {

    public static final String TABLE_NAME = "Constant_Record";

    private static final String cd = "cd";
    private static final String cd_self = "cd_self";
    private static final String name = "name";
    private static final String sync_status = "sync_status";

    public ConstantDB() {
        super(ConstantDB.TABLE_NAME, ConstantValues.DATA_CONSTANT);
    }

    public void createTableConstant() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table Constant exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table Constant created");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + cd + " TEXT PRIMARY KEY," + cd_self + " TEXT NULL,"
                + name + " TEXT NULL," + sync_status + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        Constant eq = Synchronizer.mapper.convertValue(eqa, Constant.class);
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + cd + "," + cd_self + "," + name + "," + sync_status
                    + ")" + " VALUES(?,?,?,1) ON CONFLICT(cd) DO UPDATE SET " + cd + "=?," + cd_self + "=?," + name
                    + "=?;";
            st = conn().prepareStatement(sql);
            st.setString(1, eq.getCd());
            st.setString(2, eq.getCd_self());
            st.setString(3, eq.getName());
            st.setString(4, eq.getCd());
            st.setString(5, eq.getCd_self());
            st.setString(6, eq.getName());
            rs = st.execute();
            st.close();

            return eq;

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        return null;

    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_NAME + " where id=" + id;
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getConstant(MainApp.requestHeader, page, size);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Object getFromServerById(String id) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getConstantById(MainApp.requestHeader, id);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        Call<Object> call = APIClient.getInstance().getConstantCount(MainApp.requestHeader);
        try {
            Response<Object> response = call.execute();
            Map<String, Number> mapped = APIClient.mapper.convertValue(response.body(), Map.class);
            if (mapped.get("output") != null) {
                return mapped.get("output").intValue();
            } else {
                return 0;
            }
        } catch (IOException ex) {
            // ex.printStackTrace();
            System.out.println("[Constant getRemoteCount] " + ex.getMessage());
        }
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                ConstantLocal eq = new ConstantLocal();
                eq.setCd(cursor.getString("cd"));
                eq.setCdSelf(cursor.getString("cd_self"));
                eq.setName(cursor.getString("name"));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        // no post action
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where cd='" + id + "'";
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Object> getLocalById(String fieldId, String searchId) {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + fieldId + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, searchId);
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                ConstantLocal eq = new ConstantLocal();
                eq.setCd(cursor.getString("cd"));
                eq.setCdSelf(cursor.getString("cd_self"));
                eq.setName(cursor.getString("name"));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getLocalOneById(String fieldId, String searchId) {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + fieldId + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, searchId);
            ResultSet cursor = preparedStatement.executeQuery();
            if (cursor.next()) {
                ConstantLocal eq = new ConstantLocal();
                eq.setCd(cursor.getString("cd"));
                eq.setCdSelf(cursor.getString("cd_self"));
                eq.setName(cursor.getString("name"));
                cursor.close();
                return eq;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Constant getConstantById(String field, String search) {
        ConstantLocal cl = (ConstantLocal) this.getLocalOneById(field, search);
        if (cl != null) {
            Constant co = new Constant();
            co.setCd(cl.getCd());
            co.setCd_self(cl.getCdSelf());
            co.setName(cl.getName());
            co.setName(cl.getName());
            return co;
        } else {
            return null;
        }
    }

    public Constant getOneLocalById(String searchId) {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE cd=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, searchId);
            ResultSet cursor = preparedStatement.executeQuery();
            if (cursor.next()) {
                Constant eq = new Constant();
                eq.setCd(cursor.getString("cd"));
                eq.setCd_self(cursor.getString("cd_self"));
                eq.setName(cursor.getString("name"));
                cursor.close();
                return eq;
            } else {
                cursor.close();
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
