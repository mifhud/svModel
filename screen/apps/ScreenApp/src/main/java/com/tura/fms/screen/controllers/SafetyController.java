package com.tura.fms.screen.controllers;

import com.google.gson.Gson;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXSnackbarLayout;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.database.Activity_DB;
import com.tura.fms.screen.database.Actual_DB;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.SessionFMS;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.activity_model.ActivityData;
import com.tura.fms.screen.models.activity_model.ActivityDetail;
import com.tura.fms.screen.models.activity_model.ActivityModel;
import com.tura.fms.screen.models.activity_model.SubmitActivity;
import com.tura.fms.screen.models.actual_model.ActualDetail;
import com.tura.fms.screen.models.equipment.EquipmentLogStartRequest;
import com.tura.fms.screen.models.safety_status.SafetyStatusRequest;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;
import com.tura.fms.screen.presenter.ActivityPresenter;

import org.json.JSONObject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Callback;
import javafx.application.Platform;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static com.tura.fms.screen.constanst.ConstantValues.EQUIPMENT_HAULER;
import static com.tura.fms.screen.constanst.ConstantValues.STATUS_STATE_START;
import static org.javalite.app_config.AppConfig.p;

public class SafetyController implements Initializable {

    private NavigationUI navigationUI = new NavigationUI();

    @FXML
    private Button TitleBar;

    @FXML
    private GridPane gridata;

    private ObservableList<ActivityModel> recordsData = FXCollections.observableArrayList();

    // SessionFMS sessionFMS;

    // String token;

    ActivityData activityData;
    ActivityPresenter activityPresenter = new ActivityPresenter();
    Activity_DB activity_db;
    Actual_DB actual_db;

    ActualDetail actualDetail;

    @FXML
    VBox rootpane;

    JFXSnackbar snackbar;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MainApp.clockDashboard.stop();

        // sessionFMS = new SessionFMS();
        // sessionFMS.touchFms();
        // token = sessionFMS.getSessionFms("token");

        activity_db = new Activity_DB();
        actual_db = new Actual_DB();
        snackbar = new JFXSnackbar(rootpane);
        snackbar.setPrefWidth(300);
        snackbar.getStyleClass().add("-fx-background-color: #ccc;");

        drawerAction();

        TitleBar.setText(p("app.textLoading"));

        getData();
    }

    private void getData() {
        List<Object> localData = MainApp.constantDB.getLocalById("cd_self", "018");

        if (localData != null) {

            TitleBar.setText(p("app.titleSafety"));
            ActivityData act = new ActivityData();
            act.setOutputObject(localData);
            tampilkanData(act);

        } else {

            if (NetworkConnection.getServiceStatus()) {

                Call<ActivityData> call = APIClient.getInstance().GetActivity(MainApp.requestHeader, "018");
                call.enqueue(new Callback<ActivityData>() {

                    @Override
                    public void onResponse(Call<ActivityData> call, Response<ActivityData> response) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                TitleBar.setText(p("app.titleSafety"));
                                tampilkanData(response.body());
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<ActivityData> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

            } else {
                System.out.println("Service not available and local data is empty");
            }

        }
    }

    private void tampilkanData(ActivityData activityData) {

        int Col = 0;
        int Row = 0;

        String status;

        try {
            // activityData = activityPresenter.getDataRestApi(token, "018");

            if (activityData.getOutput() != null) {

                for (int i = 0; i < activityData.getOutput().size(); i++) {

                    /// System.out.println(activityData.getOutput().get(i).getCd());
                    ActivityDetail activityDetail = new ActivityDetail();
                    activityDetail.setCd(activityData.getOutput().get(i).getCd());
                    activityDetail.setName(activityData.getOutput().get(i).getName());
                    activityDetail.setCd_self(activityData.getOutput().get(i).getCd_self());

                    activity_db.insertData(activityDetail);

                    JFXButton rowbutton = new JFXButton(activityData.getOutput().get(i).getName());
                    VBox vboxForButtons = new VBox();

                    rowbutton.setButtonType(JFXButton.ButtonType.RAISED);
                    rowbutton.getStyleClass().add("button-activity");
                    rowbutton.setFont(Font.font("Arial", FontWeight.BOLD, 90));
                    rowbutton.setPrefHeight(80);
                    rowbutton.setPrefWidth(250);
                    rowbutton.setId(String.valueOf(i));

                    // rowbutton.(new Insets(0,0,0,0));
                    vboxForButtons.setPadding(new Insets(10, 5, 10, 5));

                    vboxForButtons.getChildren().add(rowbutton);

                    gridata.add(vboxForButtons, Col, Row);
                    gridata.setHalignment(rowbutton, HPos.CENTER);
                    gridata.setValignment(rowbutton, VPos.CENTER);

                    rowbutton.setOnAction((ActionEvent) -> {

                        if (MainApp.equipmentLocal == null) {

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    Utilities.showMessageDialog("Equipment not registered", "",
                                            MainApp.config("config.unit_id") + " tidak belum terdaftar dalam database");

                                    rowbutton.setDisable(false);
                                }
                            });

                        } else {

                            rowbutton.setDisable(true);

                            int index = Integer.valueOf(rowbutton.getId());

                            SafetyStatusRequest activity = new SafetyStatusRequest();
                            // if (Utilities.isLoaderHauler()) {
                            // if (MainApp.isOperator) {
                            // activity.setIdActual(MainApp.actualInfo.getId());
                            // activity.setIdLocation(MainApp.actualInfo.getId_location().getId());
                            // }
                            // } else {
                            // if (MainApp.isOperator) {
                            // activity.setIdOperationSupport(MainApp.actualSupport.getId());
                            // activity.setIdLocation(MainApp.actualSupport.getLocationId().getId());
                            // }
                            // }
                            activity.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                            activity.setIdOperationSupport(
                                    MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                            activity.setIdOperationLoader(
                                    MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
                            activity.setIdLocation(
                                    MainApp.actualInfo == null ? null : MainApp.actualInfo.getId_location().getId());
                            activity.setIdEquipment(MainApp.equipmentLocal.getId());
                            activity.setIdOperator(MainApp.user.getId());
                            activity.setCdSafety(activityData.getOutput().get(index).getCd());

                            Utilities.setSafetyStatus(activity, activityData.getOutput().get(index).getName(),
                                    new Runnable() {

                                        @Override
                                        public void run() {
                                            rowbutton.setDisable(false);
                                        }
                                    });

                            /** push to kafka */
                            Utilities.producerSafetyStatus(activityData.getOutput().get(index).getCd(),
                                    activityData.getOutput().get(index).getName());

                            snackbar.fireEvent(new JFXSnackbar.SnackbarEvent(new JFXSnackbarLayout("Success")));
                        }

                        TitleBar.fire();
                    });

                    Col++;

                    if (Col > 3) {
                        // Reset Column
                        Col = 0;
                        // Next Row
                        Row++;
                    }

                }

                status = "success";
            } else {
                status = "success";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void drawerAction() {
        TitleBar.setOnAction((ActionEvent evt) -> {
            TitleBar.setText(p("app.textLoading"));

            TitleBar.setDisable(true);

            navigationUI.back_dashboard(evt, new Runnable() {

                @Override
                public void run() {
                    TitleBar.setDisable(false);
                }
            });

            MainApp.clockDashboard.play();
        });
    }

}
