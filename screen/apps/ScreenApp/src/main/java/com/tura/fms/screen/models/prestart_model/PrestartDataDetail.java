package com.tura.fms.screen.models.prestart_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrestartDataDetail {

    @SerializedName("id")
    @Expose
    String id;

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("alias")
    @Expose
    String alias;

    @SerializedName("type")
    @Expose
    String type;

    @SerializedName("__cd_type_equipment__")
    @Expose
    PrestartEquipment prestartEquipment;

    @SerializedName("__has_cd_type_equipment__")
    @Expose
    Boolean has_cd_type_equipment;

    @SerializedName("severinity")
    @Expose
    int severinity;

    @SerializedName("date_created")
    @Expose
    int date_created;

    @SerializedName("date_modified")
    @Expose
    int date_modified;

    @SerializedName("is_deleted")
    @Expose
    int is_deleted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PrestartEquipment getPrestartEquipment() {
        return prestartEquipment;
    }

    public void setPrestartEquipment(PrestartEquipment prestartEquipment) {
        this.prestartEquipment = prestartEquipment;
    }

    public Boolean getHas_cd_type_equipment() {
        return has_cd_type_equipment;
    }

    public void setHas_cd_type_equipment(Boolean has_cd_type_equipment) {
        this.has_cd_type_equipment = has_cd_type_equipment;
    }

    public int getSeverinity() {
        return severinity;
    }

    public void setSeverinity(int severinity) {
        this.severinity = severinity;
    }

    public int getDate_created() {
        return date_created;
    }

    public void setDate_created(int date_created) {
        this.date_created = date_created;
    }

    public int getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(int date_modified) {
        this.date_modified = date_modified;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }
}
