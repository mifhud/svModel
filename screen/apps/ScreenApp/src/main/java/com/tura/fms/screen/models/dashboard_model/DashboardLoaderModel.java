package com.tura.fms.screen.models.dashboard_model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DashboardLoaderModel {

    private final StringProperty id;
    private final StringProperty name;
    private final StringProperty decription;


    public DashboardLoaderModel(String id, String name, String description) {
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.decription = new SimpleStringProperty(description);
    }

    public String getId() {
        return id.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public StringProperty idProperty() {
        return id;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getDescription() {
        return decription.get();
    }

    public void setDescription(String decription) {
        this.decription.set(decription);
    }

    public StringProperty descriptionProperty() {
        return decription;
    }

}
