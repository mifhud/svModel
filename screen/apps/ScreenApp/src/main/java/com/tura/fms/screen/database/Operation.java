package com.tura.fms.screen.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Operation {

    private String tableName;
    private String id;

    public Operation(String table, String ids) {
        tableName = table;
        id = ids;
    }

    public String getId() {
        return id;
    }

    public static Connection conn() {
        return SQLiteJDBC.openDB();
    }

    public static boolean isExists(String tableName) {
        try {
            DatabaseMetaData md = conn().getMetaData();
            ResultSet rs = md.getTables(null, null, tableName, null);
            if (rs.next()) {
                return true;
            } else {
                rs.close();
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean isExists() {
        try {
            DatabaseMetaData md = conn().getMetaData();
            ResultSet rs = md.getTables(null, null, tableName, null);
            if (rs.next()) {
                return true;
            } else {
                rs.close();
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public int localCount() {
        try {
            Statement s = conn().createStatement();
            ResultSet r = s.executeQuery("SELECT COUNT(*) AS rowcount FROM " + tableName);
            r.next();
            int count = r.getInt("rowcount");
            r.close();
            s.close();
            return count;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    /**
     * harus di override di child class
     *
     * @return Object
     */
    public Object getFromServer(Integer page, Integer size) {
        return null;
    }

    /**
     * harus di override di child class
     *
     * @return Object
     */
    public Object getFromServerById(String id) {
        return null;
    }

    /**
     * harus di override di child class
     *
     */
    public Object insertIgnore(Object eq) {
        return null;
    }

    /**
     * harus di override di child class
     *
     */
    public void setSynchronized(String id) {
    }

    /**
     * harus di override di child class
     *
     */
    public void postBulkData(List<Object> eq) {
    }

    public void postBulkData(List<Object> eq, Runnable cbSuccess, Runnable cdFail) {
    }

    public void postData(Object eq, Runnable callback) {
    }

    /**
     * harus di override di child class
     *
     */
    public void deleteData(String id) {
    }

    /**
     * harus di override di child class
     *
     * @return Object
     */
    public Integer getRemoteCount() {
        return null;
    }

    /**
     * harus di override di child class
     *
     * @return Object
     */
    public List<Object> getFlaggedData() {
        return null;
    }

    public Object getFlaggedDataOne() {
        return null;
    }

    /**
     * harus di override di child class
     *
     * @return Object
     */
    public Object getLocalOneById(String fieldId, String searchId) {
        return null;
    }

    public List<Object> getLocalById(String fieldId, String searchId) {
        return null;
    }

    /**
     * delete salah satu data untuk enable sync
     *
     */
    public void triggerSync() {
        try {
            Statement s = conn().createStatement();
            String sql = "delete from " + tableName;
            ResultSet r = s.executeQuery(sql);
            r.next();
            r.close();
            s.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
