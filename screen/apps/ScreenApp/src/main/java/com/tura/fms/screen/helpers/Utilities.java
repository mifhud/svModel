package com.tura.fms.screen.helpers;

import static org.javalite.app_config.AppConfig.p;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.OperatingSystemMXBean;
import java.net.ConnectException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeoutException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.JsonNode;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXSpinner;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.constanst.IKafkaConstant;
import com.tura.fms.screen.controllers.BreakdownController;
import com.tura.fms.screen.controllers.loader.DashboardLoaderController;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.models.DefaultResponse;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.ResponseModelCustom;
import com.tura.fms.screen.models.ScreenConfigLocal;
import com.tura.fms.screen.models.actual_model.ActualData;
import com.tura.fms.screen.models.actual_model.ActualDetail;
import com.tura.fms.screen.models.actual_model.ActualList;
import com.tura.fms.screen.models.actual_model.ActualLoaderData;
import com.tura.fms.screen.models.actual_support.CdActivitySupport;
import com.tura.fms.screen.models.actual_support.EquipmentSupport;
import com.tura.fms.screen.models.actual_support.LocationSupport;
import com.tura.fms.screen.models.actual_support.OperationSupport;
import com.tura.fms.screen.models.actual_support.SupportData;
import com.tura.fms.screen.models.actual_support.UserSupport;
import com.tura.fms.screen.models.config.ScreenConfig;
import com.tura.fms.screen.models.config.ScreenConfigResponse;
import com.tura.fms.screen.models.constant.Constant;
import com.tura.fms.screen.models.ecu.HMLocal;
import com.tura.fms.screen.models.equipment.EquipmentAPI;
import com.tura.fms.screen.models.equipment.EquipmentLocal;
import com.tura.fms.screen.models.equipment.EquipmentLogStartLocal;
import com.tura.fms.screen.models.equipment.EquipmentLogStartRequest;
import com.tura.fms.screen.models.equipment_category.EquipmentCategory;
import com.tura.fms.screen.models.equipment_category.EquipmentCategoryLocal;
import com.tura.fms.screen.models.fatigue_model.FatigueLogLocal;
import com.tura.fms.screen.models.fatigue_model.FatigueModel;
import com.tura.fms.screen.models.gps.GPSLocal;
import com.tura.fms.screen.models.hour_meter.DailyHourMeter;
import com.tura.fms.screen.models.location.Area;
import com.tura.fms.screen.models.maintenance_model.task_model.MaintenanceDescription;
import com.tura.fms.screen.models.maintenance_model.task_model.MaintenanceDescriptionPost;
import com.tura.fms.screen.models.maintenance_model.task_model.MaintenanceDetails;
import com.tura.fms.screen.models.maintenance_model.task_model.MaintenanceDetailsPost;
import com.tura.fms.screen.models.maintenance_model.task_model.MaintenanceDetailsStopPost;
import com.tura.fms.screen.models.maintenance_model.task_model.Taskmodel;
import com.tura.fms.screen.models.operation.OperationLocal;
import com.tura.fms.screen.models.operation_log_detail.OperationLogDetailLocal;
import com.tura.fms.screen.models.operation_status.OperationStatusLocal;
import com.tura.fms.screen.models.prestart_model.submit.PrestartSubmit;
import com.tura.fms.screen.models.safety_status.SafetyStatusLocal;
import com.tura.fms.screen.models.safety_status.SafetyStatusRequest;
import com.tura.fms.screen.models.user_model.UserAPILocal;
import com.tura.fms.screen.models.user_model.UserData;
import com.tura.fms.screen.models.user_model.UserDetails;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;
import com.tura.fms.screen.presenter.CustomRunnable;
import com.tura.fms.screen.presenter.CustomRunnableX;

import org.json.JSONException;
import org.json.JSONObject;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Utilities {

    private static final Random random = new Random();

    public static DecodedJWT decodedJWT = null;

    public static ActionEvent latestEvent = null;

    public static ObservableList<FatigueModel> fatigueData = FXCollections.observableArrayList();

    public static String getMd5(String input) {
        try {

            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method is called to calculate message digest
            // of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes(StandardCharsets.UTF_8));

            // Convert byte array into signum representation
            StringBuilder sb = new StringBuilder();

            // Convert message digest into hex value
            for (byte b : messageDigest) {
                sb.append(String.format("%02x", b));
            }

            return sb.toString();
        }

        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();

        return dateFormat.format(date);
    }

    public static String getCurrentDateOnly() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();

        return dateFormat.format(date);
    }

    public static Timestamp convertStringToTimestamp(String str_date) {
        try {
            DateFormat formatter;
            formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date date = (Date) formatter.parse(str_date);
            java.sql.Timestamp timeStampDate = new Timestamp(date.getTime() / 1000);
            return timeStampDate;
        } catch (ParseException e) {
            // e.printStackTrace();
            MainApp.log.error("utilities", e);
            return null;
        }
    }

    public static Timestamp getCurrentTimestamp() {
        Calendar cal = Calendar.getInstance(MainApp.timezone);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        java.sql.Timestamp timeStampDate = new Timestamp(cal.getTimeInMillis() / 1000);
        return timeStampDate;
    }

    public static Long getCurrentUnix() {
        Calendar cal = Calendar.getInstance(MainApp.timezone);
        return cal.getTimeInMillis() / 1000;
    }

    public static Long getStartOfDayUnix() {
        Calendar cal = Calendar.getInstance(MainApp.timezone);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal.getTimeInMillis() / 1000;
    }

    public static Long getStartOfShift() {
        Calendar cal = Calendar.getInstance(MainApp.timezone);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        if (hour >= 6 && hour < 18) {
            /**
             * shift siang
             */
            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.HOUR_OF_DAY, 6);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            return cal.getTimeInMillis() / 1000;
        } else if (hour < 6) {
            /**
             * shift malam yg kemarin
             */
            cal.set(Calendar.DATE, -1);
            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.HOUR_OF_DAY, 18);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            return cal.getTimeInMillis() / 1000;
        } else {
            /**
             * shift malam tanggal current
             */
            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.HOUR_OF_DAY, 18);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            return cal.getTimeInMillis() / 1000;
        }
    }

    public static Date getEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance(MainApp.timezone);
        if (date != null) {
            calendar.setTime(date);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public static Date getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance(MainApp.timezone);
        if (date != null) {
            calendar.setTime(date);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getTomorrowDate(Date date) {
        Calendar calendar = Calendar.getInstance(MainApp.timezone);
        if (date != null) {
            calendar.setTime(date);
        }
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }

    public static Boolean isSameDay(Date dA, Date dB) {
        Calendar cal1 = Calendar.getInstance(MainApp.timezone);
        Calendar cal2 = Calendar.getInstance(MainApp.timezone);
        cal1.setTime(dA);
        cal2.setTime(dB);
        boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)
                && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
        return sameDay;
    }

    public static Boolean isSameDateAndShift(Long date1, String shift1, Long date2, String shift2) {
        Date d1 = new Date(date1 * 1000);
        Date d2 = new Date(date2 * 1000);
        if (isSameDay(d1, d2)) {
            if (shift1.equals(shift2)) {
                return true;
            } else {
                return false;
            }
        } else {
            if (shift1.equals(ConstantValues.SHIFT_NIGHT) && shift2.equals(ConstantValues.SHIFT_NIGHT)) {
                Date dd = new Date();
                if (date1 < date2) {
                    dd.setTime(date2);
                } else {
                    dd.setTime(date1);
                }
                Calendar cal = Calendar.getInstance(MainApp.timezone);
                cal.setTime(dd);
                if (cal.get(Calendar.HOUR_OF_DAY) <= 6) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public static String getTanggal() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getHariTanggal() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getMacAddress() {
        String result = "";

        try {
            for (NetworkInterface ni : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                byte[] hardwareAddress = ni.getHardwareAddress();

                if (hardwareAddress != null) {
                    for (int i = 0; i < hardwareAddress.length; i++)
                        result += String.format((i == 0 ? "" : "-") + "%02X", hardwareAddress[i]);

                    return result;
                }
            }

        } catch (SocketException e) {
            System.out.println("Could not find out MAC Adress. Exiting Application ");
            System.exit(1);
            MainApp.log.error("utilities", e);
        }
        return result;
    }

    public static String timeStampToDate(Long tanggal, String tipe) {
        if (tanggal == null) {
            return "-";
        }
        Date startWaktu = new Date(tanggal * 1000);
        DateFormat tanggal_mulai;
        if (tipe == "tanggal")
            tanggal_mulai = new SimpleDateFormat("MMM d, yyyy");
        else if (tipe == "only_tanggal")
            tanggal_mulai = new SimpleDateFormat("dd");
        else if (tipe == "only_month")
            tanggal_mulai = new SimpleDateFormat("MMM");
        else if (tipe == "day")
            tanggal_mulai = new SimpleDateFormat("EEEE");
        else if (tipe == "tanggal_mysql")
            tanggal_mulai = new SimpleDateFormat("yyyy-MM-dd");
        else if (tipe == "tanggal_started")
            tanggal_mulai = new SimpleDateFormat("MMM d, yyyy, h:mm:ss a");
        else if (tipe == "tanggal_timer")
            tanggal_mulai = new SimpleDateFormat("h:mm:ss a");
        else if (tipe == "hari")
            tanggal_mulai = new SimpleDateFormat("EEEE, dd MMMM yyyy");
        else
            tanggal_mulai = new SimpleDateFormat("HH:mm:ss");

        String tanggal_mulai1 = tanggal_mulai.format(startWaktu);

        return tanggal_mulai1;
    }

    public static String getCurrentHourMinute() {
        DateFormat hourMinute = new SimpleDateFormat("HH:mm");
        Date naw = new Date();
        return hourMinute.format(naw);
    }

    public static Long dateStringToTime(String dateStr) {
        try {
            Date dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateStr);
            // System.out.println("------ convert date " + dateStr);
            // System.out.println(dt.getTime());
            return dt.getTime() / 1000;
        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);
            return 0L;
        }
    }

    public static String getCurrentTimeString() {
        DateFormat current = new SimpleDateFormat("HH:mm:ss");
        String curString = current.format(new Date());
        return curString;
    }

    public static int createRandomNumberBetween(int min, int max) {
        return random.nextInt(max - min + 1) + min;
    }

    public static boolean authenticateToken(String token) {
        if (token == null) {
            return false;
        }
        if (token.length() == 0) {
            return false;
        }
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(IKafkaConstant.JWT_TOKEN_KEY)).build();
            verifier.verify(token);
            Utilities.decodedJWT = JWT.decode(token);
            return true;
        } catch (SignatureVerificationException | JWTDecodeException e) {
            Utilities.decodedJWT = null;
            return false;
        }
    }

    public static String decodedId() {
        if (Utilities.decodedJWT == null) {
            return null;
        }
        return Utilities.decodedJWT.getClaim("id").asString();
    }

    public static String decodedEmail() {
        if (Utilities.decodedJWT == null) {
            return null;
        }
        return Utilities.decodedJWT.getClaim("email").asString();
    }

    public static Map<String, Object> decodedRole() {
        if (Utilities.decodedJWT == null) {
            return null;
        }
        return Utilities.decodedJWT.getClaim("cd_role").asMap();
    }

    public static Map<String, Object> decodedDepartment() {
        if (Utilities.decodedJWT == null) {
            return null;
        }
        return Utilities.decodedJWT.getClaim("cd_department").asMap();
    }

    public static String decodedStaffId() {
        if (Utilities.decodedJWT == null) {
            return null;
        }
        return Utilities.decodedJWT.getClaim("staff_id").asString();
    }

    public static String decodedFingerId() {
        if (Utilities.decodedJWT == null) {
            return null;
        }
        return Utilities.decodedJWT.getClaim("fingerprint_id").asString();
    }

    public static String decodedFilePicture() {
        if (Utilities.decodedJWT == null) {
            return null;
        }
        return Utilities.decodedJWT.getClaim("file_picture").asString();
    }

    public static Integer decodedIsAuthenticated() {
        if (Utilities.decodedJWT == null) {
            return null;
        }
        return Utilities.decodedJWT.getClaim("is_authenticated").asInt();
    }

    public static Integer decodedIAT() {
        if (Utilities.decodedJWT == null) {
            return null;
        }
        return Utilities.decodedJWT.getClaim("iat").asInt();
    }

    public static Integer decodedEXP() {
        if (Utilities.decodedJWT == null) {
            return null;
        }
        return Utilities.decodedJWT.getClaim("exp").asInt();
    }

    public static String getCurrentShift() {
        Calendar cal = new GregorianCalendar(MainApp.timezone);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        if (hour >= 6 && hour < 18) {
            return ConstantValues.SHIFT_DAY;
        } else {
            return ConstantValues.SHIFT_NIGHT;
        }
    }

    public static String getCurrentDate(String format) {
        SimpleDateFormat fmt = new SimpleDateFormat(format);
        fmt.setTimeZone(MainApp.timezone);
        return fmt.format(new Date());
    }

    public static boolean isLoaderHauler() {
        if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)
                || MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isLoader() {
        if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isHauler() {
        if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER)) {
            return true;
        } else {
            return false;
        }
    }

    public static void getOperationLoader() {
        if (NetworkConnection.getServiceStatus()) {

            if (isLoader()) {

                String tanggal = Utilities.getCurrentUnix().toString();
                Call<ActualLoaderData> call = APIClient.getInstance().getActualLoader(MainApp.requestHeader, tanggal,
                        MainApp.config("config.unit_id").trim(), "");
                call.enqueue(new Callback<ActualLoaderData>() {
                    @Override
                    public void onResponse(Call<ActualLoaderData> call, Response<ActualLoaderData> response) {

                        if (response.isSuccessful()) {
                            System.out.println("----------------------- operation loader");

                            MainApp.actualLoader = response.body().getOutput();

                            if (MainApp.actualLoader != null) {
                                MainApp.dashboardInfo.setStatus(MainApp.actualLoader.getCd_operation().getName());
                                MainApp.dashboardInfo.setLoadingPoint(MainApp.actualLoader.getId_location().getName());
                                MainApp.dashboardInfo
                                        .setDumpingPoint(MainApp.actualLoader.getId_destination().getName());

                                setDashboardInfo();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ActualLoaderData> call, Throwable t) {
                        // Utilities.showMessageDialog("Exception", "", "Tidak dapat mengambil data
                        // konfigurasi");
                        // t.printStackTrace();

                        if (t instanceof ConnectException) {
                            Utilities.messageSocketException(t, "Get OperationLoader");
                        } else if (t instanceof TimeoutException) {
                            Utilities.messageTimeoutException(t, "Get OperationLoader");
                        } else {
                            Utilities.messageOtherException(t, "Get OperationLoader");
                        }
                    }
                });
            }

        } else {

            // todo
            System.out.println("get operation loader, offline mode");

        }
    }

    public static void getOperationActual() {
        if (NetworkConnection.getServiceStatus()) {

            if (isLoaderHauler()) {

                String tanggal = Utilities.getCurrentUnix().toString();
                Call<ActualData> call = APIClient.getInstance().getActual(MainApp.requestHeader, tanggal,
                        MainApp.config("config.unit_id").trim(), MainApp.config("config.unit_type").trim(), "");
                call.enqueue(new Callback<ActualData>() {
                    @Override
                    public void onResponse(Call<ActualData> call, Response<ActualData> response) {
                        // System.out.println(MainApp.actualInfo);
                        if (response.isSuccessful()) {

                            if (response.body() != null) {
                                MainApp.actualInfo = response.body().getOutput();

                                if (MainApp.actualInfo != null) {
                                    EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
                                    if (Utilities.isLoaderHauler()) {
                                        activity.setIdActual(MainApp.actualInfo.getId());
                                    } else {
                                        activity.setIdOperationSupport(MainApp.actualSupport.getId());
                                    }
                                    activity.setIdEquipment(MainApp.equipmentLocal.getId());
                                    activity.setIdOperator(MainApp.user.getId());
                                    activity.setTime("current");
                                    activity.setCdActivity(MainApp.actualInfo.getCd_operation().getCd());
                                    Utilities.setActivity(activity);

                                    MainApp.dashboardInfo.setStatus(MainApp.actualInfo.getCd_operation().getName());
                                    MainApp.dashboardInfo
                                            .setLoadingPoint(MainApp.actualInfo.getId_location().getName());
                                    MainApp.dashboardInfo
                                            .setDumpingPoint(MainApp.actualInfo.getId_destination().getName());

                                    setDashboardInfo();
                                }

                            }

                        }
                        // System.out.println(">>>>>>>>>>>>>>>>>>>>> new actual");
                        // System.out.println(MainApp.actualInfo);
                    }

                    @Override
                    public void onFailure(Call<ActualData> call, Throwable t) {
                        // Utilities.showMessageDialog("Exception", "", "Tidak dapat mengambil data
                        // konfigurasi");
                        // t.printStackTrace();

                        if (t instanceof ConnectException) {
                            Utilities.messageSocketException(t, "Get OperationActual");
                        } else if (t instanceof TimeoutException) {
                            Utilities.messageTimeoutException(t, "Get OperationActual");
                        } else {
                            Utilities.messageOtherException(t, "Get OperationActual");
                        }
                    }
                });

            } else {

                getOperationSupportActual();

            }

        } else {

            // todo
            System.out.println("get operation actual, offline mode");

        }
    }

    public static void getOperationSupportActual() {
        if (NetworkConnection.getServiceStatus()) {

            String tanggal = Utilities.getCurrentUnix().toString();
            Call<SupportData> call = APIClient.getInstance().getOperationSupport(MainApp.requestHeader, tanggal,
                    MainApp.config("config.unit_id").trim(), MainApp.config("config.unit_type").trim(), "");

            call.enqueue(new Callback<SupportData>() {
                @Override
                public void onResponse(Call<SupportData> call, Response<SupportData> response) {
                    if (response.isSuccessful()) {

                        SupportData actual = response.body();
                        MainApp.actualSupport = new OperationSupport();
                        if (actual.getOutput() != null) {
                            MainApp.actualSupport.setId(actual.getOutput().getId());
                            MainApp.actualSupport.setDate(actual.getOutput().getDate());
                            MainApp.actualSupport.setHmStart(actual.getOutput().getHm_start());
                            MainApp.actualSupport.setHmStop(actual.getOutput().getHm_stop());
                            MainApp.actualSupport.setCreatedAt(actual.getOutput().getCreated_at());
                            MainApp.actualSupport.setIsDeleted(actual.getOutput().getIs_deleted());
                            if (actual.getOutput().getEquipment_id() != null) {
                                EquipmentSupport es = new EquipmentSupport();
                                es.setId(actual.getOutput().getEquipment_id().getId());
                                es.setName(actual.getOutput().getEquipment_id().getName());
                                es.setBrand(actual.getOutput().getEquipment_id().getBrand());
                                es.setClass_(actual.getOutput().getEquipment_id().getClass_());
                                MainApp.actualSupport.setEquipmentId(es);
                            }
                            if (actual.getOutput().getLocation_id() != null) {
                                LocationSupport ls = new LocationSupport();
                                ls.setId(actual.getOutput().getLocation_id().getId());
                                ls.setName(actual.getOutput().getLocation_id().getName());
                                ls.setCdLocation(actual.getOutput().getLocation_id().getCd_location());
                                ls.setLatitude(actual.getOutput().getLocation_id().getLatitude());
                                ls.setLongitude(actual.getOutput().getLocation_id().getLongitude());
                                ls.setAltitude(actual.getOutput().getLocation_id().getAltitude());
                                MainApp.actualSupport.setLocationId(ls);
                            }
                            if (actual.getOutput().getDestination_id() != null) {
                                LocationSupport ls = new LocationSupport();
                                ls.setId(actual.getOutput().getDestination_id().getId());
                                ls.setName(actual.getOutput().getDestination_id().getName());
                                ls.setCdLocation(actual.getOutput().getDestination_id().getCd_location());
                                ls.setLatitude(actual.getOutput().getDestination_id().getLatitude());
                                ls.setLongitude(actual.getOutput().getDestination_id().getLongitude());
                                ls.setAltitude(actual.getOutput().getDestination_id().getAltitude());
                                MainApp.actualSupport.setDestinationId(ls);
                            }
                            if (actual.getOutput().getCd_activity() != null) {
                                CdActivitySupport as = new CdActivitySupport();
                                as.setCd(actual.getOutput().getCd_activity().getCd());
                                as.setCdSelf(actual.getOutput().getCd_activity().getCd_self());
                                as.setName(actual.getOutput().getCd_activity().getName());
                                MainApp.actualSupport.setCdActivity(as);
                            }
                            if (actual.getOutput().getCd_shift() != null) {
                                CdActivitySupport cs = new CdActivitySupport();
                                cs.setCd(actual.getOutput().getCd_shift().getCd());
                                cs.setCdSelf(actual.getOutput().getCd_shift().getCd_self());
                                cs.setName(actual.getOutput().getCd_shift().getName());
                                MainApp.actualSupport.setCdShift(cs);
                            }
                            if (actual.getOutput().getOperator_id() != null) {
                                UserSupport us = new UserSupport();
                                us.setId(actual.getOutput().getOperator_id().getId());
                                us.setName(actual.getOutput().getOperator_id().getName());
                                us.setEmail(actual.getOutput().getOperator_id().getEmail());
                                us.setStaffId(actual.getOutput().getOperator_id().getStaff_id());
                                us.setCdRole(actual.getOutput().getOperator_id().getCd_role());
                                us.setCdDepartment(actual.getOutput().getOperator_id().getCd_department());
                                us.setIdOrg(actual.getOutput().getOperator_id().getId_org());
                                MainApp.actualSupport.setOperatorId(us);
                            }
                            if (actual.getOutput().getSupervisor_id() != null) {
                                UserSupport us = new UserSupport();
                                us.setId(actual.getOutput().getSupervisor_id().getId());
                                us.setName(actual.getOutput().getSupervisor_id().getName());
                                us.setEmail(actual.getOutput().getSupervisor_id().getEmail());
                                us.setStaffId(actual.getOutput().getSupervisor_id().getStaff_id());
                                us.setCdRole(actual.getOutput().getSupervisor_id().getCd_role());
                                us.setCdDepartment(actual.getOutput().getSupervisor_id().getCd_department());
                                us.setIdOrg(actual.getOutput().getSupervisor_id().getId_org());
                                MainApp.actualSupport.setSupervisorId(us);
                            }
                            if (actual.getOutput().getGroup_leader_id() != null) {
                                UserSupport us = new UserSupport();
                                us.setId(actual.getOutput().getGroup_leader_id().getId());
                                us.setName(actual.getOutput().getGroup_leader_id().getName());
                                us.setEmail(actual.getOutput().getGroup_leader_id().getEmail());
                                us.setStaffId(actual.getOutput().getGroup_leader_id().getStaff_id());
                                us.setCdRole(actual.getOutput().getGroup_leader_id().getCd_role());
                                us.setCdDepartment(actual.getOutput().getGroup_leader_id().getCd_department());
                                us.setIdOrg(actual.getOutput().getGroup_leader_id().getId_org());
                                MainApp.actualSupport.setGroupLeaderId(us);
                            }

                            EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
                            if (Utilities.isLoaderHauler()) {
                                activity.setIdActual(MainApp.actualInfo.getId());
                            } else {
                                activity.setIdOperationSupport(MainApp.actualSupport.getId());
                            }
                            activity.setIdEquipment(MainApp.equipmentLocal.getId());
                            activity.setIdOperator(MainApp.user.getId());
                            activity.setTime("current");
                            activity.setCdActivity(MainApp.actualSupport.getCdActivity().getCd());
                            Utilities.setActivity(activity);

                            MainApp.dashboardInfo.setStatus(MainApp.actualSupport.getCdActivity().getName());
                            if (MainApp.actualSupport.getLocationId() != null) {
                                MainApp.dashboardInfo.setLoadingPoint(MainApp.actualSupport.getLocationId().getName());
                            }
                            if (MainApp.actualSupport.getDestinationId() != null) {
                                MainApp.dashboardInfo
                                        .setDumpingPoint(MainApp.actualSupport.getDestinationId().getName());
                            }

                            setDashboardInfo();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SupportData> call, Throwable t) {
                    // Utilities.showMessageDialog("Exception", "", "Tidak dapat mengambil data
                    // konfigurasi");
                    // t.printStackTrace();

                    if (t instanceof ConnectException) {
                        Utilities.messageSocketException(t, "Get OperationSupport");
                    } else if (t instanceof TimeoutException) {
                        Utilities.messageTimeoutException(t, "Get OperationSupport");
                    } else {
                        Utilities.messageOtherException(t, "Get OperationSupport");
                    }
                }
            });

        } else {

            // todo
            System.out.println("get operation support actual, offline mode");

        }
    }

    public static void setDashboardInfo() {
        if (isHauler()) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    // playSound("");

                    setExcavatorLabel("EXCAVATOR : " + MainApp.actualInfo.getId_loader().getName());
                    NavigationUI.setOperatorName("Operator : " + MainApp.user.getName());
                    NavigationUI.setLocationName("DATE : " + timeStampToDate(MainApp.actualInfo.getDate(), "tanggal"));
                    NavigationUI.setDestinationName("SHIFT : " + MainApp.actualInfo.getCd_shift().getName());
                }
            });
        } else if (isLoader()) {

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    // playSound("");

                    NavigationUI.setOperatorName("Operator : " + MainApp.user.getName());
                    NavigationUI
                            .setLocationName("DATE : " + timeStampToDate(MainApp.actualLoader.getDate(), "tanggal"));
                    NavigationUI.setDestinationName("SHIFT : " + MainApp.actualLoader.getCd_shift().getName());
                }

            });

        } else {

        }
    }

    public static void getUnitArea() {
        if (NetworkConnection.getServiceStatus()) {

            if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)) {

                Call<ResponseModel> call = APIClient.getInstance().getLoaderArea(MainApp.requestHeader,
                        MainApp.actualLoader.getId());
                call.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        if (response.isSuccessful()) {
                            Map<String, Object> area = Utilities.mapObject(response.body().getOutput());
                            if (area == null) {
                                return;
                            }
                            Map<String, Object> areaInfo = Utilities.mapObject(area.get("id_location"));
                            if (areaInfo == null) {
                                return;
                            }
                            Map<String, Object> areaDetail = Utilities.mapObject(areaInfo.get("id_area"));
                            if (areaDetail == null) {
                                return;
                            }
                            MainApp.area = new Area();
                            MainApp.area.setId(areaDetail.get("id").toString());
                            MainApp.area.setName(areaDetail.get("name").toString());
                            MainApp.area.setLatitude(Double.parseDouble(areaDetail.get("latitude").toString()));
                            MainApp.area.setLongitude(Double.parseDouble(areaDetail.get("longitude").toString()));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        // t.printStackTrace();

                        if (t instanceof ConnectException) {
                            Utilities.messageSocketException(t, "Get LoaderArea");
                        } else if (t instanceof TimeoutException) {
                            Utilities.messageTimeoutException(t, "Get LoaderArea");
                        } else {
                            Utilities.messageOtherException(t, "Get LoaderArea");
                        }
                    }
                });

            } else if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER)) {

                Call<ResponseModel> call = APIClient.getInstance().getHaulerArea(MainApp.requestHeader,
                        MainApp.actualInfo.getId());
                call.enqueue(new Callback<ResponseModel>() {

                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        if (response.isSuccessful()) {
                            Map<String, Object> area = Utilities.mapObject(response.body().getOutput());
                            if (area == null) {
                                return;
                            }
                            Map<String, Object> areaInfo = Utilities.mapObject(area.get("__id_location__"));
                            if (areaInfo == null) {
                                return;
                            }
                            Map<String, Object> areaDetail = Utilities.mapObject(areaInfo.get("id_area"));
                            if (areaDetail == null) {
                                return;
                            }
                            MainApp.area = new Area();
                            MainApp.area.setId(areaDetail.get("id").toString());
                            MainApp.area.setName(areaDetail.get("name").toString());
                            MainApp.area.setLatitude(Double.parseDouble(areaDetail.get("latitude").toString()));
                            MainApp.area.setLongitude(Double.parseDouble(areaDetail.get("longitude").toString()));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        // t.printStackTrace();

                        if (t instanceof ConnectException) {
                            Utilities.messageSocketException(t, "Get Haule Area");
                        } else if (t instanceof TimeoutException) {
                            Utilities.messageTimeoutException(t, "Get Hauler Area");
                        } else {
                            Utilities.messageOtherException(t, "Get Hauler Area");
                        }
                    }
                });

            } else {

                Call<ResponseModel> call = APIClient.getInstance().getSupportArea(MainApp.requestHeader,
                        MainApp.actualInfo.getId());
                call.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        if (response.isSuccessful()) {
                            Map<String, Object> area = Utilities.mapObject(response.body().getOutput());
                            if (area == null) {
                                return;
                            }
                            Map<String, Object> areaInfo = Utilities.mapObject(area.get("id_location"));
                            if (areaInfo == null) {
                                return;
                            }
                            Map<String, Object> areaDetail = Utilities.mapObject(areaInfo.get("id_area"));
                            if (areaDetail == null) {
                                return;
                            }
                            MainApp.area = new Area();
                            MainApp.area.setId(areaDetail.get("id").toString());
                            MainApp.area.setName(areaDetail.get("name").toString());
                            MainApp.area.setLatitude(Double.parseDouble(areaDetail.get("latitude").toString()));
                            MainApp.area.setLongitude(Double.parseDouble(areaDetail.get("longitude").toString()));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        // t.printStackTrace();
                        if (t instanceof ConnectException) {
                            Utilities.messageSocketException(t, "Get Support Area");
                        } else if (t instanceof TimeoutException) {
                            Utilities.messageTimeoutException(t, "Get Support Area");
                        } else {
                            Utilities.messageOtherException(t, "Get Support Area");
                        }
                    }
                });
            }

            if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)) {
                System.out.println("------------- loader");
                Call<ResponseModel> call = APIClient.getInstance().getLoaderArea(MainApp.requestHeader,
                        MainApp.actualLoader.getId());
                call.enqueue(new Callback<ResponseModel>() {

                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        if (response.isSuccessful()) {
                            // MainApp.area = (Area) response.body().getOutput()
                            Map<String, Object> area = Utilities.mapObject(response.body().getOutput());
                            System.out.println(area);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        t.printStackTrace();
                    }

                });
            }

        } else {

            // todo
            System.out.println("get hauler list, offline mode");

        }
    }

    public static void getHaulerListAsync(Runnable callback, CustomRunnable cbFail) {
        if (NetworkConnection.getServiceStatus()) {

            if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)) {
                Call<ActualList> callLoader = APIClient.getInstance().getCurrentHauler(MainApp.requestHeader,
                        MainApp.config("config.unit_id").trim());
                callLoader.enqueue(new Callback<ActualList>() {
                    @Override
                    public void onResponse(Call<ActualList> call, Response<ActualList> response) {
                        // ActualList loaderResponse = callLoader.execute().body();
                        if (response.isSuccessful()) {
                            MainApp.haulerList = response.body().getOutput();
                        }
                        // System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> hauler list");
                        // System.out.println(MainApp.haulerList);

                        if (callback != null) {
                            callback.run();
                        }
                    }

                    @Override
                    public void onFailure(Call<ActualList> call, Throwable t) {
                        // Utilities.showMessageDialog("Exception", "", "Tidak dapat mengambil data
                        // hauler");

                        if (t instanceof ConnectException) {
                            Utilities.messageSocketException(t, "Get HaulerList");
                        } else if (t instanceof TimeoutException) {
                            Utilities.messageTimeoutException(t, "Get HaulerList");
                        } else {
                            Utilities.messageOtherException(t, "Get HaulerList");
                        }

                        if (cbFail != null) {
                            cbFail.setData("Tidak dapat mengambil data hauler");
                            cbFail.run();
                        }
                        // t.printStackTrace();
                    }
                });
            }

        } else {

            // todo
            System.out.println("get hauler list, offline mode");

        }
    }

    public static void playSound(String soundCode) {
        String fn = new File(System.getProperty("user.dir") + "/sounds/alert1.wav").toURI().toString();
        Media media = new Media(fn);
        MediaPlayer player = new MediaPlayer(media);
        player.setVolume(100);
        player.play();
    }

    public static void setActivity(EquipmentLogStartRequest param, Runnable callback) {

        if (NetworkConnection.getServiceStatus()) {

            param.setCdType(MainApp.config("config.unit_type"));

            GPS gps = Utilities.getCurrentGPS();
            param.setLat(gps.getLat().toString());
            param.setLon(gps.getLon().toString());

            Call<DefaultResponse> call = APIClient.getInstance().setActivity(MainApp.requestHeader, param);
            call.enqueue(new Callback<DefaultResponse>() {
                @Override
                public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                    System.out.println(response.body().getRespon());
                    System.out.println(response.body().getMessage());
                    System.out.println(response.body().getOutput());
                    if (response.isSuccessful()) {

                        playSound("");

                        MainApp.latestActivity = Synchronizer.mapper.convertValue(response.body().getOutput(),
                                EquipmentLogStartLocal.class);

                        if (MainApp.latestActivity != null) {

                            if (MainApp.latestActivity.getCdActivity() != null) {
                                MainApp.dashboardInfo.setStatus(MainApp.latestActivity.getCdActivity().getName());
                            }
                            if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_WORKING)) {
                                MainApp.dashboardInfo.setStatusText("Activity");
                            }
                            if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DELAY)) {
                                MainApp.dashboardInfo.setStatusText("Delay");
                            }
                            if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_IDLE)) {
                                MainApp.dashboardInfo.setStatusText("Idle");
                            }
                            if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                                MainApp.dashboardInfo.setStatusText("Down");
                            }
                            if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_STANDBY)) {
                                MainApp.dashboardInfo.setStatusText("Standby");
                            }
                            if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_ASSIGNED)) {
                                MainApp.dashboardInfo.setStatusText("Assigned");
                            }
                            if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_MAINTENANCE)) {
                                MainApp.dashboardInfo.setStatusText("Maintenance");
                            }

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    String actName = MainApp.latestActivity.getCdActivity() == null ? ""
                                            : ", " + MainApp.latestActivity.getCdActivity().getName();
                                    NavigationUI.addLogActivity(MainApp.latestActivity.getCdTum().getCd(),
                                            MainApp.latestActivity.getCdTum().getName() + actName);
                                }
                            });
                        }
                    }
                    if (callback != null) {
                        callback.run();
                    }
                    System.out.println("set activity, success");
                }

                @Override
                public void onFailure(Call<DefaultResponse> call, Throwable t) {

                    /**
                     * save data to local
                     */
                    OperationStatusLocal mainAct = new OperationStatusLocal();
                    mainAct.setIdEquipment(param.getIdEquipment());
                    mainAct.setIdOperator(param.getIdOperator());
                    mainAct.setCdActivity(param.getCdActivity());
                    mainAct.setCdTum(param.getCdTum());
                    mainAct.setCdType(param.getCdType());
                    mainAct.setTime(Utilities.getCurrentUnix());
                    mainAct.setLatitude(Double.parseDouble(param.getLat()));
                    mainAct.setLongitude(Double.parseDouble(param.getLon()));
                    OperationStatusLocal saved = (OperationStatusLocal) MainApp.operationStatusDB.insertIgnore(mainAct);
                    Utilities.setLatestActivity(saved);

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            String actName = MainApp.latestActivity.getCdActivity() == null ? ""
                                    : ", " + MainApp.latestActivity.getCdActivity().getName();
                            NavigationUI.addLogActivity(MainApp.latestActivity.getCdTum().getCd(),
                                    MainApp.latestActivity.getCdTum().getName() + actName);
                        }
                    });

                    /** ============================== */

                    if (t instanceof ConnectException) {
                        Utilities.messageSocketException(t, "Set Activity");
                    } else if (t instanceof TimeoutException) {
                        Utilities.messageTimeoutException(t, "Set Activity");
                    } else {
                        Utilities.messageOtherException(t, "Set Activity");
                    }

                    if (callback != null) {
                        callback.run();
                    }

                }

            });

        } else {

            System.out.println("post activity, offline mode");

            /**
             * save data to local
             */
            OperationStatusLocal mainAct = new OperationStatusLocal();
            mainAct.setIdEquipment(param.getIdEquipment());
            mainAct.setIdOperator(param.getIdOperator());
            mainAct.setCdActivity(param.getCdActivity());
            mainAct.setCdTum(param.getCdTum());
            mainAct.setCdType(param.getCdType());
            mainAct.setTime(Utilities.getCurrentUnix());
            mainAct.setLatitude(Double.parseDouble(param.getLat()));
            mainAct.setLongitude(Double.parseDouble(param.getLon()));
            OperationStatusLocal saved = (OperationStatusLocal) MainApp.operationStatusDB.insertIgnore(mainAct);
            Utilities.setLatestActivity(saved);

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    String actName = MainApp.latestActivity.getCdActivity() == null ? ""
                            : ", " + MainApp.latestActivity.getCdActivity().getName();
                    NavigationUI.addLogActivity(MainApp.latestActivity.getCdTum().getCd(),
                            MainApp.latestActivity.getCdTum().getName() + actName);
                }

            });

        }

    }

    public static void setActivity(EquipmentLogStartRequest param) {
        setActivity(param, null);
    }

    /**
     * penentu masuk ke prestart atau tidak
     */
    public static void checkLatestActivity(Runnable cb) {
        if (NetworkConnection.getServiceStatus()) {

            if (MainApp.equipmentLocal == null) {
                Utilities.getEquipmentByName(MainApp.config("config.unit_id"));
            }

            String actual_id = MainApp.actualInfo != null ? MainApp.actualInfo.getId() : null;
            String actual_support_id = MainApp.actualSupport != null ? MainApp.actualSupport.getId() : null;

            Call<ResponseModel> latestActivity = APIClient.getInstance().getLatestActivity(MainApp.requestHeader,
                    MainApp.equipmentLocal.getId(), actual_id, actual_support_id);

            latestActivity.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                    if (response.body().getResponse().equals("success")) {
                        Object latestAct = response.body().getOutput();
                        if (latestAct != null) {

                            /** start main activity */
                            EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
                            if (Utilities.isLoaderHauler()) {
                                activity.setIdActual(MainApp.actualInfo.getId());
                                activity.setCdActivity(MainApp.actualInfo.getCd_operation().getCd());
                            } else {
                                System.out.println(MainApp.actualSupport);
                                System.out.println(MainApp.actualSupport.getCdActivity());
                                activity.setIdOperationSupport(MainApp.actualSupport.getId());
                                activity.setCdActivity(MainApp.actualSupport.getCdActivity().getCd());
                            }
                            activity.setIdEquipment(MainApp.equipmentLocal.getId());
                            activity.setIdOperator(MainApp.user.getId());
                            activity.setTime("current");
                            Utilities.setActivity(activity, new Runnable() {

                                @Override
                                public void run() {
                                    // hauler
                                    Platform.runLater(() -> {
                                        NavigationUI.to_dashboard(MainApp.config("config.unit_type"),
                                                MainApp.latestRefEvent);
                                    });
                                }
                            });

                        } else {
                            /** set change shift activity */
                            EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
                            if (Utilities.isLoaderHauler()) {
                                activity.setIdActual(MainApp.actualInfo.getId());
                            } else {
                                activity.setIdOperationSupport(MainApp.actualSupport.getId());
                            }
                            activity.setIdEquipment(MainApp.equipmentLocal.getId());
                            activity.setIdOperator(MainApp.user.getId());
                            activity.setTime("startOfShift");
                            activity.setCdActivity(ConstantValues.ACTIVITY_CHANGE_SHIFT);
                            Utilities.setActivity(activity, new Runnable() {

                                @Override
                                public void run() {

                                    /** start prestart activity */
                                    EquipmentLogStartRequest p2h = new EquipmentLogStartRequest();
                                    if (Utilities.isLoaderHauler()) {
                                        p2h.setIdActual(MainApp.actualInfo.getId());
                                    } else {
                                        p2h.setIdOperationSupport(MainApp.actualSupport.getId());
                                    }
                                    p2h.setIdEquipment(MainApp.equipmentLocal.getId());
                                    p2h.setIdOperator(MainApp.user.getId());
                                    p2h.setTime("current");
                                    p2h.setCdActivity(ConstantValues.ACTIVITY_PRESTART);
                                    Utilities.setActivity(p2h, new Runnable() {

                                        @Override
                                        public void run() {
                                            /** show prestart */
                                            cb.run();
                                        }
                                    });

                                }
                            });
                        }
                    } else

                    {

                        /** set change shift activity */
                        EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
                        if (Utilities.isLoaderHauler()) {
                            activity.setIdActual(MainApp.actualInfo.getId());
                        } else {
                            activity.setIdOperationSupport(MainApp.actualSupport.getId());
                        }
                        activity.setIdEquipment(MainApp.equipmentLocal.getId());
                        activity.setIdOperator(MainApp.user.getId());
                        activity.setTime("startOfShift");
                        activity.setCdActivity(ConstantValues.ACTIVITY_CHANGE_SHIFT);
                        Utilities.setActivity(activity);

                        /** start prestart activity */
                        EquipmentLogStartRequest p2h = new EquipmentLogStartRequest();
                        if (Utilities.isLoaderHauler()) {
                            p2h.setIdActual(MainApp.actualInfo.getId());
                        } else {
                            p2h.setIdOperationSupport(MainApp.actualSupport.getId());
                        }
                        p2h.setIdEquipment(MainApp.equipmentLocal.getId());
                        p2h.setIdOperator(MainApp.user.getId());
                        p2h.setTime("current");
                        p2h.setCdActivity(ConstantValues.ACTIVITY_PRESTART);
                        Utilities.setActivity(p2h);

                        /** show prestart */
                        cb.run();
                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    Utilities.showMessageDialog("Exception", "", t.getMessage());
                    t.printStackTrace();
                }

            });

        } else {

            // todo
            System.out.println("check latest activity, offline mode");

        }
    }

    public static void checkLatestActivity2(Runnable cb) {
        if (NetworkConnection.getServiceStatus()) {

            if (MainApp.equipmentLocal == null) {
                Utilities.getEquipmentByName(MainApp.config("config.unit_id"));
            }

            String actual_id = MainApp.actualInfo != null ? MainApp.actualInfo.getId() : null;
            String actual_support_id = MainApp.actualSupport != null ? MainApp.actualSupport.getId() : null;

            String equipId = MainApp.equipmentLocal == null ? "" : MainApp.equipmentLocal.getId();

            Call<ResponseModel> latestActivity = APIClient.getInstance().getLatestActivity(MainApp.requestHeader,
                    equipId, actual_id, actual_support_id);

            latestActivity.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                    System.out.println(response.body().getOutput());

                    if (response.isSuccessful()) {

                        if (response.body().getResponse().equals("success")) {
                            System.out.println("------------------------ set activity");
                            MainApp.latestActivity = Synchronizer.mapper.convertValue(response.body().getOutput(),
                                    EquipmentLogStartLocal.class);
                        } else {
                            System.out.println("------------------------ set activity null");
                            MainApp.latestActivity = null;
                        }

                    } else {
                        System.out.println("------------------------ set activity null");
                        MainApp.latestActivity = null;
                    }

                    if (cb != null) {
                        cb.run();
                    }

                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {

            // todo
            System.out.println("check latest activity, offline mode");

        }
    }

    public static void producerActivity(String cdActivity, String name) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            Date date = new Date();
            String strDate = formatter.format(date);
            JSONObject json = new JSONObject();
            json.put("date", strDate);
            json.put("unit_id", MainApp.equipmentLocal.getId());
            json.put("status", cdActivity);
            json.put("description", name);

            if (NetworkConnection.getKafkaStatus()) {

                MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_STATUS, json.toString());

            } else {

                // todo
                System.out.println("save activity offline, offline mode");

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);
        }
    }

    public static GPS getCurrentGPS() {
        GPS res = new GPS();
        try {
            /**
             * cara ini problem, nilai gps salah
             */
            // JSONObject dataGPS = MainApp.device.getGPS();
            // System.out.println(dataGPS.toString());
            // res.setLat(dataGPS.getJSONObject("gps").getDouble("lat"));
            // res.setLon(dataGPS.getJSONObject("gps").getDouble("lon"));
            res.setLat(MainApp.latestLat);
            res.setLon(MainApp.latestLon);
        } catch (Exception t) {
            t.printStackTrace();
            MainApp.log.error("utilities", t);
        }
        return res;
    }

    public static void setSafetyStatus(SafetyStatusRequest param, String safetyName, Runnable callback) {
        try {
            JSONObject dataGPS = MainApp.device.getGPS();
            System.out.println(dataGPS.toString());
            param.setLatitude(dataGPS.getJSONObject("gps").getDouble("lat"));
            param.setLongitude(dataGPS.getJSONObject("gps").getDouble("lon"));
        } catch (Exception t) {
            t.printStackTrace();
            MainApp.log.error("utilities", t);
        }

        if (MainApp.isCanPost()) {

            Call<ResponseModel> call = APIClient.getInstance().setSafetyStatus(MainApp.requestHeader, param);
            call.enqueue(new Callback<ResponseModel>() {

                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                    if (callback != null) {
                        callback.run();
                    }
                    System.out.println(response.body().getResponse());
                    System.out.println(response.body().getMessage());
                    System.out.println(response.body().getOutput());
                    System.out.println("set safety activity, success");

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            NavigationUI.addLogActivity("", "Safety status : " + safetyName);
                        }
                    });
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    if (callback != null) {
                        callback.run();
                    }
                    t.printStackTrace();

                    // todo
                }

            });

        } else {

            /**
             * offline mode
             */
            SafetyStatusLocal sLocal = new SafetyStatusLocal();
            sLocal.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
            sLocal.setIdOperationSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
            sLocal.setIdOperationLoader(MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
            sLocal.setDate(Utilities.getCurrentUnix());
            sLocal.setIdEquipment(MainApp.equipmentLocal == null ? null : MainApp.equipmentLocal.getId());
            sLocal.setIdOperator(MainApp.user == null ? null : MainApp.user.getId());
            sLocal.setCdSafety(param.getCdSafety());
            sLocal.setLatitude(param.getLatitude());
            sLocal.setLongitude(param.getLongitude());

            MainApp.safetyStatusDB.insertIgnore(sLocal);

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    NavigationUI.addLogActivity("", "Safety status : " + safetyName);
                }

            });

            System.out.println("post safety status, offline mode");

            if (callback != null) {
                callback.run();
            }

        }
    }

    public static void producerSafetyStatus(String cdSafety, String name) {
        try {
            if (MainApp.isCanPost()) {

                SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                Date date = new Date();
                String strDate = formatter.format(date);
                JSONObject json = new JSONObject();
                json.put("date", strDate);
                json.put("unit_id", MainApp.equipmentLocal.getId());
                json.put("status", cdSafety);
                if (isLoaderHauler()) {
                    if (MainApp.isOperator) {
                        json.put("location", MainApp.actualInfo.getId_location().getId());
                    }
                } else {
                    if (MainApp.isOperator) {
                        json.put("location", MainApp.actualSupport.getLocationId().getId());
                    }
                }
                JSONObject dataGPS = MainApp.device.getGPS();
                System.out.println(dataGPS.toString());
                json.put("latitude", String.valueOf(dataGPS.getJSONObject("gps").getDouble("lat")));
                json.put("longitude", String.valueOf(dataGPS.getJSONObject("gps").getDouble("lon")));

                MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_SAFETY, json.toString());

            } else {

                // todo
                System.out.println("save safety status offline, offline mode");

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);
        } finally {

        }
    }

    public static void getEquipmentByName(String name) {
        EquipmentLocal e = (EquipmentLocal) MainApp.equipmentDB.getLocalOneById("name",
                MainApp.config("config.unit_id"));

        if (e != null) {

            MainApp.equipmentLocal = new EquipmentAPI();
            MainApp.equipmentLocal.setId(e.getId());
            MainApp.equipmentLocal.setName(e.getName());
            MainApp.equipmentLocal.setCapacityLabs(e.getCapacity_labs());
            MainApp.equipmentLocal.setCapacityLabs2(e.getCapacity_labs_2().doubleValue());
            MainApp.equipmentLocal.setCapacityLabs3(e.getCapacity_labs_3());
            MainApp.equipmentLocal.setCapacityReal(e.getCapacity_real());

            if (e.getEquipmentCategory() != null) {
                EquipmentCategoryLocal eq = (EquipmentCategoryLocal) MainApp.equipmentCategorytDB.getLocalById("id",
                        e.getEquipmentCategory());

                EquipmentCategory eqa = new EquipmentCategory();
                eqa.setCdManufactur(MainApp.constantDB.getOneLocalById(eq.getCdManufactur()));
                eqa.setCdProduct(MainApp.constantDB.getOneLocalById(eq.getCdProduct()));
                eqa.setCdType(MainApp.constantDB.getOneLocalById(eq.getCdType()));
                eqa.setCdBrand(MainApp.constantDB.getOneLocalById(eq.getCdBrand()));
                eqa.setCdModel(MainApp.constantDB.getOneLocalById(eq.getCdModel()));
                eqa.setCdClass(MainApp.constantDB.getOneLocalById(eq.getCdClass()));
                eqa.setBucketCapacity(eq.getBucketCapacity());

                MainApp.equipmentLocal.setEquipmentCategory(eqa);
            }

        } else if (NetworkConnection.getServiceStatus()) {

            System.out.println("--- database empty");

            if (NetworkConnection.getServiceStatus() && MainApp.requestHeader != null) {
                System.out.println("--- try get from server");

                Call<DefaultResponse> call = APIClient.getInstance().getEquipmentByName(MainApp.requestHeader, name);
                try {
                    Response<DefaultResponse> response = call.execute();
                    // System.out.println("----------------------------------------- equipment from
                    // server");
                    // System.out.println(response.body().getOutput());
                    if (response.body().getOutput().toString().length() < 10) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                Utilities.showMessageDialog("Equipment not registered", "",
                                        MainApp.config("config.unit_id") + " belum terdaftar dalam database");
                            }
                        });

                    } else {
                        MainApp.equipmentLocal = Synchronizer.mapper.convertValue(response.body().getOutput(),
                                EquipmentAPI.class);
                    }

                    // System.out.println("----------------- equipment - local");
                    // mapObject(MainApp.equipmentLocal);

                    // Utilities.getLatestHM();

                } catch (ConnectException t) {
                    Utilities.messageSocketException(t, "Get Equipment");
                    MainApp.log.error("utilities", t);

                } catch (Exception t) {
                    Utilities.messageOtherException(t, "Get Equipment");
                    MainApp.log.error("utilities", t);

                }
                // finally {
                // Utilities.showMessageDialog("Equipment not registered", "",
                // MainApp.config("config.unit_id") + " belum terdaftar dalam database");
                // }

            }

        } else {
            System.out.println("[Offline]    Equipment info unavailable");
            Utilities.showMessageDialog("Equipment not registered", "",
                    MainApp.config("config.unit_id") + " belum terdaftar dalam database");

        }

        Utilities.getLatestHM();
    }

    public static void pushGPS(String longitude, String latitude, String altitude, String date, JSONObject ecu,
            Boolean engineOn, JSONObject ecus) {
        Integer writeLog = Integer.parseInt(MainApp.config("config.write_log"));

        JSONObject json = new JSONObject();

        Long tm = Utilities.getCurrentUnix();

        try {
            json.put("date", tm);
            json.put("actual_id", MainApp.actualInfo != null ? MainApp.actualInfo.getId() : "");
            json.put("actual_support_id", MainApp.actualSupport != null ? MainApp.actualSupport.getId() : "");
            json.put("operation_loader_id", MainApp.actualLoader != null ? MainApp.actualLoader.getId() : "");
            json.put("unit_id", MainApp.equipmentLocal != null ? MainApp.equipmentLocal.getId()
                    : MainApp.config("config.unit_id").trim());
            json.put("unit_type", MainApp.config("config.unit_type").trim());
            json.put("user_id", MainApp.user != null ? MainApp.user.getId() : "");
            json.put("latitude", latitude);
            json.put("longitude", longitude);
            json.put("altitude", altitude);
            json.put("user_login", MainApp.loggedIn);
            json.put("user_status",
                    MainApp.latestActivity != null ? MainApp.latestActivity.getCdActivity().getCd() : "");
            json.put("user_tum", MainApp.latestActivity != null ? MainApp.latestActivity.getCdTum().getCd() : "");
            json.put("engine", engineOn ? "on" : "off");
            json.put("status", MainApp.haulerState);
            json.put("hm_start", MainApp.latestHourMeter);
            if (MainApp.area != null) {
                if (MainApp.area.getName() != null) {
                    json.put("area_name", MainApp.area.getName());
                } else {
                    json.put("area_name", "");
                }
            } else {
                json.put("area_name", "");
            }
            json.put("material_type", MainApp.actualInfo != null ? MainApp.actualInfo.getCd_material().getType() : "");
            json.put("ecu", ecu);
            json.put("ecus", ecus);

            StorageStatus st = Utilities.deviceStorageStatus();

            CPUAndRAM cr = Utilities.memoryUsage();

            JSONObject deviceInfo = new JSONObject();
            deviceInfo.put("storage_size", st.totalSpace);
            deviceInfo.put("storage_free", st.freeSpace);

            deviceInfo.put("memory_size", cr.memSize);
            deviceInfo.put("memory_free", cr.memFreeSize);
            deviceInfo.put("cpu_process", cr.cpuProcess);
            deviceInfo.put("cpu_system", cr.cpuSystem);
            deviceInfo.put("cpu_system_load", cr.cpuSystemLoad);

            json.put("device_status", deviceInfo);

            String jsonString = json.toString();

            // System.out.print("gps -> ");
            // System.out.println(jsonString);

            if (writeLog == 1) {
                try (BufferedWriter bw = new BufferedWriter(new FileWriter("log_alat_ecu.txt", true))) {
                    bw.write(jsonString);
                    bw.newLine();
                } catch (IOException ex) {
                    ex.printStackTrace();
                    MainApp.log.error("utilities", ex);
                }
            }

            if (NetworkConnection.getKafkaStatus()) {

                if (MainApp.producer != null) {
                    MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_GPS, jsonString);

                }

            } else {
                // todo
                System.out.println("--- no kafka, save offline");

                /**
                 * offline mode save to db
                 */
                GPSLocal gpsLocal = new GPSLocal();
                gpsLocal.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                gpsLocal.setIdOperationSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                gpsLocal.setIdOperationLoader(MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
                gpsLocal.setTime(Utilities.getCurrentUnix());
                gpsLocal.setIdEquipment(MainApp.equipmentLocal == null ? null : MainApp.equipmentLocal.getId());
                gpsLocal.setIdOperator(MainApp.user == null ? null : MainApp.user.getId());
                gpsLocal.setJson(jsonString);
                if (gpsLocal.getIdEquipment() != null) {
                    MainApp.gpsDB.insertIgnore(gpsLocal);
                }

            }

            Double latU = Double.parseDouble(latitude);
            Double lonU = Double.parseDouble(longitude);
            Long dt = Utilities.dateStringToTime(date);

            Utilities.computeGPSSpeed(Utilities.getDistance(MainApp.latestLat, MainApp.latestLon, latU, lonU),
                    MainApp.latestGpsTime, dt);

            if (isHauler() && MainApp.actualInfo != null) {
                Double latL = MainApp.actualInfo.getId_location().getLatitude();
                Double lonL = MainApp.actualInfo.getId_location().getLongitude();
                Double latD = MainApp.actualInfo.getId_destination().getLatitude();
                Double lonD = MainApp.actualInfo.getId_destination().getLongitude();
                MainApp.distanceToLoadingPoint = getDistance(latU, lonU, latL, lonL);
                MainApp.distanceToDumpingPoint = getDistance(latU, lonU, latD, lonD);
                pushHaulerDistance();
            }

            MainApp.latestLat = latU;
            MainApp.latestLon = lonU;
            MainApp.latestGpsTime = dt;

        } catch (JSONException e) {
            e.printStackTrace();
            MainApp.log.error("utilities", e);
        }
    }

    public static void pushECU_not_used_(JSONObject jsonparam) {
        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Date date = new Date();
        String strDate = formatter.format(date);

        JSONObject json = new JSONObject();

        try {
            json.put("date", strDate);
            json.put("unit_id", MainApp.equipmentLocal != null ? MainApp.equipmentLocal.getId()
                    : MainApp.config("config.unit_id").trim());
            json.put("unit_name", MainApp.config("config.unit_id").trim());
            if (Utilities.isLoaderHauler()) {
                json.put("shift", MainApp.actualInfo != null ? MainApp.actualInfo.getCd_shift().getCd() : "");
                json.put("actual_id", MainApp.actualInfo != null ? MainApp.actualInfo.getId() : "");
            } else {
                json.put("shift", MainApp.actualInfo != null ? MainApp.actualSupport.getCdShift().getCd() : "");
                json.put("actual_support_id", MainApp.actualInfo != null ? MainApp.actualSupport.getId() : "");
            }

            JSONObject subEcu = new JSONObject();

            JSONObject subHour = new JSONObject();
            subHour.put("unit", jsonparam.getString("unit_hour"));
            subHour.put("value", jsonparam.getString("value_hour"));
            subEcu.put("hour_meter", subHour);

            String vHour = jsonparam.getString("value_hour");
            Double vva = MainApp.latestHourMeter;
            if (vHour != null) {
                vva += Double.parseDouble(vHour) / 3600;
            }
            MainApp.dashboardInfo.setHourMeter(vva);

            JSONObject subFuel = new JSONObject();
            subFuel.put("unit", jsonparam.getString("unit_fuel"));
            subFuel.put("value", jsonparam.getString("value_fuel"));
            subEcu.put("fuel_rate", subFuel);

            String vFuel = jsonparam.getString("value_fuel");
            MainApp.dashboardInfo.setFuel(vFuel != null ? Double.parseDouble(vFuel) : 0);

            JSONObject subDistance = new JSONObject();
            subDistance.put("unit", jsonparam.getString("unit_distance"));
            subDistance.put("value", jsonparam.getString("value_distance"));
            subEcu.put("distance", subDistance);

            // DISTANCE diset di start dump, bukan dari ecu langsung
            // String vDistance = jsonparam.getString("value_distance");
            // MainApp.dashboardInfo.setDistance(vDistance != null ?
            // Double.parseDouble(vDistance) : 0);

            JSONObject subTemperatur = new JSONObject();
            subTemperatur.put("unit", jsonparam.getString("unit_temperature"));
            subTemperatur.put("value", jsonparam.getString("value_temperature"));
            subEcu.put("engine_temperature", subTemperatur);

            String vTemperature = jsonparam.getString("value_temperature");
            MainApp.dashboardInfo.setTemperatur(vTemperature != null ? Double.parseDouble(vTemperature) : 0);

            JSONObject subPayload = new JSONObject();
            subPayload.put("unit", jsonparam.getString("unit_payload"));
            subPayload.put("value", jsonparam.getString("value_payload"));
            subEcu.put("payload", subPayload);

            // String vPayload = jsonparam.getString("value_payload");
            if (Utilities.isHauler()) {
                // PAYLOAD di hauler dashboard adalah nilai ritase berdasarkan formula
                // payload LOADCELL dan ujipetik, dari server
                // MainApp.dashboardInfo.setPayload(vPayload != null ?
                // Double.parseDouble(vPayload) : 0);
            } else {
                // di loader, tonase/payload yang tampil adalah payload
                // hauler yang sedang diisi
            }

            JSONObject subSpeed = new JSONObject();
            subSpeed.put("unit", jsonparam.getString("unit_speed"));
            subSpeed.put("value", jsonparam.getString("value_speed"));
            subEcu.put("vehicle_speed", subSpeed);

            String vSpeed = jsonparam.getString("value_speed");
            MainApp.dashboardInfo.setSpeed(vSpeed != null ? Double.parseDouble(vSpeed) : 0);

            json.put("ecu", subEcu);

            json.put("travel", MainApp.travel);

            String jsonString = json.toString();

            System.out.print("ecu -> ");
            System.out.println(jsonString);

            if (NetworkConnection.getKafkaStatus() && MainApp.producer != null) {

                MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_ECU, jsonString);

            } else {

                // todo
                System.out.println("--- no kafka, save offline");

            }

        } catch (JSONException e) {
            e.printStackTrace();
            MainApp.log.error("utilities", e);
        }

    }

    public static void pushRequestData(String dataRequested, String destinationId) {
        /**
         * dipanggil oleh loader untuk meminta data payload dari hauler
         */
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            Date date = new Date();
            String strDate = formatter.format(date);
            JSONObject json = new JSONObject();
            json.put("date", strDate);
            json.put("state", ConstantValues.KAFKA_DATA_STATE_REQUEST);
            json.put("sender", MainApp.equipmentLocal.getId());
            json.put("sender_name", MainApp.config("config.unit_id"));
            json.put("type", dataRequested);
            json.put("destination", destinationId);
            MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_DATA, json.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);
        }
    }

    public static void pushResponseData(String dataRequested, String destinationId, String data) {
        /**
         * dipanggil oleh hauler untuk mengirim data payload
         */
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            Date date = new Date();
            String strDate = formatter.format(date);
            JSONObject json = new JSONObject();
            json.put("date", strDate);
            json.put("state", ConstantValues.KAFKA_DATA_STATE_RESPONSE);
            json.put("sender", MainApp.equipmentLocal.getId());
            json.put("sender_name", MainApp.config("config.unit_id"));
            json.put("type", dataRequested);
            json.put("destination", destinationId);
            json.put("data", data);
            MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_DATA, json.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);
        }
    }

    public static void setNotificationText(String text) {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                playSound("");

                NavigationUI.setLabelText(text);
            }
        });

    }

    public static void setExcavatorLabel(String text) {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                // playSound("");

                NavigationUI.setExcaLabel(text);
            }
        });

    }

    public static void getOperationSupport(Label erroLabel, JFXSpinner loading, HBox kotakError, Event event,
            Boolean setSession_param, UserDetails userDetails_param, UserAPILocal userAPILocal) {

        String tanggal = Utilities.getCurrentUnix().toString();
        // System.out.println(tanggal);

        if (NetworkConnection.getServiceStatus()) {

            Call<SupportData> call = APIClient.getInstance().getOperationSupport(MainApp.requestHeader, tanggal,
                    MainApp.config("config.unit_id").trim(), MainApp.config("config.unit_type").trim(), "");

            call.enqueue(new Callback<SupportData>() {
                @Override
                public void onResponse(Call<SupportData> call, Response<SupportData> response) {
                    System.out.println(response.body());
                    // MainApp.actualSupport = response.body().getOutput();
                }

                @Override
                public void onFailure(Call<SupportData> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            // todo
            System.out.println("get latest actual support from local");

        }
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> mapObject(Object obj) {
        try {
            Map<String, Object> map = Synchronizer.mapper.convertValue(obj, Map.class);
            System.out.println(map);
            return map;
        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<String> mapListString(Object obj) {
        try {
            List<String> result = Synchronizer.mapper.convertValue(obj, List.class);
            System.out.println(result);
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<Map<String, Object>> mapListObject(Object obj) {
        try {
            List<Map<String, Object>> result = new ArrayList<>();
            List<Object> daftar = Synchronizer.mapper.convertValue(obj, List.class);
            for (Object ob : daftar) {
                Map<String, Object> map = Synchronizer.mapper.convertValue(ob, Map.class);
                result.add(map);
            }
            System.out.println(result);
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<Map<String, Object>> mapJson(Object obj) {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        try {
            List<Object> daftar = Synchronizer.mapper.readValue(obj.toString(), List.class);
            for (Object ob : daftar) {
                Map<String, Object> map = Synchronizer.mapper.readValue(ob.toString(), Map.class);
                result.add(map);
            }
            System.out.println(result);
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);
        }
        return result;
    }

    public static JsonNode parseJson(String obj) {
        JsonNode result = null;
        try {
            result = Synchronizer.mapper.readTree(obj);
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);
        }
        return result;
    }

    public static void showInputDialog(CustomRunnable cb) {
        TextInputDialog dialog = new TextInputDialog("Tran");

        dialog.setTitle("o7planning");
        dialog.setHeaderText("Enter your name:");
        dialog.setContentText("Name:");

        Optional<String> result = dialog.showAndWait();

        result.ifPresent(name -> {
            cb.setData(name);
            if (cb != null) {
                cb.run();
            }
        });
    }

    public static void showMessageDialog(String title, String header, String message) {
        showMessageDialog(title, header, message, null);
    }

    public static void showMessageDialog(String title, String header, String message, Runnable cbNo) {

        if (NavigationUI.publicStackPane == null) {

            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle(title);
            alert.setHeaderText(header);
            alert.setContentText(message);
            alert.showAndWait();

        } else {

            JFXDialogLayout content = new JFXDialogLayout();
            content.getStyleClass().add("bg-dark-light");
            Text txtTitle = new Text(title);
            txtTitle.setStyle("-fx-font-size: 20px;-fx-text-fill: #ff3333;");
            content.setHeading(txtTitle);
            Text txt = new Text(message);
            txt.setStyle("-fx-font-size: 20px;");
            content.setBody(txt);

            JFXDialog dialog = new JFXDialog(NavigationUI.publicStackPane, content, JFXDialog.DialogTransition.CENTER,
                    false);

            JFXButton keluar = new JFXButton("Tutup");
            keluar.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
            keluar.setPrefHeight(30.0);
            keluar.setPrefWidth(100.0);
            keluar.getStyleClass().add("button-raised");

            keluar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if (cbNo != null) {
                        cbNo.run();
                    }
                    dialog.close();
                }
            });
            content.setActions(keluar);
            dialog.show();

        }
    }

    public static void showCustomDialogProblem(ActionEvent event, Runnable callback) {
        if (MainApp.dialogCustomDialogShow) {
            return;
        }

        try {

            JFXDialogLayout content = new JFXDialogLayout();
            content.getStyleClass().add("bg-dark-sub");

            JFXSpinner loading = new JFXSpinner();
            loading.setVisible(false);

            Text txtTitle = new Text("Input Problem");
            txtTitle.setStyle("-fx-font-size: 20px;");
            txtTitle.setFill(Color.WHITE);
            content.setHeading(txtTitle);

            VBox vbox = new VBox();
            vbox.setSpacing(15.0);
            Text txt = new Text("Silakan masukan problem :");
            txt.setFill(Color.WHITE);
            txt.setStyle("-fx-font-size: 20px;-fx-text-fill: WHITE;");

            TextField isian = new TextField();
            isian.setStyle("-fx-font-size: 20px;");
            isian.setPrefWidth(500);
            isian.setPrefHeight(50);

            vbox.getChildren().add(txt);
            vbox.getChildren().add(isian);
            content.setBody(vbox);

            JFXDialog dialog = new JFXDialog(NavigationUI.publicStackPane, content, JFXDialog.DialogTransition.CENTER,
                    false);

            JFXButton keluar = new JFXButton("Batal");
            keluar.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
            keluar.setPrefHeight(30.0);
            keluar.setPrefWidth(100.0);
            keluar.getStyleClass().add("button-cancel");
            keluar.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    MainApp.dialogCustomDialogShow = false;
                    dialog.close();
                }
            });

            JFXButton logout = new JFXButton("Lanjutkan >>");
            logout.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
            logout.setPrefHeight(30.0);
            logout.setPrefWidth(170.0);
            logout.getStyleClass().add("button-raised");
            logout.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    BreakdownController.breakdownInfo = isian.getText();

                    if (callback != null) {
                        callback.run();
                    }

                    MainApp.dialogCustomDialogShow = false;
                    dialog.close();
                }
            });

            content.setActions(loading, keluar, logout);
            dialog.show();

            MainApp.dialogCustomDialogShow = true;

        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);
        }
    }

    public static void showHourMeterDialog(ActionEvent event) {
        if (MainApp.dialogHourMeterShow) {
            return;
        }

        MainApp.dialogHourMeterLogoutClick = false;

        try {
            // Utilities.latestEvent = event;
            // Parent parent =
            // FXMLLoader.load(Utilities.class.getResource(p("app.linkHourMeterForm")));
            // // CustomDialog dialogController = FXMLLoader.<CustomDialog>getController();
            // Scene scene = new Scene(parent, 300, 200);
            // Stage stage = new Stage();
            // stage.initModality(Modality.APPLICATION_MODAL);
            // stage.setScene(scene);
            // stage.showAndWait();

            JFXDialogLayout content = new JFXDialogLayout();
            content.getStyleClass().add("bg-dark-sub");

            JFXSpinner loading = new JFXSpinner();
            loading.setVisible(false);

            Text txtTitle = new Text("Hour Meter");
            txtTitle.setStyle("-fx-font-size: 20px;");
            txtTitle.setFill(Color.WHITE);
            content.setHeading(txtTitle);

            VBox vbox = new VBox();
            vbox.setSpacing(15.0);
            Text txt = new Text("Silakan isi hour meter");
            txt.setFill(Color.WHITE);
            txt.setStyle("-fx-font-size: 20px;-fx-text-fill: WHITE;");

            TextField isian = new TextField();
            isian.setStyle("-fx-font-size: 20px;");
            isian.getProperties().put("vkType", "numeric");

            if (MainApp.latestHourMeter != null) {
                isian.setText(String.valueOf(MainApp.latestHourMeter.intValue()));
            }

            vbox.getChildren().add(txt);
            vbox.getChildren().add(isian);
            content.setBody(vbox);

            JFXDialog dialog = new JFXDialog(NavigationUI.publicStackPane, content, JFXDialog.DialogTransition.CENTER,
                    false);

            JFXButton keluar = new JFXButton("Batal");
            keluar.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
            keluar.setPrefHeight(30.0);
            keluar.setPrefWidth(100.0);
            keluar.getStyleClass().add("button-cancel");
            keluar.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    MainApp.dialogHourMeterShow = false;
                    dialog.close();
                }
            });
            JFXButton logout = new JFXButton("Logout");
            logout.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
            logout.setPrefHeight(30.0);
            logout.setPrefWidth(100.0);
            logout.getStyleClass().add("button-raised");
            logout.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if (MainApp.dialogHourMeterLogoutClick == false) {
                        MainApp.dialogHourMeterLogoutClick = true;

                        String hmStr = isian.getText();
                        if (hmStr.length() == 0) {
                            hmStr = "0";
                        }
                        Double ihm = Double.parseDouble(hmStr);

                        if (ihm < MainApp.latestHourMeter) {
                            Utilities.showMessageDialog("Hour Meter Tidak Valid", "",
                                    "HM lebih kecil dari HM terakhir (" + MainApp.latestHourMeter.toString() + ")",
                                    new Runnable() {

                                        @Override
                                        public void run() {
                                        }
                                    });
                            MainApp.dialogHourMeterLogoutClick = false;
                            return;
                        }

                        loading.setVisible(true);

                        DailyHourMeter dhm = new DailyHourMeter();
                        if (Utilities.isLoaderHauler()) {
                            if (MainApp.actualInfo != null) {
                                dhm.setActualId(MainApp.actualInfo.getId());
                            }
                        } else {
                            if (MainApp.actualSupport != null) {
                                dhm.setActualSupportId(MainApp.actualSupport.getId());
                            }
                        }
                        dhm.setHm(ihm);
                        dhm.setEquipmentId(MainApp.equipmentLocal.getId());
                        dhm.setIsDeleted(0);
                        dhm.setType("stop");

                        MainApp.latestHourMeter = ihm;

                        if (NetworkConnection.getServiceStatus()) {
                            Call<ResponseModel> call = APIClient.getInstance().postDailyStop(MainApp.requestHeader,
                                    dhm);
                            call.enqueue(new Callback<ResponseModel>() {

                                @Override
                                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                                    Utilities.doLogOut(event, null);

                                    MainApp.dialogHourMeterLogoutClick = false;

                                    loading.setVisible(false);

                                    MainApp.dialogHourMeterShow = false;

                                    dialog.close();
                                }

                                @Override
                                public void onFailure(Call<ResponseModel> call, Throwable t) {
                                    Utilities.doLogOut(event, null);

                                    MainApp.dialogHourMeterShow = false;

                                    MainApp.dialogHourMeterLogoutClick = false;

                                    loading.setVisible(false);

                                    // t.printStackTrace();

                                    dialog.close();
                                }
                            });

                        } else {
                            // save local hm
                            Utilities.doLogOut(event, null);

                            MainApp.dialogHourMeterShow = false;

                            MainApp.dialogHourMeterLogoutClick = false;

                            loading.setVisible(false);

                            dialog.close();

                        }

                    } else {
                        // MainApp.dialogHourMeterShow = false;
                        // dialog.close();
                    }
                }
            });

            content.setActions(loading, logout, keluar);
            dialog.show();

            MainApp.dialogHourMeterShow = true;

        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);

        }
    }

    public static void doLogOut(Event event, Stage stage) {

        GPS gps = Utilities.getCurrentGPS();

        if (NetworkConnection.getServiceStatus()) {

            Call<UserData> call = APIClient.getInstance().ProsesLogin(MainApp.username, MainApp.password,
                    MainApp.username + "/" + MainApp.config("config.unit_id"), ConstantValues.AUTH_STATE_LOGOUT,
                    MainApp.equipmentLocal.getId(), MainApp.config("config.unit_type"), gps.getLat(), gps.getLon());

            call.enqueue(new Callback<UserData>() {
                @Override
                public void onResponse(Call<UserData> call, Response<UserData> response) {
                    // do nothing
                }

                @Override
                public void onFailure(Call<UserData> call, Throwable t) {
                    // t.printStackTrace();

                    if (t instanceof ConnectException) {
                        Utilities.messageSocketException(t, "Do Logout");
                    } else if (t instanceof TimeoutException) {
                        Utilities.messageTimeoutException(t, "Do Logout");
                    } else {
                        Utilities.messageOtherException(t, "Do Logout");
                    }

                    /**
                     * save offline
                     */

                    HMLocal hml = new HMLocal();
                    hml.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                    hml.setIdOperationSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                    hml.setIdOperationLoader(MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
                    hml.setTime(Utilities.getCurrentUnix());
                    hml.setIdEquipment(MainApp.equipmentLocal == null ? null : MainApp.equipmentLocal.getId());
                    hml.setIdOperator(MainApp.user == null ? null : MainApp.user.getId());
                    hml.setType("stop");
                    hml.setHm(MainApp.latestHourMeter);
                    hml.setInfo("");
                    MainApp.hmDB.insertIgnore(hml);

                }
            });

        } else {

            HMLocal hml = new HMLocal();
            hml.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
            hml.setIdOperationSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
            hml.setIdOperationLoader(MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
            hml.setTime(Utilities.getCurrentUnix());
            hml.setIdEquipment(MainApp.equipmentLocal == null ? null : MainApp.equipmentLocal.getId());
            hml.setIdOperator(MainApp.user == null ? null : MainApp.user.getId());
            hml.setType("stop");
            hml.setHm(MainApp.latestHourMeter);
            hml.setInfo("");
            MainApp.hmDB.insertIgnore(hml);

        }

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                try {
                    playSound("");

                    if (MainApp.clockDashboard != null) {
                        MainApp.clockDashboard.stop();
                    }

                    // ((Node) (event.getSource())).getScene().getWindow().hide();
                    // SessionFMS.logout();

                    MainApp.loggedIn = false;
                    MainApp.isOperator = false;
                    MainApp.isGroupLeader = false;
                    MainApp.isSupervisor = false;
                    MainApp.isMechanic = false;
                    MainApp.actualInfo = null;
                    MainApp.actualSupport = null;
                    MainApp.actualLoader = null;
                    MainApp.activeHauler = 0;

                    if (MainApp.haulerList != null) {
                        MainApp.haulerList.clear();
                    }
                    if (isLoader()) {
                        if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)) {
                            DashboardLoaderController.buttonHauler.clear();
                            DashboardLoaderController.buttonCycle1.clear();
                            // DashboardLoaderController.buttonCycle2.clear();
                        }
                    } else {
                        System.out.println("---- log out support ----");
                    }
                    if (Utilities.latestEvent != null) {
                        // Stage stg = (Stage) ((Node)
                        // (Utilities.latestEvent.getSource())).getScene().getWindow();
                        // if (stg != null) {
                        // stg.close();
                        // }
                        NavigationUI.windowsNewScene(p("app.linkLogin"), p("app.titleLogin"), Utilities.latestEvent);
                    } else if (event != null) {
                        NavigationUI.windowsNewScene(p("app.linkLogin"), p("app.titleLogin"), event);
                    } else if (stage != null) {
                        NavigationUI.windowsNewScene(p("app.linkLogin"), p("app.titleLogin"), stage);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    MainApp.log.error("utilities", ex);

                }
            }

        });
    }

    public static void setupFatigueTime() {
        String fatigueTime = null;
        try {
            Utilities.fatigueData.clear();
            fatigueTime = MainApp.remoteConfig.get(ConstantValues.SCREEN_CONFIG_FATIGUE_TIME);
            if (fatigueTime != null) {
                String[] valuesFatigue = fatigueTime.split(",");
                // ArrayList list = new ArrayList(Arrays.asList(valuesFatigue));
                List<String> list = Arrays.asList(valuesFatigue);
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String currentDate = formatter.format(new Date());

                for (int i = 0; i < list.size(); i++) {
                    Utilities.fatigueData.add(
                            new FatigueModel(list.get(i).toString(), currentDate, 0, "belum", 0, MainApp.user.getId()));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            MainApp.log.error("utilities", e);

        }
    }

    public static void postFatigueStatus() {
        if (MainApp.fatigueLevel == 0) {
            System.out.println("------------- Fatigue level 0, not post");
            return;
        }

        List<FatigueLogLocal> data = new ArrayList<FatigueLogLocal>();
        FatigueLogLocal fatigue = new FatigueLogLocal();
        fatigue.setIdUser(MainApp.user.getId());
        fatigue.setIdEquipment(MainApp.equipmentLocal.getId());
        fatigue.setLevel(MainApp.fatigueLevel);
        // if (isLoaderHauler()) {
        // fatigue.setIdActual(MainApp.actualInfo.getId());
        // fatigue.setIdLocation(MainApp.actualInfo.getId_location().getId());
        // } else {
        // fatigue.setIdActualSupport(MainApp.actualSupport.getId());
        // fatigue.setIdLocation(MainApp.actualSupport.getLocationId().getId());
        // }
        fatigue.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
        fatigue.setIdActualSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
        fatigue.setIdActualLoader(MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
        fatigue.setIdLocation(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId_location().getId());
        data.add(fatigue);

        if (MainApp.isCanPost()) {

            fatigue.setDate("current");

            Call<ResponseModel> call = APIClient.getInstance().postFatigueOperation(MainApp.requestHeader, data);
            call.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                    MainApp.fatigueLevel = 0;

                    if (response.isSuccessful()) {
                        System.out.println(response.body().getMessage());
                    } else {
                        System.out.println(response.body());
                    }

                    FatigueUI.closeFatgiueDialog();
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    FatigueUI.closeFatgiueDialog();
                    // t.printStackTrace();

                    if (t instanceof ConnectException) {
                        Utilities.messageSocketException(t, "postFatigue");
                    } else if (t instanceof TimeoutException) {
                        Utilities.messageTimeoutException(t, "postFatigue");
                    } else {
                        Utilities.messageOtherException(t, "postFatigue");
                    }

                    fatigue.setDate(Utilities.getCurrentUnix().toString());

                    MainApp.fatigueLogDB.insertIgnore(fatigue);

                    MainApp.fatigueLevel = 0;

                    FatigueUI.closeFatgiueDialog();
                }
            });

        } else {

            fatigue.setDate(Utilities.getCurrentUnix().toString());

            MainApp.fatigueLogDB.insertIgnore(fatigue);

            MainApp.fatigueLevel = 0;

            FatigueUI.closeFatgiueDialog();
            // todo
            System.out.println("save fatigue status offline");

        }
    }

    public static void messageSocketException(Throwable t, String title) {
        Platform.runLater(() -> {
            Utilities.showMessageDialog("Socket Exception", "Socket exception", title + ", " + t.getMessage());
        });
    }

    public static void messageTimeoutException(Throwable t, String title) {
        Platform.runLater(() -> {
            Utilities.showMessageDialog("Timeout Exception", "Timeout exception", title + ", " + t.getMessage());
        });
    }

    public static void messageOtherException(Throwable t, String title) {
        Platform.runLater(() -> {
            Utilities.showMessageDialog("Exception", "Exception happened", title + ", " + t.getMessage());
        });
    }

    /**
     * get configuration from server
     */
    public static void getScreenConfig(Runnable callback) {
        MainApp.remoteConfig = new HashMap<String, String>();

        if (NetworkConnection.getServiceStatus()) {

            Call<ScreenConfigResponse> call = APIClient.getInstance().getScreenConfig();
            call.enqueue(new Callback<ScreenConfigResponse>() {

                @Override
                public void onResponse(Call<ScreenConfigResponse> call, Response<ScreenConfigResponse> response) {
                    if (response.body() == null) {
                        // no action
                    } else if (response.body().getResponse() == null) {
                        // no action
                    } else if (response.body().getResponse().equals("ok")) {
                        for (ScreenConfig item : response.body().getOutput()) {
                            MainApp.remoteConfig.put(item.getName(), item.getValue());

                            /**
                             * save to lokal DB
                             */
                            ScreenConfigLocal lokal = new ScreenConfigLocal();
                            lokal.setName(item.getName());
                            lokal.setValue(item.getValue());
                            MainApp.screenConfigDB.insertIgnore(lokal);

                            System.out.println("name : " + item.getName());
                        }

                        if (callback != null) {
                            callback.run();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ScreenConfigResponse> call, Throwable t) {

                    if (t instanceof ConnectException) {
                        Utilities.messageSocketException(t, "Get Screen Config");
                    } else if (t instanceof TimeoutException) {
                        Utilities.messageTimeoutException(t, "Get Screen Config");
                    } else {
                        Utilities.messageOtherException(t, "Get Screen Config");
                    }

                    /**
                     * offline config
                     */
                    List<ScreenConfigLocal> remConfig = MainApp.screenConfigDB.getAllLocal();

                    for (ScreenConfigLocal item : remConfig) {
                        MainApp.remoteConfig.put(item.getName(),
                                item.getValue() == null ? null : item.getValue().toString());
                    }

                }

            });

        } else {
            System.out.println("[Offline]    Can't get screen config ");

            List<ScreenConfigLocal> remConfig = MainApp.screenConfigDB.getAllLocal();

            for (ScreenConfigLocal item : remConfig) {
                MainApp.remoteConfig.put(item.getName(), item.getValue() == null ? null : item.getValue().toString());
            }

        }
    }

    public static void setActivityChangeShift(Runnable callback) {
        /** set change shift activity */
        EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
        if (Utilities.isLoaderHauler()) {
            activity.setIdActual(MainApp.actualInfo.getId());
        } else {
            activity.setIdOperationSupport(MainApp.actualSupport.getId());
        }
        activity.setIdEquipment(MainApp.equipmentLocal.getId());
        activity.setIdOperator(MainApp.user.getId());
        activity.setTime("startOfShift");
        activity.setCdActivity(ConstantValues.ACTIVITY_CHANGE_SHIFT);
        Utilities.setActivity(activity, new Runnable() {

            @Override
            public void run() {
                System.out.println("------- set activity change shift");
                if (callback != null) {
                    callback.run();
                }
            }
        });
    }

    public static void setActivityPrestart(Runnable callback) {
        /** start prestart activity */
        EquipmentLogStartRequest p2h = new EquipmentLogStartRequest();
        if (Utilities.isLoaderHauler()) {
            p2h.setIdActual(MainApp.actualInfo.getId());
        } else {
            p2h.setIdOperationSupport(MainApp.actualSupport.getId());
        }
        p2h.setIdEquipment(MainApp.equipmentLocal.getId());
        p2h.setIdOperator(MainApp.user.getId());
        p2h.setTime("current");
        p2h.setCdActivity(ConstantValues.ACTIVITY_PRESTART);
        Utilities.setActivity(p2h, new Runnable() {

            @Override
            public void run() {
                System.out.println("------- set activity prestart");
                if (callback != null) {
                    callback.run();
                }
            }
        });
    }

    public static void setMainActivity(Runnable callback) {
        EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
        if (Utilities.isLoaderHauler()) {
            activity.setIdActual(MainApp.actualInfo.getId());
            activity.setCdActivity(MainApp.actualInfo.getCd_operation().getCd());
        } else {
            System.out.println(MainApp.actualSupport);
            System.out.println(MainApp.actualSupport.getCdActivity());
            activity.setIdOperationSupport(MainApp.actualSupport.getId());
            activity.setCdActivity(MainApp.actualSupport.getCdActivity().getCd());
        }
        activity.setIdEquipment(MainApp.equipmentLocal.getId());
        activity.setIdOperator(MainApp.user.getId());
        activity.setTime("current");
        Utilities.setActivity(activity, new Runnable() {

            @Override
            public void run() {
                System.out.println("------- set main activity");
                if (callback != null) {
                    callback.run();
                }
            }
        });
    }

    public static void confirm(StackPane stackPane, String text, Runnable cbYes, Runnable cbNo) {
        JFXDialogLayout content = new JFXDialogLayout();
        content.getStyleClass().add("bg-dark-light");
        Text txt = new Text(text);
        txt.setStyle("-fx-font-size: 20px;");
        content.setHeading(txt);
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER, false);
        JFXButton confirm = new JFXButton("Yes");
        confirm.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        confirm.setPrefHeight(30.0);
        confirm.setPrefWidth(100.0);
        confirm.getStyleClass().add("button-primary");
        confirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (cbYes != null) {
                    cbYes.run();
                }
                dialog.close();
            }
        });

        JFXButton keluar = new JFXButton("No");
        keluar.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        keluar.setPrefHeight(30.0);
        keluar.setPrefWidth(100.0);
        keluar.getStyleClass().add("button-primary");

        keluar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (cbNo != null) {
                    cbNo.run();
                }
                dialog.close();
            }
        });
        content.setActions(confirm, keluar);
        dialog.show();
    }

    public static void getLatestHM() {
        if (NetworkConnection.getServiceStatus() && MainApp.requestHeader != null) {

            String eqId = MainApp.equipmentLocal == null ? "" : MainApp.equipmentLocal.getId();
            Call<ResponseModel> call = APIClient.getInstance().getLatestHM(MainApp.requestHeader, eqId);
            call.enqueue(new Callback<ResponseModel>() {

                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                    if (response.isSuccessful()) {
                        Object obj = response.body().getOutput();
                        if (obj != null) {
                            Map<String, Object> mapped = mapObject(obj);
                            if (mapped.get("hm") != null) {
                                Double hm = Double.parseDouble(mapped.get("hm").toString());
                                MainApp.latestHourMeter = hm;
                                System.out.println("--------- latest HM");
                                System.out.println(MainApp.latestHourMeter);

                                HMLocal hml = new HMLocal();
                                hml.setHm(MainApp.latestHourMeter);
                                hml.setTime(Utilities.getCurrentUnix());
                                if (MainApp.equipmentLocal != null) {
                                    hml.setIdEquipment(MainApp.equipmentLocal.getId());
                                }
                                if (MainApp.actualInfo != null) {
                                    hml.setIdActual(MainApp.actualInfo.getId());
                                }
                                if (MainApp.actualLoader != null) {
                                    hml.setIdOperationLoader(MainApp.actualLoader.getId());
                                }
                                if (MainApp.actualSupport != null) {
                                    hml.setIdOperationSupport(MainApp.actualSupport.getId());
                                }
                                hml = (HMLocal) MainApp.hmDB.insertIgnore(hml);
                                MainApp.hmDB.setSynchronized(hml.getId());

                            } else {
                                MainApp.latestHourMeter = 0.0;
                            }
                        } else {
                            MainApp.latestHourMeter = 0.0;
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    // t.printStackTrace();

                    // if (t instanceof ConnectException) {
                    // Utilities.messageSocketException(t, "Get Latest HM");
                    // } else if (t instanceof TimeoutException) {
                    // Utilities.messageTimeoutException(t, "Get Latest HM");
                    // } else {
                    // Utilities.messageOtherException(t, "Get Latest HM");
                    // }

                    /**
                     * data offline
                     */

                    if (MainApp.equipmentLocal != null) {
                        HMLocal latest = MainApp.hmDB.getLatestHM(MainApp.equipmentLocal.getId());
                        if (latest != null) {
                            MainApp.latestHourMeter = latest.getHm();
                        } else {
                            MainApp.latestHourMeter = 0.0;
                        }

                    } else {
                        MainApp.latestHourMeter = 0.0;

                    }

                }
            });

        } else {

            if (MainApp.equipmentLocal != null) {
                System.out.println("------------- get from local by " + MainApp.equipmentLocal.getId());
                HMLocal latest = MainApp.hmDB.getLatestHM(MainApp.equipmentLocal.getId());
                if (latest != null) {
                    MainApp.latestHourMeter = latest.getHm();
                } else {
                    MainApp.latestHourMeter = 0.0;
                }

            } else {
                System.out.println("-------------equipment local null");
                MainApp.latestHourMeter = 0.0;

            }

            System.out.println("[Offline]    HM info unavailable, use local " + MainApp.latestHourMeter.toString());

        }
    }

    public static void postHMStart(Double hm, String info) {
        if (NetworkConnection.getServiceStatus()) {

            if (MainApp.equipmentLocal != null) {

                DailyHourMeter dhm = new DailyHourMeter();
                dhm.setInfo(info);

                if (Utilities.isLoaderHauler()) {
                    if (MainApp.isOperator && MainApp.actualInfo != null) {
                        dhm.setActualId(MainApp.actualInfo.getId());
                    }
                } else {
                    if (MainApp.isOperator && MainApp.actualSupport != null) {
                        dhm.setActualSupportId(MainApp.actualSupport.getId());
                    }
                }

                // try {
                // JSONObject payload = MainApp.device.getHourMeter();
                // Double hm = payload.getJSONObject("hour_meter").getDouble("value");
                // dhm.setHm(hm.toString());
                // } catch (Exception ex) {
                // ex.printStackTrace();
                // }

                dhm.setHm(hm);
                dhm.setType("start");
                dhm.setEquipmentId(MainApp.equipmentLocal.getId());
                dhm.setIsDeleted(0);

                Call<ResponseModel> call = APIClient.getInstance().postDailyStart(MainApp.requestHeader, dhm);
                call.enqueue(new Callback<ResponseModel>() {

                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                        MainApp.latestHourMeter = hm;

                        System.out.println(response.body());
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        // t.printStackTrace();
                        System.out.println("[Utilities postHMStart] " + t.getMessage());

                        if (t instanceof ConnectException) {
                            Utilities.messageSocketException(t, "Set Activity");
                        } else if (t instanceof TimeoutException) {
                            Utilities.messageTimeoutException(t, "Set Activity");
                        } else {
                            Utilities.messageOtherException(t, "Set Activity");
                        }

                        /**
                         * save offline
                         */
                        HMLocal hml = new HMLocal();
                        hml.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                        hml.setIdOperationSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                        hml.setIdOperationLoader(MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
                        hml.setTime(Utilities.getCurrentUnix());
                        hml.setIdEquipment(MainApp.equipmentLocal == null ? null : MainApp.equipmentLocal.getId());
                        hml.setIdOperator(MainApp.user == null ? null : MainApp.user.getId());
                        hml.setType("start");
                        hml.setHm(hm);
                        hml.setInfo(info);
                        MainApp.hmDB.insertIgnore(hml);

                    }
                });

            } else {
                System.out.println("-------- equipment not registered, HM not saved");

            }

        } else {

            /**
             * save offline
             */

            System.out.println("-------- offline post hm start");

            HMLocal hml = new HMLocal();
            hml.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
            hml.setIdOperationSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
            hml.setIdOperationLoader(MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
            hml.setTime(Utilities.getCurrentUnix());
            hml.setIdEquipment(MainApp.equipmentLocal == null ? null : MainApp.equipmentLocal.getId());
            hml.setIdOperator(MainApp.user == null ? null : MainApp.user.getId());
            hml.setType("start");
            hml.setHm(hm);
            hml.setInfo(info);
            MainApp.hmDB.insertIgnore(hml);

        }
    }

    public static void postHMStop(Double hm) {
        DailyHourMeter dhm = new DailyHourMeter();
        if (Utilities.isLoaderHauler()) {
            dhm.setActualId(MainApp.actualInfo.getId());
        } else {
            dhm.setActualSupportId(MainApp.actualSupport.getId());
        }
        // try {
        // JSONObject payload = MainApp.device.getHourMeter();
        // Double hm = payload.getJSONObject("hour_meter").getDouble("value");
        // dhm.setHm(hm.toString());
        // } catch (Exception ex) {
        // ex.printStackTrace();
        // }
        dhm.setHm(hm);
        dhm.setType("stop");
        dhm.setEquipmentId(MainApp.equipmentLocal.getId());
        dhm.setIsDeleted(0);

        if (NetworkConnection.getServiceStatus()) {
            Call<ResponseModel> call = APIClient.getInstance().postDailyStop(MainApp.requestHeader, dhm);
            call.enqueue(new Callback<ResponseModel>() {

                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                    MainApp.latestHourMeter = hm;

                    // System.out.println(response.body());
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    // t.printStackTrace();

                    if (t instanceof ConnectException) {
                        Utilities.messageSocketException(t, "postHMStop");
                    } else if (t instanceof TimeoutException) {
                        Utilities.messageTimeoutException(t, "postHMStop");
                    } else {
                        Utilities.messageOtherException(t, "postHMStop");
                    }

                    MainApp.latestHourMeter = hm;
                }
            });
        } else {
            System.out.println("-------- offline on post hm stop");
        }
    }

    public static void getActualCycle(String id_actual, Runnable callback) {

        if (!NetworkConnection.getServiceStatus()) {
            return;
        }

        Call<ResponseModel> call = APIClient.getInstance().getActualCycle(MainApp.requestHeader, id_actual);
        call.enqueue(new Callback<ResponseModel>() {

            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()) {
                    Object obj = response.body().getOutput();
                    System.out.println("--- get cycle");
                    System.out.println(obj);
                    Map<String, Object> map = mapObject(obj);
                    Object cycle = map.get("cycle");
                    System.out.println("--- cycle " + cycle.toString());
                    if (cycle != null) {
                        Double cyc = Double.parseDouble(cycle.toString());
                        MainApp.dashboardInfo.setCycle(cyc);
                    }
                }
                if (callback != null) {
                    callback.run();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                // t.printStackTrace();

                if (t instanceof ConnectException) {
                    Utilities.messageSocketException(t, "Get ActualCycle");
                } else if (t instanceof TimeoutException) {
                    Utilities.messageTimeoutException(t, "Get ActualCycle");
                } else {
                    Utilities.messageOtherException(t, "Get ActualCycle");
                }

                if (callback != null) {
                    callback.run();
                }
            }

        });
    }

    public static void getActualCycleLoader(String id_loader, Runnable callback) {

        if (!NetworkConnection.getServiceStatus()) {
            return;
        }

        Call<ResponseModel> call = APIClient.getInstance().getActualCycleLoader(MainApp.requestHeader, id_loader);
        call.enqueue(new Callback<ResponseModel>() {

            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()) {
                    Object obj = response.body().getOutput();
                    System.out.println("--- get cycle");
                    System.out.println(obj);
                    Map<String, Object> map = mapObject(obj);
                    Object cycle = map.get("cycle");
                    System.out.println("--- cycle " + cycle.toString());
                    if (cycle != null) {
                        Double cyc = Double.parseDouble(cycle.toString());
                        MainApp.dashboardInfo.setCycle(cyc);
                    }
                }
                if (callback != null) {
                    callback.run();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                t.printStackTrace();
                if (callback != null) {
                    callback.run();
                }
            }

        });
    }

    public static Double getBCMFromTonnage(Double tonnage, Double density) {
        return tonnage / density;
    }

    public static Double getTonnageFromBCM(Double bcm, Double density) {
        return bcm * density;
    }

    public static void setSatuan() {
        String materialType;

        if (isHauler() && MainApp.actualInfo != null) {
            materialType = MainApp.actualInfo.getCd_material().getType() == null ? ""
                    : MainApp.actualInfo.getCd_material().getType().trim();
        } else if (isLoader() && MainApp.actualLoader != null) {
            materialType = MainApp.actualLoader.getCd_material().getType() == null ? ""
                    : MainApp.actualLoader.getCd_material().getType().trim();
        } else {
            materialType = "";
        }

        if (materialType.equals(ConstantValues.MATERIAL_COAL)) {
            MainApp.satuan = "Ton";
        } else if (materialType.equals(ConstantValues.MATERIAL_OB)) {
            MainApp.satuan = "BCM";
        } else if (materialType.equals(ConstantValues.MATERIAL_COAL)) {
            MainApp.satuan = "BCM";
        } else {
            MainApp.satuan = "";
        }
    }

    public static Double getPayloadFromDevice() {
        JSONObject dataPayload = MainApp.device.getPayload();
        try {
            Double payload = dataPayload.getJSONObject("payload").getDouble("value");
            return payload;

        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);

        }
        return 0.0;
    }

    public static void handleFirstBucketOnHauler() {
        String materialType = MainApp.actualInfo.getCd_material().getType() == null ? ""
                : MainApp.actualInfo.getCd_material().getType().trim();

        Integer capacityReal = MainApp.equipmentLocal.getCapacityReal();
        Double payload = getPayloadFromDevice();
        Double density = MainApp.actualInfo.getCd_material().getDensity() == null ? 0.0
                : Double.parseDouble(MainApp.actualInfo.getCd_material().getDensity().toString());

        if (materialType.equals(ConstantValues.MATERIAL_COAL)) {

            MainApp.tonnage = payload - capacityReal;
            MainApp.tonnageUjiPetik = MainApp.equipmentLocal.getCapacityLabs2();
            Double bucketBcm = 1 * MainApp.actualInfo.getId_loader().getEquipmentCategory().getBucketCapacity();
            MainApp.tonnageBucket = getTonnageFromBCM(bucketBcm, density);

        } else if (materialType.equals(ConstantValues.MATERIAL_OB)) {

            MainApp.tonnage = getBCMFromTonnage(payload - capacityReal, density);
            MainApp.tonnageUjiPetik = MainApp.equipmentLocal.getCapacityLabs().doubleValue();
            Double bucketBcm = 1 * MainApp.actualInfo.getId_loader().getEquipmentCategory().getBucketCapacity();
            MainApp.tonnageBucket = bucketBcm;

        } else if (materialType.equals(ConstantValues.MATERIAL_MUD)) {

            MainApp.tonnage = getBCMFromTonnage(payload - capacityReal, density);
            MainApp.tonnageUjiPetik = MainApp.equipmentLocal.getCapacityLabs3().doubleValue();
            Double bucketBcm = 1 * MainApp.actualInfo.getId_loader().getEquipmentCategory().getBucketCapacity();
            MainApp.tonnageBucket = bucketBcm;

        }

        // MainApp.dashboardInfo.setTonnagePayload(MainApp.tonnage);
        // MainApp.dashboardInfo.setTonnageUjiPetik(MainApp.tonnageUjiPetik);
        MainApp.dashboardInfo.setTonnageBucket(MainApp.tonnageBucket);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                // NavigationUI.addLogActivity("", "Tonase payload: " + MainApp.tonnage + " " +
                // MainApp.satuan);
                // NavigationUI.addLogActivity("", "Tonase uji petik: " +
                // MainApp.tonnageUjiPetik + " " + MainApp.satuan);
                NavigationUI.addLogActivity("", "First load: " + MainApp.tonnageBucket + " " + MainApp.satuan);
            }
        });

        // SEND payload to loader
        Utilities.pushResponseData(ConstantValues.KAFKA_DATA_TYPE_PAYLOAD, MainApp.actualInfo.getId_loader().getId(),
                payload.toString());
    }

    public static void handleFirstBucketOnLoader() {
        if (MainApp.activeHauler != null && MainApp.haulerList.get(MainApp.activeHauler) != null) {

        } else {
            System.out.println("------ invalid active hauler");
            return;
        }

        String materialType = MainApp.actualLoader.getCd_material().getType() == null ? ""
                : MainApp.actualLoader.getCd_material().getType().trim();

        Integer capacityReal = MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getCapacity_real();

        /** TODO: get payload yang terbaca pada hauler */
        Double payload = MainApp.activeHaulerPayload;

        Double density = MainApp.actualLoader.getCd_material().getDensity() == null ? 0.0
                : Double.parseDouble(MainApp.actualLoader.getCd_material().getDensity().toString());

        if (materialType.equals(ConstantValues.MATERIAL_COAL)) {

            MainApp.tonnage = payload - capacityReal;
            MainApp.tonnageUjiPetik = MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getCapacity_labs_2();
            Double bucketBcm = 1 * MainApp.equipmentLocal.getEquipmentCategory().getBucketCapacity();
            MainApp.tonnageBucket = getTonnageFromBCM(bucketBcm, density);

        } else if (materialType.equals(ConstantValues.MATERIAL_OB)) {

            MainApp.tonnage = getBCMFromTonnage(payload - capacityReal, density);
            MainApp.tonnageUjiPetik = MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getCapacity_labs();
            Double bucketBcm = 1 * MainApp.equipmentLocal.getEquipmentCategory().getBucketCapacity();
            MainApp.tonnageBucket = bucketBcm;

        } else if (materialType.equals(ConstantValues.MATERIAL_MUD)) {

            MainApp.tonnage = getBCMFromTonnage(payload - capacityReal, density);
            MainApp.tonnageUjiPetik = MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getCapacity_labs_3();
            Double bucketBcm = 1 * MainApp.equipmentLocal.getEquipmentCategory().getBucketCapacity();
            MainApp.tonnageBucket = bucketBcm;

        }

        // MainApp.dashboardInfo.setTonnagePayload(MainApp.tonnage);
        // MainApp.dashboardInfo.setTonnageUjiPetik(MainApp.tonnageUjiPetik);
        MainApp.dashboardInfo.setTonnageBucket(MainApp.tonnageBucket);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                // NavigationUI.addLogActivity("", "Tonase payload: " + MainApp.tonnage + " " +
                // MainApp.satuan);
                // NavigationUI.addLogActivity("", "Tonase uji petik: " +
                // MainApp.tonnageUjiPetik + " " + MainApp.satuan);
                Utilities.setNotificationText(
                        "First load to " + MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getName() + " : "
                                + MainApp.tonnageBucket + " " + MainApp.satuan);
            }
        });
    }

    public static void handleFullBucketOnHauler() {
        String materialType = MainApp.actualInfo.getCd_material().getType() == null ? ""
                : MainApp.actualInfo.getCd_material().getType().trim();

        Integer capacityReal = MainApp.equipmentLocal.getCapacityReal();
        Double payload = getPayloadFromDevice();
        Double density = MainApp.actualInfo.getCd_material().getDensity() == null ? 0.0
                : Double.parseDouble(MainApp.actualInfo.getCd_material().getDensity().toString());

        if (materialType.equals(ConstantValues.MATERIAL_COAL)) {

            MainApp.tonnage = payload - capacityReal;
            MainApp.tonnageUjiPetik = MainApp.equipmentLocal.getCapacityLabs2();
            Double bucketBcm = MainApp.bucketCount
                    * MainApp.actualInfo.getId_loader().getEquipmentCategory().getBucketCapacity();
            MainApp.tonnageBucket = getTonnageFromBCM(bucketBcm, density);

        } else if (materialType.equals(ConstantValues.MATERIAL_OB)) {

            MainApp.tonnage = getBCMFromTonnage(payload - capacityReal, density);
            MainApp.tonnageUjiPetik = MainApp.equipmentLocal.getCapacityLabs().doubleValue();
            Double bucketBcm = MainApp.bucketCount
                    * MainApp.actualInfo.getId_loader().getEquipmentCategory().getBucketCapacity();
            MainApp.tonnageBucket = bucketBcm;

        } else if (materialType.equals(ConstantValues.MATERIAL_MUD)) {

            MainApp.tonnage = getBCMFromTonnage(payload - capacityReal, density);
            MainApp.tonnageUjiPetik = MainApp.equipmentLocal.getCapacityLabs3().doubleValue();
            Double bucketBcm = MainApp.bucketCount
                    * MainApp.actualInfo.getId_loader().getEquipmentCategory().getBucketCapacity();
            MainApp.tonnageBucket = bucketBcm;

        }

        MainApp.dashboardInfo.setTonnagePayload(MainApp.tonnage);
        MainApp.dashboardInfo.setTonnageUjiPetik(MainApp.tonnageUjiPetik);
        MainApp.dashboardInfo.setTonnageBucket(MainApp.tonnageBucket);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                NavigationUI.addLogActivity("", "Payload: " + MainApp.tonnage + " " + MainApp.satuan);
                NavigationUI.addLogActivity("", "Truck factor: " + MainApp.tonnageUjiPetik + " " + MainApp.satuan);
                NavigationUI.addLogActivity("", "Full load: " + MainApp.tonnageBucket + " " + MainApp.satuan);
            }
        });

        // SEND payload to loader
        Utilities.pushResponseData(ConstantValues.KAFKA_DATA_TYPE_PAYLOAD, MainApp.actualInfo.getId_loader().getId(),
                payload.toString());
    }

    public static void handleFullBucketOnLoader() {
        if (MainApp.activeHauler != null && MainApp.haulerList.get(MainApp.activeHauler) != null) {

        } else {
            System.out.println("------ invalid active hauler");
            return;
        }

        String materialType = MainApp.actualLoader.getCd_material().getType() == null ? ""
                : MainApp.actualLoader.getCd_material().getType().trim();

        Integer capacityReal = MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getCapacity_real();

        /** TODO: get payload from hauler */
        Double payload = MainApp.activeHaulerPayload;

        Double density = MainApp.actualLoader.getCd_material().getDensity() == null ? 0.0
                : Double.parseDouble(MainApp.actualLoader.getCd_material().getDensity().toString());

        if (materialType.equals(ConstantValues.MATERIAL_COAL)) {

            MainApp.tonnage = payload - capacityReal;
            MainApp.tonnageUjiPetik = MainApp.haulerList.get(MainApp.activeHauler).getId_hauler()
                    .getCapacity_labs_2() == null ? 0.0
                            : MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getCapacity_labs_2();
            Double bucketBcm = MainApp.bucketCount * MainApp.equipmentLocal.getEquipmentCategory().getBucketCapacity();
            MainApp.tonnageBucket = getTonnageFromBCM(bucketBcm, density);

        } else if (materialType.equals(ConstantValues.MATERIAL_OB)) {

            MainApp.tonnage = getBCMFromTonnage(payload - capacityReal, density);
            MainApp.tonnageUjiPetik = MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getCapacity_labs();
            Double bucketBcm = MainApp.bucketCount * MainApp.equipmentLocal.getEquipmentCategory().getBucketCapacity();
            MainApp.tonnageBucket = bucketBcm;

        } else if (materialType.equals(ConstantValues.MATERIAL_MUD)) {

            MainApp.tonnage = getBCMFromTonnage(payload - capacityReal, density);
            MainApp.tonnageUjiPetik = MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getCapacity_labs();
            Double bucketBcm = MainApp.bucketCount * MainApp.equipmentLocal.getEquipmentCategory().getBucketCapacity();
            MainApp.tonnageBucket = bucketBcm;

        }

        MainApp.dashboardInfo.setTonnagePayload(MainApp.tonnage);
        MainApp.dashboardInfo.setTonnageUjiPetik(MainApp.tonnageUjiPetik);
        MainApp.dashboardInfo.setTonnageBucket(MainApp.tonnageBucket);

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                if (payload > 0) {
                    Utilities.setNotificationText(
                            "Payload " + MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getName() + " : "
                                    + MainApp.tonnage + " " + MainApp.satuan);
                }
                Utilities.setNotificationText(
                        "Truck factor " + MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getName() + " : "
                                + MainApp.tonnageUjiPetik + " " + MainApp.satuan);
                Utilities.setNotificationText(
                        "Full load " + MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getName() + " : "
                                + MainApp.tonnageBucket + " " + MainApp.satuan);
            }
        });
    }

    public static void handleTimbanganOnHauler() {
        if (isHauler()) {
            String materialType = MainApp.actualInfo.getCd_material().getType() == null ? ""
                    : MainApp.actualInfo.getCd_material().getType().trim();

            if (materialType.equals(ConstantValues.MATERIAL_COAL)) {
                MainApp.tonnageTimbangan = MainApp.beratTimbangan - MainApp.equipmentLocal.getCapacityReal();
            } else if (materialType.equals(ConstantValues.MATERIAL_OB)) {
                Double ton = MainApp.beratTimbangan - MainApp.equipmentLocal.getCapacityReal();
                Double density = Double.parseDouble(MainApp.actualInfo.getCd_material().getDensity().toString());
                MainApp.tonnageTimbangan = getBCMFromTonnage(ton, density);
            } else if (materialType.equals(ConstantValues.MATERIAL_COAL)) {
                Double ton = MainApp.beratTimbangan - MainApp.equipmentLocal.getCapacityReal();
                Double density = Double.parseDouble(MainApp.actualInfo.getCd_material().getDensity().toString());
                MainApp.tonnageTimbangan = getBCMFromTonnage(ton, density);
            }

            MainApp.dashboardInfo.setTonnageTimbangan(MainApp.tonnageTimbangan);

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    NavigationUI.addLogActivity("", "Weigher: " + MainApp.tonnageTimbangan + " " + MainApp.satuan);
                }
            });
        }
    }

    public static Double getUjiPetikHauler() {
        if (MainApp.isCanPost()) {
            /**
             * online mode
             */

            if (isHauler()) {
                if (MainApp.actualInfo.getCd_material().getType().equals(ConstantValues.MATERIAL_OB)) {
                    Double cap = MainApp.equipmentLocal.getCapacityLabs() == null ? 0.0
                            : MainApp.equipmentLocal.getCapacityLabs();
                    return cap;
                }
                if (MainApp.actualInfo.getCd_material().getType().equals(ConstantValues.MATERIAL_COAL)) {
                    Double cap = MainApp.equipmentLocal.getCapacityLabs2() == null ? 0.0
                            : MainApp.equipmentLocal.getCapacityLabs2().doubleValue();
                    return cap;
                }
                if (MainApp.actualInfo.getCd_material().getType().equals(ConstantValues.MATERIAL_MUD)) {
                    Double cap = MainApp.equipmentLocal.getCapacityLabs3() == null ? 0.0
                            : MainApp.equipmentLocal.getCapacityLabs3().doubleValue();
                    return cap;
                }
                return 0.0;

            } else if (isLoader()) {
                if (MainApp.activeHauler == null || MainApp.haulerList == null) {
                    return 0.0;
                } else if (MainApp.activeHauler != null && MainApp.haulerList.get(MainApp.activeHauler) != null) {

                } else {
                    return 0.0;
                }

                if (MainApp.actualLoader.getCd_material().getType().equals(ConstantValues.MATERIAL_OB)) {
                    Double cap = MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getCapacity_labs() == null
                            ? 0.0
                            : MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getCapacity_labs();
                    return cap;
                }
                if (MainApp.actualLoader.getCd_material().getType().equals(ConstantValues.MATERIAL_COAL)) {
                    Double cap = MainApp.haulerList.get(MainApp.activeHauler).getId_hauler()
                            .getCapacity_labs_2() == null ? 0.0
                                    : MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getCapacity_labs_2();
                    return cap;
                }
                if (MainApp.actualLoader.getCd_material().getType().equals(ConstantValues.MATERIAL_MUD)) {
                    Double cap = MainApp.haulerList.get(MainApp.activeHauler).getId_hauler()
                            .getCapacity_labs_3() == null ? 0.0
                                    : MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getCapacity_labs_3();
                    return cap;
                }
                return 0.0;
            } else {
                return 0.0;
            }

        } else {
            /**
             * offline mode
             */
            return 0.0;

        }
    }

    /**
     * Distance from lat lon in meter
     *
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     */
    public static Double getDistance(Double lat1, Double lon1, Double lat2, Double lon2) {
        Double R = 6371.0; // Radius of the earth in km
        Double dLat = deg2rad(lat2 - lat1); // deg2rad below
        Double dLon = deg2rad(lon2 - lon1);
        Double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        Double d = R * c; // Distance in km
        Double dM = d * 1000; // Distance in meter
        System.out.println("------ distance " + dM.toString());
        return dM;
    }

    public static Double deg2rad(Double deg) {
        return deg * (Math.PI / 180);
    }

    public static void pushHaulerDistance() {
        if (!isHauler()) {
            System.out.println("------ not hauler");
            return;
        }

        /**
         * dipanggil oleh loader untuk meminta data payload dari hauler
         */
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            Date date = new Date();
            String strDate = formatter.format(date);
            JSONObject json = new JSONObject();
            json.put("date", strDate);
            json.put("hauler", MainApp.equipmentLocal.getId());
            json.put("hauler_name", MainApp.equipmentLocal.getName().trim());
            json.put("loader", MainApp.actualInfo.getId_loader().getId());
            json.put("distance_to_loading", MainApp.distanceToLoadingPoint);
            json.put("distance_to_dumping", MainApp.distanceToDumpingPoint);
            MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_DISTANCE, json.toString());

        } catch (Exception ex) {
            ex.printStackTrace();
            MainApp.log.error("utilities", ex);

        }
    }

    // dalam meter/seken
    public static void computeGPSSpeed(Double distance, Long gpsTime1, Long gpsTime2) {
        Long tm = Math.abs(gpsTime1 - gpsTime2);
        if (tm > 0) {
            MainApp.gpsSpeed = distance / tm;
        } else {
            MainApp.gpsSpeed = distance;
        }
        System.out.println("----- tm " + tm.toString());
        System.out.println("----- gps speed " + MainApp.gpsSpeed.toString());
        if (!isLoaderHauler()) {
            if (MainApp.dashboardInfo != null && MainApp.gpsSpeed != null) {
                MainApp.dashboardInfo.setSpeed(MainApp.gpsSpeed);
            }
        }
    }

    public static void postMaintenanceDescription(MaintenanceDescriptionPost eq,
            CustomRunnableX<MaintenanceDescription> callback) {
        if (NetworkConnection.getServiceStatus()) {

            Call<ResponseModelCustom<MaintenanceDescription>> call = APIClient.getInstance()
                    .postMaintenanceDescription(MainApp.requestHeader, eq);
            call.enqueue(new Callback<ResponseModelCustom<MaintenanceDescription>>() {

                @Override
                public void onResponse(Call<ResponseModelCustom<MaintenanceDescription>> call,
                        Response<ResponseModelCustom<MaintenanceDescription>> response) {
                    if (response.isSuccessful()) {
                        if (callback != null) {
                            callback.setData(response.body().getOutput());
                            callback.run();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModelCustom<MaintenanceDescription>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            System.out.println("-------------- offline ");
        }
    }

    public static void postEditMaintenanceDescription(MaintenanceDescriptionPost eq,
            CustomRunnableX<MaintenanceDescription> callback) {
        if (NetworkConnection.getServiceStatus()) {

            Call<ResponseModelCustom<MaintenanceDescription>> call = APIClient.getInstance()
                    .putMaintenanceDescription(MainApp.requestHeader, eq, eq.getId());
            call.enqueue(new Callback<ResponseModelCustom<MaintenanceDescription>>() {

                @Override
                public void onResponse(Call<ResponseModelCustom<MaintenanceDescription>> call,
                        Response<ResponseModelCustom<MaintenanceDescription>> response) {
                    if (response.isSuccessful()) {
                        if (callback != null) {
                            callback.setData(response.body().getOutput());
                            callback.run();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModelCustom<MaintenanceDescription>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            System.out.println("-------------- offline ");
        }
    }

    public static void postMaintenanceDetails(MaintenanceDetailsPost eq, CustomRunnableX<MaintenanceDetails> callback) {
        if (NetworkConnection.getServiceStatus()) {

            Call<ResponseModelCustom<MaintenanceDetails>> call = APIClient.getInstance()
                    .postMaintenanceDetails(MainApp.requestHeader, eq);
            call.enqueue(new Callback<ResponseModelCustom<MaintenanceDetails>>() {

                @Override
                public void onResponse(Call<ResponseModelCustom<MaintenanceDetails>> call,
                        Response<ResponseModelCustom<MaintenanceDetails>> response) {
                    if (response.isSuccessful()) {
                        if (callback != null) {
                            callback.setData(response.body().getOutput());
                            callback.run();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModelCustom<MaintenanceDetails>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            System.out.println("-------------- offline ");
        }
    }

    public static void postEditMaintenanceDetails(MaintenanceDetailsPost eq,
            CustomRunnableX<MaintenanceDetails> callback) {
        if (NetworkConnection.getServiceStatus()) {

            Call<ResponseModelCustom<MaintenanceDetails>> call = APIClient.getInstance()
                    .putMaintenanceDetails(MainApp.requestHeader, eq, eq.getId());
            call.enqueue(new Callback<ResponseModelCustom<MaintenanceDetails>>() {

                @Override
                public void onResponse(Call<ResponseModelCustom<MaintenanceDetails>> call,
                        Response<ResponseModelCustom<MaintenanceDetails>> response) {
                    if (response.isSuccessful()) {
                        if (callback != null) {
                            callback.setData(response.body().getOutput());
                            callback.run();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModelCustom<MaintenanceDetails>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            System.out.println("-------------- offline ");
        }
    }

    public static void deleteMaintenanceDetails(String id, CustomRunnableX<MaintenanceDetails> callback) {
        if (NetworkConnection.getServiceStatus()) {

            Call<ResponseModelCustom<MaintenanceDetails>> call = APIClient.getInstance()
                    .deleteMaintenanceDetails(MainApp.requestHeader, id);
            call.enqueue(new Callback<ResponseModelCustom<MaintenanceDetails>>() {

                @Override
                public void onResponse(Call<ResponseModelCustom<MaintenanceDetails>> call,
                        Response<ResponseModelCustom<MaintenanceDetails>> response) {
                    if (response.isSuccessful()) {
                        if (callback != null) {
                            callback.setData(response.body().getOutput());
                            callback.run();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModelCustom<MaintenanceDetails>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            System.out.println("-------------- offline ");
        }
    }

    public static void stopMaintenanceDetails(MaintenanceDetailsStopPost eq,
            CustomRunnableX<MaintenanceDetails> callback) {
        if (NetworkConnection.getServiceStatus()) {

            Call<ResponseModelCustom<MaintenanceDetails>> call = APIClient.getInstance()
                    .stopMaintenanceDetails(MainApp.requestHeader, eq);
            call.enqueue(new Callback<ResponseModelCustom<MaintenanceDetails>>() {

                @Override
                public void onResponse(Call<ResponseModelCustom<MaintenanceDetails>> call,
                        Response<ResponseModelCustom<MaintenanceDetails>> response) {
                    if (response.isSuccessful()) {
                        if (callback != null) {
                            callback.setData(response.body().getOutput());
                            callback.run();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModelCustom<MaintenanceDetails>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            System.out.println("-------------- offline ");
        }
    }

    public static void stopMaintenance(String id, CustomRunnableX<Taskmodel> callback) {
        if (NetworkConnection.getServiceStatus()) {

            Call<ResponseModelCustom<Taskmodel>> call = APIClient.getInstance()
                    .postMaintenanceStop(MainApp.requestHeader, id);
            call.enqueue(new Callback<ResponseModelCustom<Taskmodel>>() {

                @Override
                public void onResponse(Call<ResponseModelCustom<Taskmodel>> call,
                        Response<ResponseModelCustom<Taskmodel>> response) {
                    if (response.isSuccessful()) {
                        if (callback != null) {
                            callback.setData(response.body().getOutput());
                            callback.run();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModelCustom<Taskmodel>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            System.out.println("-------------- offline ");
        }
    }

    public static void startMaintenance(String id, CustomRunnableX<Taskmodel> callback) {
        if (NetworkConnection.getServiceStatus()) {

            Call<ResponseModelCustom<Taskmodel>> call = APIClient.getInstance()
                    .postMaintenanceStart(MainApp.requestHeader, id);
            call.enqueue(new Callback<ResponseModelCustom<Taskmodel>>() {

                @Override
                public void onResponse(Call<ResponseModelCustom<Taskmodel>> call,
                        Response<ResponseModelCustom<Taskmodel>> response) {
                    if (response.isSuccessful()) {
                        if (callback != null) {
                            callback.setData(response.body().getOutput());
                            callback.run();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModelCustom<Taskmodel>> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            System.out.println("-------------- offline ");
        }
    }

    public static ActualDetail getHaulerActualById(String id) {
        if (NetworkConnection.getServiceStatus()) {

            Call<ResponseModelCustom<ActualDetail>> call = APIClient.getInstance()
                    .getHaulerActualById(MainApp.requestHeader, id);

            try {
                Response<ResponseModelCustom<ActualDetail>> response = call.execute();

                if (response.isSuccessful()) {
                    return response.body().getOutput();
                } else {
                    return null;
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                MainApp.log.error("utilities", ex);
                return null;
            }

        } else {
            System.out.println("-------------- offline ");
            return null;
        }
    }

    public static void setLatestActivity(OperationStatusLocal saved) {
        MainApp.latestActivity = new EquipmentLogStartLocal();
        MainApp.latestActivity.setId(saved.getId());
        MainApp.latestActivity.setIdActual(saved.getIdActual() == null ? null : saved.getIdActual().toString());
        MainApp.latestActivity.setIdOperationSupport(
                saved.getIdOperationSupport() == null ? null : saved.getIdOperationSupport().toString());
        MainApp.latestActivity.setIdOperationLoader(
                saved.getIdOperationLoader() == null ? null : saved.getIdOperationLoader().toString());
        MainApp.latestActivity
                .setIdEquipment(saved.getIdEquipment() == null ? null : saved.getIdEquipment().toString());
        MainApp.latestActivity.setIdOperator(saved.getIdOperator() == null ? null : saved.getIdOperator().toString());
        MainApp.latestActivity.setTime(saved.getTime().intValue());

        Constant activity = MainApp.constantDB.getConstantById("cd",
                saved.getCdActivity() == null ? null : saved.getCdActivity().toString());
        if (activity != null) {
            MainApp.latestActivity.setCdActivity(activity);
        }
        Constant tum = MainApp.constantDB.getConstantById("cd",
                saved.getCdTum() == null ? null : saved.getCdTum().toString());
        if (tum != null) {
            MainApp.latestActivity.setCdTum(tum);
        }
        MainApp.latestActivity.setCdType(saved.getCdType() == null ? null : saved.getCdType().toString());

        MainApp.latestActivity.setLatitude(saved.getLatitude());
        MainApp.latestActivity.setLongitude(saved.getLongitude());
    }

    public static void initOnline() {
        System.out.print("Syncing");

        if (NetworkConnection.getServiceStatus()) {
            System.out.println(" ---- ");

            /**
             * Syncing ketika jaringan aktif pada current shift
             */

            if (MainApp.equipmentLocal == null) {
                Utilities.getEquipmentByName(MainApp.config("config.unit_id"));
            }

            if (MainApp.requestHeader == null) {
                MainApp.initProcess = true;

                Integer timeout = Integer.parseInt(MainApp.config("config.request_timeout"));
                Long naw = Utilities.getCurrentUnix();

                if ((naw - MainApp.latestRequest) >= timeout) {

                    GPS gps = Utilities.getCurrentGPS();

                    MainApp.latestRequest = Utilities.getCurrentUnix();

                    Call<UserData> call = APIClient.getInstance().backgroundLogin(MainApp.username, MainApp.password,
                            MainApp.username + "/" + MainApp.config("config.unit_id"), ConstantValues.AUTH_STATE_LOGIN,
                            MainApp.equipmentLocal == null ? "" : MainApp.equipmentLocal.getId(),
                            MainApp.config("config.unit_type"), gps.getLat(), gps.getLon());

                    call.enqueue(new Callback<UserData>() {

                        @Override
                        public void onResponse(Call<UserData> call, Response<UserData> response) {
                            MainApp.offlineMode = false;

                            UserData userData2 = response.body();

                            if (response.isSuccessful()) {
                                MainApp.loginCallback(true, userData2.getKeluaran());
                                MainApp.initProcess = false;
                            }
                        }

                        @Override
                        public void onFailure(Call<UserData> call, Throwable t) {
                            System.out.println(t.getMessage());
                        }
                    });

                }

            }

            if (MainApp.requestHeader != null && MainApp.syncingCounter == 20) {

                if (MainApp.loggedIn == true) {

                    if (MainApp.user.getCd_role().getCd().equals(ConstantValues.ROLE_OPERATOR)) {

                        if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)) {
                            if (MainApp.actualLoader == null) {
                                MainApp.initProcess = true;
                                Utilities.getOperationLoader();
                            }

                            if (MainApp.actualLoader == null) {

                                // send notif ke dispatch untuk seting plan

                            } else {
                                MainApp.initProcess = false;
                            }

                        } else if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER)) {
                            if (MainApp.actualInfo == null) {
                                MainApp.initProcess = true;
                                Utilities.getOperationActual();
                            }

                            if (MainApp.actualInfo == null) {

                                // send notif ke dispatch untuk seting plan

                            } else {
                                MainApp.initProcess = false;
                            }

                        } else {
                            if (MainApp.actualSupport == null) {
                                MainApp.initProcess = true;
                                Utilities.getOperationSupportActual();
                            }

                            if (MainApp.actualSupport == null) {

                                // send notif ke dispatch untuk seting plan

                            } else {
                                MainApp.initProcess = false;
                            }

                        }

                    }

                }
            }

            /**
             * Syncing offline data yang sudah masuk database lokal
             */

            if (MainApp.requestHeader != null && MainApp.syncingCounter > 0) {

                /**
                 * data ritase
                 */
                OperationLocal ritaseOff = (OperationLocal) MainApp.operationDB.getFlaggedDataOne();
                if (ritaseOff != null) {

                    MainApp.operationDB.postData(ritaseOff, new Runnable() {

                        @Override
                        public void run() {
                            MainApp.operationDB.setSynchronized(ritaseOff.getId());
                        }
                    });

                } else {

                    if (isLoaderHauler()) {

                        /**
                         * data cycle activity khusus hauler dan loader
                         */

                        OperationLogDetailLocal ldOff = (OperationLogDetailLocal) MainApp.operationLogDetailDB
                                .getFlaggedDataOne();

                        if (ldOff != null) {
                            MainApp.operationLogDetailDB.postData(ldOff, new Runnable() {

                                @Override
                                public void run() {
                                    MainApp.operationLogDetailDB.setSynchronized(ldOff.getId());
                                }
                            });

                            return;
                        }

                    }

                    /**
                     * data activity status
                     */
                    OperationStatusLocal osOff = (OperationStatusLocal) MainApp.operationStatusDB.getFlaggedDataOne();

                    if (osOff != null) {
                        MainApp.operationStatusDB.postData(osOff, new Runnable() {

                            @Override
                            public void run() {
                                MainApp.operationStatusDB.setSynchronized(osOff.getId());
                            }
                        });

                    } else {

                        /**
                         * data P2H
                         */
                        PrestartSubmit preOff = (PrestartSubmit) MainApp.prestartLogDB.getFlaggedDataOne();

                        if (preOff != null) {
                            MainApp.prestartLogDB.postData(preOff, new Runnable() {

                                @Override
                                public void run() {
                                    MainApp.prestartLogDB.setSynchronized(preOff.getId());
                                }
                            });

                        } else {

                            /**
                             * data safety status
                             */
                            SafetyStatusLocal safeOff = (SafetyStatusLocal) MainApp.safetyStatusDB.getFlaggedDataOne();
                            if (safeOff != null) {
                                MainApp.safetyStatusDB.postData(safeOff, new Runnable() {

                                    @Override
                                    public void run() {
                                        MainApp.safetyStatusDB.setSynchronized(safeOff.getId());
                                    }
                                });

                            } else {

                                /**
                                 * data fatigue
                                 */
                                FatigueLogLocal fatOff = (FatigueLogLocal) MainApp.fatigueLogDB.getFlaggedDataOne();
                                if (fatOff != null) {
                                    MainApp.fatigueLogDB.postData(fatOff, new Runnable() {

                                        @Override
                                        public void run() {
                                            MainApp.fatigueLogDB.setSynchronized(fatOff.getId());
                                        }
                                    });

                                } else {

                                    /**
                                     * data HM
                                     */
                                    HMLocal hmOff = (HMLocal) MainApp.hmDB.getFlaggedDataOne();

                                    if (hmOff != null) {
                                        MainApp.hmDB.postData(hmOff, new Runnable() {

                                            @Override
                                            public void run() {
                                                MainApp.hmDB.setSynchronized(hmOff.getId());
                                            }
                                        });

                                    } else {

                                        /**
                                         * data GPS
                                         */
                                        GPSLocal gpsOff = (GPSLocal) MainApp.gpsDB.getFlaggedDataOne();
                                        if (gpsOff != null) {

                                            MainApp.gpsDB.postData(gpsOff, new Runnable() {

                                                @Override
                                                public void run() {
                                                    MainApp.gpsDB.setSynchronized(gpsOff.getId());
                                                }
                                            });

                                        }
                                    }
                                }
                            }
                        }
                    }

                }

            }
            // end of main wrapper

        }
        // end of check online
    }
    // end of function

    public static StorageStatus deviceStorageStatus() {
        File f = new File("C:\\");
        // System.out.println("Printing the total space");
        // System.out.println(f.getTotalSpace() + " bytes");
        // System.out.println(f.getTotalSpace() / 1000.00 + " Kilobytes");
        // System.out.println(f.getTotalSpace() / 1000000.00 + " Megabytes");
        // System.out.println(f.getTotalSpace() / 1000000000.00 + " Gigabytes");
        // System.out.println("----------------------------");
        // System.out.println("Printing the free space");
        // System.out.println(f.getFreeSpace() + " bytes");
        // System.out.println(f.getFreeSpace() / 1000.00 + " Kilobytes");
        // System.out.println(f.getFreeSpace() / 1000000.00 + " Megabytes");
        // System.out.println(f.getFreeSpace() / 1000000000.00 + " Gigabytes");

        StorageStatus ss = new StorageStatus();
        ss.freeSpace = f.getFreeSpace() / 1000000; // in megabytes
        ss.totalSpace = f.getTotalSpace() / 1000000; // in megabytes

        return ss;
    }

    public static CPUAndRAM memoryUsage() {
        long memorySize = ((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean())
                .getTotalPhysicalMemorySize();
        long freeSize = ((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean())
                .getFreePhysicalMemorySize();
        double cpuProcess = ((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean())
                .getProcessCpuLoad();
        double cpuSystem = ((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean())
                .getSystemCpuLoad();
        double cpuSystemLoad = ((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean())
                .getSystemLoadAverage();

        // System.out.println("RAM Size =" + memorySize / 1000000.00 + " Bytes");
        // System.out.println("RAM Free =" + freeSize / 1000000.00 + " Bytes");
        // System.out.println("CPU Process =" + cpuProcess);
        // System.out.println("CPU System =" + cpuSystem);
        // System.out.println("CPU SystemLoad =" + cpuSystemLoad);

        CPUAndRAM cr = new CPUAndRAM();
        cr.memSize = memorySize / 1000000.00; // MB
        cr.memFreeSize = freeSize / 1000000.00; // MB
        cr.cpuProcess = cpuProcess;
        cr.cpuSystem = cpuSystem;
        cr.cpuSystemLoad = cpuSystemLoad;

        return cr;
    }

}

class StorageStatus {
    public long totalSpace;
    public long freeSpace;
}

class CPUAndRAM {
    public double memSize;
    public double memFreeSize;
    public double cpuProcess;
    public double cpuSystem;
    public double cpuSystemLoad;
}