const constant = require('./constant');
const moment = require('moment');

/**
 * Distance from lat lon in meter
 * @param lat1
 * @param lon1
 * @param lat2
 * @param lon2
 */

module.exports.getDistance = function (lat1, lon1, lat2, lon2) {
    const R = 6371; // Radius of the earth in km
    const dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    const dLon = this.deg2rad(lon2 - lon1);
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c; // Distance in km
    const dM = d * 1000; // Distance in meter
    return dM;
}

module.exports.mysqlQuery = (my, sql) => {
    return new Promise((resolve, reject) => {
        my.query(sql, function (err, result, fields) {
            if (err) reject(err);
            resolve(result);
        });
    });
};

module.exports.mongoAggregate = (mo, opt) => {
    return new Promise((resolve, reject) => {
        const mongo = mo.db(opt.database).collection(opt.collection).aggregate(opt.option);
        mongo.toArray(function (err, result) {
            if (err) reject(err);
            resolve(result);
        });
    });
};

module.exports.getCurrentShift = () => {
    const naw = moment().unix();
    const tm0 = moment().set({ hour: 0, minute: 0 }).unix();
    const tm6 = moment().set({ hour: 6, minute: 0 }).unix();
    const tm18 = moment().set({ hour: 18, minute: 0 }).unix();

    if (naw > tm0 && naw < tm6) {
        return constant.SHIFT_NIGHT;
    }
    if (naw > tm6 && naw < tm18) {
        return constant.SHIFT_DAY;
    }
    if (naw > tm18) {
        return constant.SHIFT_NIGHT;
    }
    return null;
};

module.exports.isBefore6 = () => {
    const naw = moment().unix();
    const tm6 = moment().set({ hour: 6, minute: 0 }).unix();
    if (naw < tm6) {
        return true;
    } else {
        return false;
    }
};

module.exports.getShiftDayStartTime = () => {
    let tm;
    if (this.isBefore6()) {
        tm = moment().subtract(1, 'days').startOf('day').set({ hour: 6 }).unix();
    } else {
        tm = moment().startOf('day').set({ hour: 6 }).unix();
    }
    return tm;
};

module.exports.getShiftNightStartTime = () => {
    let tm;
    if (this.isBefore6()) {
        tm = moment().subtract(1, 'days').startOf('day').set({ hour: 18 }).unix();
    } else {
        tm = moment().startOf('day').set({ hour: 18 }).unix();
    }
    return tm;
};

module.exports.getYesterdayTime = () => {
    const start = moment().subtract(1, 'days').startOf('day').unix();
    const end = moment().subtract(1, 'days').endOf('day').unix();
    return {
        start: start,
        end: end
    };
};