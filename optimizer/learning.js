const Math = require('mathjs');
const { e, mod } = require('mathjs');

module.exports = function () {
    const module = {};

    module.dummyFactor = [
        { code: 'DISTANCE', factor: 'Jarak terdekat', ahp_weight: 0 },
        { code: 'PRIORITAS', factor: 'Prioritas loader', ahp_weight: 0 },
        { code: 'PRODUKTIVITAS', factor: 'Produktivitas loader', ahp_weight: 0 },
        { code: 'HAULER', factor: 'Jumlah hauler yang dibutuhkan', ahp_weight: 0 },
        { code: 'ANTRIAN', factor: 'Jumlah antrian', ahp_weight: 0 },
    ];

    /**
     * MATRIX
     * dummy matrix
     * pembacaan matrix adalah KOLOM, BARIS
     */
    module.dummyMatrix = {
        DISTANCE: {
            DISTANCE: 1,
            PRIORITAS: 5,
            PRODUKTIVITAS: 3,
            HAULER: 1,
            ANTRIAN: 6
        },
        PRIORITAS: {
            PRIORITAS: 1,
            PRODUKTIVITAS: 2,
            HAULER: 0.5,
            ANTRIAN: 2
        },
        PRODUKTIVITAS: {
            PRODUKTIVITAS: 1,
            HAULER: 0.5,
            ANTRIAN: 2
        },
        HAULER: {
            HAULER: 1,
            ANTRIAN: 5
        },
        ANTRIAN: {
            ANTRIAN: 1
        }
    };

    /**
     * nilai Random Index (RI)
     */
    module.RI = {
        '1': 0,
        '2': 0,
        '3': 0.58,
        '4': 0.90,
        '5': 1.12,
        '6': 1.24,
        '7': 1.32,
        '8': 1.41,
        '9': 1.45,
        '10': 1.49,
        '11': 1.51,
        '12': 1.48,
        '13': 1.56,
        '14': 1.57,
        '15': 1.59
    };

    /**
     * menghitung nilai matrix segitiga atas dan summary nya
     * @param {*} source daftar kriteria
     * @param {*} mtx matrix weight bagian segitiga bawah
     */
    module.generateArrayOfWeight = function (source, mtx) {
        let matrix = [];
        let summary = [];
        let sum = {};
        for (let so of source) {
            let item = [];
            for (let sc of source) {
                if (mtx[so.code][sc.code] !== undefined) {
                    item.push(mtx[so.code][sc.code]);
                } else if (mtx[sc.code][so.code] !== undefined) {
                    mtx[so.code][sc.code] = 1.0 / mtx[sc.code][so.code];
                    item.push(mtx[so.code][sc.code]);
                } else {
                    item.push(undefined);
                }
            }
            matrix.push(item);
            summary.push(Math.sum(item));
            sum[so.code] = Math.sum(item);
        }
        return {
            matrix: matrix,
            summary: summary,
            mtx: mtx,
            sumCol: sum
        };
    }

    /**
     * menghitung nilai eigen dan sum row
     * @param {*} mtx matrix weight
     * @param {*} sum sum colomn
     */
    module.computeEigen = function (mtx, sum) {
        let matrix = {};
        for (let col of Object.keys(mtx)) {
            for (let row of Object.keys(mtx[col])) {
                if (matrix[col] === undefined) {
                    matrix[col] = {};
                }
                matrix[col][row] = mtx[col][row] / sum[col];
            }
        }

        // hitung sum row
        const cc = Object.keys(matrix);
        let sumRow = {};
        let sumRowAvg = {};

        for (let row of cc) {
            let total = 0;
            for (let col of cc) {
                total += matrix[col][row];
            }
            sumRow[row] = total;
            sumRowAvg[row] = total / cc.length;
        }

        return {
            eigen: matrix,
            sumRow: sumRow,
            sumRowAvg: sumRowAvg
        };
    }

    /**
     * menghitung lambda
     */
    module.computeLambda = function (sumCol, sumRowAvg) {
        let lambda = [];
        for (let cc of Object.keys(sumCol)) {
            lambda.push(sumCol[cc] * sumRowAvg[cc]);
        }
        return Math.sum(lambda);
    }

    /**
     * menghitung Consistency Index (CI)
     */
    module.computeCI = function (lambda, size) {
        return (lambda - size) / (size - 1);
    }

    /**
     * menghitung Consistency Ratio (CR)
     */
    module.computeCR = function (ci, ri) {
        return ci / ri;
    }

    /**
     * learning AHP
     */
    module.learning = function () {
        const mtx = module.generateArrayOfWeight(module.dummyFactor, module.dummyMatrix);
        const mtxx = module.computeEigen(mtx.mtx, mtx.sumCol);
        const lambda = module.computeLambda(mtx.sumCol, mtxx.sumRowAvg);
        const ci = module.computeCI(lambda, module.dummyFactor.length);
        const cr = module.computeCR(ci, module.RI[module.dummyFactor.length]);
        return {
            matrix: mtxx,
            lambda: lambda,
            ci: ci,
            cr: cr
        };
    };

    /**
     * data faktor yang di custom oleh user
     */
    module.getFactor = function () {
        return module.dummyFactor;
    }

    return module;
};