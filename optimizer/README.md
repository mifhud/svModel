# FMS Optimizer
Optimizer bekerja dengan trigger dari kafka, setiap ada data masuk maka akan
dilakukan komputasi untuk mencari solusi terbaik, menggunakan metode AHP.
Hasil perhitungan akan diinfokan melalui kafka, beserta detail perhitungan.
Parameter untuk optimizer dapat secara dinamis dikurangi ataupun ditambah, namun
semuanya harus sudah memiliki proses pengambilan data yang sudah didefinisikan.

## Deployment
Run with PM2

## Tipe Optimizer
- Mencari jumlah optimal hauler yang dibutuhkan setiap loader, agar produksinya optimum
- Mencari loader terbaik untuk hauler yang akan beroperasi

## Kebutuhan data

## code:DISTANCE
- Jarak terdekat, jarak hauler ke setiap loader yang ada, dapat dihitung dari titik gps antar unit

## code:PRIORITAS
- Prioritas loader, loader yang diprioritaskan untuk dipilih, ditentukan user

## code:PRODUKTIVITAS
- Produktivitas loader, hasil perhitungan dari produksi

## code:HAULER
- Kebutuhan hauler, jumlah hauler yang dibutuhkan loader

## code:ANTRIAN
- Antrian hauler pada loader bersangkutan, jumlah hauler yang arrive loading dan belum full bucket

# Data Source
- kafka topic gps
- kafka topic cycle
- data master equipment
- data factor ahp
- data matrix ahp
- mongodb collection production_raw (crawler run)
- mongodb collection activity_raw (crawler run)

# Query
```
# backend & front end
optimizer_factor
    code varchar(10) primary
    factor varchar
    ahp_weight double

optimizer_matrix
    code_factor1 varchar(10)
    code_factor2 varchar(10)
    weight double

# edit equipment & new front end
equipment add prioritas

# backend & front end
loader_need
    id varchar primary
    date int
    loader_id varchar
    need_hauler int
```

