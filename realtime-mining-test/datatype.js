module.exports.activity = {
    "id_actual": "",
    "id_operation_support": "",
    "id_operation_loader": "",
    "id_equipment": "",
    "id_equipment_user": "",
    "id_operator": "",
    "cd_activity": "",
    "cd_tum": "",
    "cd_type": "",
    "time": "",
    "latitude": 0,
    "longitude": 0
};

module.exports.prestart = {
    "id_equipment": "",
    "id_user": "",
    "date": "",
    "id_prestart": "",
    "id_actual": "",
    "id_actual_support": "",
    "value": "",
    "qty": ""
};

module.exports.logDetail = {
    "date": 0,
    "id_actual": "",
    "cd_time": "",
    "id_equipment": "",
    "id_operator": "",
    "cd_equipment_type": "",
    "id_location": "",
    "id_loader": "",
    "latitude": 0,
    "longitude": 0,
    "bucket_count": 0
};

module.exports.dataTopicCycle = {
    "date": 0,
    "type": "",
    "sender": "",
    "sender_name": "",
    "destination": "",
    "message": ""
}

module.exports.dataRitase = {
    "id_actual": "",
    "material_type": "",
    "shift": "",
    "date": 0,
    "id_hauler_operator": "",
    "id_material": "",
    "id_hauler": "",
    "id_loader_operator": "",
    "id_loader": "",
    "id_location": "",
    "id_destination": "",
    "date_operated": 0,
    "load_weight": 0,
    "empty_weight": 0,
    "tonnage": 0,
    "tonnage_ujipetik": 0,
    "tonnage_timbangan": 0,
    "tonnage_bucket": 0,
    "distance": 0,
    "bucket_count": 0,
    "is_supervised": 0,
    "json": {},
    "latitude": 0,
    "longitude": 0,
    "is_volume": 0
}