module.exports = function (producer) {
    const module = {};

    const partition = null;
    const headers = [
        { header: "header value" }
    ];

    module.send = function (topic, key, message) {
        console.log('push to topic ', topic);
        const buffer = Buffer.from(JSON.stringify(message));
        producer.produce(topic, partition, buffer, key, Date.now(), "", headers);
    };

    return module;
};