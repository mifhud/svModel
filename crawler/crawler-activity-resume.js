const async = require('async');
const moment = require('moment-timezone');
const config = require('./config');
const util = require('./util');

moment.tz.setDefault('Asia/Jakarta');

module.exports.run = (mysql, mongo, clog, callback) => {

    /**
     * generate shift range
     */
    async function generateShift(mo, shiftCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const shifts = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            for (let mm = 0; mm < 12; mm++) {
                const latestDate = moment().set({ year: ye, month: mm }).endOf('month').date();
                for (let dt = 1; dt <= latestDate; dt++) {
                    shifts.push({
                        year: ye,
                        month: mm,
                        date: dt,
                        shift: util.SHIFT_DAY,
                        start: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 6, minute: 0, second: 0 }).unix(),
                        stop: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 18, minute: 0, second: 0 }).unix(),
                    });
                    shifts.push({
                        year: ye,
                        month: mm,
                        date: dt,
                        shift: util.SHIFT_NIGHT,
                        start: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 18, minute: 0, second: 0 }).unix(),
                        stop: moment().set({ year: ye, month: mm, date: dt }).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix(),
                    });
                }
            }
        }
        shiftCallback(shifts);
    }

    /**
     * get all week
     */
    async function generateWeeks(mo, weekCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const weeks = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            const totalWeek = moment().set({ year: ye }).endOf('year').add(-1, 'week').week();
            for (let wee = 1; wee <= totalWeek; wee++) {
                weeks.push({
                    year: ye,
                    week: wee,
                    start: moment().set({ year: ye }).week(wee).startOf('week').unix(),
                    stop: moment().set({ year: ye }).week(wee).endOf('week').unix(),
                });
            }
        }
        weekCallback(weeks);
    }

    /**
     * get all month
     */
    async function generateMonth(mo, monthCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const months = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            const totalMonth = 12;
            for (let m = 0; m < totalMonth; m++) {
                months.push({
                    year: ye,
                    month: m,
                    start: moment().set({ year: ye, month: m }).startOf('month').unix(),
                    stop: moment().set({ year: ye, month: m }).endOf('month').unix(),
                });
            }
        }
        monthCallback(months);
    }

    /**
     * generate year
     */
    async function generateYears(mo, yearCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const years = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            years.push({
                year: ye,
                start: moment().set({ year: ye }).startOf('year').unix(),
                stop: moment().set({ year: ye }).endOf('year').unix(),
            });
        }
        yearCallback(years);
    }

    /**
     * generate all
     */
    async function generateAll(mo, allCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const mins = min.length > 0 ? min[0].start : moment().unix();
        const maxs = max.length > 0 ? max[0].start : moment().unix();
        const all = [];
        all.push({
            start: mins,
            stop: maxs + 1
        });
        allCallback(all);
    }

    /**
     * get raw data
     */
    async function getRawData(mo, rawDataCallback) {
        const rawData = await util.mongoFind(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {}
        });
        rawDataCallback(rawData);
    }

    /**
     * resume
     */
    async function resumeByTimes(times, rawData, mo, collection, resumeCallback) {
        const resultData = [];
        const time = Object.assign([], times);

        /**
         * resume duration
         */
        for (let dx = 0; dx < rawData.length; dx++) {
            for (let me = 0; me < time.length; me++) {

                if (time[me]['operator_activity'] === undefined) {
                    time[me]['operator_activity'] = [];
                }
                if (time[me]['operator_tum'] === undefined) {
                    time[me]['operator_tum'] = [];
                }
                if (time[me]['equipment_activity'] === undefined) {
                    time[me]['equipment_activity'] = [];
                }
                if (time[me]['equipment_tum'] === undefined) {
                    time[me]['equipment_tum'] = [];
                }
                if (time[me]['activity'] === undefined) {
                    time[me]['activity'] = [];
                }
                if (time[me]['tum'] === undefined) {
                    time[me]['tum'] = [];
                }

                if (rawData[dx].start >= time[me].start && rawData[dx].start < time[me].stop) {
                    const opAct = time[me]['operator_activity'].filter(item => {
                        return item.operator.id === rawData[dx].operator.id && item.activity.cd === rawData[dx].activity.cd;
                    });
                    if (opAct.length > 0) {
                        opAct[0].duration += rawData[dx].duration;
                    } else {
                        time[me]['operator_activity'].push({
                            operator: rawData[dx].operator,
                            activity: rawData[dx].activity,
                            duration: rawData[dx].duration
                        });
                    }

                    const opTum = time[me]['operator_tum'].filter(item => {
                        return item.operator.id === rawData[dx].operator.id && item.tum.cd === rawData[dx].tum.cd;
                    });
                    if (opTum.length > 0) {
                        opTum[0].duration += rawData[dx].duration;
                    } else {
                        time[me]['operator_tum'].push({
                            operator: rawData[dx].operator,
                            tum: rawData[dx].tum,
                            duration: rawData[dx].duration
                        });
                    }

                    const eqAct = time[me]['equipment_activity'].filter(item => {
                        return item.equipment.id === rawData[dx].equipment.id && item.activity.cd === rawData[dx].activity.cd;
                    });
                    if (eqAct.length > 0) {
                        eqAct[0].duration += rawData[dx].duration;
                    } else {
                        time[me]['equipment_activity'].push({
                            equipment: rawData[dx].equipment,
                            activity: rawData[dx].activity,
                            duration: rawData[dx].duration
                        });
                    }

                    const eqTum = time[me]['equipment_tum'].filter(item => {
                        return item.equipment.id === rawData[dx].equipment.id && item.tum.cd === rawData[dx].tum.cd;
                    });
                    if (eqTum.length > 0) {
                        eqTum[0].duration += rawData[dx].duration;
                    } else {
                        time[me]['equipment_tum'].push({
                            equipment: rawData[dx].equipment,
                            tum: rawData[dx].tum,
                            duration: rawData[dx].duration
                        });
                    }

                    const aaAct = time[me]['activity'].filter(item => {
                        return item.activity.cd === rawData[dx].activity.cd;
                    });
                    if (aaAct.length > 0) {
                        aaAct[0].duration += rawData[dx].duration;
                    } else {
                        time[me]['activity'].push({
                            activity: rawData[dx].activity,
                            duration: rawData[dx].duration
                        });
                    }

                    const aaTum = time[me]['tum'].filter(item => {
                        return item.tum.cd === rawData[dx].tum.cd;
                    });
                    if (aaTum.length > 0) {
                        aaTum[0].duration += rawData[dx].duration;
                    } else {
                        time[me]['tum'].push({
                            tum: rawData[dx].tum,
                            duration: rawData[dx].duration
                        });
                    }
                }
            }
        }

        /**
         * generate raw data
         */
        time.map(_raw => {
            const baseObj = {};
            for (let ok of Object.keys(_raw)) {
                if (ok !== 'operator_activity'
                    && ok !== 'operator_tum'
                    && ok !== 'equipment_activity'
                    && ok !== 'equipment_tum'
                    && ok !== 'activity'
                    && ok !== 'tum') {
                    baseObj[ok] = _raw[ok];
                }
            }

            for (let ook of Object.keys(_raw)) {
                if (ook !== 'operator_activity'
                    && ook !== 'operator_tum'
                    && ook !== 'equipment_activity'
                    && ook !== 'equipment_tum'
                    && ook !== 'activity'
                    && ook !== 'tum') {
                    // do nothing
                } else {
                    _raw[ook].map(_rawItem => {
                        _rawItem['type'] = ook;

                        const _rawObj = {
                            ...baseObj, ..._rawItem
                        };

                        resultData.push(_rawObj);
                    });
                }
            }
        });

        /**
         * hapus data lama
         */
        await util.mongoDelete(mo, {
            collection: collection,
            option: {}
        });

        await util.mongoInsert(mo, {
            collection: collection,
            data: resultData
        });

        resumeCallback();
    }

    async.waterfall([

        (cb) => {
            /**
             * raw data
             */
            clog.info('------ get raw data');
            getRawData(mongo, (result) => {
                cb(null, result);
            });
        },

        (rawData, cb) => {
            /**
             * generate time range week
             */
            clog.info('------ generate shift range');
            generateShift(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, shifts, cb) => {
            /**
             * resume
             */
            clog.info('------ resume shift');
            resumeByTimes(shifts, rawData, mongo, config.database.ACTIVITY_SHIFT, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time range week
             */
            clog.info('------ generate week range');
            generateWeeks(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, weeks, cb) => {
            /**
             * resume
             */
            clog.info('------ resume week');
            resumeByTimes(weeks, rawData, mongo, config.database.ACTIVITY_WEEK, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time month
             */
            clog.info('------ generate month range');
            generateMonth(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, months, cb) => {
            /**
             * resume
             */
            clog.info('------ resume month');
            resumeByTimes(months, rawData, mongo, config.database.ACTIVITY_MONTH, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time year
             */
            clog.info('------ generate year range');
            generateYears(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, years, cb) => {
            /**
             * resume
             */
            clog.info('------ resume year');
            resumeByTimes(years, rawData, mongo, config.database.ACTIVITY_YEAR, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time year
             */
            clog.info('------ generate all range');
            generateAll(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, alls, cb) => {
            /**
             * resume
             */
            clog.info('------ resume all');
            resumeByTimes(alls, rawData, mongo, config.database.ACTIVITY_ALL, () => {
                cb(null, rawData);
            })
        },

    ], (res) => {
        callback();
    });
}