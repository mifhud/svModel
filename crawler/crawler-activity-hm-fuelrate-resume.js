const async = require('async');
const moment = require('moment-timezone');
const config = require('./config');
const util = require('./util');

moment.tz.setDefault('Asia/Jakarta');

module.exports.run = (mysql, mongo, clog, callback) => {

    /**
     * generate shift range
     */
    async function generateShift(mo, shiftCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_HM_FUELRATE,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_HM_FUELRATE,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const shifts = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            for (let mm = 0; mm < 12; mm++) {
                const latestDate = moment().set({ year: ye, month: mm }).endOf('month').date();
                for (let dt = 1; dt <= latestDate; dt++) {
                    shifts.push({
                        year: ye,
                        month: mm,
                        date: dt,
                        shift: util.SHIFT_DAY,
                        start: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 6, minute: 0, second: 0 }).unix(),
                        stop: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 18, minute: 0, second: 0 }).unix(),
                    });
                    shifts.push({
                        year: ye,
                        month: mm,
                        date: dt,
                        shift: util.SHIFT_NIGHT,
                        start: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 18, minute: 0, second: 0 }).unix(),
                        stop: moment().set({ year: ye, month: mm, date: dt }).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix(),
                    });
                }
            }
        }
        shiftCallback(shifts);
    }

    /**
     * get all week
     */
    async function generateWeeks(mo, weekCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_HM_FUELRATE,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_HM_FUELRATE,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const weeks = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            const totalWeek = moment().set({ year: ye }).endOf('year').add(-1, 'week').week();
            for (let wee = 1; wee <= totalWeek; wee++) {
                weeks.push({
                    year: ye,
                    week: wee,
                    start: moment().set({ year: ye }).week(wee).startOf('week').unix(),
                    stop: moment().set({ year: ye }).week(wee).endOf('week').unix(),
                });
            }
        }
        weekCallback(weeks);
    }

    /**
     * get all month
     */
    async function generateMonth(mo, monthCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_HM_FUELRATE,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_HM_FUELRATE,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const months = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            const totalMonth = 12;
            for (let m = 0; m < totalMonth; m++) {
                months.push({
                    year: ye,
                    month: m,
                    start: moment().set({ year: ye, month: m }).startOf('month').unix(),
                    stop: moment().set({ year: ye, month: m }).endOf('month').unix(),
                });
            }
        }
        monthCallback(months);
    }

    /**
     * generate year
     */
    async function generateYears(mo, yearCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_HM_FUELRATE,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_HM_FUELRATE,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const years = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            years.push({
                year: ye,
                start: moment().set({ year: ye }).startOf('year').unix(),
                stop: moment().set({ year: ye }).endOf('year').unix(),
            });
        }
        yearCallback(years);
    }

    /**
     * generate all
     */
    async function generateAll(mo, allCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_HM_FUELRATE,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_HM_FUELRATE,
            option: {},
            sort: {
                start: -1
            }
        });
        const mins = min.length > 0 ? min[0].start : moment().unix();
        const maxs = max.length > 0 ? max[0].start : moment().unix();
        const all = [];
        all.push({
            start: mins,
            stop: maxs + 1
        });
        allCallback(all);
    }

    /**
     * get raw data
     */
    async function getRawData(mo, rawDataCallback) {
        const rawData = await util.mongoFind(mo, {
            collection: config.database.ACTIVITY_HM_FUELRATE,
            option: {}
        });

        rawDataCallback(rawData);
    }

    /**
     * resume
     */
    async function resumeByTimes(times, rawData, mo, collection, resumeCallback) {
        const resultData = [];

        /**
         * resume duration
         */
        for (let iTimes = 0; iTimes < times.length; iTimes++) {
            const elTimes = times[iTimes];
            const activityHmFuelRateRaw = await util.mongoAggregate(mo, {
                collection: config.database.ACTIVITY_HM_FUELRATE,
                option: [
                    {
                        $match: {
                            start: {
                                $gte: elTimes.start,
                                $lt: elTimes.stop,
                            },
                        }
                    },
                    {
                        $group: {
                            _id: {
                                equipment: "$equipment",
                                operator: "$operator",
                                tum: "$tum",
                                activity: "$activity"
                            },
                            duration: { $sum: "$duration" },
                            duration_cycle: { $sum: "$duration_cycle" },
                            distance_survei: { $avg: "$distance_survei" },
                            distance_gps: { $avg: "$distance_gps" },
                            tonnage_payload: { $sum: "$tonnage_payload" },
                            tonnage_ujipetik: { $sum: "$tonnage_ujipetik" },
                            tonnage_timbangan: { $sum: "$tonnage_timbangan" },
                            tonnage_bucket: { $sum: "$tonnage_bucket" },
                            duration_hour_meter: { $sum: "$duration_hour_meter" },
                            fuel_rate: { $avg: "$fuel_rate" },
                            fuel_consumption: { $sum: "$fuel_consumption" },
                            fuel_ratio_payload: { $sum: "$fuel_ratio_payload" },
                            fuel_ratio_ujipetik: { $sum: "$fuel_ratio_ujipetik" },
                            fuel_ratio_timbangan: { $sum: "$fuel_ratio_timbangan" },
                            fuel_ratio_bucket: { $sum: "$fuel_ratio_bucket" },
                        }
                    }
                ],
            });

            if (activityHmFuelRateRaw.length > 0) {
                for (let iActivityHmFuelRate = 0; iActivityHmFuelRate < activityHmFuelRateRaw.length; iActivityHmFuelRate++) {
                    const elActivityHmFuelRate = activityHmFuelRateRaw[iActivityHmFuelRate];
                    const singleActivityHmFuelRate = {
                        equipment: elActivityHmFuelRate._id.equipment,
                        operator: elActivityHmFuelRate._id.operator,
                        tum: elActivityHmFuelRate._id.tum,
                        activity: elActivityHmFuelRate._id.activity,

                        duration: elActivityHmFuelRate.duration,
                        duration_cycle: elActivityHmFuelRate.duration_cycle,

                        distance_survei: elActivityHmFuelRate.distance_survei,
                        distance_gps: elActivityHmFuelRate.distance_gps,

                        tonnage_payload: elActivityHmFuelRate.tonnage_payload,
                        tonnage_ujipetik: elActivityHmFuelRate.tonnage_ujipetik,
                        tonnage_timbangan: elActivityHmFuelRate.tonnage_timbangan,
                        tonnage_bucket: elActivityHmFuelRate.tonnage_bucket,

                        duration_hour_meter: elActivityHmFuelRate.duration_hour_meter,

                        fuel_rate: elActivityHmFuelRate.fuel_rate,

                        // HM * fuel rate
                        fuel_consumption: elActivityHmFuelRate.duration_hour_meter * elActivityHmFuelRate.fuel_rate
                    }

                    /**
                     * fuel ratio
                     */
                    if (singleActivityHmFuelRate.tonnage_payload > 0) {
                        singleActivityHmFuelRate['fuel_ratio_payload'] = singleActivityHmFuelRate.fuel_consumption / singleActivityHmFuelRate.tonnage_payload;
                    } else {
                        singleActivityHmFuelRate['fuel_ratio_payload'] = 0;
                    }
                    if (singleActivityHmFuelRate.tonnage_ujipetik > 0) {
                        singleActivityHmFuelRate['fuel_ratio_ujipetik'] = singleActivityHmFuelRate.fuel_consumption / singleActivityHmFuelRate.tonnage_ujipetik;
                    } else {
                        singleActivityHmFuelRate['fuel_ratio_ujipetik'] = 0;
                    }
                    if (singleActivityHmFuelRate.tonnage_timbangan > 0) {
                        singleActivityHmFuelRate['fuel_ratio_timbangan'] = singleActivityHmFuelRate.fuel_consumption / singleActivityHmFuelRate.tonnage_timbangan;
                    } else {
                        singleActivityHmFuelRate['fuel_ratio_timbangan'] = 0;
                    }
                    if (singleActivityHmFuelRate.tonnage_bucket > 0) {
                        singleActivityHmFuelRate['fuel_ratio_bucket'] = singleActivityHmFuelRate.fuel_consumption / singleActivityHmFuelRate.tonnage_bucket;
                    } else {
                        singleActivityHmFuelRate['fuel_ratio_bucket'] = 0;
                    }

                    resultData.push(singleActivityHmFuelRate)
                }
            }
        }

        /**
         * hapus data lama
         */
        await util.mongoDelete(mo, {
            collection: collection,
            option: {}
        });

        if (resultData.length > 0) {
            await util.mongoInsert(mo, {
                collection: collection,
                data: resultData
            });
        }

        resumeCallback();
    }

    async.waterfall([

        (cb) => {
            /**
             * raw data
             */
            clog.info('------ get raw data');
            getRawData(mongo, (result) => {
                cb(null, result);
            });
        },

        (rawData, cb) => {
            /**
             * generate time range week
             */
            clog.info('------ generate shift range');
            generateShift(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, shifts, cb) => {
            /**
             * resume
             */
            clog.info('------ resume shift');
            resumeByTimes(shifts, rawData, mongo, config.database.ACTIVITY_HM_FUELRATE_SHIFT, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time range week
             */
            clog.info('------ generate week range');
            generateWeeks(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, weeks, cb) => {
            /**
             * resume
             */
            clog.info('------ resume week');
            resumeByTimes(weeks, rawData, mongo, config.database.ACTIVITY_HM_FUELRATE_WEEK, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time month
             */
            clog.info('------ generate month range');
            generateMonth(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, months, cb) => {
            /**
             * resume
             */
            clog.info('------ resume month');
            resumeByTimes(months, rawData, mongo, config.database.ACTIVITY_HM_FUELRATE_MONTH, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time year
             */
            clog.info('------ generate year range');
            generateYears(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, years, cb) => {
            /**
             * resume
             */
            clog.info('------ resume year');
            resumeByTimes(years, rawData, mongo, config.database.ACTIVITY_HM_FUELRATE_YEAR, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time year
             */
            clog.info('------ generate all range');
            generateAll(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, alls, cb) => {
            /**
             * resume
             */
            clog.info('------ resume all');
            resumeByTimes(alls, rawData, mongo, config.database.ACTIVITY_HM_FUELRATE_ALL, () => {
                cb(null, rawData);
            })
        },

    ], (res) => {
        callback();
    });
}