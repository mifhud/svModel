# Crawler

FMS Data Warehousing crawler

## Deploy
Schedule as cronjob 
- Default schedule less than 60 second
- Resume schedule about 30 minutes

## Buld crawler

`npm run build`

## Copy config to dist

`cp config.json dist/`

## Run crawler today

`node dist/fms.crawler.js`

## Run crawler start from date

`node dist/fms.crawler.js 2019-01-01`

## Generate resume report

`node dist/fms.crawler.js resume`

## Clear Database
db.activity_all.drop()
db.activity_month.drop()
db.activity_raw.drop()
db.activity_shift.drop()
db.activity_week.drop()
db.activity_year.drop()
db.cycle_activity_all.drop()
db.cycle_activity_month.drop()
db.cycle_activity_raw.drop()
db.cycle_activity_shift.drop()
db.cycle_activity_week.drop()
db.cycle_activity_year.drop()
db.ecus.drop()
db.locations.drop()
db.maintenance_month.drop()
db.maintenance_raw.drop()
db.maintenance_shift.drop()
db.maintenance_week.drop()
db.maintenance_year.drop()
db.operators.drop()
db.production_all.drop()
db.production_month.drop()
db.production_raw.drop()
db.production_shift.drop()
db.production_week.drop()
db.production_year.drop()
db.units.drop()
