const async = require('async');
const moment = require('moment-timezone');
const config = require('./config');
const util = require('./util');
const request = require('request');

moment.tz.setDefault('Asia/Jakarta');

module.exports.run = (mysql, mongo, clog, callback) => {

    const paramStart = process.argv[2];
    const times = [];
    let activityRaw = [];

    /**
     * prepare time set
     */
    if (paramStart) {
        const naw = moment().endOf('day').unix();
        let start = moment(paramStart, 'YYYY-MM-DD').startOf('day').unix();
        while (start < naw) {
            times.push({
                start: moment.unix(start).set({ hour: 6, minute: 0, second: 0 }).unix(),
                stop: moment.unix(start).set({ hour: 18, minute: 0, second: 0 }).unix(),
                shift: util.SHIFT_DAY
            });
            times.push({
                start: moment.unix(start).set({ hour: 18, minute: 0, second: 0 }).unix(),
                stop: moment.unix(start).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix(),
                shift: util.SHIFT_NIGHT
            });
            start = moment.unix(start).add(1, 'days').unix();
        }
    } else {
        times.push({
            start: util.util.translateTimestamp('startOfShift'),
            stop: util.util.translateTimestamp('endOfShift'),
            shift: util.util.getCurrentShift()
        });
    }

    /**
     * get raw data activity
     */
    async function getActivityRaw(time, mo, rawDataCallback) {
        const rawData = await util.mongoFind(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {
                start: {
                    $gte: time.start,
                    $lt: time.stop
                },
            },
            sort: {
                start: 1
            }
        });

        rawDataCallback(rawData);
    }

    /**
     * map raw data
     */
    async function mapRaw(mo, activityRaw, collection, rawDataCallback) {

        for (let iActivityRaw = 0; iActivityRaw < activityRaw.length; iActivityRaw++) {

            /**
             * masih ada kemungkinan stop=0
             * maka blok ini diperlukan
             */
            if (activityRaw[iActivityRaw].stop === 0) {
                const next = await util.mongoFindOne(mo, {
                    collection: config.database.ACTIVITY_RAW,
                    option: {
                        start: {
                            $gt: activityRaw[iActivityRaw].start
                        },
                        'equipment.id': activityRaw[iActivityRaw].equipment.id
                    },
                    sort: {
                        start: 1
                    }
                });
                if (next.length > 0) {
                    const stop = next[0].start;
                    const duration = stop - activityRaw[iActivityRaw].start;
                    activityRaw[iActivityRaw].stop = stop;
                    activityRaw[iActivityRaw].duration = duration;
                }
            }

            /**
             * production tentunya berada pada TUM WORKING
             */
            if (activityRaw[iActivityRaw].stop !== 0 && activityRaw[iActivityRaw].tum.cd === util.TUM_WORKING_TIME) {
                /**
                 * date activity berada di start
                 * date production berada di stop
                 */
                const productionRaw = await util.mongoAggregate(mo, {
                    collection: config.database.PRODUCTION_RAW,
                    option: [
                        {
                            $match: {
                                stop: {
                                    $gte: activityRaw[iActivityRaw].start,
                                    $lt: activityRaw[iActivityRaw].stop,
                                },
                                'hauler.id': activityRaw[iActivityRaw].equipment.id,
                                actual: activityRaw[iActivityRaw].actual
                            }
                        },
                        {
                            $group: {
                                _id: null,
                                duration: { $sum: "$duration" },
                                distance_survei: { $avg: "$distance_survei" },
                                distance_gps: { $avg: "$distance_gps" },
                                tonnage_payload: { $sum: "$tonnage_payload" },
                                tonnage_ujipetik: { $sum: "$tonnage_ujipetik" },
                                tonnage_timbangan: { $sum: "$tonnage_timbangan" },
                                tonnage_bucket: { $sum: "$tonnage_bucket" },
                            }
                        }
                    ],
                });

                /**
                 * merge data
                 */
                if (productionRaw.length > 0) {
                    activityRaw[iActivityRaw]['duration_cycle'] = productionRaw[0]["duration"];
                    activityRaw[iActivityRaw]['distance_survei'] = productionRaw[0]["distance_survei"];
                    activityRaw[iActivityRaw]['distance_gps'] = productionRaw[0]["distance_gps"];
                    activityRaw[iActivityRaw]['tonnage_payload'] = productionRaw[0]["tonnage_payload"];
                    activityRaw[iActivityRaw]['tonnage_ujipetik'] = productionRaw[0]["tonnage_ujipetik"];
                    activityRaw[iActivityRaw]['tonnage_timbangan'] = productionRaw[0]["tonnage_timbangan"];
                    activityRaw[iActivityRaw]['tonnage_bucket'] = productionRaw[0]["tonnage_bucket"];
                }

                /**
                 * data hour meter dan fuel rate
                 */
                const gpsRaw = await util.mongoFind(mo, {
                    collection: config.database.GPS,
                    projection: ['date', 'ecu'],
                    option: {
                        date: {
                            $gte: activityRaw[iActivityRaw].start,
                            $lt: activityRaw[iActivityRaw].stop,
                        },
                        unit_id: activityRaw[iActivityRaw].equipment.id
                    },
                    sort: {
                        date: 1
                    }
                });

                /**
                 * resume data
                 */
                if (gpsRaw.length > 0) {

                    const duration_hour_meter = Number(gpsRaw[gpsRaw.length - 1].ecu.hour_meter.value) - Number(gpsRaw[0].ecu.hour_meter.value);
                    const sumFuelRate = gpsRaw.map(m => m.ecu.fuel_rate.value).reduce((a, c) => a + c);
                    const avgFuelRate = sumFuelRate / gpsRaw.length;

                    activityRaw[iActivityRaw]['duration_hour_meter'] = duration_hour_meter;
                    activityRaw[iActivityRaw]['fuel_rate'] = avgFuelRate;
                    activityRaw[iActivityRaw]['fuel_consumption'] = duration_hour_meter * avgFuelRate;

                    if (activityRaw[iActivityRaw]['tonnage_payload'] > 0) {
                        activityRaw[iActivityRaw]['fuel_ratio_payload'] = activityRaw[iActivityRaw]['fuel_consumption'] / activityRaw[iActivityRaw]['tonnage_payload'];
                    } else {
                        activityRaw[iActivityRaw]['fuel_ratio_payload'] = 0;
                    }
                    if (activityRaw[iActivityRaw]['tonnage_ujipetik'] > 0) {
                        activityRaw[iActivityRaw]['fuel_ratio_ujipetik'] = activityRaw[iActivityRaw]['fuel_consumption'] / activityRaw[iActivityRaw]['tonnage_ujipetik'];
                    } else {
                        activityRaw[iActivityRaw]['fuel_ratio_ujipetik'] = 0;
                    }
                    if (activityRaw[iActivityRaw]['tonnage_timbangan'] > 0) {
                        activityRaw[iActivityRaw]['fuel_ratio_timbangan'] = activityRaw[iActivityRaw]['fuel_consumption'] / activityRaw[iActivityRaw]['tonnage_timbangan'];
                    } else {
                        activityRaw[iActivityRaw]['fuel_ratio_timbangan'] = 0;
                    }
                    if (activityRaw[iActivityRaw]['tonnage_bucket'] > 0) {
                        activityRaw[iActivityRaw]['fuel_ratio_bucket'] = activityRaw[iActivityRaw]['fuel_consumption'] / activityRaw[iActivityRaw]['tonnage_bucket'];
                    } else {
                        activityRaw[iActivityRaw]['fuel_ratio_bucket'] = 0;
                    }

                } else {

                    activityRaw[iActivityRaw]['duration_hour_meter'] = 0;
                    activityRaw[iActivityRaw]['fuel_rate'] = 0;
                    activityRaw[iActivityRaw]['fuel_consumption'] = 0;
                    activityRaw[iActivityRaw]['fuel_ratio_payload'] = 0;
                    activityRaw[iActivityRaw]['fuel_ratio_ujipetik'] = 0;
                    activityRaw[iActivityRaw]['fuel_ratio_timbangan'] = 0;
                    activityRaw[iActivityRaw]['fuel_ratio_bucket'] = 0;

                }

                /**
                 * console.log
                 */
                if (productionRaw.length > 0) {
                    // console.log(activityRaw[iActivityRaw]);
                }
            }

        }

        /**
         * insert to collection
         */
        if (activityRaw.length > 0) {
            await util.mongoInsert(mo, {
                collection: collection,
                data: activityRaw
            });
        }

        rawDataCallback(activityRaw);
    }

    async.waterfall([

        (cb) => {
            /**
             * raw data
             */
            clog.info('------ get raw data');
            async.forEachOf(times, (time, key, _cb) => {
                getActivityRaw(time, mongo, (result) => {
                    activityRaw = [...activityRaw, ...result];
                    _cb();
                });
            }, (err) => {
                cb();
            });
        },

        (cb) => {
            /**
             * delete old data
             */
            clog.info('------ delete old data');
            async.forEachOf(times, (time, key, _cb) => {
                util.mongoDelete(mongo, {
                    collection: config.database.ACTIVITY_HM_FUELRATE,
                    option: {
                        start: {
                            $gte: time.start,
                            $lt: time.stop
                        },
                    }
                }).then(res => {
                    _cb();
                });
            }, (err) => {
                cb();
            });
        },

        (cb) => {
            /**
             * raw data
             */
            clog.info('------ mapping raw data');
            mapRaw(mongo, activityRaw, config.database.ACTIVITY_HM_FUELRATE, (result) => {
                cb();
            });
        },

    ], (res) => {
        callback();
    });
}