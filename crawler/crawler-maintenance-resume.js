const async = require('async');
const moment = require('moment-timezone');
const config = require('./config');
const util = require('./util');

moment.tz.setDefault('Asia/Jakarta');

module.exports.run = (mysql, mongo, clog, callback) => {

    /**
     * generate shift range
     */
    async function generateShift(mo, shiftCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const shifts = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            for (let mm = 0; mm < 12; mm++) {
                const latestDate = moment().set({ year: ye, month: mm }).endOf('month').date();
                for (let dt = 1; dt <= latestDate; dt++) {
                    shifts.push({
                        year: ye,
                        month: mm,
                        date: dt,
                        shift: util.SHIFT_DAY,
                        start: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 6, minute: 0, second: 0 }).unix(),
                        stop: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 18, minute: 0, second: 0 }).unix(),
                    });
                    shifts.push({
                        year: ye,
                        month: mm,
                        date: dt,
                        shift: util.SHIFT_NIGHT,
                        start: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 18, minute: 0, second: 0 }).unix(),
                        stop: moment().set({ year: ye, month: mm, date: dt }).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix(),
                    });
                }
            }
        }
        shiftCallback(shifts);
    }

    /**
     * get all week
     */
    async function generateWeeks(mo, weekCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.MAINTENANCE_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.MAINTENANCE_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const weeks = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            const totalWeek = moment().set({ year: ye }).endOf('year').add(-1, 'week').week();
            for (let wee = 1; wee <= totalWeek; wee++) {
                weeks.push({
                    year: ye,
                    week: wee,
                    start: moment().set({ year: ye }).week(wee).startOf('week').unix(),
                    stop: moment().set({ year: ye }).week(wee).endOf('week').unix(),
                });
            }
        }
        weekCallback(weeks);
    }

    /**
     * get all month
     */
    async function generateMonth(mo, monthCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.MAINTENANCE_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.MAINTENANCE_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const months = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            const totalMonth = 12;
            for (let m = 0; m < totalMonth; m++) {
                months.push({
                    year: ye,
                    month: m,
                    start: moment().set({ year: ye, month: m }).startOf('month').unix(),
                    stop: moment().set({ year: ye, month: m }).endOf('month').unix(),
                });
            }
        }
        monthCallback(months);
    }

    /**
     * generate year
     */
    async function generateYears(mo, yearCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.MAINTENANCE_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.MAINTENANCE_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const years = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            years.push({
                year: ye,
                start: moment().set({ year: ye }).startOf('year').unix(),
                stop: moment().set({ year: ye }).endOf('year').unix(),
            });
        }
        yearCallback(years);
    }

    /**
     * get raw data
     */
    async function getRawData(mo, rawDataCallback) {
        const rawData = await util.mongoFind(mo, {
            collection: config.database.MAINTENANCE_RAW,
            option: {}
        });
        rawDataCallback(rawData);
    }

    /**
     * resume
     */
    async function resumeByTimes(times, rawData, mo, collection, resumeCallback) {
        const resultData = [];
        const time = Object.assign([], times);

        /**
         * resume duration
         */
        for (let dx = 0; dx < rawData.length; dx++) {
            for (let me = 0; me < time.length; me++) {

                if (time[me]['equipment_problem'] === undefined) {
                    time[me]['equipment_problem'] = [];
                }
                if (time[me]['problem'] === undefined) {
                    time[me]['problem'] = [];
                }

                if (rawData[dx].start >= time[me].start && rawData[dx].start < time[me].stop) {
                    const opAct = time[me]['equipment_problem'].filter(item => {
                        return item.equipment.id === rawData[dx].equipment.id && item.problem.cd === rawData[dx].problem.cd;
                    });
                    if (opAct.length > 0) {
                        opAct[0].duration += rawData[dx].duration;
                    } else {
                        time[me]['equipment_problem'].push({
                            equipment: rawData[dx].equipment,
                            problem: rawData[dx].problem,
                            duration: rawData[dx].duration
                        });
                    }

                    const aaProblem = time[me]['problem'].filter(item => {
                        return item.problem.cd === rawData[dx].problem.cd;
                    });
                    if (aaProblem.length > 0) {
                        aaProblem[0].duration += rawData[dx].duration;
                    } else {
                        time[me]['problem'].push({
                            problem: rawData[dx].problem,
                            duration: rawData[dx].duration
                        });
                    }
                }
            }
        }

        /**
         * generate raw data
         */
        time.map(_raw => {
            const baseObj = {};
            for (let ok of Object.keys(_raw)) {
                if (ok !== 'equipment_problem' && ok !== 'problem') {
                    baseObj[ok] = _raw[ok];
                }
            }

            _raw.equipment_problem.map(_rawOpAct => {
                const _rawObj = {
                    ...baseObj, ...{
                        type: 'equipment_problem',
                        equipment: _rawOpAct.equipment,
                        problem: _rawOpAct.problem,
                        duration: _rawOpAct.duration
                    }
                };

                resultData.push(_rawObj);
            });

            _raw.problem.map(_rawTum => {
                const _rawObj = {
                    ...baseObj, ...{
                        type: 'problem',
                        problem: _rawTum.problem,
                        duration: _rawTum.duration
                    }
                };

                resultData.push(_rawObj);
            });
        });

        /**
         * hapus data lama
         */
        await util.mongoDelete(mo, {
            collection: collection,
            option: {}
        });

        await util.mongoInsert(mo, {
            collection: collection,
            data: resultData
        });

        resumeCallback();
    }

    async.waterfall([

        (cb) => {
            /**
             * raw data
             */
            clog.info('------ get raw data');
            getRawData(mongo, (result) => {
                cb(null, result);
            });
        },

        (rawData, cb) => {
            /**
             * generate time range week
             */
            clog.info('------ generate shift range');
            generateShift(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, shifts, cb) => {
            /**
             * resume
             */
            clog.info('------ resume shift');
            resumeByTimes(shifts, rawData, mongo, config.database.MAINTENANCE_SHIFT, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time range week
             */
            clog.info('------ generate week range');
            generateWeeks(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, weeks, cb) => {
            /**
             * resume
             */
            clog.info('------ resume week');
            resumeByTimes(weeks, rawData, mongo, config.database.MAINTENANCE_WEEK, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time month
             */
            clog.info('------ generate month range');
            generateMonth(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, months, cb) => {
            /**
             * resume
             */
            clog.info('------ resume month');
            resumeByTimes(months, rawData, mongo, config.database.MAINTENANCE_MONTH, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time year
             */
            clog.info('------ generate year range');
            generateYears(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, years, cb) => {
            /**
             * resume
             */
            clog.info('------ resume year');
            resumeByTimes(years, rawData, mongo, config.database.MAINTENANCE_YEAR, () => {
                cb(null, rawData);
            })
        },

    ], (res) => {
        callback();
    });
}