const async = require('async');
const moment = require('moment-timezone');
const config = require('./config');
const util = require('./util');

moment.tz.setDefault('Asia/Jakarta');

module.exports.run = (mysql, mongo, clog, callback) => {

    let paramStart = process.argv[2];
    let times = [];

    /**
     * prepare time set
     */
    if (paramStart) {
        const naw = moment().endOf('day').unix();
        let start = moment(paramStart, 'YYYY-MM-DD').startOf('day').unix();
        while (start < naw) {
            times.push({
                start: moment.unix(start).set({ hour: 6, minute: 0, second: 0 }).unix(),
                stop: moment.unix(start).set({ hour: 18, minute: 0, second: 0 }).unix(),
                shift: util.SHIFT_DAY
            });
            times.push({
                start: moment.unix(start).set({ hour: 18, minute: 0, second: 0 }).unix(),
                stop: moment.unix(start).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix(),
                shift: util.SHIFT_NIGHT
            });
            start = moment.unix(start).add(1, 'days').unix();
        }
    } else {
        times.push({
            start: util.util.translateTimestamp('startOfShift'),
            stop: util.util.translateTimestamp('endOfShift'),
            shift: util.util.getCurrentShift()
        });
    }

    /**
     * shift crawler method
     * sudut pandang equipment
     */
    async function shiftCrawler(time, my, mo, shiftCrawlerCallback) {
        const rawData = await util.mysqlQuery(my, "SELECT  \
                m.id, m.date, m.id_actual, \
                hauler.id hauler_id, hauler.name hauler_name, hauler.cd_equipment hauler_type, \
                loader.id loader_id, loader.name loader_name, loader.cd_equipment loader_type, \
                us.id user_id, us.name user_name, us.cd_role user_role, us.staff_id user_nik, us.cd_department user_dept, \
                type.cd type_cd, type.name type_name,  \
                cycle.cd cycle_cd, cycle.name cycle_name  \
            FROM operation_log_detail m \
            LEFT JOIN equipment hauler ON hauler.id=m.id_equipment \
            LEFT JOIN equipment loader ON loader.id=m.id_loader \
            LEFT JOIN user us ON us.id=m.id_operator \
            LEFT JOIN constant type ON type.cd=m.cd_equipment_type \
            LEFT JOIN constant cycle ON cycle.cd=m.cd_time \
            WHERE m.date>=" + time.start + " AND m.date<" + time.stop + " \
            ORDER BY m.date ASC");
        const mapped = {};
        for (let iter = 0; iter < rawData.length; iter++) {
            if (mapped[rawData[iter].hauler_id] === undefined) {
                mapped[rawData[iter].hauler_id] = [];
            }

            const raw = {
                start: rawData[iter].date,
                stop: 0,
                actual: rawData[iter].id_actual,
                operator: {
                    id: rawData[iter].user_id,
                    nik: rawData[iter].user_nik,
                    name: rawData[iter].user_name,
                    role: rawData[iter].user_role,
                    department: rawData[iter].user_dept
                },
                equipment: {
                    id: rawData[iter].hauler_id,
                    name: rawData[iter].hauler_name
                },
                shift: {
                    cd: time.shift,
                    name: time.shift === util.SHIFT_DAY ? 'Day' : 'Night'
                },
                duration: 0
            };
            if (rawData[iter].cycle_cd === util.TIME_CYCLE_ARRIVE_LOADING) {
                raw['state'] = {
                    cd: util.CYCLE_QUEUING_LOADING,
                    name: 'Queuing at Loading'
                };

                const findFullBucketOtherTruck = rawData.filter(item => item.cycle_cd === util.TIME_CYCLE_FULL_BUCKET
                    /**
                     * truk manapun yang penting loader sama
                     */
                    && item.loader_id === rawData[iter].loader_id
                    && item.date > rawData[iter].date
                    && item.used === undefined);
                const sortAsc = findFullBucketOtherTruck.sort((ia, ib) => ia.date - ib.date);

                if (sortAsc.length > 0) {
                    const forSpotting = Object.assign({}, raw);

                    raw.stop = sortAsc[0].date;
                    raw.duration = raw.stop - raw.start;
                    sortAsc[0]['used'] = true;

                    mapped[rawData[iter].hauler_id].push(raw);

                    forSpotting.start = sortAsc[0].date;
                    forSpotting['state'] = {
                        cd: util.CYCLE_SPOTTING_LOADING,
                        name: 'Spotting at Loading'
                    };

                    const findFirstBucket = rawData.filter(item => item.cycle_cd === util.TIME_CYCLE_FIRST_BUCKET
                        /**
                         * pastikan loader sama
                         */
                        && item.loader_id === rawData[iter].loader_id
                        && item.hauler_id === rawData[iter].hauler_id
                        && item.date > forSpotting.start);
                    const sortAsc = findFirstBucket.sort((ia, ib) => ia.date - ib.date);

                    if (sortAsc.length > 0) {
                        forSpotting.stop = sortAsc[0].date;
                        forSpotting.duration = forSpotting.stop - forSpotting.start;
                    }

                    mapped[rawData[iter].hauler_id].push(forSpotting);
                }

            } else if (rawData[iter].cycle_cd === util.TIME_CYCLE_FIRST_BUCKET) {
                if (mapped[rawData[iter].loader_id] === undefined) {
                    mapped[rawData[iter].loader_id] = [];
                }

                raw.equipment.id = rawData[iter].loader_id;
                raw.equipment.name = rawData[iter].loader_name;
                raw['state'] = {
                    cd: util.CYCLE_LOADING,
                    name: 'Loading'
                };
                raw['hauler'] = {
                    id: rawData[iter].hauler_id,
                    name: rawData[iter].hauler_name
                };

                const findFullBucket = rawData.filter(item => item.cycle_cd === util.TIME_CYCLE_FULL_BUCKET
                    && item.hauler_id === rawData[iter].hauler_id
                    && item.loader_id === rawData[iter].loader_id
                    && item.date > rawData[iter].date);
                const sortAsc = findFullBucket.sort((ia, ib) => ia.date - ib.date);

                if (sortAsc.length > 0) {
                    raw.stop = sortAsc[0].date;
                    raw.duration = raw.stop - raw.start;
                }

                mapped[rawData[iter].loader_id].push(raw);

            } else if (rawData[iter].cycle_cd === util.TIME_CYCLE_FULL_BUCKET) {
                raw['state'] = {
                    cd: util.CYCLE_FULL_TRAVEL,
                    name: 'Full Travel'
                };

                const findArriveDump = rawData.filter(item => item.cycle_cd === util.TIME_CYCLE_ARRIVE_DUMPING
                    && item.hauler_id === rawData[iter].hauler_id
                    && item.date > rawData[iter].date);
                const sortAsc = findArriveDump.sort((ia, ib) => ia.date - ib.date);

                if (sortAsc.length > 0) {
                    raw.stop = sortAsc[0].date;
                    raw.duration = raw.stop - raw.start;
                }

                mapped[rawData[iter].hauler_id].push(raw);

                /**
                 * compute hanging
                 */
                const forHanging = Object.assign({}, raw);

                if (mapped[rawData[iter].loader_id] === undefined) {
                    mapped[rawData[iter].loader_id] = [];
                }

                forHanging.equipment.id = rawData[iter].loader_id;
                forHanging.equipment.name = rawData[iter].loader_name;
                forHanging['state'] = {
                    cd: util.CYCLE_HANGING,
                    name: 'Hanging'
                };

                const findFirstBucket = rawData.filter(item => item.cycle_cd === util.TIME_CYCLE_FIRST_BUCKET
                    && item.loader_id === rawData[iter].loader_id
                    && item.date > rawData[iter].date);
                const sortAsc1 = findFirstBucket.sort((ia, ib) => ia.date - ib.date);

                if (sortAsc1.length > 0) {
                    forHanging.stop = sortAsc1[0].date;
                    forHanging.duration = forHanging.stop - forHanging.start;
                }

                mapped[rawData[iter].loader_id].push(forHanging);

            } else if (rawData[iter].cycle_cd === util.TIME_CYCLE_ARRIVE_DUMPING) {
                raw['state'] = {
                    cd: util.CYCLE_SPOTTING_DUMPING,
                    name: 'Spotting at Dumping'
                };

                const findStartDump = rawData.filter(item => item.cycle_cd === util.TIME_CYCLE_START_DUMP
                    && item.hauler_id === rawData[iter].hauler_id
                    && item.date > rawData[iter].date);
                const sortAsc = findStartDump.sort((ia, ib) => ia.date - ib.date);

                if (sortAsc.length > 0) {
                    raw.stop = sortAsc[0].date;
                    raw.duration = raw.stop - raw.start;
                }

                mapped[rawData[iter].hauler_id].push(raw);

            } else if (rawData[iter].cycle_cd === util.TIME_CYCLE_START_DUMP) {
                raw['state'] = {
                    cd: util.CYCLE_DUMPING,
                    name: 'Dumping'
                };

                const findFinishDump = rawData.filter(item => item.cycle_cd === util.TIME_CYCLE_FINISH_DUMP
                    && item.hauler_id === rawData[iter].hauler_id
                    && item.date > rawData[iter].date);
                const sortAsc = findFinishDump.sort((ia, ib) => ia.date - ib.date);

                if (sortAsc.length > 0) {
                    raw.stop = sortAsc[0].date;
                    raw.duration = raw.stop - raw.start;
                }

                mapped[rawData[iter].hauler_id].push(raw);

            } else if (rawData[iter].cycle_cd === util.TIME_CYCLE_FINISH_DUMP) {
                raw['state'] = {
                    cd: util.CYCLE_EMPTY_TRAVEL,
                    name: 'Empty Travel'
                };

                const findArriveLoading = rawData.filter(item => item.cycle_cd === util.TIME_CYCLE_ARRIVE_LOADING
                    && item.hauler_id === rawData[iter].hauler_id
                    && item.date > rawData[iter].date);
                const sortAsc = findArriveLoading.sort((ia, ib) => ia.date - ib.date);

                if (sortAsc.length > 0) {
                    raw.stop = sortAsc[0].date;
                    raw.duration = raw.stop - raw.start;
                }

                mapped[rawData[iter].hauler_id].push(raw);
            }

        }

        let remap = [];
        for (let items of Object.values(mapped)) {
            remap = remap.concat(items);
        }

        /**
         * hapus data lama
         */
        await util.mongoDelete(mo, {
            collection: config.database.CYCLE_ACTIVITY_RAW,
            option: {
                start: {
                    $gte: time.start,
                    $lt: time.stop
                }
            }
        });


        if (remap.length > 0) {
            await util.mongoInsert(mo, {
                collection: config.database.CYCLE_ACTIVITY_RAW,
                data: remap
            });
        }

        shiftCrawlerCallback();
        /**
         * ingat beberapa data khususnya data di akhir shift akan memiliki
         * duration == 0
         * maka perlu dipasangkan dengan stop nya
         */
    }

    /**
     * refinement
     */
    async function refinementZero(mo, refinementCallback) {
        const zeros = await util.mongoFind(mo, {
            collection: config.database.CYCLE_ACTIVITY_RAW,
            option: {
                stop: 0,
                duration: 0
            }
        });

        for (let dx = 0; dx < zeros.length; dx++) {

            if (zeros[dx].state.cd === util.CYCLE_QUEUING_LOADING) {

                let endShift;
                if (zeros[dx].shift.cd === util.SHIFT_DAY) {
                    endShift = moment.unix(zeros[dx].start).set({ hour: 18, minute: 0, second: 0 }).unix();
                } else {
                    endShift = moment.unix(zeros[dx].start).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix();
                }
                const next = await util.mongoFindOne(mo, {
                    collection: config.database.CYCLE_ACTIVITY_RAW,
                    option: {
                        start: {
                            $gt: zeros[dx].start,
                            $lt: endShift
                        },
                        'shift.cd': zeros[dx].shift.cd,
                        'state.cd': util.CYCLE_SPOTTING_LOADING,
                        'equipment.id': zeros[dx].equipment.id
                    },
                    sort: {
                        start: 1
                    }
                });
                if (next.length > 0) {
                    const stop = next[0].start;
                    const duration = stop - zeros[dx].start;
                    await util.mongoUpdate(mo, {
                        collection: config.database.CYCLE_ACTIVITY_RAW,
                        option: {
                            _id: zeros[dx]._id
                        },
                        data: {
                            $set: {
                                stop: stop,
                                duration: duration
                            }
                        }
                    });
                }

            } else if (zeros[dx].state.cd === util.CYCLE_SPOTTING_LOADING) {

                let endShift;
                if (zeros[dx].shift.cd === util.SHIFT_DAY) {
                    endShift = moment.unix(zeros[dx].start).set({ hour: 18, minute: 0, second: 0 }).unix();
                } else {
                    endShift = moment.unix(zeros[dx].start).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix();
                }
                const next = await util.mongoFindOne(mo, {
                    collection: config.database.CYCLE_ACTIVITY_RAW,
                    option: {
                        start: {
                            $gt: zeros[dx].start,
                            $lt: endShift
                        },
                        'shift.cd': zeros[dx].shift.cd,
                        'state.cd': util.CYCLE_LOADING,
                        'hauler.id': zeros[dx].equipment.id
                    },
                    sort: {
                        start: 1
                    }
                });
                if (next.length > 0) {
                    const stop = next[0].start;
                    const duration = stop - zeros[dx].start;
                    await util.mongoUpdate(mo, {
                        collection: config.database.CYCLE_ACTIVITY_RAW,
                        option: {
                            _id: zeros[dx]._id
                        },
                        data: {
                            $set: {
                                stop: stop,
                                duration: duration
                            }
                        }
                    });
                }

            } else if (zeros[dx].state.cd === util.CYCLE_LOADING) {

                let endShift;
                if (zeros[dx].shift.cd === util.SHIFT_DAY) {
                    endShift = moment.unix(zeros[dx].start).set({ hour: 18, minute: 0, second: 0 }).unix();
                } else {
                    endShift = moment.unix(zeros[dx].start).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix();
                }
                const next = await util.mongoFindOne(mo, {
                    collection: config.database.CYCLE_ACTIVITY_RAW,
                    option: {
                        start: {
                            $gt: zeros[dx].start,
                            $lt: endShift
                        },
                        'shift.cd': zeros[dx].shift.cd,
                        'state.cd': util.CYCLE_FULL_TRAVEL,
                        'equipment.id': zeros[dx].hauler.id
                    },
                    sort: {
                        start: 1
                    }
                });
                if (next.length > 0) {
                    const stop = next[0].start;
                    const duration = stop - zeros[dx].start;
                    await util.mongoUpdate(mo, {
                        collection: config.database.CYCLE_ACTIVITY_RAW,
                        option: {
                            _id: zeros[dx]._id
                        },
                        data: {
                            $set: {
                                stop: stop,
                                duration: duration
                            }
                        }
                    });
                }

            } else if (zeros[dx].state.cd === util.CYCLE_FULL_TRAVEL) {

                let endShift;
                if (zeros[dx].shift.cd === util.SHIFT_DAY) {
                    endShift = moment.unix(zeros[dx].start).set({ hour: 18, minute: 0, second: 0 }).unix();
                } else {
                    endShift = moment.unix(zeros[dx].start).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix();
                }
                const next = await util.mongoFindOne(mo, {
                    collection: config.database.CYCLE_ACTIVITY_RAW,
                    option: {
                        start: {
                            $gt: zeros[dx].start,
                            $lt: endShift
                        },
                        'shift.cd': zeros[dx].shift.cd,
                        'state.cd': util.CYCLE_SPOTTING_DUMPING,
                        'equipment.id': zeros[dx].equipment.id
                    },
                    sort: {
                        start: 1
                    }
                });
                if (next.length > 0) {
                    const stop = next[0].start;
                    const duration = stop - zeros[dx].start;
                    await util.mongoUpdate(mo, {
                        collection: config.database.CYCLE_ACTIVITY_RAW,
                        option: {
                            _id: zeros[dx]._id
                        },
                        data: {
                            $set: {
                                stop: stop,
                                duration: duration
                            }
                        }
                    });
                }

            } else if (zeros[dx].state.cd === util.CYCLE_SPOTTING_DUMPING) {

                let endShift;
                if (zeros[dx].shift.cd === util.SHIFT_DAY) {
                    endShift = moment.unix(zeros[dx].start).set({ hour: 18, minute: 0, second: 0 }).unix();
                } else {
                    endShift = moment.unix(zeros[dx].start).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix();
                }
                const next = await util.mongoFindOne(mo, {
                    collection: config.database.CYCLE_ACTIVITY_RAW,
                    option: {
                        start: {
                            $gt: zeros[dx].start,
                            $lt: endShift
                        },
                        'shift.cd': zeros[dx].shift.cd,
                        'state.cd': util.CYCLE_DUMPING,
                        'equipment.id': zeros[dx].equipment.id
                    },
                    sort: {
                        start: 1
                    }
                });
                if (next.length > 0) {
                    const stop = next[0].start;
                    const duration = stop - zeros[dx].start;
                    await util.mongoUpdate(mo, {
                        collection: config.database.CYCLE_ACTIVITY_RAW,
                        option: {
                            _id: zeros[dx]._id
                        },
                        data: {
                            $set: {
                                stop: stop,
                                duration: duration
                            }
                        }
                    });
                }

            } else if (zeros[dx].state.cd === util.CYCLE_DUMPING) {

                let endShift;
                if (zeros[dx].shift.cd === util.SHIFT_DAY) {
                    endShift = moment.unix(zeros[dx].start).set({ hour: 18, minute: 0, second: 0 }).unix();
                } else {
                    endShift = moment.unix(zeros[dx].start).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix();
                }
                const next = await util.mongoFindOne(mo, {
                    collection: config.database.CYCLE_ACTIVITY_RAW,
                    option: {
                        start: {
                            $gt: zeros[dx].start,
                            $lt: endShift
                        },
                        'shift.cd': zeros[dx].shift.cd,
                        'state.cd': util.CYCLE_EMPTY_TRAVEL,
                        'equipment.id': zeros[dx].equipment.id
                    },
                    sort: {
                        start: 1
                    }
                });
                if (next.length > 0) {
                    const stop = next[0].start;
                    const duration = stop - zeros[dx].start;
                    await util.mongoUpdate(mo, {
                        collection: config.database.CYCLE_ACTIVITY_RAW,
                        option: {
                            _id: zeros[dx]._id
                        },
                        data: {
                            $set: {
                                stop: stop,
                                duration: duration
                            }
                        }
                    });
                }

            } else if (zeros[dx].state.cd === util.CYCLE_EMPTY_TRAVEL) {

                let endShift;
                if (zeros[dx].shift.cd === util.SHIFT_DAY) {
                    endShift = moment.unix(zeros[dx].start).set({ hour: 18, minute: 0, second: 0 }).unix();
                } else {
                    endShift = moment.unix(zeros[dx].start).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix();
                }
                const next = await util.mongoFindOne(mo, {
                    collection: config.database.CYCLE_ACTIVITY_RAW,
                    option: {
                        start: {
                            $gt: zeros[dx].start,
                            $lt: endShift
                        },
                        'shift.cd': zeros[dx].shift.cd,
                        'state.cd': util.CYCLE_QUEUING_LOADING,
                        'equipment.id': zeros[dx].equipment.id
                    },
                    sort: {
                        start: 1
                    }
                });
                if (next.length > 0) {
                    const stop = next[0].start;
                    const duration = stop - zeros[dx].start;
                    await util.mongoUpdate(mo, {
                        collection: config.database.CYCLE_ACTIVITY_RAW,
                        option: {
                            _id: zeros[dx]._id
                        },
                        data: {
                            $set: {
                                stop: stop,
                                duration: duration
                            }
                        }
                    });
                }

            }

        }

        refinementCallback();
    }

    async.waterfall([

        (cb) => {
            /**
             * do crawl raw data by times
             */
            clog.info('------ crawling by times, ', times.length, ' items');
            async.forEachOf(times, (time, key, _cb) => {
                shiftCrawler(time, mysql, mongo, () => {
                    _cb();
                });
            }, (err) => {
                cb();
            });
        },

        (cb) => {
            /**
             * refinement zero duration
             */
            clog.info('------ refinement zero');
            refinementZero(mongo, () => {
                cb();
            });
        },

    ], (res) => {
        callback();
    });

}